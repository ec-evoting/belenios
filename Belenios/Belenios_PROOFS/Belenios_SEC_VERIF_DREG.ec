require import FMap List Int FSet Real LeftOrRight AllCore.
require import IntExtra RealExtra Distr.
require (*  *) Belenios_SEC_VERIF_DBB.
require (*  *) Ex_Plug_and_Pray.


clone include Belenios_SEC_VERIF_DBB.

module type ValidInd'(E: Scheme, SS: SignScheme, H: HOracle.ARO, J: JOracle.ARO) = {
  proc validInd(b: (ident * upkey * cipher*sign), 
                pk: pkey)
       : bool  {H.o J.o}
}.

section DREG.

(* ** abstract algorithms ** *) 
declare module E: Scheme      {BS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO}.
declare module Pz: Prover     {BS, BH, BW, BU, JRO.RO, HRO.RO, GRO.RO, E}.
declare module Vz: Verifier   {BS, BH, BW, BU, JRO.RO, HRO.RO, GRO.RO, E, Pz}.
declare module SS: SignScheme {BS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, E, Pz, Vz}.
declare module C : ValidInd'   {BS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, E, Pz, Vz, SS}.
declare module A : DREG_Adv   {BS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, E, Pz, Vz, C, SS}. 

(* ** ASSUMPTIONS ** *)
(* ** start *)

  axiom size_Voters:
    0 < size Voters.
  axiom size_qVo:
    0 < qVo.

  axiom dg_weight: 
     weight dg_out = 1%r.
  axiom dh_weight: 
     weight dh_out = 1%r.
  axiom dk_weight: 
     weight dhkey_out = 1%r.

  axiom inj_get_upk (x y : uskey):
    get_upk x = get_upk y => x = y.

(* lossless assumptions *)
  axiom Pz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Pz(G).prove.
  axiom Vz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Vz(G).verify. 
  axiom Ek_ll(H <: HOracle.ARO): 
    islossless E(H).kgen.
  axiom Ed_ll(H <: HOracle.ARO): 
    islossless H.o => islossless E(H).dec.
  axiom Ee_ll(H <: HOracle.ARO): 
    islossless H.o => islossless E(H).enc. 
  axiom SSk_ll (SS<: SignScheme) (O<: JOracle.ARO): 
      islossless O.o => islossless SS(O).kgen.
  axiom SSs_ll (SS<: SignScheme) (O<: JOracle.ARO): 
      islossless O.o => islossless SS(O).sign.
  axiom SSv_ll (O<: JOracle.ARO): 
      islossless O.o => islossless SS(O).verify.
  axiom C_ll (H <: HOracle.ARO) (J<: JOracle.ARO): 
    islossless H.o => 
    islossless J.o => islossless C(E,SS,H,J).validInd.

  axiom Aa2_ll (O <: DREG_Oracle { A }):
    islossless O.corrupt => 
    islossless O.vote => 
    islossless O.cast =>
    islossless O.board =>
    islossless O.h   =>
    islossless O.g   =>
    islossless O.j   =>
    islossless A(O).a2.
  axiom Aa3_ll (O <: DREG_Oracle { A }):
    islossless O.check => 
    islossless O.board =>
    islossless O.h   =>
    islossless O.g   =>
    islossless O.j  =>
    islossless A(O).a3.

(* axioms that link algorithms to operators *)

  (* axiom for stating that the keys are generated *)
  axiom Kgen_get_pk (H<: HOracle.ARO):
    equiv [E(H).kgen ~ E(H).kgen:  ={glob H, glob E} ==> 
          ={glob H, glob E,  res} /\ 
          res{2}.`1 = get_pk res{2}.`2].

  (* axiom for stating that the keys are generated *)
  axiom Kgen_get_upk (J<: JOracle.ARO):
    equiv [SS(J).kgen ~ SS(J).kgen:  ={glob J, glob SS} ==> 
          ={glob J, glob SS,  res} /\ 
          res{2}.`2 = get_upk res{2}.`1].

  axiom  Kgen_get_upk' (J1 <: JOracle.ARO) (J2 <: JOracle.ARO):
    equiv [SS(J1).kgen ~ SS(J2).kgen:  ={ glob SS} ==> 
          ={glob SS,  res} /\ 
          res{2}.`2 = get_upk res{2}.`1].

  (* axiom for linking E.dec to dec_cipher operator *)   
  axiom dec_Dec_one (ge: (glob E)) (sk2: skey)(l2: upkey) (c2: cipher):
    phoare [E(HRO.RO).dec:  
           (glob E) =ge /\ arg = (sk2, l2, c2)
           ==>   
           (glob E) =ge /\
           res = dec_cipher sk2 l2 c2 HRO.RO.m ] = 1%r.

  (* axiom for transforming an encryption into decryption (two-sided) *)
  axiom Enc_dec_two (sk2: skey) (pk2: pkey) (l2: upkey) (p2: vote): 
    (pk2 = get_pk sk2) =>
    equiv [E(HRO.RO).enc ~ E(HRO.RO).enc : 
          ={glob HRO.RO, glob E, arg} /\ arg{2}=( pk2, l2, p2) 
          ==> 
          ={glob HRO.RO, glob E,  res} /\
          Some p2 = dec_cipher sk2 l2 res{2} HRO.RO.m{2}]. 

   axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)
             ==>
            (glob SS) =gs /\
            res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
   axiom ver_Ver_one' (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(MEUF_Oracle(SS,JRO.RO)).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)
             ==>
            (glob SS) =gs /\
            res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.

  axiom Sig_ver_two (usk2: uskey) (upk2: upkey) (c2: cipher):
  (upk2 = get_upk usk2) =>
  equiv [SS(JRO.RO).sign ~ SS(JRO.RO).sign :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (usk2,c2)
           ==>   
           ={glob JRO.RO, glob SS, res} /\
           ver_sign upk2 c2 res{2} JRO.RO.m{2} ].

  (* axiom for state of verifier Vz w.r.t to an eager radom oracle *)   
  axiom Vz_keepstate (gv: (glob Vz)) :
    phoare [Vz(GRO.RO).verify:  
           (glob Vz) = gv ==> (glob Vz) = gv ] = 1%r.

  axiom SSs_eq (gss: (glob SS)):
    phoare [SS(JRO.RO).sign: 
          (glob SS) = gss ==> (glob SS) = gss] = 1%r.
  axiom SSk_eq (gs : (glob SS)) (O <: ARO):
    phoare [SS(O).kgen: 
          (glob SS) = gs ==>  
           (glob SS) = gs /\ res.`2 = get_upk res.`1] = 1%r.

  axiom validInd_dec_one (gc: (glob C)) (ge: (glob E)) (gs: (glob SS))
                         (sk: skey) (pk: pkey) 
                         (b: ident * upkey * cipher* sign):
    (pk = get_pk sk)=>
     phoare [C(E,SS,HRO.RO,JRO.RO).validInd  : 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\arg=(b,pk) 
          ==> 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\
          res = (dec_cipher sk b.`2 b.`3 HRO.RO.m <> None/\
                 ver_sign b.`2 b.`3 b.`4 JRO.RO.m)] = 1%r.

(* AXIOMS ON POLICY *)
  (* axiom for membership of Sanitize *)
  axiom San_mem (L: (ident * (vote * upkey * cipher*sign)) list) x:
    x \in Sanitize L => x \in L.

  axiom San_mem'(L : (ident * (upkey * cipher * sign)) list) x:
    x \in Sanitize L => x \in L. 

  axiom San_mem''(L : (upkey * (ident * cipher * sign)) list) x:
    x \in Sanitize L => x \in L. 

  axiom San_map' (sk: skey, 
                 bb: (upkey * cipher*sign) list, 
                 mm: (h_in,h_out) map):
    let f   = fun (x : upkey * cipher*sign),
                  (x.`1, oget (dec_cipher sk x.`1 x.`2 mm)) in 
    Sanitize (map f bb) = 
    map (f \o (fun (x : upkey * (cipher * sign)), (x.`1, x.`2.`1,x.`2.`2))) 
         (Sanitize (map (fun (x : upkey * cipher * sign), (x.`1, (x.`2,x.`3))) bb)). 


  (* axioms restricting possible values for Sanitize *)
  axiom San_uniq (L: (ident * vote * upkey * cipher * sign) list): 
    uniq (Sanitize (map pack4 L)).

  axiom San_uniq_fst (L: (upkey * (ident * cipher * sign)) list):
     uniq (map fst (Sanitize L)).
 
  axiom San_uniq_fst' (L : (ident * (upkey * cipher * sign)) list):
     uniq (map fst (Sanitize L)). 
  axiom San_uniq_snd (S: (ident * (upkey * cipher * sign)) list ):
     uniq (map snd S) =>
     uniq (map snd (Sanitize S)). 
 
  axiom San_uniq' (L : (ident * (upkey * cipher * sign)) list):
      uniq (Sanitize L).


  (* axiom that maintains Sanitize membership w.r.t a filtered list *)
  axiom San_mem_filter (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in Sanitize L => 
     x \in Sanitize (filter f L).


  axiom San_filter_ident (L: (ident * (upkey * cipher * sign)) list) p:
    let f = (fun (x: ident * (upkey * cipher * sign)), p x.`1) in
    Sanitize (filter f L) = 
    filter f (Sanitize L). 
(* 
 op lb['a 'b] (bb : ('a * 'b) list) =
    with bb = "[]"      => []
    with bb = (::) b bb => if   has (pred1 b.`1 \o fst) bb
                         then lb bb
                         else b :: lb bb.
local lemma uu_lb (A: (ident *upkey*cipher*sign) list):
    map (remi\o uopen) (lb (map upack A)) = map uopen_2 (lb (map (upack_2 \o remi) A)). 
proof.
  elim: A=>//=. 
  move => x A HA. 
  have ->: has (pred1 (upack x).`1 \o fst ) (map upack A) = 
           has (pred1 ((\o) upack_2 remi x).`1 \o fst) (map (upack_2 \o remi) A).
    rewrite ?hasP /upack /upack_2 /(\o) /remi ?mapP //=. 
    rewrite eq_iff; progress. 
    + move: H; rewrite mapP //=.
      elim => y [HyA Hyx0].         
      exists (y.`2, (y.`3, y.`4)). smt.   
    - move: H; rewrite mapP //=.
      elim => y [HyA Hyx0].         
      exists (y.`2, (y.`1, y.`3, y.`4)). smt.
  smt.
qed.

local lemma us_lb (L: (ident*upkey*cipher*sign) list):
  (forall x y, x \in L => y \in L => x.`1 = y.`1 => x.`2 = y.`2) =>
  (forall x y, x \in L => y \in L => x.`2 = y.`2 => x.`1 = y.`1) =>
  map open3 (lb (map pack3 L)) = map uopen (lb (map upack L)).
proof.
  elim: L=>//=. 
  move => a L HL HL1 HL2.
  have ->: has (pred1 (pack3 a).`1 \o fst) (map pack3 L) = 
           has (pred1 (upack a).`1 \o fst) (map upack L).
    rewrite ?hasP //= /pack3 /upack /(\o). 
    rewrite eq_iff; progress.
    + move: H; rewrite mapP //=.
      elim => y [HyA Hyx].         
      exists (y.`2, (y.`1, y.`3, y.`4)). smt.   
    - move: H; rewrite mapP //=.
      elim => y [HyA Hyx].         
      exists (y.`1, (y.`2, y.`3, y.`4)). smt.
  smt.
qed.
*)
  axiom USan_USanitize (A: (ident *upkey*cipher*sign) list):
    map remi (USanitize A) = USan (map remi A). 

(* fixme: try to remove *)
op Sanitize' (L : (ident* upkey * cipher * sign) list) =
   map open3 (Sanitize (map pack3 L)).

  axiom San'_USan (L: (ident*upkey*cipher*sign) list):
  (forall x y, x \in L => y \in L => x.`1 = y.`1 => x.`2 = y.`2) =>
  (forall x y, x \in L => y \in L => x.`2 = y.`2 => x.`1 = y.`1) =>
  Sanitize' L = USanitize L.
 

  (* axiom for determinism of Count, based on L1 is a permutation of L2 *)
  axiom Count_perm (L1 L2: vote list):
    perm_eq L1 L2 =>
    Count L1 = Count L2.

  (* op (+): result -> result -> result. *)
  axiom Count_split (L1 L2: vote list):
    Count (L1 ++ L2) = Count L1 + Count L2.
  axiom add_com (a b : result):
    a + b = b + a. 
(* ** end*)

(* decryption should not change the state of an eager random oracle *) 
local lemma dec_Dec_two (sk2: skey)(l2: upkey) (c2: cipher):
  equiv [E(HRO.RO).dec ~ E(HRO.RO).dec :
          ={glob HRO.RO, glob E, arg}/\ arg{2} = (sk2, l2, c2)
           ==>   
           ={glob HRO.RO, glob E, res} /\
           res{2} = dec_cipher sk2 l2 c2 HRO.RO.m{2} ].
proof.
  proc*=>//=. 
  exists* (glob E){1}; elim* => ge.
  call{1} (dec_Dec_one ge sk2 l2 c2 ).
  call{2} (dec_Dec_one ge sk2 l2 c2 ). 
  by auto.
qed.

local lemma ver_Ver_two (upk2: upkey) (c2: cipher) (s2: sign):
  equiv [SS(JRO.RO).verify ~ SS(JRO.RO).verify :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (upk2,c2,s2)
           ==>   
           ={glob JRO.RO, glob SS, res} /\
           res{2} = ver_sign upk2 c2 s2 JRO.RO.m{2} ].
proof.
  proc*=>//=. 
  exists* (glob SS){1}; elim* => gs.
  call{1} (ver_Ver_one gs upk2 c2 s2).
  call{2} (ver_Ver_one gs upk2 c2 s2). 
  by auto.
qed.

local lemma validInd_dec (sk: skey) (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
    (pk = get_pk sk)=>
     equiv [C(E,SS,HRO.RO, JRO.RO).validInd ~ C(E,SS,HRO.RO, JRO.RO).validInd : 
          ={glob HRO.RO, glob JRO.RO, glob C, glob E, glob SS, arg} /\ arg{2}=(b,pk) 
          ==> 
          ={glob HRO.RO, glob JRO.RO, glob C, glob E, glob SS, res} /\
          res{1} = (dec_cipher sk b.`2 b.`3 HRO.RO.m{1} <> None/\
                    ver_sign b.`2 b.`3 b.`4 JRO.RO.m{1})].
proof.
  move => Hkp.
  proc*.
  exists* (glob C){1}, (glob E){1}, (glob SS){1}; elim* => gc ge gs.
  call{1} (validInd_dec_one gc ge gs sk pk b Hkp).
  call{2} (validInd_dec_one gc ge gs sk pk b Hkp).
  by auto=>/>.
qed.

local lemma San'_empty['a, 'b]:
  USanitize [] = []. 
  rewrite /USanitize.
  have ->: (map upack []) = [] by smt. 
  smt.
qed. 

local lemma uniq_perm_eq_filter['a] (L S: (ident * 'a) list):
  uniq S =>
  uniq L =>
  (forall b, mem L b => mem S b)=>
  perm_eq L (filter (mem L) S).
proof.
  rewrite perm_eqP.
  move => HuS HuL Hmem p.
  elim: L S HuL Hmem HuS =>//=. smt.
  move => x L Hcount S Hnmem.
  rewrite count_filter.
  have Hc: forall (s: (ident * 'a) list) p1 p2,
    count (predU p1 p2) s = count p1 s + count p2 s - count (predI p1 p2) s.
    move => s p1 p2.
    elim: s=>//=.
    move => y s Hcs.
    rewrite Hcs //=.
    have Hk: b2i (predU p1 p2 y)  =
             b2i (p1 y) + b2i (p2 y) - b2i (predI p1 p2 y).
      by smt.
    by smt.
  have Hc': forall (s: (ident * 'a) list) p1 p2,
    count (predI p1 p2) s = count p1 s + count p2 s - count (predU p1 p2) s.
    move => s p1 p2.
    rewrite Hc //=.
    by smt.

  have Hc'': forall (s: (ident * 'a) list) p x,
    !mem s x =>
    count (predI p (transpose (=) x)) s =0.
    move => s p0.
    elim: s =>//=.
    progress. 
    have Hnn: x1 <> x0  by smt.
    have Hnmm: ! mem l x1 by smt. 
    rewrite (H x1 Hnmm).
    rewrite /predI //=.
    smt.
    
  have ->: (predI p (mem (x :: L))) =
           (fun y, (p y /\ y = x) \/ (p y /\ mem L y)). 
    rewrite -cat1s /predI. 
    rewrite fun_ext /(==).
    move => y.
    rewrite !mem_cat. 
    smt. 

  have ->: (fun (y : ident * 'a) => p y /\ y = x \/ p y /\ mem L y) =
           predU (predI p (transpose (=) x)) (predI p (mem L)). 
    by rewrite /predU /predI //=.
  rewrite Hc. 

  move => Hmemb HuS. 
  rewrite (Hcount S _ _ HuS); first 2 by smt.
  rewrite count_filter.
  have ->: (b2i (p x) + count (predI p (mem L)) S =
             count (predI p (transpose (=) x)) S + 
             count (predI p (mem L)) S -
             count (predI (predI p (transpose (=) x)) (predI p (mem L))) S) =
           (b2i (p x)  =
            count (predI p (transpose (=) x)) S  -
            count (predI (predI p (transpose (=) x)) (predI p (mem L))) S).
    by smt.
  have ->: count (predI (predI p (transpose (=) x)) (predI p (mem L))) S = 0.
  have ->: (predI (predI p (transpose (=) x)) (predI p (mem L))) = 
           (fun y, false).
    rewrite /predI //=.
    rewrite fun_ext /(==).
    move => y. smt.
    smt.
  simplify.
  have HmemS: mem S x by smt.

  
  have Hs: forall (s: (ident * 'a) list),
      uniq s =>
      mem s x =>
      b2i (p x) = count (predI p (transpose (=) x)) s.
    elim =>//=. 
    progress. 
    case(x0 = x). 
    + move => Heq.
      have ->: x0 = x by smt.
      rewrite Hc''. smt.      
      rewrite /predI //=. 
    - move => Hneq.
      have ->: b2i (predI p (transpose (=) x) x0) = 0 by rewrite /predI //=; smt.
      have Hmm: mem l x by smt.
      by rewrite (H H1 Hmm).
  
  by rewrite (Hs S HuS HmemS).
qed.

(* similar to above, but no id *)
local lemma uniq_perm_eq_filter_two['a] (L S: 'a list):
  uniq S =>
  uniq L =>
  (forall b, mem L b => mem S b)=>
  perm_eq L (filter (mem L) S).
proof.
  rewrite perm_eqP.
  move => HuS HuL Hmem p.
  elim: L S HuL Hmem HuS =>//=. smt.
  move => x L Hcount S Hnmem.
  rewrite count_filter.
  have Hc: forall (s: 'a list) p1 p2,
    count (predU p1 p2) s = count p1 s + count p2 s - count (predI p1 p2) s.
    move => s p1 p2.
    elim: s=>//=.
    move => y s Hcs.
    rewrite Hcs //=.
    have Hk: b2i (predU p1 p2 y)  =
             b2i (p1 y) + b2i (p2 y) - b2i (predI p1 p2 y).
      by smt.
    by smt.
  have Hc': forall (s: 'a list) p1 p2,
    count (predI p1 p2) s = count p1 s + count p2 s - count (predU p1 p2) s.
    move => s p1 p2.
    rewrite Hc //=.
    by smt.

  have Hc'': forall (s: 'a list) p x,
    !mem s x =>
    count (predI p (transpose (=) x)) s =0.
    move => s p0.
    elim: s =>//=.
    progress. 
    have Hnn: x1 <> x0  by smt.
    have Hnmm: ! mem l x1 by smt. 
    rewrite (H x1 Hnmm).
    rewrite /predI //=.
    smt.
    
  have ->: (predI p (mem (x :: L))) =
           (fun y, (p y /\ y = x) \/ (p y /\ mem L y)). 
    rewrite -cat1s /predI. 
    rewrite fun_ext /(==).
    move => y.
    rewrite !mem_cat. 
    smt. 

  have ->: (fun (y : 'a) => p y /\ y = x \/ p y /\ mem L y) =
           predU (predI p (transpose (=) x)) (predI p (mem L)). 
    by rewrite /predU /predI //=.
  rewrite Hc. 

  move => Hmemb HuS. 
  rewrite (Hcount S _ _ HuS); first 2 by smt.
  rewrite count_filter.
  have ->: (b2i (p x) + count (predI p (mem L)) S =
             count (predI p (transpose (=) x)) S + 
             count (predI p (mem L)) S -
             count (predI (predI p (transpose (=) x)) (predI p (mem L))) S) =
           (b2i (p x)  =
            count (predI p (transpose (=) x)) S  -
            count (predI (predI p (transpose (=) x)) (predI p (mem L))) S).
    by smt.
  have ->: count (predI (predI p (transpose (=) x)) (predI p (mem L))) S = 0.
  have ->: (predI (predI p (transpose (=) x)) (predI p (mem L))) = 
           (fun y, false).
    rewrite /predI //=.
    rewrite fun_ext /(==).
    move => y. smt.
    smt.
  simplify.
  have HmemS: mem S x by smt.

  
  have Hs: forall (s: 'a list),
      uniq s =>
      mem s x =>
      b2i (p x) = count (predI p (transpose (=) x)) s.
    elim =>//=. 
    progress. 
    case(x0 = x). 
    + move => Heq.
      have ->: x0 = x by smt.
      rewrite Hc''. smt.      
      rewrite /predI //=. 
    - move => Hneq.
      have ->: b2i (predI p (transpose (=) x) x0) = 0 by rewrite /predI //=; smt.
      have Hmm: mem l x by smt.
      by rewrite (H H1 Hmm).
  
  by rewrite (Hs S HuS HmemS).
qed.



module DREG_Exp_tally (V: VotingScheme', A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  hC, hNC, hN, r', pi', ev';
    BW.bad  <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ V(H,G,J).setup();
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.pk,BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ V(H,G,J).tally (map remi BW.bb,BW.sk,BW.hk);
    ev'      <@ V(H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    (* votes of honest voters that checked *)
    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2)  BW.check;
    (* votes of honest voters that didn't check *)
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), 
                    b.`2) hNC;

   bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ r <> Count (hC ++ L ++ D));
    return ev /\ bool ;
  }
}.

local lemma dbb_dbbtally &m:
  Pr[DREG_Exp (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] =
  Pr[DREG_Exp_tally (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  swap{2} [24..27] -2.
  seq 24 25 : (={ev, bool}); first by sim.
   
  inline*; wp.
  call{2} (Vz_ll GRO.RO GRO.RO_o_ll).
  wp; call{2} (Pz_ll GRO.RO GRO.RO_o_ll).
  wp; while{2} (true) (size fbb{2} -i0{2}). 
  + move => _ z.
    wp; call{2} (SSv_ll JRO.RO JRO.RO_o_ll).
    call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  by auto=>/>; smt (@Int @IntExtra). 
qed.

(* op preha (x: ident * upkey * cipher * sign) = 
         (get_maybe x.`1, x.`2, x.`3, x.`4). *)

module DREG_Oracle_check(V: VotingScheme',
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc h        = DREG_Oracle(V,H,G,J).h
  proc g        = DREG_Oracle(V,H,G,J).g
  proc j        = DREG_Oracle(V,H,G,J).j

  proc corrupt  = DREG_Oracle(V,H,G,J).corrupt
  proc vote     = DREG_Oracle(V,H,G,J).vote
  proc cast     = DREG_Oracle(V,H,G,J).cast
  proc board    = DREG_Oracle(V,H,G,J).board

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL, ve;
    i <- 0;

    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.pk,BW.hk);
    (* verify all honest ballots *)
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      (* this checks hash membership *)
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), 
                                     BW.pbb, BW.hk);
      (* this checks hash input membership *)
      ve <- (remv h) \in 
            map open3 (Sanitize (map pack3 BW.bb));
      (* take the ballot asigned to id, and if voter has not checked before append *)
      if (id = h.`1 /\ vv /\ ve /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }else{
        if (vv /\ !ve){
            BW.bad <- vv /\ !ve;
            BH.l  <- preha (remv h)
                     (* (remv h).`2, (remv h).`3, get_hash_in ((remv h).`1, (remv h).`4)*);
            BH.r  <- nth witness (map (preha \o open3) (Sanitize (map pack3 BW.bb)))
                      (find (fun x, hash BW.hk BH.l = hash BW.hk x /\ BH.l <> x) 
                           (map (preha \o open3) (Sanitize (map pack3 BW.bb))));
                   
        }
      }

      i <- i +1;
    }
  }

}.


module DREG_Exp_check (V: VotingScheme', A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle_check(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, hC, hNC, hN, r', pi', ev';
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ V(H,G,J).setup();
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.pk,BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ V(H,G,J).tally (map remi BW.bb,BW.sk,BW.hk);
    ev'      <@ V(H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;

    bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ r <> Count (hC ++ L ++ D));
    return ev /\ bool;
  }
}.

local lemma dbbtally_dbbcheck &m:
  Pr[DREG_Exp_tally (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DREG_Exp_check (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] +
  Pr[DREG_Exp_check (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad].
proof.
  byequiv =>/>.
  proc.
  auto; progress. 
  conseq (: _ ==> ={ev, r, BW.hvote, BW.bb, BW.qCo}/\
                  (!BW.bad{2} => ={BW.check}))=>/>; first by smt.
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim. 
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim.   
  (* call A.a3 *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO,  
           BW.bb, BW.pk, BW.hvote, BW.uL,BW.hk}/\
          (!BW.bad{2} => ={BW.check})). 
  + proc.
    while (={i, pL, id, BW.bb, BW.pbb, BW.uL, BW.pk, BW.hk, 
             glob HRO.RO}/\
           BW.pbb{2} = map (fun (x: upkey * cipher * sign), 
                                  (get_pub_data x , hash BW.hk{2} x )) 
                                  (USan (map remi BW.bb{2}))/\
           (!BW.bad{2} => ={BW.check})).  
      by inline*;auto; progress; smt.
    inline*; wp.
    by auto.

  + by proc.  
    by proc.
    by proc.
    by proc. 

  (* call A.a2: fill the board *)
  wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             glob C, BW.uL, BW.qCo}); first 7 by sim.
  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim.
  inline B'(E, Pz, Vz, C(E, SS), SS, HRO.RO, GRO.RO, JRO.RO).setup.
  wp; rnd; call (: ={glob HRO.RO}).
  call (: true ==> ={glob JRO.RO}); first by sim.
  call (: true ==> ={glob GRO.RO}); first by sim.
  call (: true ==> ={glob HRO.RO}); first by sim. 
  by auto. 
qed.

module DREG_Oracle_check'(V: VotingScheme',
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc h        = DREG_Oracle(V,H,G,J).h
  proc g        = DREG_Oracle(V,H,G,J).g
  proc j        = DREG_Oracle(V,H,G,J).j

  proc corrupt  = DREG_Oracle(V,H,G,J).corrupt
  proc vote     = DREG_Oracle(V,H,G,J).vote
  proc cast     = DREG_Oracle(V,H,G,J).cast
  proc board    = DREG_Oracle(V,H,G,J).board

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL, veh;
    i <- 0;

    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.pk, BW.hk);
    (* verify all honest ballots *)
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), 
                                     BW.pbb, BW.hk);
      veh <- (preha \o remv) h \in (USan (map remi BW.bb));

      if (id = h.`1 /\ vv /\ veh /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }else{
        if (!BW.bad){
            BW.bad <- vv /\ !veh ;
            BH.l  <- preha (remv h);
            BH.r  <- nth witness (USan (map remi BW.bb))
                      (find (fun (x: upkey * cipher * sign), 
                                 hash BW.hk BH.l = hash BW.hk x /\ BH.l <> x) 
                            (USan (map remi BW.bb)));
                   
        }
      }
      i <- i +1;
    }
  }

}.


module DREG_Exp_check' (V: VotingScheme', A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle_check'(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, hC, hNC, hN, r', pi', ev';
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ V(H,G,J).setup();
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.pk,BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ V(H,G,J).tally (map remi BW.bb,BW.sk,BW.hk);
    ev'      <@ V(H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;

    bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ r <> Count (hC ++ L ++ D));
    return ev /\ bool;
  }
}.

local lemma dbbtally_dbbcheck_prime &m:
  Pr[DREG_Exp_tally  (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DREG_Exp_check' (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] +
  Pr[DREG_Exp_check' (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad].
proof.
  byequiv =>/>.
  proc.
  auto; progress. 
  conseq (: _ ==> ={ev, r, BW.hvote, BW.bb, BW.qCo}/\
                  (!BW.bad{2} => ={BW.check}))=>/>; first by smt.
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim. 
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim.   
  (* call A.a3 *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO,  
           BW.bb, BW.pk, BW.hvote, BW.uL,BW.hk}/\
          (!BW.bad{2} => ={BW.check})). 
  + proc.
    while (={i, pL, id, BW.bb, BW.pbb, BW.uL, BW.pk,BW.hk,
             glob HRO.RO}/\
           BW.pbb{2} = map (fun (x: upkey * cipher * sign), 
                                  (get_pub_data x, hash BW.hk{2} x)) 
                                  (USan (map remi BW.bb{2}))/\
           (!BW.bad{2} => ={BW.check}) (*/\
            (BW.bad{2} => BH.l{2} <> BH.r{2} /\
                          hash BH.l{2} = hash BH.r{2})*)).  
      inline*; auto; progress. 
      + smt. + smt. smt. smt.

    inline*; wp. 
    by auto.

  + by proc.  
    by proc.
    by proc. 
    by proc.

  (* call A.a2: bb *)
  wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, glob C, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}); first 7 by sim.
  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim.
  inline B'(E, Pz, Vz, C(E, SS), SS, HRO.RO, GRO.RO, JRO.RO).setup.
  wp; rnd; call (: ={glob HRO.RO}).
  call (: true ==> ={glob JRO.RO}); first by sim.
  call (: true ==> ={glob GRO.RO}); first by sim.
  call (: true ==> ={glob HRO.RO}); first by sim.
  by auto. 
qed.


module BHash_Adv'(V: VotingScheme', A: DREG_Adv,
                H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={
  module O = DREG_Oracle_check'(V,H,G,J)

  proc find(hk: hash_key): (upkey * cipher * sign) * 
               (upkey * cipher * sign) ={

    var r, pi, bool, i, hk';
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.uLL  <- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, hk') <@ V(H,G,J).setup();
    BW.hk <- hk;
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    return (BH.l, BH.r);
  }
}.


local lemma dbbcheck_dbbhash &m:
  Pr[DREG_Exp_check' (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad]<=
  Pr[Hash_Exp(BHash_Adv'(B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  inline BHash_Adv'(B'(E, Pz, Vz, C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).find.
  seq 19 22: ( ={BW.bad, BW.hk} /\ BW.hk{2} = hk{2}/\
               (BW.bad{2} => BH.l{2} <> BH.r{2} /\ 
                             hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})). 
    (* call A.a3: check *)
    wp.
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.hk,
           BW.bb, BW.pk, BW.hvote, BW.uL, BW.bad, BW.check}/\
          (BW.bad{2} => BH.l{2} <>BH.r{2} /\ 
                         hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})).
    + proc. inline *.
      while ( ={ i, pL, id, BW.pbb, BW.pk, BW.bb, BW.check, BW.bad, BW.hk,
                glob HRO.RO}/\ 0<= i{2}/\
             BW.pbb{2} = map (fun (x: upkey * cipher * sign), 
                                  (get_pub_data x, hash BW.hk{2} x))
                                  (USan (map remi BW.bb{2}))/\
             (BW.bad{2} => BH.l{2} <>BH.r{2} /\ 
                           hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})/\
             (!BW.bad{2}=> forall x, x \in (take i{2} pL{2}) =>
                            !(hash' BW.hk{2} (remv x) \in map (hash BW.hk{2}) (USan (map remi BW.bb{2})) /\
                              !(preha \o remv) x \in (USan (map remi BW.bb{2})))
             )).
            
auto=>/>; progress. 
+ smt. 
+ move: H1 H3 H4 H5.
  rewrite /preha /hash'.
  pose b:= (nth witness pL{2} i{2}).
  pose BB:= (USan (map remi BW.bb{2})).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
  have ->: map (fun (p : pub_data * hash_out) => p.`2)
          (map (fun (x0 : upkey * cipher * sign) =>
               (get_pub_data x0, hash BW.hk{2} x0)) (USan (map remi BW.bb{2}))) =
          map (hash BW.hk{2}) BB
    by rewrite -?map_comp /(\o) //=. 
  move => H1 H3 H4 H5.
  move: H7; rewrite (take_nth witness); first by smt.
  rewrite -cats1 mem_cat //=; move => H7.
  case (x \in take i{2} pL{2}).
    move => Hmem.
    cut := H1 H6 x Hmem. 
    by done.
  move => Hnmem. 
  have Hx: x = nth witness pL{2} i{2} by smt.
  rewrite Hx -/b.
  by rewrite H3 //=.
+ smt.
+ move: H1 H3 H4 H5 H6.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
  pose BB:= (USan (map remi BW.bb{2})).
  have ->: map (fun (p : pub_data * hash_out) => p.`2)
          (map (fun (x0 : upkey * cipher * sign) =>
               (get_pub_data x0, hash BW.hk{2} x0)) (USan (map remi BW.bb{2}))) =
          map (hash BW.hk{2}) BB
    by rewrite -?map_comp /(\o) //=. 
  move => H1 H3 H4 H5 H6.
  pose c:= remi (remv b).
  move: H6 H5; rewrite /(\o) -/c; move => H6.
  rewrite mapP. elim => y [HyBB Hcy].
  cut := nth_find witness (fun (x : upkey * cipher * sign) =>
                         hash BW.hk{2} c = hash BW.hk{2} x /\ c <> x) 
                         BB _. 
  + rewrite hasP.
    rewrite //=. 
    exists y. 
    rewrite HyBB Hcy //=. 
    smt. (* c and y mem BB *)
  by done. 
+ move : H1 H3 H5 H6.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
  pose BB:= (USan (map remi BW.bb{2})).
  have ->: map (fun (p : pub_data * hash_out) => p.`2)
          (map (fun (x0 : upkey * cipher * sign) =>
               (get_pub_data x0, hash BW.hk{2} x0)) (USan (map remi BW.bb{2}))) =
          map (hash BW.hk{2}) BB
    by rewrite -?map_comp /(\o) //=. 
  move => H1 H3 H5 H6.
  pose c:= remi (remv b).
  move: H6 H5; rewrite /(\o) -/c; move => H6.
  rewrite mapP. elim => y [HyBB Hcy].
  cut := nth_find witness (fun (x : upkey * cipher * sign) =>
                         hash BW.hk{2} c = hash BW.hk{2} x /\ c <> x) 
                         BB _. 
  + rewrite hasP.
    rewrite //=. 
    exists y. 
    rewrite HyBB Hcy //=. 
    smt. (* c and y mem BB *)
  by done. 

+ move : H1 H3 H5 H6.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
  pose BB:= (USan (map remi BW.bb{2})).
  have ->: map (fun (p : pub_data * hash_out) => p.`2)
          (map (fun (x0 : upkey * cipher * sign) =>
               (get_pub_data x0, hash BW.hk{2} x0)) (USan (map remi BW.bb{2}))) =
          map (hash BW.hk{2}) BB
    by rewrite -?map_comp /(\o) //=. 
  move => H1 H3 H5 H6.
  move: H6; rewrite (take_nth witness); first by smt.
  rewrite -cats1 mem_cat //=; move => H6.
  case (x \in take i{2} pL{2}).
    move => Hmem.
    cut := H1 H4 x Hmem. 
    by done.
  move => Hnmem. 
  have Hx: x = nth witness pL{2} i{2} by smt.
  by rewrite Hx -/b H5. 

+ move: H1 H3.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
  pose BB:= (USan (map remi BW.bb{2})).
  have ->: map (fun (p : pub_data * hash_out) => p.`2)
          (map (fun (x0 : upkey * cipher * sign) =>
               (get_pub_data x0, hash BW.hk{2} x0)) (USan (map remi BW.bb{2}))) =
          map (hash BW.hk{2}) BB
    by rewrite -?map_comp /(\o) //=. 
  move => H1 H6. 
  smt.
 


auto; progress. 
move: H1. rewrite take0. smt.

    + by proc.
    + by proc.
    + by proc.
    + by proc.
    conseq (: _ ==> ={glob A, GRO.RO.m, HRO.RO.m, JRO.RO.m, (* , glob Pp, *)
                      BW.bb, BW.pk, BW.hvote, BW.uL, BW.bad, BW.check, BW.hk
                      }/\ BW.hk{2} = hk{2}/\
                    (BW.bad{2} => BH.l{2} <> BH.r{2} /\
                                   hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})) =>//=.
   
    (* call A.a2: bb *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo, (* glob Pp, *)
             glob SS, glob E, glob C, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}); first 7 by sim.
    (* call A.a1: credentials *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
      first 3 by sim.
    inline B'(E, Pz, Vz, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).setup. 
    wp; rnd{2}; swap{1} 17 -16.
    call (: ={glob HRO.RO}).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto; progress; smt.
  
  wp.
  inline *; wp.
  call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
  wp; call{1} (Pz_ll GRO.RO GRO.RO_o_ll).
  wp; while{1} (true) (size fbb{1} -i0{1}); progress. 
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll).
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto; progress; smt.
  wp;  call{1} (Vz_ll GRO.RO GRO.RO_o_ll). 
  by auto; progress; smt. 
qed. 


(* .....................*)

local lemma ssd['a] (p1 p2 : 'a -> bool) 
          (s : 'a list):
  perm_eq (filter p1 s ++ 
           filter (predI p2 (predC p1)) s ++ 
           filter (predI (predC p2) (predC p1)) s) s.
proof.
  cut := perm_filterC p2 (filter (predC p1) s).
  cut := perm_filterC p1 s.
  rewrite -?filter_predI. 
  pose a := filter p1 s.
  pose b:= filter (predC p1) s.
  pose b1:= filter (predI p2 (predC p1)) s.
  pose b2:=filter (predI (predC p2) (predC p1)) s.
  move => Hab Hb.
  smt (@List).
qed.

local lemma nth_vote (S: (ident * upkey * cipher * sign) list) 
               (L: (ident * vote * upkey * cipher * sign) list) f:
  (forall x, x\in S => x \in (map remv L))=>
  (forall x, x \in L => x.`2 =  (f\o remv) x      )=>
  map (fun (b : ident * upkey * cipher * sign),
           (nth witness L (index b (map remv L))).`2)
       S =
  map f S. 
proof.
  move => HmemSL Hxf; move: HmemSL.
  elim: S =>//=.
  move => x S Hp Hmem. 
  have : x \in map remv L. 
    smt. 
  rewrite mapP. 
  elim =>z Hz.
  split.
  + cut := Hxf z _; first by rewrite (andWl _ _ Hz).
    have ->: (nth witness L (index x (map remv L))) = z. 
    + cut := nth_index_map witness remv L z _ _. 
      + rewrite /remv; move => a b Ha Hb Hab. 
        cut := Hxf a Ha.
        cut := Hxf b Hb.
        rewrite /remv /(\o). 
        move => Hb2 Hba.
        by smt.
      + by rewrite (andWl _ _ Hz).
      by rewrite (andWr _ _ Hz).
    by rewrite (andWr _ _ Hz) /(\o).
  - cut := Hp _. 
      by smt.
    by done.
qed.   

local lemma filter_simplify (S: (ident * upkey * cipher * sign) list) 
                      (C H: (ident * vote * upkey * cipher * sign) list):

  filter (mem (map remv (filter (predC (mem (map fst5 C)) \o fst5) H))) S =
  filter (fun (x: ident * upkey * cipher * sign), 
              (mem (map remv H) x) /\ (!mem (map fst5 C) x.`1)) S.
proof.
  cut := filter_map remv  (fun (x: ident * upkey * cipher * sign),
                               predC (mem (map fst5 C)) x.`1) H .
  rewrite /preim //=.
  move => Hmm.
  have ->: (predC (mem (map fst5 C)) \o fst5) =
           (fun (x : ident * vote * upkey * cipher * sign) =>
                predC (mem (map fst5 C)) x.`1).
    by rewrite fun_ext /(==) /predC /fst5 /(\o). 
  rewrite -Hmm.


  have ->: (mem (filter (fun (x : ident * upkey * cipher * sign) =>
                             predC (mem (map fst5 C)) x.`1)
                        (map remv H))) =
           (fun (x : ident * upkey * cipher * sign) =>
                (x \in (map remv H) /\ ! (x.`1 \in map fst5 C))). 
    rewrite fun_ext /(==). 
    move => z. 
    rewrite /predC /fst5 //=. 
    smt.
  by done.
qed. 


local lemma Hsub_map['a 'b] (S L: 'a list) 
               (f: 'a -> 'b):
  subseq S L => subseq (map f S) (map f L).
proof. 
  elim: L S=>//=. smt.
  move=> x S Hp L. 
  case (L = []). smt.
  move => HL_ne.
  have ->: L = head witness L :: behead L by smt.
  case (x = head witness L).
  + move => Heq. 
    rewrite Heq //=.
    by cut := Hp (behead L).
  - move => Hneq.
    have ->: !x=head witness L by smt.
    case (!f x = f (head witness L)). smt. 
    simplify. 
    move => Hfeq.
    rewrite Hfeq //=. 
    smt.
qed.

(*
local lemma filter_san_filter
      (L: (ident * upkey * cipher * sign) list)
      (S: (ident * vote * upkey * cipher * sign) list)
      (fd: (ident * upkey * cipher * sign) -> bool):
  let fs = fun (x : ident * upkey * cipher * sign) =>
               x.`1 \in map fst5 S in 
  filter fs (Sanitize' (filter fd L)) =
  (Sanitize' (filter (predI fs fd) L)).    
proof.
  move => fs.
  rewrite /Sanitize'.
  rewrite ?filter_map /preim.
  rewrite filter_predI.
  cut Hk:= filter_map pack3 (fun (x: ident * (upkey * cipher * sign)),
                                 fd (x.`1,x.`2.`1,x.`2.`2,x.`2.`3)) L.
    rewrite /preim /pack3 //= in Hk.
    move: Hk; simplify.
    have ->: (fun (x : ident * upkey * cipher * sign) =>
            fd (x.`1, x.`2, x.`3, x.`4)) = fd. 
      rewrite fun_ext /(==); move => x; smt. 
    move => Hk.
  rewrite -Hk -/open3 -/pack3.
  have ->: (fun (x : ident * (upkey * cipher * sign)) =>
                  fd (x.`1, x.`2.`1, x.`2.`2, x.`2.`3)) =
              fd \o open3.
      rewrite fun_ext /(==) /open3 //=; move => z; smt.
  have ->: (map pack3 (filter fs (filter fd L))) =
           (filter (fs \o open3) (filter (fd \o open3) (map pack3 L))).
    rewrite -?filter_predI.
    rewrite filter_map /preim /(\o).   
    have ->: (predI fs fd) = 
             (fun (x : ident * upkey * cipher * sign) =>
        predI (fun (x0 : ident * (upkey * cipher * sign)) => fs (open3 x0))
          (fun (x0 : ident * (upkey * cipher * sign)) => fd (open3 x0))
          (pack3 x)).
        rewrite fun_ext /(==) /open3 /pack3 /predI //=. 
        move => z.  smt.
      by done.
  pose X:= (filter (fd \o open3) (map pack3 L)).
  cut := San_filter_ident X (mem (map fst5 S)).
    have ->: (fs \o open3) =
             (fun (x : ident * (upkey * cipher * sign)) => x.`1 \in map fst5 S).
      by rewrite fun_ext /(==) //=; move => z.  
  pose f:= fun (x : ident * (upkey * cipher * sign)) => 
               x.`1 \in map fst5 S. 
  simplify.
  move => Hsf. 
  by rewrite Hsf.
qed.
*)

local lemma eq_map_mem['a 'b] (f1 f2 : 'a -> 'b) (s: 'a list):
     (forall x, x \in s => f1 x = f2 x)
  =>  map f1 s = map f2 s.
proof. 
  elim: s=>//=.
  move => x s Hm Hor.
  smt.
qed.


local lemma nth_vote' (L: (ident * vote * upkey * cipher * sign) list) f:
  (forall x, x\in L => x.`2 = f (remv x))=>
  map (fun (b : ident * upkey * cipher * sign),
           (nth witness L (index b (map remv L))).`2)
      (map remv L) =
  map (fun (b : ident * vote * upkey * cipher * sign), 
                                  b.`2)  L. 
proof.
  move => Hmem_f. 
  rewrite -map_comp. 
  rewrite /(\o). 
  have Ho: forall x, x\in L =>
             (nth witness L (index (remv x) (map remv L))).`2 =
             x.`2.
    move => x Hmem_x.
    cut:= nth_index_map witness remv L x _ Hmem_x.
      move => a b Ha Hb. 
      rewrite /remv. 
      move => Hab. 
      cut := Hmem_f a Ha.
      cut := Hmem_f b Hb.
      move => Hb2 Hba.
      smt.
    by smt.
  by rewrite (eq_map_mem _ _ L Ho).
qed.   

local lemma uniq_filter_fst (L S: (ident * (upkey * cipher * sign)) list):
  let fst4= fun (x: ident * (upkey * cipher * sign)), x.`1 in
  uniq L =>
  (forall x, x\in S /\ fst4 x \in (map fst4 L) => x \in L)=>
  filter (mem L) S =
  filter ((mem (map fst4 L))\o fst4) S.
proof.
  move => fst4  HuL HmLS.
  cut := eq_in_filter (mem L) (mem (map fst4 L) \o fst4) S _ . 
  + move => x Hx_S.
    progress.
    + rewrite /(\o) /fst4 mapP.
      exists x.     
      by rewrite H.     
    - move : H.
      rewrite /(\o) mapP.
      elim => y Hmem_y.
      cut Hl:= andWl _ _ Hmem_y. 
      have Hr: x.`1 = y.`1 by move: Hmem_y; rewrite /fst //=. 
      cut := HmLS x _. 
        rewrite Hx_S (andWr _ _ Hmem_y). 
        rewrite mapP. 
        by exists y; rewrite Hl //=.
      by done.
  by done.
qed.

module DREG_Exp_dish (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme, A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle_check'(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  hCN, r', 
        pi', ev', vbb, b,m,e, j, c,
        dL, hon_mem, val_bb, hC, hNC, hN;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B'(E,P,Ve,C,SS,H,G,J).setup();
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.pk, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- USan (map rem_id vbb);
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    (* size of dishonest valid ballots *)
    BW.ball <- size dL <= BW.qCo;

    bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ 
         r <> Count (hC ++ L ++ D));

    return ev /\ bool ;
  }
}.


local lemma dbb_dish &m:
  Pr[DREG_Exp_check' (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DREG_Exp_dish(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  byequiv =>/>.
  proc.
  conseq (: ={ev, bool})=>//.
  swap{2} 40 -12. 
  seq 27 28 : ( ={ev, bool}); first by sim. 
  wp; while{2} (true) ( size(filter (predC hon_mem) val_bb){2} - j{2}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).    
    by auto=>/>; smt.
  wp;while{2} (true) (size (filter hon_mem val_bb){2} -j{2}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).    
    by auto=>/>; smt.
  wp; while{2} (true) (size BW.bb{2} - i{2}); progress.
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll);
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).    
    by auto=>/>; smt. 
  by auto=>/>; smt.
qed.


local lemma uniq_size_has['a] (L1 L2 :'a list):
  uniq L2 =>
  size L1 < size L2 => has (predC (mem L1)) L2.
proof.
  move => HuL HsL.
  elim : L2 L1 HuL HsL =>//=. 
  smt.
  move => x L2 HL1 L1 Hand Hs.
  case (!x \in L1). smt.
  + simplify. 
    move => HxL1.
    have ->: !predC (mem L1) x by rewrite /predC.
    simplify.   
    have := cat_take_drop (index x L1) L1.
    rewrite (drop_nth x). smt. 
    rewrite -cat1s.  
    have ->: nth x L1 (index x L1) = x. smt.
    pose A:= take (index x L1) L1.
    pose B:= drop (index x L1 + 1) L1.
    move => HdL1.
    move : Hs.
    have ->: size L1 = 1 + size (A++B). smt.
    move => Hs.
    have HABL: size (A++B) < size L2. smt.
    cut := HL1 (A++B) _ HABL. smt.
    rewrite !hasP. 
    elim => y [HyL2 HyL1].
    exists y; rewrite HyL2 //= -HdL1 //=.
    rewrite /predC -cat1s !mem_cat //=.
    move: HyL1; rewrite /predC mem_cat. smt.
qed. 

local lemma uniq_size_has_2 (L1 : ident list) (L2 : (ident * upkey * cipher * sign) list) 
                    (f: ident * upkey * cipher * sign -> ident):
  uniq (map f L2) =>
  size L1 < size L2 => has (predC (mem L1) \o f) L2.
proof.
  move => HuL HsL.
  elim : L2 L1 HuL HsL =>//=. 
  smt.
  move => x L2 HL1 L1 Hand Hs. 
  case (!f x \in L1). 
  + rewrite /(\o) /predC //=. 
    smt. 
  + simplify. 
    move => HxL1.
    have ->: !(\o) (predC (mem L1)) f x by rewrite /predC /(\o).
    simplify.   
    have := cat_take_drop (index (f x) L1) L1.
    rewrite (drop_nth (f x)). smt. 
    rewrite -cat1s.  
    have ->: nth (f x) L1 (index (f x) L1) = (f x). smt.
    pose A:= take (index (f x) L1) L1.
    pose B:= drop (index (f x) L1 + 1) L1.
    move => HdL1.
    move : Hs.
    have ->: size L1 = 1 + size (A++B). smt.
    move => Hs.
    have HABL: size (A++B) < size L2. smt.
    cut := HL1 (A++B) _ HABL. smt.
    rewrite !hasP. 
    elim => y [HyL2 HyL1].
    exists y; rewrite HyL2 //= -HdL1 //=.
    rewrite /predC -cat1s /(\o) ?mem_cat //=.
    move: HyL1; rewrite /predC /(\o) mem_cat. 
    rewrite !negb_or.
    elim => Hl Hr.
    rewrite Hl Hr //=.
    
    have Hx: !(f x \in map f L2). smt.
    have Hy:  (f y \in map f L2). smt.
    smt.
qed. 


local lemma uniq_filter['a 'b] (L: 'a list) (f:'a -> 'b) p:
  uniq (map f L) =>
  uniq (map f (filter p L)).
proof.
  elim: L =>//=.
  move=> x L HL Hf.
  case (p x). smt.
  smt.
qed.

local lemma uniq_mem_map['a 'b] (A: 'a list) a b (f:'a -> 'b):
  uniq (map f A) => a \in A => b \in A => f a = f b =>
  a = b.
proof.
  elim: A =>//=. 
  move => x A HuA HaA. smt.
qed. 

local lemma uniq_nomem_map['a 'b] (A: 'a list) a b (f:'a -> 'b):
  uniq (map f A) => a \in A => b \in A => a <> b => f a <> f b.
proof.
  elim: A =>//=. 
  move => x A HuA HaA. smt.
qed. 

local lemma ddd (L: (ident * upkey * cipher * sign) list) 
          (S: (ident * uskey * upkey) list):
 let f= fun (x : ident * upkey * cipher * sign) =>
           (nth witness S (index x.`2 (map thr3 S))).`1 in
 let g= fun (x : ident * upkey * cipher * sign) => x.`2 in
 (* L = Usanitize of upk *)
 uniq (map (fun (x: ident * uskey * upkey), x.`1) S)=>
 (forall x, x \in map g L =>
            x \in map thr3 S)=>
 uniq (map g L) =>
 uniq (map f L).
proof.
  move => f g HuS HLS HuL.  
  pose f':= fun (x : upkey) =>
           (nth witness S (index x (map thr3 S))).`1.
  have ->: f = f'\o g by rewrite /(\o) /f /g /f'.
  rewrite map_comp. 
  pose L':= map g L.
  move: HLS HuL; rewrite -/L'; move => HLS HuL.
  have HH: forall (x y : upkey), x \in L' => y \in L' => f' x = f' y => x = y.
  + move => x y Hx Hy Hxy. 
    move: Hxy. rewrite /f'. 
    have Hsx: (index x (map thr3 S)) < size (map thr3 S). smt.
    have Hsy: (index y (map thr3 S)) < size (map thr3 S). smt.
    have HiS: forall a b, a\in S => b \in S => a.`1 = b.`1 => a = b. smt.
    move => Hxy.
    have Hxy': nth witness S (index x (map thr3 S)) =
               nth witness S (index y (map thr3 S)). smt.
    have Hxx: exists (id:ident) (z: uskey), 
              nth witness S (index x (map thr3 S)) = (id,z,x). smt.
    have Hyy: exists (id:ident) (z: uskey), 
              nth witness S (index y (map thr3 S)) = (id,z,y). smt.
    smt.
  by rewrite (map_inj_in_uniq  f' L' HH) HuL.
qed.

local lemma ddd'['a] (L: (upkey * 'a) list) 
                (S: (ident * uskey * upkey) list):

 let f= fun (x : upkey) =>
            (nth witness S (index x (map thr3 S))).`1 in
 (* L = Usanitize of upk *)
 uniq (map (fun (x: ident * uskey * upkey), x.`1) S)=>
 (forall x, x \in map fst L =>  x \in map thr3 S)=>
 uniq (map fst L) => uniq (map f (map fst L)).
proof.
  move => f HuS.
  pose L':= map fst L.
  move => HLS HuL. 
  have HH: forall (x y : upkey), x \in L' => y \in L' => f x = f y => x = y.
  + move => x y Hx Hy Hxy. 
    move: Hxy. rewrite /f. 
    have Hsx: (index x (map thr3 S)) < size (map thr3 S). smt.
    have Hsy: (index y (map thr3 S)) < size (map thr3 S). smt.
    have HiS: forall a b, a\in S => b \in S => a.`1 = b.`1 => a = b. smt.
    move => Hxy.
    have Hxy': nth witness S (index x (map thr3 S)) =
               nth witness S (index y (map thr3 S)). smt.
    have Hxx: exists (id:ident) (z: uskey), 
              nth witness S (index x (map thr3 S)) = (id,z,x). smt.
    have Hyy: exists (id:ident) (z: uskey), 
              nth witness S (index y (map thr3 S)) = (id,z,y). smt.
    smt.
  by rewrite (map_inj_in_uniq f L' HH) HuL.
qed.

local lemma take_drop_uniq' (L :'a list) x i:
   uniq L =>
   x \in take (i+1) L =>
     !x \in drop (i+1) L.
proof.
  cut HL:= cat_take_drop (i+1) L.
  move=> HuL; rewrite -HL in HuL.
  move: HuL. rewrite cat_uniq. 
  elim => Hut [Hhas Hud].
  move: Hhas; rewrite hasPn; move => Hhas. smt.
qed. 


(* ....... DO reduction to <= qCo *) 
module DREG_Exp_qCo (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle_check'(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, hC, hNC, hN, r', pi', ev', 
        vbb, b, m, e, hon_mem, val_bb, hCN, j, c, dL;
    BW.bad  <- false;
    BW.bow  <- false;
    BW.ibad  <$ [0..qVo-1];
    BW.bb   <- [];
    BW.coL  <- [];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL  <@ A(O).a1(BW.sk);
              A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.pk, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;

    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- USan (map rem_id vbb);
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }

    bool <- (forall (X L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/ r <> Count (hC ++ L ++ dL));
    return ev /\ bool /\size dL <= BW.qCo ;
  }
}.

local lemma filter_eq_filter['a] (L: 'a list) p1 p2 f:
  filter p1 L = filter p2 L =>
  filter (fun x, p1 x \/ f x) L = filter (fun x, p2 x \/ f x) L.
proof.
  elim: L=>//=.
  move => x L Hp1p2 Hif.
  case (f x).
  + move => Hfx. smt.
  smt.
qed.


local lemma size_filter_eq_filter['a] (L: 'a list) p1 p2 f:
  count (predI p2 f) L = 0 =>
  size (filter p1 L) <= size (filter p2 L) =>
  size (filter (fun x, p1 x \/ f x) L) <= size (filter (fun x, p2 x \/ f x) L).
proof. 
  have Hc: forall (s: 'a list) a b,
    count (predU a b) s = count a s + count b s - count (predI a b) s.
    move => s a b.
    elim: s=>//=.
    move => y s Hcs.
    rewrite Hcs //=.
    have Hk: b2i (predU p1 p2 y)  =
             b2i (p1 y) + b2i (p2 y) - b2i (predI p1 p2 y).
      by smt.
    by smt.
  have ->: (fun (x : 'a) => p1 x \/ f x) = predU p1 f by rewrite /predU.
  have ->: (fun (x : 'a) => p2 x \/ f x) = predU p2 f by rewrite /predU.
  rewrite ?size_filter.
  rewrite ?Hc. 
  smt. 
qed.

local lemma size_less_filter['a] (L: 'a list) p f:
  size (filter (fun x, p x /\ f x) L) <= size (filter p L).
proof.
  elim: L=>//=.
  move => x L Hp.
  case (f x).
  + move => Hfx. simplify. smt.
  smt.
qed.

local lemma size_less_filter_two['a] (L: 'a list) p1 p2 f:
  size (filter p1 L) <= size (filter p2 L) =>
  size (filter (fun x, p1 x /\ f x) L) <= size (filter p2 L).
proof.
  move => HL.
  cut HR:= size_less_filter L p1 f. 
  smt.
qed.


local lemma uniq_perm_eq_filter'['a] (L S: 'a list):
  uniq S =>
  uniq L =>
  (forall b, mem L b => mem S b)=>
  perm_eq L (filter (mem L) S).
proof.
  rewrite perm_eqP.
  move => HuS HuL Hmem p.
  elim: L S HuL Hmem HuS =>//=. smt.
  move => x L Hcount S Hnmem.
  rewrite count_filter.
  have Hc: forall (s: 'a list) p1 p2,
    count (predU p1 p2) s = count p1 s + count p2 s - count (predI p1 p2) s.
    move => s p1 p2.
    elim: s=>//=.
    move => y s Hcs.
    rewrite Hcs //=.
    have Hk: b2i (predU p1 p2 y)  =
             b2i (p1 y) + b2i (p2 y) - b2i (predI p1 p2 y).
      by smt.
    by smt.
  have Hc': forall (s: 'a list) p1 p2,
    count (predI p1 p2) s = count p1 s + count p2 s - count (predU p1 p2) s.
    move => s p1 p2.
    rewrite Hc //=.
    by smt.

  have Hc'': forall (s: 'a list) p x,
    !mem s x =>
    count (predI p (transpose (=) x)) s =0.
    move => s p0.
    elim: s =>//=.
    progress. 
    have Hnn: x1 <> x0  by smt.
    have Hnmm: ! mem l x1 by smt. 
    rewrite (H x1 Hnmm).
    rewrite /predI //=.
    smt.
    
  have ->: (predI p (mem (x :: L))) =
           (fun y, (p y /\ y = x) \/ (p y /\ mem L y)). 
    rewrite -cat1s /predI. 
    rewrite fun_ext /(==).
    move => y.
    rewrite !mem_cat. 
    smt. 

  have ->: (fun (y : 'a) => p y /\ y = x \/ p y /\ mem L y) =
           predU (predI p (transpose (=) x)) (predI p (mem L)). 
    by rewrite /predU /predI //=.
  rewrite Hc. 

  move => Hmemb HuS. 
  rewrite (Hcount S _ _ HuS); first 2 by smt.
  rewrite count_filter.
  have ->: (b2i (p x) + count (predI p (mem L)) S =
             count (predI p (transpose (=) x)) S + 
             count (predI p (mem L)) S -
             count (predI (predI p (transpose (=) x)) (predI p (mem L))) S) =
           (b2i (p x)  =
            count (predI p (transpose (=) x)) S  -
            count (predI (predI p (transpose (=) x)) (predI p (mem L))) S).
    by smt.
  have ->: count (predI (predI p (transpose (=) x)) (predI p (mem L))) S = 0.
  have ->: (predI (predI p (transpose (=) x)) (predI p (mem L))) = 
           (fun y, false).
    rewrite /predI //=.
    rewrite fun_ext /(==).
    move => y. smt.
    smt.
  simplify.
  have HmemS: mem S x by smt.

  
  have Hs: forall (s: 'a list),
      uniq s =>
      mem s x =>
      b2i (p x) = count (predI p (transpose (=) x)) s.
    elim =>//=. 
    progress. 
    case(x0 = x). 
    + move => Heq.
      have ->: x0 = x by smt.
      rewrite Hc''. smt.      
      rewrite /predI //=. 
    - move => Hneq.
      have ->: b2i (predI p (transpose (=) x) x0) = 0 by rewrite /predI //=; smt.
      have Hmm: mem l x by smt.
      by rewrite (H H1 Hmm).
  
  by rewrite (Hs S HuS HmemS).
qed.

local lemma neeed_uniq_size['a] (L S: 'a list):
  uniq S =>
  uniq L =>
  size (filter (mem L) S) <= size L.
proof.
  move => HuS HuL.
  cut := uniq_perm_eq_filter' (filter (mem S) L) S HuS _ _. 
  + by rewrite filter_uniq 1: HuL.
  + move => b.
    by rewrite mem_filter //=. 
  have ->: filter (mem (filter (mem S) L)) S =
           filter (mem L) S.  print eq_in_filter. 
    rewrite (eq_in_filter (mem (filter (mem S) L)) (mem L) _). 
      move => x HxS.
      by rewrite mem_filter.
    by done.
  move => Hp.
  search perm_eq size. 
  cut Hs:= (perm_eq_size (filter (mem S) L) (filter (mem L) S)  Hp).
  by rewrite -Hs  size_filter count_size. 
qed.

local lemma iodf (L S: (ident * upkey*cipher*sign) list):
  size (filter (predC (mem (map rem_id L)) \o rem_id) S) <=
  size (filter (predC (mem L)) S). 
proof.
  have Habcd: forall (a b c d: int), c <= d => a <= b => a + c <= b + d by smt.
  have ->: size (filter (predC (mem (map rem_id L)) \o rem_id) S) =
           size (filter (predC (mem (map rem_id L))) (map rem_id S)). print filter_map.
    cut := filter_map rem_id (predC (mem (map rem_id L))) S.
    have ->: (preim rem_id (predC (mem (map rem_id L)))) = 
             (predC (mem (map rem_id L)) \o rem_id) by rewrite /preim.
    move => Hfm.
    by rewrite -(size_map rem_id) Hfm.
  rewrite ?size_filter.
  have ->: (predC (mem (map rem_id L))) = (fun x, forall a, !a\in L \/ rem_id a <> x).
     rewrite /predC fun_ext /(==).
     move => x. rewrite mapP negb_exists //=. smt.
  rewrite /predC //=. 
  elim: S=>//=.
  move => x S Hp.
  have Ho: b2i (forall (a : ident * upkey * cipher * sign),
                       ! (a \in L) \/ rem_id a <> rem_id x) <= b2i (! (x \in L)).
    smt.
  move: Hp Ho.
  pose a:= b2i
  (forall (a : ident * upkey * cipher * sign),
     ! (a \in L) \/ rem_id a <> rem_id x).
  pose b:= b2i (! (x \in L)).
  pose c:= count
  (fun (x0 : upkey * cipher * sign) =>
     forall (a0 : ident * upkey * cipher * sign),
       ! (a0 \in L) \/ rem_id a0 <> x0) (map rem_id S).
  pose d:= count (fun (x0 : ident * upkey * cipher * sign) => ! (x0 \in L)) S.
  move => Hp Ho. 
  by rewrite (Habcd _ _ _ _ Hp Ho).
 qed. 


  
local lemma dbb_qCo &m:
  Pr[DREG_Exp_dish (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]  <=
  Pr[DREG_Exp_qCo (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  wp. simplify.
  while ( ={j, hon_mem, val_bb, dL, BW.sk, glob E, glob HRO.RO}/\
          0<= j{1} /\
          let g = (fun (x: upkey*cipher*sign),
                       oget (dec_cipher BW.sk{1} x.`1 x.`2 HRO.RO.m{1})) in 
          dL{1} = map g (take j{1} (filter (predC hon_mem) val_bb){1})).
    wp; sp; exists * BW.sk{1}, c{1}; elim*=> skx cx.
    call{1} (dec_Dec_two skx cx.`1 cx.`2).
    by auto=>/>; progress; smt. 
  wp; while (={j, hon_mem, val_bb, hCN, BW.sk, glob E, glob HRO.RO});
    first by sim.
  wp; while (={i, BW.bb, BW.sk, glob E, glob SS, JRO.RO.m, HRO.RO.m, vbb}/\
             0<= i{1} /\
             (forall x, x\in BW.bb{1} =>
                dec_cipher BW.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None /\
                ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1})/\
             vbb{1} = take i{1} BW.bb{1}).
    sp; exists * BW.sk{1}, b{1} ; elim* => skx x.
    wp; call{1} (ver_Ver_two x.`2 x.`3 x.`4).
    call{1} (dec_Dec_two skx x.`2 x.`3).
    by auto=>/>; progress; smt. 
  wp; progress.
  call(: ={GRO.RO.m, glob Vz}); first by sim.
  call(: ={JRO.RO.m, GRO.RO.m, HRO.RO.m, glob SS, glob Pz, glob E});
    first by sim.
  call(: ={GRO.RO.m, glob Vz}); first by sim.
  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish ; wp.
  (* A.a3: check *)
  call(: ={HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.bb, BW.pk, BW.uL, BW.bad,
           BW.hvote, BW.check, BW.hk}); first 5 by sim.
  (* A.a2: bb *)
  call(: ={glob E, glob C, glob SS, HRO.RO.m, JRO.RO.m, GRO.RO.m,
           BW.bb, BW.sk, BW.pk, BW.uL, BW.coL, BW.qVo, BW.qCo, BW.hvote}/\
         BW.pk{1} = get_pk BW.sk{1} /\
         (forall x, x\in BW.bb{1} =>
                dec_cipher BW.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None /\
                ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1})/\
         uniq BW.coL{1} /\
         (forall x, x \in BW.bb{1} => 
             (predC (mem (map remv BW.hvote{1}))) x =  (mem BW.coL{1} \o fst4) x)/\
         filter (predC (mem (map remv BW.hvote{1}))) (USanitize BW.bb{1}) =
         filter (mem BW.coL{1} \o fst4) (USanitize BW.bb{1})/\
         size BW.coL{1} = BW.qCo{1}/\
         (forall x, x\in BW.coL{1} => !x \in map fst5 BW.hvote{1})/\
         Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1)).
  + (* corrrupt *) 
    proc.                
    if =>//=.
    auto=>/>; progress.
    + by rewrite cat_uniq H8 H0. 
    + cut:= filter_map remv  (predC1 id{2} \o fst4) BW.hvote{2}.
      have ->: map remv (filter (preim remv (predC1 id{2} \o Top.fst4)) 
                      BW.hvote{2}) =
             map remv (filter (predC1 id{2} \o fst5) BW.hvote{2}). 
        by rewrite /preim /Top.fst4 /fst5 /remv /predC1 /(\o).
      move => Hs.
      rewrite -Hs. 
      have ->: (predC (mem (filter (predC1 id{2} \o Top.fst4) (map remv BW.hvote{2})))) =
             (predU (predC (mem (map remv BW.hvote{2}))) (pred1 id{2} \o Top.fst4)). 
        rewrite /predC /predU fun_ext /(==). move => y.
        rewrite mem_filter. 
        rewrite negb_and /predC1 /pred1 /(\o) //=.
        smt. 
      rewrite /predU /(\o) mem_cat. smt.
      cut:= filter_map remv  (predC1 id{2} \o fst4) BW.hvote{2}.
      have ->: map remv (filter (preim remv (predC1 id{2} \o Top.fst4)) 
                      BW.hvote{2}) =
             map remv (filter (predC1 id{2} \o fst5) BW.hvote{2}). 
        by rewrite /preim /Top.fst4 /fst5 /remv /predC1 /(\o).
      move => Hs.
      rewrite -Hs. 
      have ->: (predC (mem (filter (predC1 id{2} \o Top.fst4) (map remv BW.hvote{2})))) =
             (predU (predC (mem (map remv BW.hvote{2}))) (pred1 id{2} \o Top.fst4)). 
        rewrite /predC /predU fun_ext /(==). move => y.
        rewrite mem_filter. 
        rewrite negb_and /predC1 /pred1 /(\o) //=.
        smt. 
       
  
    rewrite (eq_in_filter 
               (predU (predC (mem (map remv BW.hvote{2}))) (pred1 id{2} \o Top.fst4)) 
               (mem (BW.coL{2} ++ [id{2}]) \o Top.fst4) (USanitize BW.bb{2}) _).
      move => x HxS.
      cut := H1 x _. 
        move: HxS; rewrite mapP.
        elim => y [HyS Hxy]. 
        cut := San_mem'' (map upack BW.bb{2}) y HyS.
        rewrite mapP.
        elim => z [HzS Hyz].
        have ->: x = z by rewrite Hxy Hyz uopen_upack.
        by done.
      rewrite /(\o) mem_cat /fst4 //=. smt.
    by done.
    + smt.
    + rewrite mem_cat mem_seq1 in H9. 
      have ->: map fst5 (filter (predC1 id{2} \o fst5) BW.hvote{2}) =
               filter (predC1 id{2}) (map fst5 BW.hvote{2}).
        by rewrite filter_map /preim. 
      rewrite mem_filter /predC1 //= negb_and. 
      smt.

  + (* vote *)    
    proc.
    sp; if=>//=.
    seq 1 1: (={id, v, o, b, glob E, glob C, glob SS, HRO.RO.m, JRO.RO.m, GRO.RO.m, 
                BW.bb, BW.sk, BW.pk, BW.uL, BW.coL, BW.qVo, BW.qCo, BW.hvote} /\
              BW.pk{1} = get_pk BW.sk{1} /\
              (forall x, x \in BW.bb{1} =>
                 dec_cipher BW.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None /\
                 ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}) /\
              uniq BW.coL{1} /\
              (forall x, x \in BW.bb{1} => 
             (predC (mem (map remv BW.hvote{1}))) x =  (mem BW.coL{1} \o fst4) x)/\
         filter (predC (mem (map remv BW.hvote{1}))) (USanitize BW.bb{1}) =
         filter (mem BW.coL{1} \o fst4) (USanitize BW.bb{1})/\
              size BW.coL{1} = BW.qCo{1} /\
              BW.uL{1}.[id{1}] <> None /\ 
              ! (id{1} \in BW.coL{1}) /\ BW.qVo{1} < qVo /\
              dec_cipher BW.sk{1} b{1}.`2 b{1}.`3 HRO.RO.m{1} <> None /\
              ver_sign b{1}.`2 b{1}.`3 b{1}.`4 JRO.RO.m{1}/\
              b{1}.`1 = id{2}/\
              (forall x, x\in BW.coL{1} => !x \in map fst5 BW.hvote{1})/\
              Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1)).
    inline*; wp. sp.
    seq 1 1: (={id, v, o, glob E, glob C, glob SS, HRO.RO.m, JRO.RO.m, GRO.RO.m, 
                BW.bb, BW.sk, BW.pk, BW.uL, BW.coL, BW.qVo, BW.qCo, BW.hvote} /\
              BW.pk{1} = get_pk BW.sk{1} /\
              (forall x, x \in BW.bb{1} =>
                 dec_cipher BW.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None /\
                 ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}) /\
               uniq BW.coL{1} /\
              (forall x, x \in BW.bb{1} => 
             (predC (mem (map remv BW.hvote{1}))) x =  (mem BW.coL{1} \o fst4) x)/\
         filter (predC (mem (map remv BW.hvote{1}))) (USanitize BW.bb{1}) =
         filter (mem BW.coL{1} \o fst4) (USanitize BW.bb{1})/\
              size BW.coL{1} = BW.qCo{1} /\
              BW.uL{1}.[id{1}] <> None /\ 
              ! (id{1} \in BW.coL{1}) /\ BW.qVo{1} < qVo /\
              dec_cipher BW.sk{1} upk{1} c{1} HRO.RO.m{1} <> None /\
              ={id0, v0, pk, usk, upk, c}/\
              id0{1} = id{1} /\ v0{1} = v{1} /\ pk{1} = BW.pk{1} /\ upk{1} = get_upk usk{1}/\
              (forall x, x\in BW.coL{1} => !x \in map fst5 BW.hvote{1})/\
              Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1)). 
      exists* BW.sk{1}, upk{1}, v0{1}; elim *=> skx upkx vx.
      call{1} (Enc_dec_two skx (get_pk skx) upkx vx _ ); first by done.
      by auto=>/>; smt. 
    exists* usk{1}, c{1}; elim* => uskx cx.
    call{1} (Sig_ver_two uskx (get_upk uskx) cx _); first by done.
    by auto=>/>; smt.
    inline *; wp; sp.
    exists* BW.sk{1}, b0{1}; elim* => skx bx.
    call{1} (validInd_dec skx (get_pk skx) bx _); first by done.
    auto=>/>; progress.    
    + rewrite mem_cat mem_seq1 in H13. smt. 
    + rewrite mem_cat mem_seq1 in H13. smt. 
    + rewrite map_cat.   
      have ->: map remv [(b{2}.`1, v{2}, b{2}.`2, b{2}.`3, b{2}.`4)] = [b{2}]
               by rewrite /remv; smt. 
      rewrite eq_iff. 
      rewrite mem_cat mem_seq1 in H13.
      rewrite/predC  mem_cat.
      smt. 
    + rewrite map_cat.   
      have ->: map remv [(b{2}.`1, v{2}, b{2}.`2, b{2}.`3, b{2}.`4)] = [b{2}]
               by rewrite /remv; smt. 
      rewrite (eq_in_filter
                  (predC (mem (map remv BW.hvote{2} ++ [b{2}])))
                  (mem BW.coL{2} \o Top.fst4) (USanitize (BW.bb{2} ++ [b{2}])) _ ).
        move => x HxS. 
        have HxS': x \in (BW.bb{2} ++ [b{2}]). 
          move: HxS; rewrite mapP.
          elim => y [HyS Hxy]. 
          cut := San_mem'' _ y HyS.
          rewrite mapP.
          elim => z [HzS Hyz].
          have ->: x = z by rewrite Hxy Hyz uopen_upack.
          by done.
        rewrite mem_cat mem_seq1 in HxS'.
        rewrite/predC  mem_cat.
        smt. 
      by done.
    + rewrite map_cat mem_cat /fst5 //= -/fst5. smt.
    + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       + move => x y.
         rewrite ?mem_cat //=. smt.
      by done.
    + move: H13 H14; rewrite ?mem_cat //=. smt.
    + move: H13 H14; rewrite ?mem_cat //=. smt.      
    + rewrite map_cat.   
      have ->: map remv [(b{2}.`1, v{2}, b{2}.`2, b{2}.`3, b{2}.`4)] = [b{2}]
               by rewrite /remv; smt. 
      rewrite eq_iff. 
      rewrite/predC  mem_cat.
      smt. 
    + rewrite map_cat.   
      have ->: map remv [(b{2}.`1, v{2}, b{2}.`2, b{2}.`3, b{2}.`4)] = [b{2}]
               by rewrite /remv; smt. 
      rewrite (eq_in_filter
                  (predC (mem (map remv BW.hvote{2} ++ [b{2}])))
                  (mem BW.coL{2} \o Top.fst4) (USanitize BW.bb{2}) _ ).
        move => x HxS. 
        have HxS': x \in BW.bb{2}. 
          move: HxS; rewrite mapP.
          elim => y [HyS Hxy]. 
          cut := San_mem'' _ y HyS.
          rewrite mapP.
          elim => z [HzS Hyz].
          have ->: x = z by rewrite Hxy Hyz uopen_upack.
          by done. 
        cut := H1 x HxS'.
        rewrite /predC  mem_cat.
        smt. 
      by done.
    + rewrite map_cat mem_cat /fst5 //= -/fst5.
      smt.

  + (* cast *)    
    proc.
    sp; if=>//=.
    inline *; wp; sp.
    exists* BW.sk{1}, b0{1}; elim* => skx bx.
    call{1} (validInd_dec skx (get_pk skx) bx _); first by done.
    auto=>/>; progress.    
    + rewrite mem_cat mem_seq1 in H12. smt. 
    + rewrite mem_cat mem_seq1 in H12. smt.  
    + rewrite mem_cat mem_seq1 in H12. 
      case (x \in BW.bb{2}). smt. 
      move => Hnm.
      have Hx: x = b{2} by smt. 
      rewrite /predC /(\o) /fst4 //= Hx H8 //=. 
      rewrite mapP negb_exists //=.
      rewrite eqT. move => a.
      cut := H3 b{2}.`1 H8.
      rewrite mapP negb_exists /fst5 //=.
      move => Ha.
      cut := Ha a. rewrite /remv //=. smt.
    + rewrite (eq_in_filter
                  (predC (mem (map remv BW.hvote{2})))
                  (mem BW.coL{2} \o Top.fst4) (USanitize (BW.bb{2} ++[b{2}])) _ ).
        move => x HxS. 
        have HxS': x \in (BW.bb{2} ++ [b{2}]).
          move: HxS; rewrite mapP.
          elim => y [HyS Hxy]. 
          cut := San_mem'' _ y HyS.
          rewrite mapP.
          elim => z [HzS Hyz].
          have ->: x = z by rewrite Hxy Hyz uopen_upack.
          by done. 
        rewrite mem_cat mem_seq1 in HxS'.
        case (x \in BW.bb{2}). smt. 
        move => Hnm.
        have Hx: x = b{2} by smt. 
        rewrite /predC /(\o) /fst4 //= Hx H8 //=. 
        rewrite mapP negb_exists //=.
        move => a.
        cut := H3 b{2}.`1 H8.
        rewrite mapP negb_exists /fst5 //=.
        move => Ha.
        cut := Ha a. rewrite /remv //=. smt.
      by done.
    + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       + move => x y.
         rewrite ?mem_cat //=. smt.
      by done.
    + move: H13 H14; rewrite ?mem_cat //=. smt.
    + move: H13 H14; rewrite ?mem_cat //=. smt.    


  + (* rest *) 
    by proc. by proc. by proc. by proc.
  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim.
  inline  B'(E, Pz, Vz, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).setup.
  wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
  auto=>/>; progress. 
  + smt.  
  + smt (San'_empty).  
  + smt.
  + smt(take0).
  + smt(take0).
  + rewrite ?take_oversize; first 3 by smt.
    pose D:= map (fun (x : upkey * cipher * sign) =>
                      oget (dec_cipher result_R.`2 x.`1 x.`2 m_R))
                (filter (predC (mem (map (rem_id \o remv) hvote_R)))
                 (USan (map rem_id bb_R))).
    cut Hxdl:= H22 X D L.
    have Hd: size D <= size coL_R. 
    + move: H7. move => H7. 
      rewrite /D size_map.
      rewrite -USan_USanitize.
      rewrite filter_map. 
      have ->: (preim rem_id (predC (mem (map (rem_id \o remv) hvote_R)))) =
             (predC (mem (map (rem_id \o remv) hvote_R))) \o rem_id.
        by rewrite /preim /(\o). 
      rewrite size_map. print neeed_uniq_size.
      cut := neeed_uniq_size coL_R (map Top.fst4 (Sanitize' bb_R)) _ H6.
      + rewrite /USanitize -map_comp. 
        cut := San_uniq_fst' (map pack3 bb_R).
        by rewrite /Top.fst4 /open3 /(\o) //=. 
      rewrite -H10.
      have ->: (filter (mem coL_R) (map Top.fst4 (Sanitize' bb_R))) = 
             map Top.fst4 (filter (mem coL_R \o Top.fst4) (Sanitize' bb_R)).
        rewrite filter_map /preim /(\o). smt.
      rewrite size_map.
      rewrite H10 -H8 -H10. 
      cut := iodf (map remv hvote_R) (Sanitize' bb_R).
      by smt.
    by smt.
  + rewrite ?take_oversize; first 3 by smt.
    rewrite size_map.
    move: H7. move => H7. 
    rewrite -USan_USanitize. 
    rewrite filter_map. 
    
    have ->: (preim rem_id (predC (mem (map (rem_id \o remv) hvote_R)))) =
             (predC (mem (map (rem_id \o remv) hvote_R))) \o rem_id.
        by rewrite /preim /(\o). 
      rewrite size_map. 
      rewrite -H10.
      cut := neeed_uniq_size coL_R (map Top.fst4 (Sanitize' bb_R)) _ H6.
      + rewrite /Sanitize' -map_comp. 
        cut := San_uniq_fst' (map pack3 bb_R).
        by rewrite /Top.fst4 /open3 /(\o) //=.
      have ->: (filter (mem coL_R) (map Top.fst4 (Sanitize' bb_R))) = 
             map Top.fst4 (filter (mem coL_R \o Top.fst4) (Sanitize' bb_R)).
        rewrite filter_map /preim /(\o). smt.
      rewrite size_map.
      rewrite H10 -H8 -H10. 
      cut := iodf (map remv hvote_R) (Sanitize' bb_R).
      by smt.
qed. 


module DREG_Oracle_vote(V: VotingScheme',
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc h        = DREG_Oracle(V,H,G,J).h
  proc g        = DREG_Oracle(V,H,G,J).g
  proc j        = DREG_Oracle(V,H,G,J).j

  proc corrupt  = DREG_Oracle(V,H,G,J).corrupt
  proc cast     = DREG_Oracle(V,H,G,J).cast
  proc board    = DREG_Oracle(V,H,G,J).board

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL /\ BW.qVo <qVo){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      o <- Some b;
      if (!(b.`2,b.`3) \in (map (fun (x: ident * vote * upkey * cipher*sign),
                         (x.`3,x.`4))
                     BW.hvote)){
        BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)]; 
       }elif(!BW.bow){
         BW.bow <- true;
         BW.ibad <- BW.qVo;
       }
      if (forall x, x\in BW.bb => 
                       (x.`1,x.`2) = (b.`1,b.`2) \/
                       (x.`1 <> b.`1 /\ x.`2 <> b.`2)) {
           BW.bb<- BW.bb ++ [b];
        }
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL, veh;
    i <- 0;

    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.pk, BW.hk);
    BW.pbb <- map (fun (x: ident * upkey * cipher * sign), 
                       (get_pub_data (preha x), hash' BW.hk x)) 
                       (Sanitize' BW.bb);
    (* verify all honest ballots *)
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), 
                                     BW.pbb, BW.hk);
      veh <- (preha \o remv) h \in 
             map (preha \o open3 ) (Sanitize (map pack3 BW.bb));

      if (id = h.`1 /\ vv /\ veh /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }
      i <- i +1;
    }
  }

}.

module DREG_Exp_vote (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, hC, hNC, hN, r', pi', ev', 
        vbb, b, m, e, hon_mem, val_bb, hCN, j, c, dL;
    BW.bad  <- false;
    BW.bow  <- false;
    BW.ibad  <$ [0..qVo-1];
    BW.bb   <- [];
    BW.coL  <- [];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb <- map (fun (x: ident * upkey * cipher * sign), 
                       (get_pub_data (preha x), hash' BW.hk x)) 
                       (Sanitize' BW.bb);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;

    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id (Sanitize' vbb));
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }

    bool <- (forall (X L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/ r <> Count (hC ++ L ++ dL));
    return ev /\ bool /\size dL <= BW.qCo ;
  }
}.

local lemma dbb_wspread &m:
  Pr[DREG_Exp_qCo  (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]  <=
  Pr[DREG_Exp_vote (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] +
  Pr[DREG_Exp_vote (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: 
     BW.bow /\ 0 <= BW.ibad < qVo].
proof.
  byequiv =>/>.
  proc.
  wp. simplify.

  seq 25 25: (  0 <= BW.ibad{2} < qVo /\
             ={ev, bool, r, BW.bb, BW.qCo, glob E, glob SS, 
               HRO.RO.m, JRO.RO.m, BW.sk}/\
             (!BW.bow{2} => 
                ={BW.hvote, BW.check})/\
             Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1))=>/>.
(*    progress.
    cut Hof:= H4 X D L.    
    by rewrite -(andEl _ _ (H1 H2)) -(andEr _ _ (H1 H2)) Hof. 
*)
  wp; call (: ={BW.pk, BW.pbb, glob Vz, glob HRO.RO, GRO.RO.m});
    first by sim.
  call (: ={BW.sk, BW.bb, glob Pz, glob E, glob SS, 
            HRO.RO.m, GRO.RO.m, JRO.RO.m});
    first by sim.
  call (: ={BW.pk, BW.pbb, glob Vz, glob HRO.RO, GRO.RO.m});
    first by sim.
  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish; wp.
   (* A.a3 call: check *)
  call (: ={BW.bb, BW.uL, BW.pk, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.hk}/\
          (!BW.bow{2} =>  ={BW.hvote, BW.check}) /\
          Sanitize' BW.bb{1} = USanitize BW.bb{1}).
  + proc.
    inline *.
    case (!BW.bow{2}).
    + while (={i, pL, BW.check, BW.hvote, BW.bb, id, BW.pbb, BW.hk}/\
             Sanitize' BW.bb{1} = USanitize BW.bb{1}).
        auto=>/>; progress. 
        + move: H0 H1 H2 H3 H4.
          pose x:= (nth witness pL{2} i{2}).
          rewrite /hash' /preha map_comp -/Sanitize'.
          rewrite -USan_USanitize -H /Sanitize'.
          have ->: (x.`1, x.`3, x.`4, x.`5) = remv x by rewrite /remv. 
          rewrite /(\o).  
          smt.
        + move: H0 H1 H2 H3 H4.
          pose x:= (nth witness pL{2} i{2}).
          rewrite /hash' /preha map_comp -/Sanitize'.
          rewrite -USan_USanitize -H /Sanitize'.
          have ->: (x.`1, x.`3, x.`4, x.`5) = remv x by rewrite /remv. 
          rewrite /(\o).  
          smt.
      auto=>/>; progress.
      + smt. smt. smt. 
      + by rewrite H0 -USan_USanitize -map_comp /hash' /preha /(\o).
      + smt. smt. 
    - conseq (: BW.bow{2})=>/>.
      while{1} (true) (size pL{1} - i{1}); progress.    
        auto=>/>; smt.
      while{2} (true) (size pL{2} - i{2}); progress.    
        auto=>/>; smt.
      auto=>/>; smt.
  + by proc.
  + by proc. 
  + by proc.
  + by proc. 

  (* A.a2 call: fill bb *)
  wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
            glob SS, glob E, glob C, BW.bb, BW.pk, BW.coL, BW.sk,
            BW.uL, BW.qCo}/\
           BW.pk{1} = get_pk BW.sk{1}/\
          (!BW.bow{2} => ={BW.hvote})/\
          0<= BW.ibad{2} <qVo /\ 
          0 <= BW.qVo{2} /\
          Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1) ).
   + proc. 
     if =>//=.
     by auto =>/>; smt.
   + proc.
     sp; if=>//=.
     seq 2 1: (={id, v, o, GRO.RO.m, HRO.RO.m, JRO.RO.m, 
                 BW.qVo, glob SS, glob E, BW.bb, BW.pk, BW.sk,
                 BW.coL, BW.uL, BW.qCo, glob C} /\
               BW.pk{1} = get_pk BW.sk{1} /\
               (!BW.bow{2} => ={BW.hvote}) /\ 
               0 <= BW.ibad{2} < qVo /\ 0 <= BW.qVo{2} /\
               BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\ 
               BW.qVo{1} < qVo /\ o{1} = None/\
               ={b} /\ 
              (e{1} <=> (forall x, x\in BW.bb{2} => 
                           (x.`1,x.`2) = (b{2}.`1,b{2}.`2) \/
                           (x.`1 <> b{2}.`1 /\ x.`2 <> b{2}.`2)))/\
              Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1)).
       seq 1 1: (={id, v, o, GRO.RO.m, HRO.RO.m, JRO.RO.m, 
                 BW.qVo, glob SS, glob E, BW.bb, BW.pk, BW.sk,
                 BW.coL, BW.uL, BW.qCo, glob C} /\
               BW.pk{1} = get_pk BW.sk{1}/\
               (!BW.bow{2} => ={BW.hvote}) /\ 
               0 <= BW.ibad{2} < qVo /\ 0 <= BW.qVo{2} /\
               BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\ 
               BW.qVo{1} < qVo /\ o{1} = None/\
               ={b} /\ 
               dec_cipher BW.sk{1} b{1}.`2 b{1}.`3 HRO.RO.m{1} <> None /\
               ver_sign b{1}.`2 b{1}.`3 b{1}.`4 JRO.RO.m{1} /\
               Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1)).
         inline*; wp; sp.
         seq 1 1: (id0{2} = id{2} /\
               v0{2} = v{2} /\
               pk{2} = BW.pk{2} /\
               usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
               upk{2} = get_upk usk{2} /\
               id0{1} = id{1} /\
               v0{1} = v{1} /\
               pk{1} = BW.pk{1} /\
               usk{1} = (oget BW.uL{1}.[id{1}]).`2 /\
               upk{1} = get_upk usk{1} /\
               (o{2} = None /\
                 o{1} = None /\
                 ={id, v} /\
                 ={GRO.RO.m, HRO.RO.m, JRO.RO.m, BW.qVo, glob C,
                   glob SS, glob E, BW.bb, BW.pk,
                   BW.coL, BW.sk, BW.uL, BW.qCo} /\
                   BW.pk{1} = get_pk BW.sk{1} /\
                 (!BW.bow{2} => ={BW.hvote}) /\ 0 <= BW.ibad{2} < qVo /\ 
                 0 <= BW.qVo{2}) /\
                   BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\
                 BW.qVo{1} < qVo/\ ={c} /\ 
                 dec_cipher BW.sk{1} upk{1} c{1} HRO.RO.m{1} <> None /\
                 Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
         (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1)).
           exists* BW.sk{1}, upk{1}, v0{1}; elim* => skx upkx cx. 
           call{1} (Enc_dec_two skx (get_pk skx) upkx cx _); first by done.
           by auto=>/>; smt.
       exists* usk{1}, c{1}; elim*=> uskx cx.
       call{1} (Sig_ver_two uskx (get_upk uskx) cx _); first by done.
       by auto=>/>.
     inline *; wp; sp.
     exists* (glob C){1}, (glob E){1}, (glob SS){1}, b0{1}, BW.sk{1}; elim* => gc ge gs bx skx. 
     call{1} (validInd_dec_one gc ge gs skx (get_pk skx) bx _); 
        first by done.      
     by auto=>/>; smt.
     auto=>/>; progress.
     +  smt. smt. 
     + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       by done.
    + move: H11 H12; rewrite ?mem_cat //=. smt.
    + move: H11 H12; rewrite ?mem_cat //=. smt.   
    + smt. smt. smt. 
    + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       + move => x y.
         rewrite ?mem_cat //=. smt.
      by done.
    + move: H12 H13; rewrite ?mem_cat //=. smt.
    + move: H12 H13; rewrite ?mem_cat //=. smt.   
    + smt. smt.
    + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       + move => x y.
         rewrite ?mem_cat //=. smt.
      by done.
    + move: H12 H13; rewrite ?mem_cat //=. smt.
    + move: H12 H13; rewrite ?mem_cat //=. smt.  
    + smt. 

   + proc.
     if =>//=.
     inline*; wp.
     call(: ={glob JRO.RO, glob HRO.RO}); first 2 by sim.
     auto=>/>; progress.
     + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       + move => x y.
         rewrite ?mem_cat //=. smt.
       by done.
     + move: H10 H11; rewrite ?mem_cat //=. smt.
     + move: H10 H11; rewrite ?mem_cat //=. smt.   
   + by proc.
   + by proc.
   + by proc.
   + by proc.

   (* call A.a1: credentials *)
   call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
     first 3 by sim.
   inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
   wp; rnd; call{1} (Kgen_get_pk HRO.RO).
   call (: true ==> ={glob JRO.RO}); first by sim.
   call (: true ==> ={glob GRO.RO}); first by sim.
   call (: true ==> ={glob HRO.RO}); first by sim.
   wp; rnd{2}; wp.
   auto=>/>; progress. 
   + smt. smt. smt. smt. 
   + by rewrite H11 -USan_USanitize -map_comp /hash' /preha /(\o).
   + by rewrite H11 -USan_USanitize -map_comp /hash' /preha /(\o).
   case (!BW.bow{2}).
     while (={j, dL, glob E, glob HRO.RO, hon_mem, val_bb, BW.sk});
      first by sim.
     wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, hCN, BW.sk});
      first by sim.
     wp; while (={i, BW.bb, glob E, glob SS, HRO.RO.m, JRO.RO.m, BW.sk, vbb} /\
                0 <= i{1} /\
                 let f = fun (x: ident * upkey * cipher * sign),
                       dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} 
               in vbb{2} = filter f (take i{1} BW.bb{2})).
        wp; sp. 
        exists* BW.sk{1}, b{1}; elim* => skx bx. 
        call{1} (ver_Ver_two bx.`2 bx.`3 bx.`4).
        call{1} (dec_Dec_two skx bx.`2 bx.`3).
        auto=>/>; progress.
        + smt. 
        + rewrite (take_nth witness); first by smt.
          by rewrite filter_rcons cats1 //= H2 H1 //=. 
        + smt.  
        + rewrite (take_nth witness); first by smt.  
          by rewrite filter_rcons //= H1.
      auto; progress.
      + smt (take0). smt. 
      + rewrite take_oversize; first by smt.
        rewrite -USan_USanitize.
        pose f:= (fun (x0 : ident * upkey * cipher * sign) =>
       dec_cipher BW.sk{2} x0.`2 x0.`3 HRO.RO.m{2} <> None /\
       ver_sign x0.`2 x0.`3 x0.`4 JRO.RO.m{2}).
        rewrite San'_USan. 
        + move => x y.
          rewrite ?mem_filter.
          smt.
        + move => x y.
          rewrite ?mem_filter.
          smt.
        by rewrite /rem_id.
      + move: H9.
        rewrite take_oversize; first by smt.
        rewrite -USan_USanitize.
        pose f:= (fun (x0 : ident * upkey * cipher * sign) =>
       dec_cipher BW.sk{2} x0.`2 x0.`3 HRO.RO.m{2} <> None /\
       ver_sign x0.`2 x0.`3 x0.`4 JRO.RO.m{2}).
        rewrite San'_USan. 
        + move => x y.
          rewrite ?mem_filter.
          smt.
        + move => x y.
          rewrite ?mem_filter.
          smt.
        by rewrite /rem_id; smt.
      + move: H9.
        rewrite take_oversize; first by smt.
        rewrite -USan_USanitize.
        pose f:= (fun (x0 : ident * upkey * cipher * sign) =>
                  dec_cipher BW.sk{2} x0.`2 x0.`3 HRO.RO.m{2} <> None /\
                  ver_sign x0.`2 x0.`3 x0.`4 JRO.RO.m{2}).
        rewrite San'_USan. 
        + move => x y.
          rewrite ?mem_filter.
          smt.
        + move => x y.
          rewrite ?mem_filter.
          smt.
        by smt.
      + smt. smt. smt.
   wp; while{1} (true) (size (filter (predC hon_mem) val_bb){1} - j{1}); progress.
     wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{2} (true) (size (filter (predC hon_mem) val_bb){2} - j{2}); progress.
     wp; call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{1} (true) (size (filter hon_mem val_bb){1} - j{1}); progress.
     wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{2} (true) (size (filter hon_mem val_bb){2} - j{2}); progress.
     wp; call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{1} (true) (size BW.bb{1} - i{1}); progress.
     wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll).
     wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{2} (true) (size BW.bb{2} - i{2}); progress.
     wp; call{2} (SSv_ll JRO.RO JRO.RO_o_ll).
     wp; call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   by auto=>/>; smt.
qed.


module DREG_Exp_check_two (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  hC, hNC, hN, r', 
        pi', ev', vbb, b,m,e, j, c, hon_mem, val_bb, dL, hCN;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb <- map (fun (x: ident * upkey * cipher * sign), 
                       (get_pub_data (preha x), hash' BW.hk x)) 
                       (Sanitize' BW.bb);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id (Sanitize' vbb));
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }

    hC <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.check)) 
                            (map rem_id (Sanitize' vbb)))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.check)) 
                               (map rem_id (Sanitize' vbb))) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hC<- hC ++ [oget m];
      j <- j + 1;
    }
    
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;
    bool <- (forall (X L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/ 
         r <> Count (hC ++ L ++ dL));
    
    return ev /\ bool /\ size dL <= BW.qCo ;
  }
}.
 
local lemma uniq_snd_san (L : (ident * (upkey * cipher*sign)) list):
  (forall x y, x \in L =>
             y \in L => x.`2.`1 = y.`2.`1 => x.`1 = y.`1) =>
  uniq (map fst L) =>
  uniq (map snd L).
proof.
  have Ho : forall (X:(upkey * cipher*sign)list),
     uniq (map (fun (x: upkey * cipher*sign), x.`1) X) => uniq X. print uniq_map.
     move => X HuX. 
     by rewrite (uniq_map (fun (x: upkey * cipher*sign), x.`1) X HuX).
  cut := Ho (map snd L). rewrite -map_comp /(\o) //=.
  move => HsL.
  move => Hxy Huf.
  move : HsL =>-> //=. 
  elim: L Hxy Huf =>//=.
  move => a L HuL Hxy Huf.  
  cut := HuL _ _.
  + move => x y HxL HyL Hxy1.
    cut := Hxy x y _ _ Hxy1. smt. smt.
    by done.
  + by rewrite (andWr _ _ Huf).
  move => HuL2.
  rewrite HuL2 //=. 
  cut:= (andWl _ _ Huf).
  rewrite mapP negb_exists //=.
  move => HafL. 
  rewrite mapP negb_exists //=. 
  move => b. 
  cut := HafL b. rewrite ?negb_and. smt. 
qed.
 
local lemma dbbcheck_dbbcheck_two &m:
  Pr[DREG_Exp_vote (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DREG_Exp_check_two (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  
  seq 19 17: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob C, glob HRO.RO, BW.qVo,
                glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
                BW.uL, BW.qCo, BW.pk, BW.sk, bool, BW.bad, BW.hk}/\ 
              BW.bb{1} =[] /\ 
              BW.coL{1} =[] /\ 
              BW.hvote{1} =[] /\
              BW.check{1} =[]/\
              BW.qVo{1} = 0/\
              bool{2} /\ BW.pk{2} = get_pk BW.sk{2}). 
    (* call A.a1: credentials *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
      first 3 by sim.
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt.

  seq 6 6: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob C, glob HRO.RO, BW.qVo,
              glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
              BW.uL, BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi', BW.hk}/\ 
            bool{2}/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
            (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in  Sanitize' BW.bb{2})/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.check{2})/\
            (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: ={glob E, glob SS, glob Pz, 
             glob JRO.RO, glob HRO.RO, glob GRO.RO}); first by sim.
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    wp.
    (* A.a3 call *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO,  BW.hk,
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in map remv BW.check{2} => 
                exists id, (id,x.`2,x.`3,x.`4) \in  Sanitize' BW.bb{2})/\
           (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
           (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb, BW.hk}/\
             0 <= i{2} /\
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in BW.check{2} => 
                        x \in BW.hvote{2})/\
             (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in  Sanitize' BW.bb{2})/\
             BW.pbb{2} = map (fun (x: ident * upkey * cipher * sign), 
                                  (get_pub_data (preha x), hash' BW.hk{2} x)) 
                             (Sanitize' BW.bb{2})/\
             (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
        auto =>/>; progress.
        + smt. 
        + move: H7; rewrite mem_cat mem_seq1; move => Hmor.
          case (x \in BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.   
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt. 
            rewrite -Hx. 
            move => HmS. 
            cut := San_mem (map pack4 BW.hvote{2}) (pack4 x) _. 
              + move: HmS; rewrite mapP.
                elim=> y HymS.     
                rewrite (andWr _ _ HymS).       
                have ->: pack4 (open4 y) = y by rewrite /open4 /pack4; smt.        
                rewrite (andWl _ _ HymS).
            rewrite mapP //=; elim => y HymS.
            have ->: x = y by  cut:= (andWr _ _ HymS); rewrite /pack4 ; smt.
            rewrite (andWl _ _ HymS).
        + move: H7; rewrite map_cat //= mem_cat mem_seq1; move => Hmem.
          case (x \in map remv BW.check{2}). smt.
          (* use the exists id *)
          move => Hn.
          have Hx: x = remv (nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2})
            by move :Hmem; rewrite Hn.
          move: H5. 
          rewrite map_comp /preha -/Sanitize' //= mapP.
          have ->: map open3 (Sanitize (map pack3 BW.bb{2})) = Sanitize' BW.bb{2}.
            by rewrite /Sanitize'.
          elim => y.       
          elim => Hym . rewrite /(\o) -Hx. smt.
         
        + rewrite map_cat //=.  
          rewrite cats1 rcons_uniq (H2 H7) //=.
          move: H3 H4 H5 H6;
            pose L:= map open4 (Sanitize (map pack4 BW.hvote{2}));
            pose x:= nth witness L i{2}.
          move => H3 H4 H5 H6.      
          rewrite mapP negb_exists //=.
          move => a. rewrite negb_and.           
          case (!a \in BW.check{2}). smt.
          simplify. move => HmaC. 
          have HmaH: a \in BW.hvote{2} by rewrite H0.
          rewrite /(\o) /rem_id /remv //=.  
          have HmxH: x \in BW.hvote{2}. 
            rewrite /x /L. search mem Sanitize. 
            cut := San_mem (map pack4 BW.hvote{2}) 
                           (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) _. smt.
            rewrite mapP.
            elim => y Hymem. 
            have ->: nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} = 
                      open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}). smt.
            have ->: open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) = y. 
              by rewrite (andWr _ _ Hymem) /open4 /pack4 //=; smt.
            rewrite (andWl _ _ Hymem).
          
          move: H6. rewrite mapP negb_exists /fst5 //=.
          move => H6.
          cut := H6 x. rewrite //=.
          move => HmxC. rewrite ?andabP. 
          have Hmust: forall (X: (ident * upkey * cipher * sign) list) a b,
                       uniq (map rem_id X) => 
                       a \in X => 
                       b \in X => 
                      a <> b =>
                      rem_id a <> rem_id b. 
          + move => X c b HuX Hma Hmb Hneq. 
            cut Hia:= nth_index witness c X Hma.
            cut Hib:= nth_index witness b X Hmb.
            case (index c X < index b X).
             + move => Hiab.
               have Hea: exists A B C, X = A ++ [c] ++ B ++ [b] ++ C. 
                cut := take_nth witness (index c X) X _. smt.
                rewrite -cats1 Hia.
                move => HA. 
                exists (take (index c X) X).
                rewrite -HA.
                cut := take_nth witness (index b X) X _ . smt.
                rewrite -cats1 Hib.
                move => HB. 
                exists (drop (index c X +1) (take (index b X) X)). 
                have ->: take (index c X + 1) X ++ drop (index c X + 1) (take (index b X) X) ++ [b]
                 = take (index b X + 1) X.
                  rewrite HB.
                have ->: take (index c X + 1) X = take (index c X + 1) (take (index b X) X). 
                  pose i:= index c X + 1.
                  pose j:= index b X.
                  cut := cat_take_drop j X . 
                  smt.
                smt.
                exists (drop (index b X +1) X).
                by rewrite cat_take_drop.
               move: Hea. elim => A B C HX.
               move: HuX. rewrite HX. 
               rewrite ?map_cat ?cat_uniq.
               simplify. 
               case( rem_id b  = rem_id c).  
                  move => Heq.
                  have ->: rem_id b \in map rem_id A ++ [rem_id c] ++ map rem_id B. smt.
                  by done.               
               by smt.  
             - move => Hiab.   
               have Hi_dif: index c X <> index b X. smt.
               have Hiab': index b X < index c X by smt.
               have Hea: exists A B C, X = A ++ [b] ++ B ++ [c] ++ C. 
                cut := take_nth witness (index b X) X _. smt.
                rewrite -cats1 Hib.
                move => HB. 
                exists (take (index b X) X).
                rewrite -HB.
                cut := take_nth witness (index c X) X _ . smt.
                rewrite -cats1 Hia.
                move => HA. 
                exists (drop (index b X +1) (take (index c X) X)). 
                have ->: take (index b X + 1) X ++ drop (index b X + 1) (take (index c X) X) ++ [c]
                 = take (index c X + 1) X.
                  rewrite HA.
                have ->: take (index b X + 1) X = take (index b X + 1) (take (index c X) X). 
                  pose i:= index b X + 1.
                  pose j:= index c X.
                  cut := cat_take_drop j X . 
                  smt.
                smt.
                exists (drop (index c X +1) X).
                by rewrite cat_take_drop.
               move: Hea. elim => A B C HX.
               move: HuX. rewrite HX. 
               rewrite ?map_cat ?cat_uniq.
               simplify. 
               case(rem_id b  = rem_id c). 
                 move => Heq.
                 have ->: rem_id c \in map rem_id A ++ [rem_id b] ++ map rem_id B. smt.
                 by done.               
               by smt.
            cut := Hmust (map remv BW.hvote{2}) (remv a) (remv x) _ _ _ _.
             + rewrite -map_comp H7. 
             + rewrite mapP. exists a. rewrite HmaH //=.
             + rewrite mapP. exists x. rewrite HmxH //=.
             + smt.
            rewrite /rem_id /remv //= ?andabP.
            by smt.
        + smt.
      by auto=>/>; progress. 

    (* H, G, J *)
    + by proc. 
    + by proc. 
    + by proc.
    + by proc.

    (* A.a1 call *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo, glob C}/\
             BW.pk{2} = get_pk BW.sk{2}/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                           (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                           ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
             (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.hvote{2})/\
             (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
    + proc. 
      auto=>/>; progress. smt. smt. smt. smt. 

    + proc. 
      sp; if =>//=.
      exists * BW.sk{2}; elim* => skx.
      inline*; sp; wp.
      seq 1 1: (id0{2} = id{2} /\
             v0{2} = v{2} /\
             pk{2} = BW.pk{2} /\
             usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
             upk{2} = get_upk usk{2} /\
             id0{1} = id{1} /\
             v0{1} = v{1} /\
             pk{1} = BW.pk{1} /\
             usk{1} = (oget BW.uL{1}.[id{1}]).`2 /\
             upk{1} = get_upk usk{1} /\
             skx = BW.sk{2} /\
             o{2} = None /\
             o{1} = None /\
             ={id, v, GRO.RO.m, HRO.RO.m, JRO.RO.m, BW.qVo, 
               glob SS, glob E, BW.bb, BW.pk, glob C,
               BW.hvote, BW.coL, BW.uL, BW.qCo} /\
             BW.pk{2} = get_pk BW.sk{2} /\
             (forall (y : ident * vote * upkey * cipher * sign),
               y \in BW.hvote{2} =>
               dec_cipher BW.sk{2} y.`3 y.`4 HRO.RO.m{2} <> None /\
               ver_sign y.`3 y.`4 y.`5 JRO.RO.m{2}) /\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
             (forall (x : ident * vote * upkey * cipher * sign),
               x \in BW.hvote{2} =>
               x.`2 = oget (dec_cipher BW.sk{2} (remv x).`2 (remv x).`3 
                      HRO.RO.m{2})) /\
             uniq (map (rem_id \o remv) BW.hvote{2}) /\
             BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\ 
             BW.qVo{1} < qVo /\ ={c} /\
             dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2} <> None /\
             v0{2} = oget (dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2})/\
             (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
        exists* v0{1}, upk{2};  elim* => vx upkx. 
        call{1} (Enc_dec_two skx (get_pk skx) upkx vx _); first by done.
        by auto; progress; smt. 
      exists* usk{1}, c{1}; elim*=> uskx cx.
      call{1} (Sig_ver_two uskx (get_upk uskx) cx _ );
        first by done.
      auto=>/>; progress. 
      + smt. 
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite mem_cat mem_seq1 in H11; smt. 
      + rewrite mem_cat mem_seq1 in H11; smt. 
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite map_cat //=. 
        have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H9. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt. 
      + move: H11 H12; rewrite ?mem_cat. 
        move => H11 H12. smt.
      + smt. smt. smt. 
      + rewrite map_cat //=. 
        have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H9. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt.
      + smt. smt.
      + move: H11 H12; rewrite ?mem_cat //=; move => H11 H12.
        smt.
    + proc. 
      if =>//=.
      inline*; wp; sp. 
      exists* b0{1}, BW.sk{2}; elim* => bx skx. 
      call{1} (validInd_dec skx (get_pk skx) bx _); 
        first by done.      
      auto=>/>; progress. 
      + smt. smt.
      + move: H9 H10; rewrite ?mem_cat //=; move => H9 H10.
        smt.
    (* H, G, J *)
    + by proc.     
    + by proc.
    + by proc.
    + by proc.
    by auto => />; progress; smt. 
    
  swap{1} [1..3] 11.
  seq 11 11: (={dL, glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
      glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, BW.uL,
      BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi'} /\
  bool{2} /\
  (let fd = fun (x : ident * upkey * cipher * sign) =>
                dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
     forall (y : ident * vote * upkey * cipher * sign),
       y \in BW.hvote{2} => fd (y.`1, y.`3, y.`4, y.`5)) /\
  (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
  (forall (x : ident * upkey * cipher * sign),
     x \in map remv BW.check{2} =>
     exists (id0 : ident), (id0, x.`2, x.`3, x.`4) \in Sanitize' BW.bb{2}) /\
  (forall (x : ident * vote * upkey * cipher * sign),
     x \in BW.check{2} => x \in BW.hvote{2}) /\
  (let f =
     (fun (x : ident * upkey * cipher * sign) =>
        oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) in
   (forall (x : ident * vote * upkey * cipher * sign),
      x \in BW.hvote{2} => x.`2 = f (remv x))) /\
  uniq (map (rem_id \o remv) BW.check{2})/\
  vbb{2} = BW.bb{2} /\
  (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
     while (={j, dL, glob E, glob HRO.RO, hon_mem, val_bb, BW.sk});
      first by sim.
     wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, hCN, BW.sk});
      first by sim.
     wp; while (={vbb, glob E, glob SS, HRO.RO.m, JRO.RO.m, BW.bb, i, BW.sk}/\
                0<=i{2} <= size BW.bb{2}/\
            let f = fun (x: ident * upkey * cipher * sign),
                        dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).
    wp; sp. 
    exists* (glob SS){1}, (glob E){1}, BW.sk{1}, b{1}; elim* => gs ge skx bx.
    call{1} (ver_Ver_two bx.`2 bx.`3 bx.`4).
    call{1} (dec_Dec_two skx bx.`2 bx.`3).
    auto; progress.
    + smt. smt. smt. smt. smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons H3.      
    
    auto; progress. 
    + smt. smt (take0). 
    + rewrite take_oversize; first by smt. 
      rewrite -all_filterP allP.
      smt.
    

  seq 1 3: (={dL,r, ev, bool, BW.hvote, BW.qCo, BW.bb, BW.check} /\
            perm_eq hC{1} hC{2}).
    wp; while{2} ( 0 <= j{2} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id (Sanitize' vbb{2})) in
                   hC{2} = map g (take j{2} xbb))
                 (size (filter (mem (map (rem_id \o remv) BW.check{2})) 
                               (map rem_id (Sanitize' vbb{2}))) - j{2}); progress.
      wp; sp.
      exists* (glob E), BW.sk, c; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
      + smt.
    auto =>/>; progress.
    + smt. smt.
    + pose g:= fun (x : upkey * cipher * sign) =>
              oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}).
      pose fd:= (fun (x : ident * upkey * cipher * sign) =>
              (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
              ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2})).
      move: H5; rewrite -/fd; move => Hsize.
      rewrite take_oversize. smt.
      cut := nth_vote' BW.check{2} (fun (x : ident * upkey * cipher * sign) =>
                                        oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) _ .
      + move => x HxmC. simplify. 
        cut := H4 x _; first by rewrite (H3 x HxmC). 
        by done.
      move => Heq.
      rewrite -Heq. print nth_vote.
      cut Hc:= nth_vote (map remv BW.check{2}) BW.check{2} 
                        (fun (x : ident * upkey * cipher * sign) =>
                                        oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) _ _.
      + done.
      + move => x HxmC.
        cut := H4 x _; first by rewrite (H3 x HxmC). 
        by done.
      rewrite Hc.
      rewrite -map_comp.
      have ->: ((fun (x : ident * upkey * cipher * sign) =>
                oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) \o
                remv) = (g \o rem_id \o remv).
        by rewrite  /g /rem_id /remv /(\o) //=.       
      pose S:= (Sanitize' BW.bb{2}).
      pose L:= map (rem_id \o remv) BW.check{2}.
      have ->: (map (g \o rem_id \o remv) BW.check{2}) = map g L 
         by rewrite /L ?map_comp.
      have Hp: perm_eq L (filter (mem L) (map rem_id S)).
      have Hcheck: forall x, x \in L =>
                   x \in (map rem_id S).
         rewrite /L. 
         move => x. rewrite map_comp mapP.
         elim => y HymC. 
         cut := H2 y _; first by rewrite (andWl _ _ HymC).
         rewrite /S mapP.
         elim => id HmS.
         exists (id, y.`2, y.`3, y.`4).
         by rewrite HmS (andWr _ _ HymC) /rem_id //=. 
      cut Hperm:= uniq_perm_eq_filter_two L (map rem_id S) _ Hsize Hcheck. 
       + rewrite /S / Sanitize' -map_comp.  print uniq_snd_san. 
         cut := uniq_snd_san (Sanitize (map pack3 BW.bb{2})) _ _.
         + move => x y HxS HyS Hxy.
           search Sanitize mem.
           have Hx: open3 x \in BW.bb{2}. 
             cut := San_mem' (map pack3 BW.bb{2}) x HxS.
             rewrite mapP. 
             elim => z [HzB Hzx]. 
             by rewrite Hzx -open_pack_3 HzB. 
           have Hy: open3 y \in BW.bb{2}. 
             cut := San_mem' (map pack3 BW.bb{2}) y HyS.
             rewrite mapP. 
             elim => z [HzB Hzy]. 
             by rewrite Hzy -open_pack_3 HzB. 
           cut := H6 (open3 x) (open3 y) Hx Hy _; first by rewrite /open3 //= Hxy.
           by rewrite /open3.
         + by rewrite San_uniq_fst'. 
         have ->: snd = (rem_id \o open3). 
            by rewrite /(\o) /rem_id /open3 //= fun_ext /(==); move => x; smt.
         by done.   

      by rewrite Hperm.       
      by rewrite (perm_eq_map _ _ _ Hp). 

  auto =>/>; progress. 
  cut := H1 X L.
  by smt.
qed.    

module DREG_Exp_hvote (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  hC, hNC, hN, r', 
        pi', ev', vbb, b,m,e, j, c,dL, hon_mem, val_bb, hCN;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    
    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb <- map (fun (x: ident * upkey * cipher * sign), 
                       (get_pub_data (preha x), hash' BW.hk x)) 
                       (Sanitize' BW.bb);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id (Sanitize' vbb));
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }

    hC <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.check)) 
                            (map rem_id (Sanitize' vbb)))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.check)) 
                               (map rem_id (Sanitize' vbb))) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hC<- hC ++ [oget m];
      j <- j + 1;
    }

    (* votes of honest voters that didn't check *)
    hNC<- filter (predC (mem (map (rem_id \o remv) BW.check))) 
                 (map (rem_id \o remv) BW.hvote);
    j <- 0;
    hN <- [];
    while (j < size hNC){
      c <- nth witness hNC j;
      m <@ E(H).dec (BW.sk, c.`1,c.`2);
      hN <- hN ++ [oget m];
      j <- j + 1;
    }
    bool <- (forall (L: vote list),
             ! subseq L hN \/ 
             r <> Count (hC ++ L ++ dL));
    
    return ev /\ bool /\size dL <= BW.qCo  ;
  }
}.


local lemma subseq_perm_exists['a] (A B a: 'a list):
  perm_eq A B =>
  subseq a A =>
  exists b, perm_eq a b => subseq b B.
proof.
  move => HpAB HsaA.
  smt.
qed. 
  

          
local lemma dbbcheck_two_dbbhvote &m:
  Pr[DREG_Exp_check_two (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DREG_Exp_hvote (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  
  seq 17 17: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
                glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
                BW.uL, BW.qCo, BW.pk, BW.sk, bool, BW.bad, glob C, BW.hk}/\ 
              BW.bb{1} =[] /\ 
              BW.coL{1} =[] /\ 
              BW.hvote{1} =[] /\
              BW.check{1} =[]/\
              BW.qVo{1} = 0/\
              bool{2} /\ BW.pk{2} = get_pk BW.sk{2}). 
    (* call A.a1: credentials *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
      first 3 by sim.
    inline B'(E, Pz, Vz, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt.

  seq 6 6: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
              glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
              BW.uL, BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi'}/\ 
            bool{2}/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
             forall y, y \in BW.bb{2} => fd y)/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.check{2})/\
             uniq (map (rem_id \o remv) BW.hvote{2})).
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: ={glob E, glob SS, glob Pz, 
             glob JRO.RO, glob HRO.RO, glob GRO.RO}); first by sim.
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    wp.
    (* A.a3 call *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.hk, 
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
           (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb, BW.hk}/\
             0 <= i{2} /\
             (* uniq (map id BW.check) *)
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in BW.check{2} => 
                        x \in BW.hvote{2}) /\
             BW.pbb{2} = map (fun (x: ident * upkey * cipher * sign), 
                                  (get_pub_data (preha x), hash' BW.hk{2} x)) 
                             (Sanitize' BW.bb{2})/\
             (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
        auto =>/>; progress.
        + smt. 
        + move: H6; rewrite mem_cat mem_seq1; move => Hmor.
          case (x \in BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.   
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt. 
            rewrite -Hx. 
            move => HmS. 
            cut := San_mem (map pack4 BW.hvote{2}) (pack4 x) _. 
              + move: HmS; rewrite mapP.
                elim=> y HymS.     
                rewrite (andWr _ _ HymS).       
                have ->: pack4 (open4 y) = y by rewrite /open4 /pack4; smt.        
                rewrite (andWl _ _ HymS).
            rewrite mapP //=; elim => y HymS.
            have ->: x = y by  cut:= (andWr _ _ HymS); rewrite /pack4 ; smt.
            rewrite (andWl _ _ HymS). 
        + rewrite map_cat //=.  
          rewrite cats1 rcons_uniq (H1 H6) //=.
          move: H3 H4 H5 H6;
            pose L:= map open4 (Sanitize (map pack4 BW.hvote{2}));
            pose x:= nth witness L i{2}.
          move => H3 H4 H5 H6.      
          rewrite mapP negb_exists //=.
          move => a. rewrite negb_and.           
          case (!a \in BW.check{2}). smt.
          simplify. move => HmaC. 
          have HmaH: a \in BW.hvote{2} by rewrite H0.
          rewrite /(\o) /rem_id /remv //=.  
          have HmxH: x \in BW.hvote{2}. 
            rewrite /x /L. search mem Sanitize. 
            cut := San_mem (map pack4 BW.hvote{2}) 
                           (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) _. smt.
            rewrite mapP.
            elim => y Hymem. 
            have ->: nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} = 
                      open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}). smt.
            have ->: open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) = y. 
              by rewrite (andWr _ _ Hymem) /open4 /pack4 //=; smt.
            rewrite (andWl _ _ Hymem).
          
          move: H5. rewrite mapP negb_exists /fst5 //=.
          move => H5.
          cut := H5 x. rewrite //=.
          move => HmxC. rewrite ?andabP. 
            cut := uniq_nomem_map (* Hmust *) (map remv BW.hvote{2}) (remv x) (remv a) rem_id _ _ _ _.
             + rewrite -map_comp H6. 
             + rewrite mapP. exists x. rewrite HmxH //=.
             + rewrite mapP. exists a. rewrite HmaH //=.
             + smt.
            by rewrite /rem_id /remv //= ?andabP.
        + smt.
        auto=>/>; smt.

    + by proc. by proc. by proc. by proc.
    (* A.a2 call *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo, glob C}/\
             BW.pk{2} = get_pk BW.sk{2}/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                           (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                           ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
             (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.hvote{2})/\
             (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
    + proc. 
      auto=>/>; progress. smt. smt. smt. smt. 

    + proc. 
      sp; if =>//=.
      exists * BW.sk{2}; elim* => skx.
      inline*; sp; wp.
      seq 1 1: (id0{2} = id{2} /\
             v0{2} = v{2} /\
             pk{2} = BW.pk{2} /\
             usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
             upk{2} = get_upk usk{2} /\
             id0{1} = id{1} /\
             v0{1} = v{1} /\
             pk{1} = BW.pk{1} /\
             usk{1} = (oget BW.uL{1}.[id{1}]).`2 /\
             upk{1} = get_upk usk{1} /\
             skx = BW.sk{2} /\
             o{2} = None /\
             o{1} = None /\
             ={id, v, GRO.RO.m, HRO.RO.m, JRO.RO.m, BW.qVo, 
               glob SS, glob E, BW.bb, BW.pk, glob C,
               BW.hvote, BW.coL, BW.uL, BW.qCo} /\
             BW.pk{2} = get_pk BW.sk{2} /\
             (forall (y : ident * vote * upkey * cipher * sign),
               y \in BW.hvote{2} =>
               dec_cipher BW.sk{2} y.`3 y.`4 HRO.RO.m{2} <> None /\
               ver_sign y.`3 y.`4 y.`5 JRO.RO.m{2}) /\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
             (forall (x : ident * vote * upkey * cipher * sign),
               x \in BW.hvote{2} =>
               x.`2 = oget (dec_cipher BW.sk{2} (remv x).`2 (remv x).`3 
                      HRO.RO.m{2})) /\
             uniq (map (rem_id \o remv) BW.hvote{2}) /\
             BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\ 
             BW.qVo{1} < qVo /\ ={c} /\
             dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2} <> None /\
             v0{2} = oget (dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2})/\
             (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
        exists* v0{1}, upk{2};  elim* => vx upkx. 
        call{1} (Enc_dec_two skx (get_pk skx) upkx vx _); first by done.
        by auto; progress; smt. 
      exists* usk{1}, c{1}; elim*=> uskx cx.
      call{1} (Sig_ver_two uskx (get_upk uskx) cx _ );
        first by done.
      auto=>/>; progress. 
      + smt. smt. smt. smt. smt. 
      + rewrite map_cat //=. 
        have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H9. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt. 
      + move: H11 H12; rewrite ?mem_cat. 
        move => H11 H12. smt.
      + smt. smt. smt. 
      + rewrite map_cat //=. 
        have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H9. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt.
      + smt. smt.
      + move: H11 H12; rewrite ?mem_cat //=; move => H11 H12.
        smt.
    + proc. 
      if =>//=.
      inline*; wp; sp. 
      exists* b0{1}, BW.sk{2}; elim* => bx skx. 
      call{1} (validInd_dec skx (get_pk skx) bx _); 
        first by done.      
      auto=>/>; progress. 
      + smt. smt.
      + move: H9 H10; rewrite ?mem_cat //=; move => H9 H10.
        smt.
    (* H, G, J *)
    + by proc.     
    + by proc.
    + by proc.
    + by proc.
    by auto => />; progress; smt. 

  seq 14 14: (={dL,bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb,  hC} /\ 
            bool{2}/\
            (let f = fun (x : ident * upkey * cipher * sign) =>
                     dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                      ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = BW.bb{2} /\
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})).

    inline *; wp. 
    while( ={glob HRO.RO, glob JRO.RO, glob E, glob SS, BW.sk, 
             BW.check, vbb, hC, j}/\
            0 <= j{2} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id (Sanitize' vbb{2})) in
                   hC{2} = map g (take j{2} xbb)).
                 
      wp; sp.
      exists* (glob E){1}, BW.sk{1}, c{1}; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      call{2} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
    wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, BW.sk, dL});
      first by sim.
    wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, BW.sk, hCN});
      first by sim.
    wp; while ( ={ i, BW.bb, BW.sk, glob E, glob SS, glob HRO.RO, glob JRO.RO, vbb}/\
            0<=i{2} <= size BW.bb{2}/\
            let f = fun (x: ident * upkey * cipher * sign),
                        dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).
      wp; sp. 
      exists* (glob SS){1}, (glob E){1}, BW.sk{1}, b{1}; elim* => gs ge skx bx.
      call{1} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{2} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{1} (dec_Dec_one ge skx bx.`2 bx.`3).
      call{2} (dec_Dec_one ge skx bx.`2 bx.`3).
      auto=>/>; progress.
      + smt. smt. smt. smt. smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite filter_rcons H2. 
    auto=>/>; progress. 
    + smt. smt. smt. 
    + rewrite take_oversize; first by smt.
      rewrite -all_filterP allP. 
      smt.

  seq 2 4: ( ={dL,ev, bool, hC, BW.qCo, r}/\ 
             perm_eq hN{1} hN{2}). 

     
  wp; while{2} (0 <= j{2} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   hN{2} = map g (take j{2} hNC{2}))
               (size hNC{2} - j{2})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.            
  auto=>/>; progress.
  + smt. + smt.
  + pose f:= fun (x : ident * upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}).
    pose g:= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}).
    move: H5; rewrite -/g; move => Hg.
    have H3': forall y, y \in BW.hvote{2} => y.`2 = f (remv y). smt.

    have ->: (take j_R (filter (predC (mem (map (rem_id \o remv) BW.check{2})))
           (map (rem_id \o remv) BW.hvote{2}))) =
             (filter (predC (mem (map (rem_id \o remv) BW.check{2})))
           (map (rem_id \o remv) BW.hvote{2})) by smt.
    
    cut Hk:= nth_vote' (filter (predC (mem BW.check{2})) BW.hvote{2}) f _.
    + move => x Hxm.
      smt.
    rewrite -Hk. 
    cut Hc:= nth_vote (map remv (filter (predC (mem BW.check{2})) BW.hvote{2}))
                       (filter (predC (mem BW.check{2})) BW.hvote{2}) f _ _.
    + by done.
    + move => x Hxm.
      smt.
    rewrite Hc.
    have ->: f = g \o rem_id by rewrite /(\o) /f /g /rem_id.
    rewrite filter_map -?map_comp. 
    have ->: preim (rem_id \o remv) 
                     (predC (mem (map (rem_id \o remv) BW.check{2}))) = 
               (predC (mem (map (rem_id \o remv) BW.check{2}))) \o rem_id \o remv .
        by rewrite /preim.
    rewrite (perm_eq_map (g \o rem_id \o remv)).
    pose riv:= (rem_id \o remv).
    have ->: (predC (mem (map riv BW.check{2})) \o rem_id \o remv) =
             (predC (mem (map riv BW.check{2})) \o riv) by rewrite /riv.
    have eq_filter_mem: forall (A : (ident * vote * upkey * cipher * sign) list) p1 p2,
               (forall x, x\in A => (p1 x <=> p2 x)) => filter p1 A = filter p2 A
               by move => A p1 p2; elim: A => //= x l Ha; smt.      
    rewrite (eq_filter_mem BW.hvote{2} (predC (mem (map riv BW.check{2})) \o riv) 
                                           (predC (mem BW.check{2})) _ ).
          move => x HxmH.
          progress. 
          + move: H5. rewrite /(\o) /predC. 
            rewrite mapP negb_exists //=. 
            by move => Hac; cut := Hac x; rewrite //=.
          - move: H5. rewrite /(\o) /predC.
            move => Hpc.
            rewrite mapP negb_exists //=.
            move => a.
            rewrite negb_and.
            case (riv x <> riv a). done.
            simplify. move => Heq_xa.
            move: H4; rewrite -/riv; move => Hu4.
            case (a = x).
            + move => Heq. 
              by rewrite Heq Hpc. 
            move => Hneq.
            case (a \in BW.hvote{2}).          
            + move => HmaH. 
              cut Heq:= uniq_mem_map BW.hvote{2} a x riv Hu4 HmaH HxmH _ ; first by rewrite Heq_xa.
              by rewrite Heq Hpc.
            move => Hnm. 
            cut Himp:= H2 x.  
            by smt.
          by rewrite perm_eq_refl.

   auto=>/>; progress. 
     cut := H1 hN{2} L. 
     by rewrite perm_eq_sym H.
qed.      


module DREG_Exp_hvote_two (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd, 
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  hC, hN, r', 
        pi', ev', vbb, b,m,e, j, c, dL, hon_mem, val_bb;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL <@ A(O).a1(BW.sk);
             A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb <- map (fun (x: ident * upkey * cipher * sign), 
                       (get_pub_data (preha x), hash' BW.hk x)) 
                       (Sanitize' BW.bb);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hC <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.check)) 
                            (map rem_id (Sanitize' vbb)))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.check)) 
                               (map rem_id (Sanitize' vbb))) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hC<- hC ++ [oget m];
      j <- j + 1;
    }
   
    hN <- [];
    j <- 0;
    while (j < size (filter (predI (predC (mem (map (rem_id \o remv) BW.check)))
                                   (mem (map (rem_id \o remv) BW.hvote)))
                            (map rem_id (Sanitize' vbb)))
          ){
      c <- nth witness (filter (predI (predC (mem (map (rem_id \o remv) BW.check)))
                                      (mem (map (rem_id \o remv) BW.hvote)))
                               (map rem_id (Sanitize' vbb))) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hN<- hN ++ [oget m];
      j <- j + 1;
    }
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id (Sanitize' vbb));

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    bool <- r <> Count (hC ++ hN ++ dL);

    return ev /\ bool /\ size dL <= BW.qCo ;
  }
}.

local lemma exists_two_elem['a] (X : 'a list) c b:
  c \in X => b \in X =>
  index c X < index b X =>
  exists A B C, X = A ++ [c] ++ B ++ [b] ++ C.
proof.
  move => Hma Hmb Hiab.
  cut Hia:= nth_index witness c X Hma.
  cut Hib:= nth_index witness b X Hmb.
  cut := take_nth witness (index c X) X _; 
    first by smt.
  rewrite -cats1 Hia.
  move => HA. 
  exists (take (index c X) X).
  rewrite -HA.
  cut := take_nth witness (index b X) X _ . smt.
  rewrite -cats1 Hib.
  move => HB. 
  exists (drop (index c X +1) (take (index b X) X)). 
  have ->: take (index c X + 1) X ++ drop (index c X + 1) (take (index b X) X) ++ [b] = 
           take (index b X + 1) X.
    rewrite HB.
    have ->: take (index c X + 1) X = take (index c X + 1) (take (index b X) X). 
      cut := cat_take_drop (index b X) X.
      by smt. 
    by smt.
  exists (drop (index b X +1) X).
  by rewrite cat_take_drop.
qed.

local lemma index_diff['a] (X: 'a list) c b:
  c \in X => b \in X => c <> b =>
  index c X <> index b X.
proof. smt. qed.


local lemma dbbhvote_dbbhvote_two &m:
  Pr[DREG_Exp_hvote (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DREG_Exp_hvote_two (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  
  seq 17 17: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob C, glob HRO.RO, BW.qVo,
                glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
                BW.uL, BW.qCo, BW.pk, BW.sk, bool, BW.bad, BW.uLL, BW.hk}/\ 
              BW.bb{1} =[] /\ 
              BW.coL{1} =[] /\ 
              BW.hvote{1} =[] /\
              BW.check{1} =[]/\
              BW.qVo{1} = 0/\
              bool{2} /\ BW.pk{2} = get_pk BW.sk{2}). 
    (* call A.a1: credentials *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
      first 3 by sim.
    inline B'(E, Pz, Vz, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt.


  seq 6 6: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
              glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
              BW.uL, BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi'}/\ 
            bool{2}/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
             forall y, y \in BW.bb{2} => fd y)/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.check{2})/\
             uniq (map (rem_id \o remv) BW.hvote{2})/\
            (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: ={glob E, glob SS, glob Pz, 
             glob JRO.RO, glob HRO.RO, glob GRO.RO}); first by sim.
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    wp.
    (* A.a3 call *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO,  BW.hk,
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
           (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb, BW.hk}/\
             0 <= i{2} /\
             (* uniq (map id BW.check) *)
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in BW.check{2} => 
                        x \in BW.hvote{2}) /\
             BW.pbb{2} = map (fun (x: ident * upkey * cipher * sign), 
                                  (get_pub_data (preha x), hash' BW.hk{2} x)) 
                             (Sanitize' BW.bb{2})/\
             (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
        auto =>/>; progress.
        + smt. 
        + move: H6; rewrite mem_cat mem_seq1; move => Hmor.
          case (x \in BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.   
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt. 
            rewrite -Hx. 
            move => HmS. 
            cut := San_mem (map pack4 BW.hvote{2}) (pack4 x) _. 
              + move: HmS; rewrite mapP.
                elim=> y HymS.     
                rewrite (andWr _ _ HymS).       
                have ->: pack4 (open4 y) = y by rewrite /open4 /pack4; smt.        
                rewrite (andWl _ _ HymS).
            rewrite mapP //=; elim => y HymS.
            have ->: x = y by  cut:= (andWr _ _ HymS); rewrite /pack4 ; smt.
            rewrite (andWl _ _ HymS). 
        + rewrite map_cat //=.  
          rewrite cats1 rcons_uniq (H1 H6) //=.
          move: H3 H4 H5 H6;
            pose L:= map open4 (Sanitize (map pack4 BW.hvote{2}));
            pose x:= nth witness L i{2}.
          move => H3 H4 H5 H6.      
          rewrite mapP negb_exists //=.
          move => a. rewrite negb_and.           
          case (!a \in BW.check{2}). smt.
          simplify. move => HmaC. 
          have HmaH: a \in BW.hvote{2} by rewrite H0.
          rewrite /(\o) /rem_id /remv //=.  
          have HmxH: x \in BW.hvote{2}. 
            rewrite /x /L. search mem Sanitize. 
            cut := San_mem (map pack4 BW.hvote{2}) 
                           (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) _. smt.
            rewrite mapP.
            elim => y Hymem. 
            have ->: nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} = 
                      open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}). smt.
            have ->: open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) = y. 
              by rewrite (andWr _ _ Hymem) /open4 /pack4 //=; smt.
            rewrite (andWl _ _ Hymem).
          
          move: H5. rewrite mapP negb_exists /fst5 //=.
          move => H5.
          cut := H5 x. rewrite //=.
          move => HmxC. rewrite ?andabP. 
            cut := uniq_nomem_map (* Hmust *) (map remv BW.hvote{2}) (remv x) (remv a) rem_id _ _ _ _.
             + rewrite -map_comp H6. 
             + rewrite mapP. exists x. rewrite HmxH //=.
             + rewrite mapP. exists a. rewrite HmaH //=.
             + smt.
            by rewrite /rem_id /remv //= ?andabP.
        + smt.
        auto=>/>; smt.

    + by proc. by proc. by proc. by proc.
    (* A.a2 call *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo, glob C}/\
             BW.pk{2} = get_pk BW.sk{2}/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                           (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                           ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
             (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.hvote{2})/\
             (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
    + proc. 
      auto=>/>; progress. smt. smt. smt. smt. 

    + proc. 
      sp; if =>//=.
      exists * BW.sk{2}; elim* => skx.
      inline*; sp; wp.
      seq 1 1: (id0{2} = id{2} /\
             v0{2} = v{2} /\
             pk{2} = BW.pk{2} /\
             usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
             upk{2} = get_upk usk{2} /\
             id0{1} = id{1} /\
             v0{1} = v{1} /\
             pk{1} = BW.pk{1} /\
             usk{1} = (oget BW.uL{1}.[id{1}]).`2 /\
             upk{1} = get_upk usk{1} /\
             skx = BW.sk{2} /\
             o{2} = None /\
             o{1} = None /\
             ={id, v, GRO.RO.m, HRO.RO.m, JRO.RO.m, BW.qVo, 
               glob SS, glob E, BW.bb, BW.pk, glob C,
               BW.hvote, BW.coL, BW.uL, BW.qCo} /\
             BW.pk{2} = get_pk BW.sk{2} /\
             (forall (y : ident * vote * upkey * cipher * sign),
               y \in BW.hvote{2} =>
               dec_cipher BW.sk{2} y.`3 y.`4 HRO.RO.m{2} <> None /\
               ver_sign y.`3 y.`4 y.`5 JRO.RO.m{2}) /\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in
             forall y, y \in BW.bb{2} => fd y)/\
             (forall (x : ident * vote * upkey * cipher * sign),
               x \in BW.hvote{2} =>
               x.`2 = oget (dec_cipher BW.sk{2} (remv x).`2 (remv x).`3 
                      HRO.RO.m{2})) /\
             uniq (map (rem_id \o remv) BW.hvote{2}) /\
             BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\ 
             BW.qVo{1} < qVo /\ ={c} /\
             dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2} <> None /\
             v0{2} = oget (dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2})/\
             (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
        exists* v0{1}, upk{2};  elim* => vx upkx. 
        call{1} (Enc_dec_two skx (get_pk skx) upkx vx _); first by done.
        by auto; progress; smt. 
      exists* usk{1}, c{1}; elim*=> uskx cx.
      call{1} (Sig_ver_two uskx (get_upk uskx) cx _ );
        first by done.
      auto=>/>; progress.
      + rewrite mem_cat mem_seq1 in H11; smt. 
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite map_cat //=. 
        have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H9. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt. 
      + move: H11 H12; rewrite ?mem_cat. 
        move => H11 H12. smt.
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite mem_cat mem_seq1 in H11; smt.
      + rewrite map_cat //=. 
        have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H9. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt.
      + rewrite mem_cat mem_seq1 in H11; smt. 
      + rewrite mem_cat mem_seq1 in H11; smt.
      + move: H11 H12; rewrite ?mem_cat //=; move => H11 H12.
        smt.
    + proc. 
      if =>//=.
      inline*; wp; sp. 
      exists* b0{1}, BW.sk{2}; elim* => bx skx. 
      call{1} (validInd_dec skx (get_pk skx) bx _); 
        first by done.      
      auto=>/>; progress. 
      + smt. smt.
      + move: H9 H10; rewrite ?mem_cat //=; move => H9 H10.
        smt.
    (* H, G, J *)
    + by proc.     
    + by proc.
    + by proc.
    + by proc.
    by auto => />; progress; smt. 
      
  seq 5 3: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
              vbb, BW.sk, HRO.RO.m, glob E} /\ 
            bool{2}/\ vbb{2} = BW.bb{2} /\
             (*(forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in  Sanitize' BW.bb{2})/\ *)
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2})  in
              (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id (Sanitize' vbb){1} /\
            (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
     wp; while ( ={ i, BW.bb, BW.sk, glob E, glob SS, glob HRO.RO, glob JRO.RO, vbb}/\
            0<=i{2} <= size BW.bb{2}/\
            let f = fun (x: ident * upkey * cipher * sign),
                        dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).
      wp; sp. 
      exists* (glob SS){1}, (glob E){1}, BW.sk{1}, b{1}; elim* => gs ge skx bx.
      call{1} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{2} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{1} (dec_Dec_one ge skx bx.`2 bx.`3).
      call{2} (dec_Dec_one ge skx bx.`2 bx.`3).
      auto=>/>; progress.
      + smt. smt. smt. smt. smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite filter_rcons H2.      
     auto=>/>; progress. 
     + smt. smt.  
     + rewrite take_oversize; first by smt.
       rewrite -all_filterP allP.
       smt. 
     

seq 3 0: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
            vbb, BW.sk, HRO.RO.m, glob E} /\ 
            bool{2}/\
           (* (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in  Sanitize' BW.bb{2})/\ *)
            vbb{2} = BW.bb{2} /\
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}in
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id (Sanitize' vbb){1}/\
            (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
   exists*  (glob E){1}; elim* => ge.
   while{1} (ge =(glob E){1}) (size (filter hon_mem val_bb){1} - j{1}); progress.
      sp; exists* BW.sk, c; elim* => skx bx.
      wp; call{1} (dec_Dec_one ge skx bx.`1 bx.`2).
      auto=>/>;  smt.
   auto=>/>; progress. smt. 

seq 3 0: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
            vbb, BW.sk, HRO.RO.m, glob E} /\ 
            bool{2}/\ vbb{2} = BW.bb{2} /\
           (* (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in  Sanitize' BW.bb{2})/\ *)
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id (Sanitize' vbb){1}/\
            dL{1} = map (fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) 
                        (filter (predC hon_mem) val_bb){1} /\
            (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
   exists* (glob E){1}; elim* => ge.
   wp; while{1} (={HRO.RO.m, BW.sk}/\ (glob E){1}=ge/\
                 0<=j{1}<= size (filter (predC hon_mem) val_bb){1} /\
                 (let g= fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in 
                  dL{1} = map g (take j{1} (filter (predC hon_mem) val_bb){1})))
                 (size (filter (predC hon_mem) val_bb){1} - j{1}); progress.
     sp; exists* BW.sk, c; elim* => skx bx.
     wp; call{1} (dec_Dec_one ge skx bx.`1 bx.`2).
     auto=>/>; progress.  
     + smt. smt. 
     + rewrite (take_nth witness). smt. 
       smt.
     + smt.
   by auto=>/>; progress; smt.
seq 3 3: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
            vbb, BW.sk, HRO.RO.m, glob E, hC} /\ 
            bool{2}/\ vbb{2} = BW.bb{2} /\
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id (Sanitize' vbb){1}/\
            dL{1} = map (fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) 
                        (filter (predC hon_mem) val_bb){1} /\
            (forall x y, x \in BW.bb{2} =>
                         y \in BW.bb{2} => x.`2 = y.`2 => x.`1 = y.`1)).
   while( ={glob HRO.RO, glob E, BW.sk, 
             BW.check, vbb, hC, j}/\
            0 <= j{2} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id (Sanitize' vbb{2})) in
                   hC{2} = map g (take j{2} xbb)).
                 
      wp; sp.
      exists* (glob E){1}, BW.sk{1}, c{1}; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      call{2} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
   auto=>/>; progress; smt. 

  seq 4 5: ( ={ev, bool, hC, BW.qCo, r} /\  
             (exists X, perm_eq X hN{2} /\ subseq X hN{1})/\
            dL{1} = map (fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) 
                        (filter (predC hon_mem) val_bb){2}
             (* hN{1} = hN{2}*)). 
    
  wp; while{2} (0 <= j{2} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                let xbb = filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                         (mem (map (rem_id \o remv) BW.hvote{2})))
                                 (map rem_id (Sanitize' vbb{2})) in
                   hN{2} = map g (take j{2} xbb))
               (size (filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                   (mem (map (rem_id \o remv) BW.hvote{2})))
                            (map rem_id (Sanitize' vbb{2}))) - j{2})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.     
  wp; while{1} ( ={BW.sk, HRO.RO.m} /\ 0 <= j{1} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   hN{1} = map g (take j{1} hNC{1}))
               (size hNC{1} - j{1})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.            

  auto=>/>; progress. 
  + by rewrite take0. 
  + smt. 
  + by rewrite take0. 
  + smt. 
  pose fd:= (fun (x : ident * upkey * cipher * sign) =>
              (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
              ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2})).
    pose f:= fun (x : ident * upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}).
    pose g:= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}).
    pose pc:= predC (mem (map (rem_id \o remv) BW.check{2})).
    pose mH:= (map (rem_id \o remv) BW.hvote{2}).
  
    move: H8 H6; rewrite -/fd -/pc -/mH -/g; move => H8 H6.
    have H1': forall y, y \in BW.hvote{2} => fd (remv y). smt.
    have H3': forall y, y \in BW.hvote{2} => y.`2 = (g \o rem_id \o remv) y. 
        by rewrite /g /rem_id /remv /(\o) //=. 
    rewrite !take_oversize; first 2 by smt.
    pose BB:= (map rem_id (Sanitize' BW.bb{2})). 
    (* is uniq, because every (u,c,s) has a uniq u set by USanitize *)
    (* rewrite /p. *)

    cut := uniq_perm_eq_filter_two (filter (mem BB) (filter pc mH)) BB _ _ _.
    + rewrite /BB /Sanitize' -map_comp.
      cut := uniq_snd_san (Sanitize (map pack3 BW.bb{2})) _ _.
         + move => x y HxS HyS Hxy.
           search Sanitize mem.
           have Hx: open3 x \in BW.bb{2}. 
             cut := San_mem' (map pack3 BW.bb{2}) x HxS.
             rewrite mapP. 
             elim => z [HzB Hzx]. 
             by rewrite Hzx -open_pack_3 HzB. 
           have Hy: open3 y \in BW.bb{2}. 
             cut := San_mem' (map pack3 BW.bb{2}) y HyS.
             rewrite mapP. 
             elim => z [HzB Hzy]. 
             by rewrite Hzy -open_pack_3 HzB. 
           cut := H5 (open3 x) (open3 y) Hx Hy _; first by rewrite /open3 //= Hxy.
           by rewrite /open3.
         + by rewrite San_uniq_fst'. 
         have ->: snd = (rem_id \o open3). 
            by rewrite /(\o) /rem_id /open3 //= fun_ext /(==); move => x; smt.
         by done.  
      
    + by rewrite !filter_uniq H4.
    + move => b.
      by rewrite mem_filter. 
    have ->: (filter (mem (filter (mem BB) (filter pc mH))) BB) = 
             filter (predI pc (mem mH)) BB. 
      have eq_filter_mem: forall (A : (upkey * cipher * sign) list) p1 p2,
            (forall x, x\in A => (p1 x <=> p2 x)) => filter p1 A = filter p2 A.
         move => A p1 p2. 
         elim: A => //= x l Hp. 
         by smt. 
      have ->: forall (A B : (upkey * cipher * sign) list),
               filter (mem (filter (mem A) B)) A = filter (mem B) A. 
      + move => A B.
        rewrite (eq_filter_mem A (mem (filter (mem A) B)) (mem B) _ ).
        + move => x HxA. 
          by smt.
        by done.
      - rewrite (eq_filter (mem (filter pc mH)) (predI pc (mem mH)) BB).
        + move => x. 
          by smt.
        by done.
    move => Hp. 
    have Hpp:= filter_subseq (mem BB) (filter pc mH).
    exists (map g (filter (mem BB) (filter pc mH))).
    rewrite perm_eq_map 1: Hp.
    by rewrite Hsub_map 1: Hpp.

wp; while{2} (0<=j{2}<= size (filter (predC hon_mem) val_bb){2} /\
                 (let g= fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in 
                  dL{2} = map g (take j{2} (filter (predC hon_mem) val_bb){2})))
                 (size (filter (predC hon_mem) val_bb){2} - j{2}); progress.
     sp; exists*  (glob E), BW.sk, c; elim* => ge skx bx.
     wp; call{1} (dec_Dec_one ge skx bx.`1 bx.`2).
     auto=>/>; progress.  
     + smt. smt. 
     + rewrite (take_nth witness). smt. 
       smt.
     + smt.
auto=>/>; progress. 
  smt. smt. smt.
  rewrite take_oversize; first by smt.
  cut := H5 X. rewrite H0 //= ?Count_split. smt.
  smt. 
qed. 


(*  *)
module DREG_Exp_hvote_three (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  hCN, r', 
        pi', ev', vbb, b,m,e, j, c, hon_mem, val_bb, dL;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL  <@ A(O).a1(BW.sk);
             A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb <- map (fun (x: ident * upkey * cipher * sign), 
                       (get_pub_data (preha x), hash' BW.hk x)) 
                       (Sanitize' BW.bb);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hCN <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.hvote)) 
                            (map rem_id (Sanitize' vbb)))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.hvote)) 
                               (map rem_id (Sanitize' vbb))) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id (Sanitize' vbb));

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    bool <- r <> Count (hCN ++ dL);
    
    return ev /\ bool /\ size dL <= BW.qCo ;
  }
}.


local lemma dbbhvotetwo_dbbhvotethree &m:
  Pr[DREG_Exp_hvote_two (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] =
  Pr[DREG_Exp_hvote_three (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  seq 26 26 : ( ={glob E, glob HRO.RO, BW.check, BW.hvote, BW.qCo, 
                 BW.sk, vbb, ev, bool, r, BW.uLL} /\
                (forall x, x \in map (rem_id \o remv) BW.check{2} =>
                           x \in map (rem_id \o remv) BW.hvote{2})).
  while (={i, BW.bb, BW.sk, vbb, glob E, glob SS, glob JRO.RO, glob HRO.RO});
    first by sim.
  wp; call (: ={glob Vz, glob GRO.RO}); first by sim.
  wp; call (: ={glob E, glob SS, glob Pz, glob HRO.RO, glob GRO.RO, glob JRO.RO});
    first by sim.
  wp; call (: ={glob Vz, glob GRO.RO}); first by sim.
  wp.
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO,  BW.hk,
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in map (rem_id \o remv) BW.check{2} =>
                           x \in map (rem_id \o remv) BW.hvote{2})).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb, BW.hk}/\
             0 <= i{2} /\
             (* uniq (map id BW.check) *)
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in map (rem_id \o remv) BW.check{2} =>
                           x \in map (rem_id \o remv) BW.hvote{2})).
        auto =>/>; progress.
        + smt. 
        + move: H5; rewrite map_cat mem_cat //=; move => Hmor. 
          case (x \in map (rem_id \o remv) BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.         
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt.
            pose y:= nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2}.
            move => HyS.
            have HH: y \in BW.hvote{2}.
              print San_mem.
              cut := San_mem (map pack4 BW.hvote{2}) (pack4 y) _.
                move: HyS; rewrite mapP. smt.
              rewrite mapP. smt.
            rewrite Hx -/y.
            rewrite mapP. exists y. 
            by rewrite HH //=.
        + smt.
      auto=>/>; smt.
    + by proc. by proc. by proc.  by proc.  
    (* A.a2 call: fill bb *)
    wp; call (: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, glob C, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}); first 7 by sim.
    (* call A.a1: credentials *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
      first 3 by sim.
    call (: ={glob E}); first by sim.
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim.
    by auto =>/>.

  wp; while (={j, hon_mem, val_bb, BW.sk, glob E, HRO.RO.m, dL}); first by sim.  

  wp; while{1} ( ={glob E, glob HRO.RO, BW.sk, vbb, BW.check, BW.hvote}/\
                0 <= j{1} /\
                let g= fun (x : upkey * cipher * sign) =>
                           oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                let xbb = filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                         (mem (map (rem_id \o remv) BW.hvote{2})))
                                 (map rem_id (Sanitize' vbb{2})) in
                   hN{1} = map g (take j{1} xbb))
               (size (filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                   (mem (map (rem_id \o remv) BW.hvote{2})))
                            (map rem_id (Sanitize' vbb{2}))) - j{1})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.              

    wp; while{1} ( ={glob HRO.RO, glob E, BW.sk, 
                 BW.check, vbb}/\
               0 <= j{1} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id (Sanitize' vbb{2})) in
                   hC{1} = map g (take j{1} xbb))
              (size (filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id (Sanitize' vbb{2}))) -j{1}); progress.
                 
      wp; sp.
      exists* (glob E), BW.sk, c; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
      + smt.
    
  wp; while{2} (={glob E, glob HRO.RO} /\0 <= j{2} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                let xbb = filter (mem (map (rem_id \o remv) BW.hvote{2}))
                                 (map rem_id (Sanitize' vbb{2})) in
                   hCN{2} = map g (take j{2} xbb))
               (size (filter (mem (map (rem_id \o remv) BW.hvote{2}))
                            (map rem_id (Sanitize' vbb{2}))) - j{2})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.            
  auto=>/>; progress.
  + smt. smt. smt. smt. smt. smt.
  + pose BB:= (map rem_id (Sanitize' vbb{2})).
    pose C:= (map (rem_id \o remv) BW.check{2}).
    pose HO:= (map (rem_id \o remv) BW.hvote{2}).
    pose g:= (fun (x : upkey * cipher * sign) =>
            oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})).
    move: H1 H3 H H0;
      rewrite -/BB -/C -/HO; move => H1 H3 H H0.
    have ->: (take j_L (filter (mem C) BB)) = filter (mem C) BB by smt.
    have ->: (take j_L0 (filter (predI (predC (mem C)) (mem HO)) BB)) =
             (filter (predI (predC (mem C)) (mem HO)) BB) by smt.
    have ->: (take j_R (filter (mem HO) BB)) = (filter (mem HO) BB) by smt. 
    rewrite -?map_cat.
    have Hperm: perm_eq (map g (filter (mem C) BB ++
                                filter (predI (predC (mem C)) (mem HO)) BB))
                        (map g (filter (mem HO) BB)).
      rewrite perm_eq_map. 
      have ->: filter (mem C) BB = filter (predI (mem C) (mem HO)) BB.
        rewrite (eq_filter (predI (mem C) (mem HO)) (mem C) BB).
          move => x. rewrite /predI. smt.
        by done.
      cut := perm_filterC (mem C) (filter (mem HO) BB).
      by rewrite -?filter_predI //=.
    by rewrite ?Count_split (Count_perm _ _ Hperm).
 qed. 

local lemma dbb_dbbvotethree &m:
  Pr[DREG_Exp (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv'(B'(E, Pz, Vz, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
  (* go to wspread *) 
  Pr[DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow /\ 0 <= BW.ibad < qVo]  +
  (* go to tally uniqness+ accuracy *)
  Pr[DREG_Exp_hvote_three(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  cut := dbb_dbbtally &m.
  cut := dbbtally_dbbcheck_prime &m.
  cut := dbbcheck_dbbhash &m.
  cut := dbb_wspread &m.
  cut := dbbcheck_dbbcheck_two &m.
  cut := dbbcheck_two_dbbhvote &m.
  cut := dbbhvote_dbbhvote_two &m.
  cut := dbbhvotetwo_dbbhvotethree &m.
  cut := dbb_dish &m.
  cut := dbb_qCo &m.
(*  cut := dbbdish_dbbball &m. 
  cut := dbbball_two &m.
  cut := dbbballtwo_meuf &m.
  cut := meuf_euf C &m C_ll. *)
  pose L := Pr[DREG_Exp (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose C1:= Pr[DREG_Exp_tally (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose E1:= Pr[DREG_Exp_check' (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose E2:= Pr[DREG_Exp_check' (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad].
  pose F1:= Pr[Hash_Exp(BHash_Adv'(B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m: res].
  pose G1:= Pr[DREG_Exp_vote   (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose G2:= Pr[DREG_Exp_vote (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: 
               BW.bow /\ 0 <= BW.ibad < qVo].
  pose H1:= Pr[DREG_Exp_check_two (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose I1:= Pr[DREG_Exp_hvote (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]. 
  pose J1:= Pr[DREG_Exp_hvote_two (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose K1:=  Pr[DREG_Exp_hvote_three (E,Pz,Vz,C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].

  pose W1:= Pr[DREG_Exp_dish(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
  pose W2:= Pr[DREG_Exp_qCo(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m :
   res].
(*  pose W2:= Pr[DREG_Exp_dish(E, Pz, Vz, C, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m :
   !BW.ball].
  pose W3:= Pr[DBB_Exp_ball(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
  pose W4:= Pr[DBB_Exp_ball2(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
  pose W5:= Pr[MEUF_Exp(SS, BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO).main() @ &m : res].
  pose W6:= (size (undup Voters))%r * 
               Pr[EUF_Exp(SS, BE(SS, C), JRO.RO).main() @ &m : res] +
            (size (undup Voters))%r *
               Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO)), 
                    JRO.RO).main () @ &m : res] +
            (size (undup Voters))%r * Pr[Correctness(SS, C, JRO.RO).main() @ &m : !res].*)
  smt. (*
  move => Hw56 Hw45 Hw34 Hw23 Hew.
  have He: E1 <= W1+ W6 by smt.
  move => Hjk Hij Hhi Hgh Heg Hef Hce (* Hcd *) Hac Hab .
  by smt. *)
qed. 

(* reduction to well spreadness *) 
(* guess bad oracle *)
clone  Ex_Plug_and_Pray as WVX with
  type tin  <- unit,
  type tres <- bool,
  op bound  <- qVo
proof bound_pos. 
realize bound_pos. by apply size_qVo. qed. 

local lemma dbbvote_guess &m:
  1%r/ qVo %r * 
  Pr[DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow /\ 0 <= BW.ibad < qVo]
  =
  Pr[WVX.Guess(DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO)).main()
         @ &m: BW.bow /\ 0<= BW.ibad <qVo /\ fst res = BW.ibad].
proof.
  (* FIXME: rrr. every time I compile this file the order of ibad and bad changes *)
   print glob DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO). 
  cut := WVX.PBound (DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO))
          (fun g b => let (ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, 
                           gv8, gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16, gv17,
                           gE, gPz, gVz, gSS, gA, gC)= g in
             (bow /\ 0<=  ibad < qVo))
          (fun g b => let (ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, 
                           gv8, gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16, gv17,
                           gE, gPz, gVz, gSS, gA, gC)= g in
           ibad) () &m. 
  simplify. 
  move => ->. 
  by rewrite Pr[mu_eq]; progress; smt. 
qed.

module DREG_Oracle_wspread(V: VotingScheme',
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc h        = DREG_Oracle_vote(V,H,G,J).h
  proc g        = DREG_Oracle_vote(V,H,G,J).g
  proc j        = DREG_Oracle_vote(V,H,G,J).j
  proc corrupt  = DREG_Oracle_vote(V,H,G,J).corrupt
  proc check    = DREG_Oracle_vote(V,H,G,J).check
  proc cast     = DREG_Oracle_vote(V,H,G,J).cast
  proc board    = DREG_Oracle_vote(V,H,G,J).board

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL /\ BW.qVo <qVo){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      o <- Some b;
      if (!(b.`2,b.`3) \in (map (fun (x: ident * vote * upkey * cipher*sign),
                         (x.`3,x.`4))
                     BW.hvote)){
          BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)]; 
        }elif(!BW.bow){
         BW.bow <- BW.ibad = BW.qVo;
        }
      if (forall x, x\in BW.bb => 
                       (x.`1,x.`2) = (b.`1,b.`2) \/
                       (x.`1 <> b.`1 /\ x.`2 <> b.`2)) {
           BW.bb<- BW.bb ++ [b];
        }
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DREG_Exp_wspread (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DREG_Oracle_wspread(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var bool, i;
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL <@ A(O).a1(BW.sk); 
             A(O).a2();
    return bool;
  }
}.


local lemma guess_dbbwspread &m:
  Pr[WVX.Guess(DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO)).main()
         @ &m: BW.bow /\ 0<= BW.ibad <qVo /\ fst res = BW.ibad] <=
  Pr[DREG_Exp_wspread(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow].
proof.
  byequiv =>/>.
  proc.
  inline DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main.
  seq 22 20 : ( BW.bow{1} /\ 
          0 <= BW.ibad{1} < qVo /\
          i{1} = BW.ibad{1} => BW.bow{2})=>/>.
    (* A.a2 call: bb *)
    call (_: ={glob E, glob HRO.RO, glob GRO.RO,
               glob SS, glob JRO.RO, glob C,
               BW.sk, BW.pk, BW.bb, BW.hvote, BW.coL, BW.uL, 
               BW.qVo, BW.qCo, BW.qCa}/\
             (BW.bow{1} /\ BW.ibad{2} = BW.ibad{1} =>
                 BW.bow{2})).
    + by proc; auto=>/>. 
    + proc. 
      inline *; sp.
      if =>//=.   
      wp; call (: ={glob JRO.RO}); first by sim.
      call (: ={glob HRO.RO}); first by sim.
      by auto; progress; smt. 
    + proc.
      if =>//=.
      inline*; wp.
      call(: ={JRO.RO.m, HRO.RO.m}); first 2 by sim.
      by auto=>/>; smt.
      
    + by proc. by proc. by proc. by proc.
    (* call A.a1: credentials *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
      first 3 by sim.
    call (_: ={glob E, glob HRO.RO});
      first by sim. 
    call (_: true ==> ={glob JRO.RO});
      first by sim.
    call (_: true ==> ={glob GRO.RO});
      first by sim.
    call (_: true ==> ={glob HRO.RO});
      first by sim.
    
    by wp; rnd{1}; auto; progress; smt. 

  inline *; wp.
  while{1} (true) (size (filter (predC hon_mem) val_bb){1} - j{1}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; while{1} (true) (size (filter hon_mem val_bb){1} - j{1}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; while{1} (true) (size BW.bb{1} - i0{1}); progress.
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll);
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll). 
  wp; call{1} (Pz_ll GRO.RO GRO.RO_o_ll).
  wp; while{1} (true) (size fbb{1} - i1{1}); progress.
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll);
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll). print Aa2_ll. 
  wp; call{1} (Aa3_ll ( <: DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, 
                                          HRO.RO, GRO.RO, JRO.RO).O) 
                       _ _ HRO.RO_o_ll GRO.RO_o_ll JRO.RO_o_ll).
  + proc. 
    while{1} (true) (size pL - i); progress.
      by inline *; auto=>/>; smt.
    by inline *; auto =>/>; smt.   
  + by proc. 
  by auto=>/>; smt.
qed.

(*
op get_uc (x: ident * vote * upkey * cipher*sign) = (x.`3,x.`4).
op get_uc' (x: ident * upkey * cipher*sign) = (x.`2,x.`3).
*)

module DREG_Oracle_wspread_two(V: VotingScheme',
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc h        = DREG_Oracle_vote(V,H,G,J).h
  proc g        = DREG_Oracle_vote(V,H,G,J).g
  proc j        = DREG_Oracle_vote(V,H,G,J).j
  proc corrupt  = DREG_Oracle_vote(V,H,G,J).corrupt
  proc check    = DREG_Oracle_vote(V,H,G,J).check
  proc cast     = DREG_Oracle_vote(V,H,G,J).cast
  proc board    = DREG_Oracle_vote(V,H,G,J).board

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
      if (forall x, x\in BW.bb => 
                       (x.`1,x.`2) = (b.`1,b.`2) \/
                       (x.`1 <> b.`1 /\ x.`2 <> b.`2)) {
           BW.bb<- BW.bb ++ [b];
        }  
      BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      BW.bow <- (get_uc' b) \in (map get_uc  BW.hvote);
      BW.obad <- None;
      if ((get_uc' b) \in (map get_uc  BW.hvote)){
        BW.obad <- Some (index (get_uc' b) (map get_uc BW.hvote));
      }
     
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DREG_Exp_wspread_two (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DREG_Oracle_wspread_two(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var bool, i;
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    BW.obad <- None;
    i <- 0;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL <@ A(O).a1(BW.sk); 
             A(O).a2();
    return BW.bow;
  }
}.

local lemma dbbwspread_dbbwspreadtwo &m:
  Pr[DREG_Exp_wspread(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow] <=
  Pr[DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.obad <> None /\ 0 <= oget BW.obad < qVo].
proof.
  byequiv =>/>.
  proc.
  call (: BW.ibad < BW.qVo
          ,={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo, BW.bow, BW.ibad, glob C} /\
             BW.ibad{2} < qVo/\
             BW.qVo{2} <= qVo/\
             size BW.hvote{2} <= BW.qVo{2}/\
             (BW.qVo{2} <= BW.ibad{2} => !BW.bow{2})/\
             (!BW.bow{2} => BW.obad{2} = None)
          ,={BW.bow}/\
           (BW.bow{2} => BW.obad{2} <> None)/\
           BW.ibad{1} < BW.qVo{1}/\
           (BW.obad{2} <> None => 
             0 <= oget BW.obad{2} <= BW.ibad{2})).
   + by exact Aa2_ll.

   (* A.a2 corrupt: oracle, lossless, preserves bow *)
   + by proc; auto =>/>; smt.
   + by progress; proc; auto =>/>.
   + by progress; proc; auto =>/>.

   (* A.a2 vote: oracle, lossless, preserves bow *)
   + proc.
     sp; if{1} =>//=.  
     + if{2} =>//=.
       + (* BW.qVo <= BW.ibad *)
         (* rcondt{2} 1; progress.*)
         inline *; wp.
         call(: ={glob JRO.RO}); first by sim.
         call(: ={glob HRO.RO}); first by sim.
         by auto=>/>; smt.
       - (* BW.ibad <= BW.qVo *)
         if{2} =>//=.
         (* BW.ibad = BW.qVo *)
         + inline *. wp.
           call(: ={glob JRO.RO}); first by sim.
           call(: ={glob HRO.RO}); first by sim.
           rewrite /get_uc' /get_uc //=. 
           auto=>/>; progress. 
           + smt. smt. 
           + rewrite oget_some; smt. 
           + smt. smt. smt. 
           + rewrite oget_some; smt.
           + smt. smt.
         - (* BW.ibad < BW.qVo *)
           inline *; wp.
           call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
           call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
           by auto=>/>; smt. 
     - rcondf{2} 1; progress.
         by auto =>/>; smt.
       rcondf{2} 1; progress. 
         by auto=>/>; smt. 
   + progress; proc.  
     inline*; wp.    
     sp; if =>//=.      
     wp; 
     call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
     call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   + progress; proc.
     inline *; wp.
     sp; if =>//=.
     + wp; 
       call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
       call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
       by auto=>/>; smt.
     - if =>//=.
       wp; 
       call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
       call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
       by auto=>/>; smt.
   
   (* A.a2: cast oracle, lossless, preserves bow *)
   + proc.
     if =>//=.
     inline*; wp.
     call(: ={glob JRO.RO, HRO.RO.m}); first 2 by sim.
     by auto=>/>; smt.
   + progress; proc.
     if=>//=; inline*; wp. 
     call{1} (C_ll HRO.RO JRO.RO HRO.RO_o_ll JRO.RO_o_ll).
     by auto=>/>; smt.
   + progress; proc.
     if=>//=; inline*; wp. 
     call{1} (C_ll HRO.RO JRO.RO HRO.RO_o_ll JRO.RO_o_ll).
     by auto=>/>; smt.

   + by proc.
   + by move=> _ _; proc.
   + by move=>  _ ; proc.

   + by proc.
   + by move=> _ _; proc.
   + by move=>  _ ; proc.
   
   + by proc.
   + by move=> _ _; proc.
   + by move=>  _ ; proc.
  
   + by proc.
   + by move=> _ _; proc.
   + by move=>  _ ; proc.

   (* call A.a1: credentials *)
   call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
     first 3 by sim.
   call(: ={glob E}); first by sim. 
   call(: true ==> ={glob JRO.RO}); first by sim.
   call(: true ==> ={glob GRO.RO}); first by sim.
   call(: true ==> ={glob HRO.RO}); first by sim.
   by wp; rnd; auto=>/>; smt.
qed.

local lemma dbbwspreadtwo_guess  &m:
  1%r/ qVo %r * 
  Pr[DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.obad <> None/\ 0 <= oget BW.obad < qVo] =
  Pr[WVX.Guess(DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO)).main()  
               @ &m: BW.obad <> None /\ 0 <= oget BW.obad < qVo /\
                     fst res = oget BW.obad ].
proof. 
  print glob DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).
  cut := WVX.PBound (DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO))
          (fun g b => let (obad, ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, gv8, 
                           gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16,
                           gE, gSS, gA, gC)= g in
             (obad<>None /\ 0<= oget obad <qVo))
          (fun g b => let (obad, ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, gv8, 
                           gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16,
                           gE, gSS, gA, gC)= g in
           oget obad) () &m. 
  simplify; move => ->. 
  by rewrite Pr[mu_eq]; progress; smt. 
qed.


module DREG_Oracle_wspread_four(V: VotingScheme',
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc h        = DREG_Oracle_vote(V,H,G,J).h
  proc g        = DREG_Oracle_vote(V,H,G,J).g
  proc j        = DREG_Oracle_vote(V,H,G,J).j
  proc corrupt  = DREG_Oracle_vote(V,H,G,J).corrupt
  proc check    = DREG_Oracle_vote(V,H,G,J).check
  proc cast     = DREG_Oracle_vote(V,H,G,J).cast
  proc board    = DREG_Oracle_vote(V,H,G,J).board

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
        if (forall x, x\in BW.bb => 
                       (x.`1,x.`2) = (b.`1,b.`2) \/
                       (x.`1 <> b.`1 /\ x.`2 <> b.`2)) {
           BW.bb<- BW.bb ++ [b];
        }
        BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      (* BW.bow <- false; *)
      if ((get_uc' b) \in (map get_uc  BW.hvote)){
        BW.bow <- BW.obad = Some (index (get_uc' b) (map get_uc BW.hvote));
      }
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DREG_Exp_wspread_four (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DREG_Oracle_wspread_four(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var bool, i, il;
    il <$ [0..qVo -1];
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.obad <- Some il;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B'(E,P,Ve,C,SS,H,G,J).setup();
    BW.uL <@ A(O).a1(BW.sk); 
             A(O).a2();
    return BW.bow;
  }
}.

local lemma dbbwspreadthree_dbbwspread_four &m:
  Pr[WVX.Guess(DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO)).main()  
               @ &m: BW.obad <> None /\ 0 <= oget BW.obad < qVo /\
                     fst res = oget BW.obad ] <=
  Pr[DREG_Exp_wspread_four(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow].
proof.
  byequiv =>/>.
  proc.
  inline DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main.
  (* A.a2: bb *)
  wp; call (: ={ BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
                 glob C, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.bb} /\
              (BW.obad{1} <> None /\ ={BW.obad} => BW.bow{2})/\
              (BW.qVo{2} <= BW.ibad{2} => !BW.bow{1} /\ !BW.bow{2})).
  + proc.
    auto=>/>; smt.
  + proc.
    sp; if =>//=.   
    + inline *; wp.
      call(: ={JRO.RO.m}); first by sim.
      call(: ={HRO.RO.m}); first by sim.
      auto=>/>; smt. 
    if =>//=.
    inline *.
    wp; call(: ={JRO.RO.m}); first by sim.
    call(: ={HRO.RO.m}); first by sim.   
    by auto=>/>; smt.
  + proc.
    if =>//=.
    inline*; wp.
    call(: ={glob JRO.RO, HRO.RO.m}); first 2 by sim.
    by auto=>/>; smt. 
  + by proc. by proc. by proc. by proc.
  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim. 
  call (_: ={ glob E, glob HRO.RO}); first by sim.
  call (_: true ==> ={glob JRO.RO});
      first by sim.
  call (_: true ==> ={glob GRO.RO});
      first by sim.
  call (_: true ==> ={glob HRO.RO});
      first by sim. 
  wp; progress. rnd; wp; progress. rnd; auto; progress. 
  by rewrite some_oget 1: H5 in H3.
qed. 
     
module DREG_Oracle_wspread_five(V: VotingScheme',
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc h        = DREG_Oracle_vote(V,H,G,J).h
  proc g        = DREG_Oracle_vote(V,H,G,J).g
  proc j        = DREG_Oracle_vote(V,H,G,J).j
  proc corrupt  = DREG_Oracle_vote(V,H,G,J).corrupt
  proc check    = DREG_Oracle_vote(V,H,G,J).check
  proc cast     = DREG_Oracle_vote(V,H,G,J).cast
  proc board    = DREG_Oracle_vote(V,H,G,J).board

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
        if (forall x, x\in BW.bb => 
                       (x.`1,x.`2) = (b.`1,b.`2) \/
                       (x.`1 <> b.`1 /\ x.`2 <> b.`2)) {
           BW.bb<- BW.bb ++ [b];
        }
      BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      BW.bow <- ( (nth witness BW.hvote (oget BW.obad)).`3 = b.`2/\
                  (nth witness BW.hvote (oget BW.obad)).`4 = b.`3);
      
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DREG_Exp_wspread_five (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DREG_Oracle_wspread_five(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var bool, i, il;
    il <$ [0..qVo -1];
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.obad <- Some il;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL <@ A(O).a1(BW.sk); 
             A(O).a2();
    return BW.bow;
  }
}.

local lemma dbbwspreadfour_dbbwspreadfive &m:
  Pr[DREG_Exp_wspread_four(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow] <=
  Pr[DREG_Exp_wspread_five(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow].
proof.
  byequiv =>/>.
  proc.   
  call(: ={BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
           glob C, BW.bb, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad} /\
              (BW.bow{1} => BW.bow{2})/\
              (!BW.ibad{1}< BW.qVo{1} => !BW.bow{1})). 
  + proc. auto=>/>; smt.
  + proc.
    sp; if =>//=.
    + inline *; wp. 
      call(: ={JRO.RO.m}); first by sim.
      call(: ={HRO.RO.m}); first by sim.
      auto=>/>; smt. 
    if=>//=. 
    seq 1 1: ((o{2} = None /\ o{1} = None /\
                ={id, v} /\
                ={BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
                  glob C, BW.bb, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad} /\
                (BW.bow{1} => BW.bow{2}) /\
                  ! (BW.qVo{1} < BW.ibad{1} /\
                  BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}))) /\
                  BW.ibad{1} = BW.qVo{1} /\
                  BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1})/\
                  ={b} /\
                (!BW.ibad{1}< BW.qVo{1} => !BW.bow{1})). inline *; wp.
    call(: ={JRO.RO.m}); first by sim.
    call(: ={HRO.RO.m}); first by sim.
    auto=>/>; progress. 
    if{1} =>//=.
    + auto; progress. 
      + move: H4; rewrite oget_some /get_uc /get_uc'.  
        pose f:= fun (x0 : ident * vote * upkey * cipher * sign) => (x0.`3, x0.`4).
        rewrite /index.
        move => Hmem.
        have ->: (nth witness BW.hvote{2}
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`3 =
               (nth witness (map f BW.hvote{2})
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`1.
        + rewrite (nth_map witness witness). 
          + cut := find_ge0 (pred1 (b{2}.`2, b{2}.`3)) 
                        (map f BW.hvote{2}). 
            smt.
          smt. 
        smt.
      + move: H4; rewrite oget_some /get_uc /get_uc'.  
        pose f:= fun (x0 : ident * vote * upkey * cipher * sign) => (x0.`3, x0.`4).
        rewrite /index.
        move => Hmem.
        have ->: (nth witness BW.hvote{2}
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`4 =
               (nth witness (map f BW.hvote{2})
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`2.
        + rewrite (nth_map witness witness). 
          + cut := find_ge0 (pred1 (b{2}.`2, b{2}.`3)) 
                        (map f BW.hvote{2}). 
            smt.
          smt. 
        smt.
      + move: H4; rewrite /get_uc /get_uc'.  
        pose f:= fun (x0 : ident * vote * upkey * cipher * sign) => (x0.`3, x0.`4).
        rewrite /index.
        move => Hmem. 
        smt. 
    by auto. 
  + proc.
    if =>//=.
    inline *; wp.
    call(: ={glob JRO.RO, glob HRO.RO}); first 2 by sim.
    by auto=>/>; smt.    
  + by proc. by proc. by proc. by proc.   

  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim.
  call (_: ={ glob E, glob HRO.RO}); first by sim.
  call (_: true ==> ={glob JRO.RO});
    first by sim.
  call (_: true ==> ={glob GRO.RO});
    first by sim.
  call (_: true ==> ={glob HRO.RO});
    first by sim.
  by auto; progress; smt. 
qed.
 

module BWspread'(V: VotingScheme', G: GOracle.Oracle, J: JOracle.Oracle, 
                B: DREG_Adv, WO:Wspread_Oracle) ={
  module O ={
    
    proc h = WO.h
    proc j = DREG_Oracle_vote(V,WO_H(WO),G,J).j
    proc g = DREG_Oracle_vote(V,WO_H(WO),G,J).g
    proc corrupt = DREG_Oracle_vote(V,WO_H(WO),G,J).corrupt
    proc check = DREG_Oracle_vote(V,WO_H(WO),G,J).check
    proc cast = DREG_Oracle_vote(V,WO_H(WO),G,J).cast
    proc board = DREG_Oracle_vote(V,WO_H(WO),G,J).board

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(WO_H(WO),G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
        if (forall x, x\in BW.bb => 
                       (x.`1,x.`2) = (b.`1,b.`2) \/
                       (x.`1 <> b.`1 /\ x.`2 <> b.`2)) {
           BW.bb<- BW.bb ++ [b];
        }
      BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      WO.test(v, get_upk (oget BW.uL.[id]).`2, 
                 (nth witness BW.hvote (oget BW.obad)).`4);
      
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
 
  }

  proc main(sk : skey):unit ={
    var i,j;
    BW.hk <$ dhkey_out;
    G.init();
    J.init();
    BW.ibad <$ [0..qVo - 1];
    BW.coL <- [];
    BW.hvote <- [];
    BW.bb  <- [];
    BW.qVo <- 0;
    BW.sk <- sk;
    BW.pk <- get_pk sk;
    BW.uL <- empty;
    i <$ [0..(qVo-1)];
    BW.obad <- Some i;
    j <- 0;
    BW.uL <@ B(O).a1(sk);
             B(O).a2();
  }
}.

local lemma dbbwspreadfive_wspread &m:
  Pr[DREG_Exp_wspread_five(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow] <=
  Pr[Wspread_Exp(E, BWspread'(B'(E, Pz, Vz,C(E,SS),SS),GRO.RO,JRO.RO,A), HRO.RO).main() 
         @ &m: BS.eq ].
proof.
  byequiv =>/>.
  proc.
  inline BWspread'(B'(E, Pz, Vz, C(E,SS), SS), GRO.RO, JRO.RO, A, 
         Wspread_Exp(E, BWspread'(B'(E, Pz, Vz, C(E,SS), SS), GRO.RO, JRO.RO, A), HRO.RO).O).main.
  call(: ={BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
           glob C, BW.bb, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad}/\
         (*(forall id, BW.uL{2}.[id] <> None =>
              get_upk (oget BW.uL{2}.[id]).`2 = (oget BW.uL{2}.[id]).`1)/\ *)
         BW.pk{1} = BS.pk{2}/\
         0<= BS.qt{2} /\
         (BW.qVo{1} <= BW.ibad{1} => BS.qt{2} <1)/\
         (BS.qt{2} < 1=> !BW.bow{1})/\
         (BW.bow{1} => BS.eq{2})).
  + proc. auto=>/>; smt.
  + proc.
    sp; if =>//=.    
    + inline *; wp.
      call(: ={glob JRO.RO}); first by sim.
      call(: ={glob HRO.RO}); first by sim.
      by auto =>/>; smt.
    if =>//=.
    inline *; wp.
    rcondt{2} 4; progress; first by auto.
    sp.
    seq 1 1:( v0{2} = v{2} /\
              l{2} = get_upk (oget BW.uL{2}.[id{2}]).`2 /\
              c{2} = (nth witness BW.hvote{2} (oget BW.obad{2})).`4 /\
              id0{1} = id{1} /\
              v0{1} = v{1} /\
              pk{1} = BW.pk{1} /\
              usk{1} = (oget BW.uL{1}.[id{1}]).`2 /\
              upk{1} = get_upk usk{1} /\
              o{2} = None /\
              o{1} = None /\
              ={id, v, BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
                glob C, BW.bb, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad} /\
              BW.pk{1} = BS.pk{2} /\
              0 <= BS.qt{2} /\
              (BW.qVo{1} <= BW.ibad{1} => BS.qt{2} < 1) /\
               (BS.qt{2} < 1 => !BW.bow{1}) /\ 
              (BW.bow{1} => BS.eq{2}) /\
              ! (BW.qVo{1} < BW.ibad{1} /\
                  BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1})) /\
              BW.ibad{1} = BW.qVo{1} /\
              BW.uL{1}.[id{1}] <> None /\ 
              ! (id{1} \in BW.coL{1}) /\ c{1} =c'{2}).
      call(: ={glob HRO.RO}); first by sim.
      by auto. 
    wp; exists* (glob SS){1}; elim* => gs. 
    call{1} (SSs_eq gs).
    by auto =>/>; progress; smt.

  + proc.
    if =>//=.
    inline*; wp.
    call(: ={glob JRO.RO, glob HRO.RO}); first 2 by sim.
    by auto=>/>; smt.    
  + by proc. by proc. by proc. by proc. 
  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim.
  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
  wp. 
  swap{2} 17 -16.
  swap{2} 10 -6.
  swap{2} [6..8] 3.
  wp; rnd; wp; call{1} (Kgen_get_pk HRO.RO).
  swap{2} 8 -3.
  wp; call (: true ==> ={glob JRO.RO}); first by sim.
  call (: true ==> ={glob GRO.RO}); first by sim.
  call (: true ==> ={glob HRO.RO}); first by sim.
  wp; rnd; wp; rnd. 
  by auto=>/>; smt.
qed. 

local lemma dbb_dbbwspread &m:
  Pr[DREG_Exp (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  (* final *) 
  Pr[Hash_Exp(BHash_Adv'(B'(E, Pz, Vz, C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
  qVo%r ^2 * Pr[Wspread_Exp(E, BWspread'(B'(E, Pz, Vz,C(E,SS),SS),GRO.RO,JRO.RO,A), HRO.RO).main() 
         @ &m: BS.eq ] +
  (* go to tally uniq+ acc *)
  Pr[DREG_Exp_hvote_three(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  cut := dbb_dbbvotethree &m .
  cut := dbbvote_guess &m.
  cut := guess_dbbwspread &m.
  cut := dbbwspread_dbbwspreadtwo &m.
  cut := dbbwspreadtwo_guess  &m.
  cut := dbbwspreadthree_dbbwspread_four &m.
  cut := dbbwspreadfour_dbbwspreadfive &m.
  cut := dbbwspreadfive_wspread &m.

  pose L:= Pr[DREG_Exp (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose R2:=Pr[DREG_Exp_hvote_three(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () 
              @ &m : res].
  pose R3:= Pr[Wspread_Exp(E, BWspread'(B'(E, Pz, Vz, C(E,SS), SS), GRO.RO, JRO.RO, A), HRO.RO).main
               () @ &m : BS.eq].
  pose A:= Pr[DREG_Exp_vote(E, Pz, Vz, C(E,SS),SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m :
              BW.bow /\ 0 <= BW.ibad < qVo].
  pose B:= Pr[WVX.Guess(DREG_Exp_vote(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO)).main
               () @ &m : BW.bow /\ 0 <= BW.ibad < qVo /\ res.`1 = BW.ibad].
  pose CD:= Pr[DREG_Exp_wspread(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main() 
               @ &m : BW.bow].
  pose D:= Pr[DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main
               () @ &m : BW.obad <> None /\ 0 <= oget BW.obad < qVo].
  pose E:= Pr[WVX.Guess(DREG_Exp_wspread_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO)).main
               () @ &m : BW.obad <> None /\ 0 <= oget BW.obad < qVo /\ res.`1 = oget BW.obad].
  pose F:= Pr[DREG_Exp_wspread_four(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main
               () @ &m : BW.bow].
  pose G:= Pr[DREG_Exp_wspread_five(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main
               () @ &m : BW.bow].

  move => Hg3 Hfg Hef Hde Hcd Hbc Hab Hla.
  have Hxyz: forall (x y z: real), 0%r < x => (1%r/ x) * y = z <=> y = x * z; 
    first by progress; smt.
  have ->: qVo%r ^2 = qVo%r * qVo%r. 
    have ->: 2 = 1+1 by smt. 
    rewrite powrS; first by smt.
    smt.

  have Hab': A= qVo%r * B.  
    move: Hab; rewrite Hxyz; first by smt.
    by done.
  have Hbd: B <= D by smt.
  have Had: A <= qVo%r * D.
    smt. 
  have Hde': D = qVo%r * E.
    move: Hde; rewrite Hxyz; first by smt.
    by done.
  have He3: E <= R3 by smt.
  have Hd3: D <= qVo%r *R3. 
    rewrite Hde'. 
    cut Hp:= size_qVo. 
    cut:= StdOrder.RealOrder.ler_wpmul2l qVo%r _ E R3 He3.
      by rewrite RealExtra.le_fromint StdOrder.IntOrder.le0r Hp. 
    by done.
  have HLR: A <= qVo%r * (qVo%r * R3).
    move: Had. rewrite Hde' //=. 
    cut := StdOrder.RealOrder.ler_wpmul2l qVo%r _ (qVo%r * E) (qVo%r * R3) _. 
      by rewrite RealExtra.le_fromint StdOrder.IntOrder.le0r size_qVo.
      smt. 
    smt.    
  smt. 
qed. 

module DREG_Exp_dish_two (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  hCN, r', 
        pi', ev', vbb, b,m,e, j, c,
        dL, hon_mem, val_bb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL   <@ A(O).a1(BW.sk);
               A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.pk, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id (Sanitize' vbb));
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    (* size of dishonest valid ballots *)
    BW.ball <- size dL <= BW.qCo;

    bool <- r <> Count (hCN ++ dL);
    
    return ev /\ bool /\BW.ball ;
  }
}.


local lemma dbbdish_dbbdishtwo &m:
  Pr[DREG_Exp_hvote_three(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DREG_Exp_dish_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  byequiv =>/>.  
  proc.
  seq 26 27 : (={BW.bb, glob E, glob SS, glob C, HRO.RO.m, JRO.RO.m, 
                 vbb, BW.sk, BW.hvote, BW.qCo, ev, r}). 
    while (={BW.bb, i, glob E, glob SS, BW.sk, HRO.RO.m, JRO.RO.m, vbb}) ; 
      first by sim.
    wp; call(: ={glob Vz, GRO.RO.m}); first by sim.
    call(: ={glob E, glob SS, glob Pz, HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.sk}).
      sim.
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish; wp.
    wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.hvote, BW.check, 
               BW.bb, BW.pk, BW.uL, BW.hk});
    first 5 by sim.
    wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.uL, BW.qVo, BW.pk, 
               BW.coL, glob SS, glob E, glob C, BW.bb, BW.hvote, BW.qCo}/\
                Sanitize' BW.bb{1} = USanitize BW.bb{1} /\
               (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
               (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1)).
    + by proc; auto.
    + proc.
      sp; if=>//=.
      inline*; wp.
      call(: ={glob JRO.RO}); first by sim.
      call(: ={glob HRO.RO}); first by sim.
      auto=>/>; progress.
      + pose a:= (id{2}, get_upk (oget BW.uL{2}.[id{2}]).`2, result_R, result_R0).
        cut := San'_USan (BW.bb{2} ++[a]) _ _.
         + move => x y.
           rewrite ?mem_cat //=. smt.
         + move => x y.
           rewrite ?mem_cat //=. smt.
        by done.
      + move: H7 H8; rewrite ?mem_cat //=. smt.
      + move: H7 H8; rewrite ?mem_cat //=; smt.
      + pose a:= (id{2}, get_upk (oget BW.uL{2}.[id{2}]).`2, result_R, result_R0).
        cut := San'_USan (BW.bb{2} ++[a]) _ _.
         + move => x y.
           rewrite ?mem_cat //=. smt.
         + move => x y.
           rewrite ?mem_cat //=. smt.
        by done.
      + move: H7 H8; rewrite ?mem_cat //=. smt.
      + move: H7 H8; rewrite ?mem_cat //=; smt.
    + proc. 
      sp; if =>//=. 
      inline*; wp. 
      call(: ={HRO.RO.m, JRO.RO.m}); first 2 by sim.
      auto=>/>; progress.
      + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
         + move => x y.
           rewrite ?mem_cat //=. smt.
         + move => x y.
           rewrite ?mem_cat //=. smt.
        by done.
      + move: H6 H7; rewrite ?mem_cat //=; smt.
      + move: H6 H7; rewrite ?mem_cat //=; smt.
    + by proc. by proc. by proc. by proc.

    call(: ={glob HRO.RO, glob GRO.RO, glob JRO.RO}); first 3 by sim.
    call(: ={glob E}); first by sim.
    call(: true ==> ={JRO.RO.m}); first by sim.
    call(: true ==> ={GRO.RO.m}); first by sim.
    call(: true ==> ={HRO.RO.m}); first by sim.
    auto=>/>; progress.
    + smt.
    + by rewrite H0 -USan_USanitize -?map_comp /hash' /preha /(\o).
  
  wp; while (={j, glob E, HRO.RO.m, dL, BW.sk, hon_mem, val_bb}); first by sim.
  wp; while (={j, glob E, HRO.RO.m, BW.sk, hCN}/\
             hon_mem{2} = mem (map (rem_id \o remv) BW.hvote{1})/\
             val_bb{2}  = map rem_id (Sanitize' vbb){1}). 
    wp; call(: ={glob HRO.RO}); first by sim.
    by auto=>/>; smt.
  by auto=>/>.
qed.

(*  *)
module DREG_Exp_result (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i,  r', 
        pi', ev';
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL  <@ A(O).a1(BW.sk);
              A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.pk, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    bool <- r <> r';
    
    return ev /\ bool ;
  }
}.

local lemma dbbdishtwo_dbbresult &m:
  Pr[DREG_Exp_dish_two(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DREG_Exp_result(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  byequiv =>/>.  
  proc.
  seq 22 22 : (={ r, ev, BW.pbb, BW.bb, BW.uL, BW.sk, glob E, BW.hk,
                 glob SS, glob Pz, glob Vz, glob C, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.pk}/\
               (let fd = (fun (x : ident * upkey * cipher * sign) =>
                 dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                 ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in 
                 forall x, x \in BW.bb{2} => fd x)/\
                 Sanitize' BW.bb{2} = USanitize BW.bb{2}).
    inline *; wp.
    call(: ={GRO.RO.m}); first by sim.
    wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.hvote, BW.check, 
               BW.bb, BW.pk, BW.uL, BW.hk});
    first 5 by sim.
    wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.uL, BW.qVo, BW.pk, 
               BW.coL, glob SS, glob E, glob C, BW.bb, BW.hvote}/\ 
               BW.pk{2} = get_pk BW.sk{2}/\
               (let fd = (fun (x : ident * upkey * cipher * sign) =>
                 dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                 ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) in 
                 forall x, x \in BW.bb{2} => fd x)/\
               Sanitize' BW.bb{2} = USanitize BW.bb{2}/\
               (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => 
                            x.`1 = y.`1 => x.`2 = y.`2) /\
               (forall x y, x \in BW.bb{1} => y \in BW.bb{1} => 
                            x.`2 = y.`2 => x.`1 = y.`1)).
    + by proc; auto.
    + proc.
      sp; if=>//=.
      inline*; wp.
      sp.
      seq 1 1: (id0{2} = id{2} /\
               v0{2} = v{2} /\
               pk{2} = BW.pk{2} /\
               usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
               upk{2} = get_upk usk{2} /\
               id0{1} = id{1} /\
               v0{1} = v{1} /\
               pk{1} = BW.pk{1} /\
               usk{1} = (oget BW.uL{1}.[id{1}]).`2 /\
               upk{1} = get_upk usk{1} /\
               o{2} = None /\
               o{1} = None /\
               ={id, v} /\
               BW.pk{2} = get_pk BW.sk{2}/\
               ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.uL, BW.qVo, BW.pk, BW.coL, glob SS,
                 glob E, glob C, BW.bb, BW.hvote} /\
               (forall (x : ident * upkey * cipher * sign),
                   x \in BW.bb{2} =>
                   dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
               Sanitize' BW.bb{2} = USanitize BW.bb{2} /\
               (forall (x y : ident * upkey * cipher * sign),
                 x \in BW.bb{1} => y \in BW.bb{1} => x.`1 = y.`1 => x.`2 = y.`2) /\
               (forall (x y : ident * upkey * cipher * sign),
                 x \in BW.bb{1} => y \in BW.bb{1} => x.`2 = y.`2 => x.`1 = y.`1) /\
               BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\ BW.qVo{1} < qVo /\
               ={c} /\ dec_cipher BW.sk{2} upk{1} c{1} HRO.RO.m{2} <> None).
        exists* v0{1}, upk{2}, BW.sk{2};  elim* => vx upkx skx. 
        call{1} (Enc_dec_two skx (get_pk skx) upkx vx _); first by done.
        by auto; progress; smt. 
      exists* usk{1}, c{1}; elim*=> uskx cx.
      call{1} (Sig_ver_two uskx (get_upk uskx) cx _ );
        first by done.
      auto=>/>; progress.
      + move: H10; rewrite mem_cat //=; smt.
      + move: H10; rewrite mem_cat //=; smt.
      + pose a:= (id{2}, get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R).
        cut := San'_USan (BW.bb{2} ++[a]) _ _.
         + move => x y.
           rewrite ?mem_cat //=. smt.
         + move => x y.
           rewrite ?mem_cat //=. smt.
        by done.
      + move: H10 H11; rewrite ?mem_cat //=;smt.
      + move: H10 H11; rewrite ?mem_cat //=; smt.
      + move: H10; rewrite mem_cat //=; smt.
      + move: H10; rewrite mem_cat //=; smt.
      + pose a:= (id{2}, get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R).
        cut := San'_USan (BW.bb{2} ++[a]) _ _.
         + move => x y.
           rewrite ?mem_cat //=. smt.
         + move => x y.
           rewrite ?mem_cat //=. smt.
        by done.
      + move: H10 H11; rewrite ?mem_cat //=;smt.
      + move: H10 H11; rewrite ?mem_cat //=; smt.
    + proc. 
      sp; if =>//=. 
      inline*; wp; sp.
      exists* BW.sk{2}, b0{1}; elim* => skx bx.
      call{1} (validInd_dec skx (get_pk skx) bx _); first by done.  
      auto=>/>; progress.
      + move: H8; rewrite mem_cat //=; smt.
      + move: H8; rewrite mem_cat //=; smt.
      + cut := San'_USan (BW.bb{2} ++[b{2}]) _ _.
         + move => x y.
           rewrite ?mem_cat //=. smt.
         + move => x y.
           rewrite ?mem_cat //=. smt.
        by done.
      + move: H8 H9; rewrite ?mem_cat //=; smt.
      + move: H8 H9; rewrite ?mem_cat //=; smt.
    + by proc. by proc. by proc. by proc.

    call(: ={glob HRO.RO, glob GRO.RO, glob JRO.RO}); first 3 by sim.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    while (={work1, JRO.RO.m}); first by sim.
    wp; while (={work0, GRO.RO.m}); first by sim.
    wp; while (={work, HRO.RO.m}); first by sim.
    auto=>/>; progress.
    + smt.

  inline *; wp.     
  while{1} ( ={BW.sk, HRO.RO.m} /\ 0<= j{1} /\
             let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
            dL{1} = map g (take j{1} (filter (predC hon_mem{1}) val_bb{1})))
           (size (filter (predC hon_mem{1}) val_bb{1}) - j{1}); progress. 
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge sk x.
    call{1} (dec_Dec_one ge sk x.`1 x.`2). 
    auto=>/>; progress. 
    + smt.
    + rewrite (take_nth witness) ; first by smt.
      by rewrite -cats1 map_cat. 
    + smt.
  wp;     
  while{1} ( ={BW.sk, HRO.RO.m} /\ 0<= j{1} /\
             let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
            hCN{1} = map g (take j{1} (filter hon_mem{1} val_bb{1})))
           (size (filter hon_mem{1} val_bb{1}) - j{1}); progress. 
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge sk x.
    call{1} (dec_Dec_one ge sk x.`1 x.`2). 
    auto=>/>; progress. 
    + smt.
    + rewrite (take_nth witness) ; first by smt.
      by rewrite -cats1 map_cat. 
    + smt.
  wp; while{1} ( ={ JRO.RO.m, HRO.RO.m, BW.sk, BW.bb} /\ 0<= i{1}/\
                 let fd= fun (x : ident * upkey * cipher * sign) =>
                     dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                     ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
                 vbb{1} = filter fd (take i{1} BW.bb{2}))
                (size BW.bb{2} - i{1}); progress.
    wp; sp.
    exists* (glob SS), (glob E), BW.sk, b; elim* => gs ge skx bx.
    call{2} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
    call{1} (dec_Dec_one ge skx bx.`2 bx.`3).
    auto=>/>; progress.
    + smt. smt. smt. smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons H1.      
    + smt.

  wp; call(: ={GRO.RO.m}); first by sim.
  wp; call(: ={glob GRO.RO}); first by sim.
  wp; while (={i0, fbb, glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb, sk}/\ 
             0<=i0{1}/\ BW.sk{2} = sk{1}/\
             let f= fun (x : upkey * cipher * sign) =>
                   (x.`1, oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) in
             let fd= fun (x : upkey * cipher * sign) =>
                     dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2} <> None /\
                     ver_sign x.`1 x.`2 x.`3 JRO.RO.m{2} in
             ubb{1} = map f (filter fd (take i0{1} fbb{2}))).
    wp; sp.             
    exists* BW.sk{2}, b{2}; elim* => skx bx.
    call{1} (ver_Ver_two bx.`1 bx.`2 bx.`3).
    call{1} (dec_Dec_two skx bx.`1 bx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons //= H1 H2 //= map_rcons //= -cats1.      
    + smt.
      rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons //= H1 //= map_rcons //= -cats1. 
  auto=>/>; progress.
  + by rewrite take0 //=. 
  + by rewrite take0. 
  + smt. 
  + by rewrite take0. 
  + smt.  
  + by rewrite take0.
  + smt.
  + rewrite take_oversize. smt.
    move: H11. rewrite ?take_oversize; first 4 by smt.
    pose fd:= (fun (x : upkey * cipher * sign) =>
                 dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2} <> None /\
                 ver_sign x.`1 x.`2 x.`3 JRO.RO.m{2}).   
    pose fd':= (fun (x : ident * upkey * cipher * sign) =>
                 dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                 ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}).  
    have ->: (filter fd (map remi BW.bb{2})) = map remi (filter fd' BW.bb{2}).
      by rewrite filter_map /preim /(\o).
    pose L:= (filter fd' BW.bb{2}).
    pose g:= (fun (x : upkey * cipher * sign) =>
              oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})).
    pose p:= (mem (map (rem_id \o remv) BW.hvote{1})).
    rewrite -map_cat.
    cut Hp:= perm_filterC p (map rem_id (Sanitize' L)).
    have ->: Count (map g
                   (filter p (map rem_id (Sanitize' L)) ++
                    filter (predC p) (map rem_id (Sanitize' L)))) =
             Count (map (g\o rem_id) (Sanitize' L)). 
     cut Hmp:= perm_eq_map g _ _ Hp.
     by rewrite (Count_perm _ _ Hmp) -map_comp.
    move => Hr.
    have Ha1: L = BW.bb{2}. search filter all. 
      rewrite /L -all_filterP allP. 
      move => x HxBB.
      cut := H x HxBB. 
      rewrite /fd'. smt. 
    have Ha2: Sanitize' BW.bb{2} = USanitize BW.bb{2} by rewrite H0.
    move: Hr; rewrite Ha1 Ha2 map_comp USan_USanitize /USan.
    print San_map'.
    cut := San_map' BW.sk{2} (map remi BW.bb{2}) HRO.RO.m{2}.
      rewrite //=; move => Hs.
    rewrite Hs.
    rewrite -map_comp. 
    have ->: ((fun (p0 : upkey * vote) => p0.`2) \o
               fun (x : upkey * (cipher * sign)) =>
              (x.`1, oget (dec_cipher BW.sk{2} x.`1 x.`2.`1 HRO.RO.m{2}))) = 
              g \o (fun (x : upkey * (cipher * sign)) => (x.`1, x.`2.`1, x.`2.`2)).
             by rewrite /(\o) /g /remi.
    rewrite -?map_comp.
    have ->: (fun (x : upkey * (cipher * sign)) => (x.`1, x.`2.`1, x.`2.`2)) = uopen_2 
      by rewrite /uopen_2.
    have ->: ((fun (p0 : upkey * vote) => p0.`2) \o
              ((fun (x : upkey * cipher * sign) =>
               (x.`1, oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}))) \o
               uopen_2)) = (g \o uopen_2) by rewrite /g /(\o) //=.   
    by done.        
qed.

module DREG_Exp_vf (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd,
                       SS: SignScheme(*, Pp: PP*), A: DREG_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DREG_Oracle_vote(B'(E,P,Ve,C,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, i,  r', 
        pi';
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    BW.uL <@ A(O).a1(BW.sk);
             A(O).a2();
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.pk, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    BW.ball      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');
    
    return (ev /\ BW.ball) /\ r <> r' ;
  }
}.

local lemma dbbresult_dbbvf &m:
  Pr[DREG_Exp_result(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DREG_Exp_vf(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res]+
  Pr[DREG_Exp_vf(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : !BW.ball].
proof.
  byequiv => //=.
  proc.
  wp.
  inline  B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).verify
          B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).tally 
          B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  wp; call(: ={GRO.RO.m}); first by sim.
  wp; call(: ={GRO.RO.m}); first by sim.
  wp; while (={i0, fbb, glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb, sk});
    first by sim.
  wp; call(: ={GRO.RO.m}); first by sim.
  wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.hvote, BW.check, 
               BW.bb, BW.pk, BW.uL, BW.hk});
    first 5 by sim.
  
  wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.uL, BW.qVo, BW.pk, 
               BW.coL, glob SS, glob E, glob C, BW.bb, BW.hvote});
    first 7 by sim.
  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim.
  call(: ={glob E}); first by sim.

  call(: true ==> ={glob JRO.RO}); first by sim.
  call(: true ==> ={GRO.RO.m}); first by sim.
  call(: true ==> ={HRO.RO.m}); first by sim.
  by auto.
qed.

(* accuracy adversary that calls the weak verifiability adversary *)
module BAcc'(V: VotingScheme', A: DREG_Adv,
            H:HOracle.ARO, G:GOracle.ARO, J: JOracle.ARO) ={
  module O = DREG_Oracle_vote(V,H,G,J)

  proc main (sk : skey, 
             ul : (ident, upkey * uskey) map,
             uLL: (ident * uskey*upkey) list,
             hk: hash_key) 
           : (ident * upkey * cipher * sign) list ={
    var i,r, pi;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    i <- 0;
    BW.uL <- ul;
    BW.sk <- sk;
    BW.pk <- get_pk BW.sk;
    BW.hk <- hk;

    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (r,pi)   <@ A(O).a3();

    return BW.bb;
  }
}.

(* lemma that bounds *)
local lemma verifreplacevf_accuracy &m:
  Pr[DREG_Exp_vf(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : !BW.ball] <=
  Pr[Acc_Exp (B'(E,Pz,Vz,C(E,SS),SS), BAcc'(B'(E,Pz,Vz,C(E,SS),SS),A), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m: !res].
proof.
  byequiv =>/>.
  proc.
  inline BAcc'(B'(E, Pz, Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main.  
  wp. progress.
  seq 19 30: ( ={glob E, glob SS, glob Vz, glob Pz, 
                 HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.bb, 
                 BW.pk, BW.sk, BW.hk}/\ BW.hk{2} = hk{2}/\
               r{1} = r0{2} /\ pi{1} = pi0{2} /\
               BW.sk{2} = sk{2} /\
               BW.pk{1} = get_pk BW.sk{1}/\
               BW.pk{2} = pk{2} ).
    (* A.a3: check *)
    call (: ={JRO.RO.m, GRO.RO.m, HRO.RO.m, BW.check, 
              BW.bb, BW.pk, BW.hvote, BW.uL, BW.hk}); 
      first 5 by sim.
    (* A.a2: bb *)
    wp; call (: ={JRO.RO.m, HRO.RO.m, GRO.RO.m, BW.pk, BW.qVo, BW.bb,
                  BW.coL, BW.uL, glob SS, glob E, glob C, BW.coL, BW.uL, BW.hvote}); 
      first 7 by sim. 
    (* call A.a1: credentials *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
      first 3 by sim.
             
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; while{2} (={glob SS}) (size (undup Voters) -i{2}); progress.
      inline*; wp. print SSk_eq.
      exists* (glob SS); elim*=> gs.
      call{1} (SSk_eq gs JRO.RO).
      by auto=>/>; smt.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO). 
    call(: true ==> ={JRO.RO.m}); first by sim.
    call(: true ==> ={GRO.RO.m}); first by sim.
    call(: true ==> ={HRO.RO.m}); first by sim. 
    by auto=>/>; progress; smt.

  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  seq 6 0: ( ={glob E, glob SS, glob Vz, glob Pz, HRO.RO.m, 
               GRO.RO.m, JRO.RO.m, BW.bb, BW.pk, BW.sk, BW.hk} /\
             r{1} = r0{2} /\ pi{1} = pi0{2} /\ BW.hk{2} = hk{2} /\
             bb{1}= BW.bb{1} /\ pk{2} = BW.pk{2} /\
             BW.sk{2} = sk{2} /\
              BW.pk{1} = get_pk BW.sk{1}/\
             BW.pbb{1} = pbb{1} /\
             pbb{1} = map (fun (x: upkey * cipher * sign)=>
                               (get_pub_data x, hash BW.hk{2} x))
                          (USan (map remi bb{1}))).
    inline *; wp; sp.
    exists * (glob Vz){1}; elim *=> gv.
    call{1} (Vz_keepstate  gv).
    by auto=>/>.
  inline *; wp.
  call(: ={glob GRO.RO}); first by sim.
  wp; call(: ={glob GRO.RO}); first by sim. 
  wp; while (={fbb, glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb}/\ 
             i0{1} = i1{2}/\ sk{1} = sk1{2}); first by sim.
  by auto=>/>. 
qed. 

module BTall'(V: VotingScheme', A: DREG_Adv,
                   H:HOracle.ARO, G:GOracle.ARO, J: JOracle.ARO) ={
  module O = DREG_Oracle_vote(V,H,G,J)

  proc main ()
       : ((ident * upkey * cipher*sign) list) * 
         pkey * result * prf * result * prf * hash_key={
    var r, pi, r', pi', i;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    i <- 0;
    BW.uLL <- [];

    (BW.pk, BW.sk, BW.hk) <@ V(H,G,J).setup();
    BW.uL <@ A(O).a1(BW.sk);
            A(O).a2();
    (r,pi)   <@ A(O).a3();
    (r',pi') <@ V(H,G,J).tally (map remi BW.bb,BW.sk, BW.hk);
    
    return (BW.bb, BW.pk, r, pi, r', pi', BW.hk);
  }

}.

local lemma dbbvf_tallyuniq &m:
  Pr[DREG_Exp_vf(E, Pz, Vz, C(E,SS), SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[TallyUniq_Exp (B'(E,Pz,Vz,C(E,SS),SS), BTall'(B'(E,Pz,Vz,C(E,SS),SS),A), 
        HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  inline BTall'(B'(E, Pz, Vz, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO).main.
  swap{1} 21 1.
  conseq (: ={ev, r, r'}/\ BW.ball{1} = ev'{2})=>//=.
  call (: ={glob Vz, GRO.RO.m}); first by sim.
  call (: ={glob Vz, GRO.RO.m}); first by sim.
  swap{1} 20 1.
  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  wp; call (: ={JRO.RO.m, GRO.RO.m, HRO.RO.m, glob SS, 
                glob Pz, glob E}); first by sim.
  wp; call (: ={JRO.RO.m, GRO.RO.m, HRO.RO.m, BW.check, 
              BW.bb, BW.pk, BW.hvote, BW.uL, BW.bb, BW.hk}); 
      first 5 by sim.
  wp; call (: ={JRO.RO.m, HRO.RO.m, GRO.RO.m, BW.pk, BW.qVo, BW.bb,
               BW.coL, BW.uL, glob SS, glob E, glob C, BW.coL, BW.uL, BW.hvote}); 
      first 7 by sim.
  (* call A.a1: credentials *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO}); 
    first 3 by sim.
  call (: ={glob E}); first by sim.
  swap{1} [13..15] -12.
  wp; call(: true ==> ={JRO.RO.m}); first by sim.
  call(: true ==> ={GRO.RO.m}); first by sim.
  call(: true ==> ={HRO.RO.m}); first by sim.
  by auto=>/>; smt.
qed. 

lemma bound_dreg &m:
  Pr[DREG_Exp (B'(E,Pz,Vz,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  (* final *) 
  Pr[Hash_Exp(BHash_Adv'(B'(E, Pz, Vz, C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
qVo%r ^2 * 
  Pr[Wspread_Exp(E, BWspread'(B'(E, Pz, Vz,C(E,SS),SS),GRO.RO,JRO.RO,A), HRO.RO).main() @ &m: BS.eq ] +
  Pr[Acc_Exp (B'(E,Pz,Vz,C(E,SS),SS), BAcc'(B'(E,Pz,Vz,C(E,SS),SS),A), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m: !res] + 
  Pr[TallyUniq_Exp (B'(E,Pz,Vz,C(E,SS),SS), BTall'(B'(E,Pz,Vz,C(E,SS),SS),A), 
        HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]. 
proof.
  cut Hp:= dbb_dbbwspread &m.
  cut := dbbdish_dbbdishtwo &m.
  cut := dbbdishtwo_dbbresult &m.
  cut := dbbresult_dbbvf &m.
  cut := verifreplacevf_accuracy &m.
  cut := dbbvf_tallyuniq &m. 
  by smt.
qed.
end section DREG.


section Bound_TallyUniq. 
(* ** abstract algorithms ** *)
declare module C: ValidInd'        {BPS, BW, HRO.RO, GRO.RO, JRO.RO}. 
declare module E: Scheme          {BW, C,   BSC, BPS, JRO.RO, HRO.RO, GRO.RO, JRO.RO}.
declare module Pz: Prover         {BW, C,   E, BPS, BSC, JRO.RO, HRO.RO, GRO.RO}.
declare module Vz: Verifier       {BW, C,   Pz, E, BPS, BSC, JRO.RO, HRO.RO, GRO.RO, JRO.RO}.
declare module Ex: PoK_Extract    {BW, C, BSC, Pz, BPS, E, GRO.RO, JRO.RO, Vz, HRO.RO}.
declare module SS: SignScheme     {BW, C, BPS, Vz, E, Pz, Ex, HRO.RO, JRO.RO, GRO.RO}.

(* work around for the fact that the TallyUniq Adv that uses Wverif_Adv 
   has access to the voting scheme *)
module type TallyUniq_Adv_2(V: VotingScheme', H : HOracle.ARO,
                           G : GOracle.ARO, J: JOracle.ARO) = {
  proc main() : (ident * upkey * cipher*sign) list * 
               pkey * result * prf *
                      result * prf * hash_key {H.o G.o J.o}
}.

(* ** ASSUMPTIONS ** *)
(* ** start *)

  axiom weight_dh_out:
    weight dh_out = 1%r.
  axiom weight_dg_out:
    weight dg_out = 1%r.
  axiom weight_dj_out:
    weight dj_out = 1%r.
  axiom weight_dk_out:
    weight dhkey_out = 1%r.

  axiom is_finite_h_in : Finite.is_finite predT <:h_in>.
  axiom is_finite_g_in : Finite.is_finite predT <:g_in>.
  axiom is_finite_j_in : Finite.is_finite predT <:j_in>.

   axiom get_pk_inj (x y: skey):
     get_pk x = get_pk y => x = y.
 
(* lossless assumptions *) 
  axiom Ee_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).enc.
  axiom Ed_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).dec.
  axiom Ex_ll (G <: GOracle.ARO):
    islossless G.o =>
    islossless Ex(G).extract.
  axiom Vz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Vz(G).verify.

  axiom A_ll (A<: TallyUniq_Adv_2) (V <: VotingScheme'{A}) (H <: HOracle.ARO{A}) 
             (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
  islossless A(V, H, G, J).main.

axiom dec_Dec_one (ge: (glob E)) (sk2: skey)(l2: upkey) (c2: cipher):
    phoare [E(HRO.RO).dec:  
           (glob E) =ge /\ arg = (sk2, l2, c2)
           ==>   
           (glob E) =ge /\
           res = dec_cipher sk2 l2 c2 HRO.RO.m ] = 1%r.

local lemma R_ll (H<: HOracle.ARO):
    islossless H.o => islossless DecRel'(E, H).main.
proof.
  move => Ho.
  proc.
  wp; while{1} (true) (size stm.`2 -i); progress.
    wp; call{1} (Ed_ll H Ho).
    by auto; smt.
  by auto; smt.  
qed.

(* tally uniqueness experiment + extractor for witness *)
local module TallyUniqE_Exp (V: VotingScheme', A: TallyUniq_Adv,
                      Ex: PoK_Extract, R: Relation,
                      H: HOracle.Oracle, G: GOracle.Oracle, 
                      J: JOracle.Oracle)={
  module PO = PoK_Oracle(G) 
  proc main(): bool ={
    var bb, pk, r1 , r2, p1, p2, ev1, ev2, sk1, sk2, rel1, rel2, hk;

    BPS.b <- false;

    H.init();
    PO.init();
    J.init();

    (bb, pk, r1, p1, 
             r2, p2, hk) <@ A(H,PO,J).main();
    BW.pbb <@ V(H,G,J).publish(bb,pk, hk);
    ev1   <@ V(H,G,J).verify ((pk, BW.pbb, r1), p1);
    sk1   <@ Ex(G).extract((pk, BW.pbb, r1), p1, BPS.io);
    rel1  <@ R.main((pk, BW.pbb, r1), sk1);

    ev2   <@ V(H,G,J).verify ((pk, BW.pbb, r2), p2);
    sk2   <@ Ex(G).extract((pk, BW.pbb, r2), p2, BPS.io);
    rel2  <@ R.main((pk, BW.pbb, r2), sk2);

    BPS.b <- (r1<> r2) /\ rel1 /\ rel2;
    return ev1 /\ ev2 /\ (r1<> r2) /\ !BPS.b;
  }
}. 

(* add witness extraction to the tally uniqueness expriment *) 
local lemma tally_uniq_witness 
            (A <: TallyUniq_Adv_2 { HRO.RO, JRO.RO, GRO.RO, BPS, Vz}) &m:
  Pr[TallyUniq_Exp(B'(E,Pz,Vz,C(E,SS),SS), A(B'(E,Pz,Vz,C(E,SS),SS)), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res] <=
  Pr[TallyUniqE_Exp(B'(E,Pz,Vz,C(E,SS),SS), A(B'(E,Pz,Vz,C(E,SS),SS)), Ex, DecRel'(E,HRO.RO), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res ]+ 
   Pr[TallyUniqE_Exp(B'(E,Pz,Vz,C(E,SS),SS), A(B'(E,Pz,Vz,C(E,SS),SS)), Ex, DecRel'(E,HRO.RO), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  BPS.b].
proof.  
 
  byequiv (_: ={glob A, glob Vz} ==> _)=>//=.
  proc.
  wp.
  call{2} (R_ll HRO.RO HRO.RO_o_ll).
  call{2} (Ex_ll GRO.RO GRO.RO_o_ll).
  call (_: ={ glob GRO.RO, glob Vz}); first by sim.
  call{2} (R_ll HRO.RO HRO.RO_o_ll).
  call{2} (Ex_ll GRO.RO GRO.RO_o_ll).
  call (_: ={glob GRO.RO, glob Vz}); first by sim.
  call (_: ={glob HRO.RO}); first by sim.
  (* adv call, where right side does more in G *)
  call (_: ={glob HRO.RO, glob GRO.RO, glob JRO.RO}).
  + sim. 
  + by proc; inline *; wp.
  + sim.
  inline TallyUniqE_Exp(B'(E,Pz,Vz,C(E,SS),SS), 
                        A(B'(E,Pz,Vz,C(E,SS),SS)), Ex, DecRel'(E,HRO.RO), 
                        HRO.RO, GRO.RO, JRO.RO).PO.init.
   call (_: true ==> ={glob JRO.RO});
    first by sim.
  call (_: true ==> ={glob GRO.RO});
    first by sim.
  wp; call (_: true ==> ={glob HRO.RO});
    first by sim.
  by auto. 
qed.

(*module PoK_TU'(A:TallyUniq_Adv, H: HOracle.Oracle, 
              J: JOracle.Oracle, G:GOracle.ARO) ={

  proc a1() ={
    var bb, pk, r1 , r2, p1, p2; 
    H.init();
    J.init();

    (bb, pk, r1, p1, 
             r2, p2) <@ A(H,G,J).main();
    BW.pbb <- map (fun (x: upkey * cipher * sign), 
                    (get_pub_data x, hash x)) (USan (map remi bb));
    BPS.s <- (pk, BW.pbb, r2);
    BPS.p <- Some p2;
    return ((pk, BW.pbb, r1), p1);
  }

  proc a2() ={
    return (BPS.s, oget BPS.p);
  }
}.*)

local lemma tally_uniq_pok 
            (A <: TallyUniq_Adv_2 {E, Ex, JRO.RO, HRO.RO, GRO.RO, Vz, BPS}) &m:
  Pr[TallyUniqE_Exp(B'(E,Pz,Vz,C(E,SS),SS), A(B'(E,Pz,Vz,C(E,SS),SS)), Ex, DecRel'(E,HRO.RO), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res ] <=
  Pr[PoK_Exp(Vz, DecRel'(E, HRO.RO), Ex, PoK_TU(A(B'(E,Pz,Vz,C(E,SS),SS)), HRO.RO, JRO.RO), 
         GRO.RO).main() @ &m :  res].
proof.  
  byequiv (_: ={glob A, glob E, glob Vz, glob Ex} ==> _ )=>//=.
  proc.
  inline PoK_TU(A(B'(E,Pz,Vz,C(E,SS),SS)), HRO.RO, JRO.RO, PoK_Exp(Vz, DecRel'(E, HRO.RO), 
                Ex, PoK_TU(A(B'(E,Pz,Vz,C(E,SS),SS)), HRO.RO, JRO.RO), GRO.RO).PO).main 
    B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  wp.
  conseq (_: _ ==> ={ rel1, rel2, ev1, ev2})=>//=; first by smt.
  call (_: ={glob E, glob HRO.RO});
    first by sim.
  call (_: ={glob GRO.RO}); first by sim.
  inline B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).verify.
  wp; call (_: ={glob GRO.RO}); first by sim.
  wp; call (_: ={glob E, glob HRO.RO});
    first by sim.
  call (_: ={glob GRO.RO}); first by sim.
  wp; call (_: ={glob GRO.RO}); first by sim.
  wp; call (_: ={glob HRO.RO, glob GRO.RO, glob JRO.RO, BPS.io}); 
    first 3 by sim.
  swap{2} 2 -1.
  call (_: true ==> ={glob JRO.RO});
    first by sim.
  call (_: true ==> ={glob GRO.RO, BPS.io});
    first by sim.
  call (_: true ==> ={glob HRO.RO});
    first by sim. 
  by auto.  
qed.

local lemma bul &m:
  Pr[Buls.main() @ &m : res] = 0%r.
proof.
  by byphoare=>/>; proc.
qed.

local lemma Ax_ll (A <: TallyUniq_Adv_2 {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS,C}):
    islossless A(B'(E, Pz, Vz,C(E,SS), SS), HRO.RO, 
                 TallyUniqE_Exp(B'(E, Pz, Vz, C(E,SS),SS), A(B'(E, Pz, Vz, C(E,SS),SS)), 
                 Ex, DecRel'(E, HRO.RO), HRO.RO, GRO.RO, JRO.RO).PO, JRO.RO).main.
proof.
  cut All:= (A_ll A (B'(E, Pz, Vz, C(E,SS),SS)) HRO.RO 
                (<:TallyUniqE_Exp(B'(E, Pz, Vz,C(E,SS), SS), 
                                  A(B'(E, Pz, Vz, C(E,SS),SS)), Ex, DecRel'(E, HRO.RO), 
                                  HRO.RO, GRO.RO, JRO.RO).PO) 
                JRO.RO HRO.RO_o_ll _ JRO.RO_o_ll).  
    by proc; inline*; wp.
  by proc* ;call{1} All. 
qed.

local lemma tally_uniq_result_uniq 
            (A <: TallyUniq_Adv_2 {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS,C}) &m:
  Pr[TallyUniqE_Exp(B'(E,Pz,Vz,C(E,SS),SS), A(B'(E,Pz,Vz,C(E,SS),SS)), Ex, DecRel'(E,HRO.RO), 
         HRO.RO, GRO.RO,JRO.RO).main() @ &m :  BPS.b ] =  Pr[Buls.main() @ &m : res].
proof.  
  byequiv=>/>.
  proc.
  wp. 
  seq 5 0: (true).  
    call{1} (Ax_ll A). 
    inline*; wp. 
    while{1} (true) (card work0{1}); progress.  
      by auto; smt. 
    wp; while{1} (true) (card work1{1}); progress.  
      by auto; smt. 
    wp; while{1} (true) (card work{1}); progress.  
      by auto; smt.  
    by auto; smt. 
  inline*; wp.
  wp; while{1} (0 <= i0{1} /\
                let f = fun (x: upkey*cipher*sign),
                        oget (dec_cipher wit0{1} x.`1 x.`2 HRO.RO.m{1}) in
               ubb0{1} = map (f\o fst) (take i0{1} stm0.`2{1})) 
               (size stm0.`2{1} - i0{1}); progress.
    wp; sp.
    exists* (glob E), wit0, b2; elim*=> ge skx bx.
    call{1} (dec_Dec_one ge skx bx.`1 bx.`2). 
    auto =>/>; progress.
    + smt. 
    + rewrite (take_nth witness). 
        by rewrite H0 H1. 
      by rewrite map_rcons /(\o) cats1 -H //=. 
    + smt.
  wp; call{1} (Ex_ll GRO.RO GRO.RO_o_ll). 
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
  wp; while{1} (0 <= i{1} /\
                let f = fun (x: upkey*cipher*sign),
                        oget (dec_cipher wit{1} x.`1 x.`2 HRO.RO.m{1}) in
               ubb{1} = map (f\o fst) (take i{1} stm.`2{1})) 
               (size stm.`2{1} - i{1}); progress.
    wp; sp.
    exists* (glob E), wit, b0; elim*=> ge skx bx.
    call{1} (dec_Dec_one ge skx bx.`1 bx.`2). 
    auto =>/>; progress.
    + smt. 
    + rewrite (take_nth witness). 
        by rewrite H0 H1. 
      by rewrite map_rcons /(\o) cats1 -H //=. 
    + smt.
  wp; call{1} (Ex_ll GRO.RO GRO.RO_o_ll).
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
  auto=>/>; progress.
  + smt (take0).
  + smt (take_oversize).
  + smt (take0).
  + smt (take_oversize).
  + pose L:= (map ((fun (x : upkey * cipher * sign) => 
                    (get_pub_data x, hash hk{1} x)) \o remi) (USanitize bb{1})).
    pose g1:= (fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher result x.`1 x.`2 HRO.RO.m{1})).
    pose g2:=(fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher result0 x.`1 x.`2 HRO.RO.m{1})).
    rewrite ?take_oversize; first 2 by smt.
    pose fst:= fun (p : (upkey * cipher * sign) * hash_out) => p.`1. 
    case (get_pk result = get_pk result0).
    + move => Hpk. 
      cut Hsk:= get_pk_inj _ _ Hpk.
      rewrite /g1 Hsk -/g2.
      smt. 
    - move => Hpk.
      have Hsk: result <> result0.
        cut Hsk:= get_pk_inj result result0.
        smt.
      case (pk{1} = get_pk result).
      + move => Hpk1.
        have ->: !pk{1} = get_pk result0. smt.
        rewrite neqF ?neg_and //=.
      - move => Hpk1.
        by rewrite neqF ?neg_and //=.
qed.


local lemma tally_uniq_result_uniq' 
            (A <: TallyUniq_Adv_2 {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS,C}) &m:
  Pr[TallyUniqE_Exp(B'(E,Pz,Vz,C(E,SS),SS), A(B'(E,Pz,Vz,C(E,SS),SS)), Ex, DecRel'(E,HRO.RO), 
         HRO.RO, GRO.RO,JRO.RO).main() @ &m :  BPS.b ] = 0%r.
proof.  
  by rewrite (tally_uniq_result_uniq A &m) (bul &m). 
qed.


lemma tally_uniq_dreg 
      (A <: TallyUniq_Adv_2 {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS,C}) &m:
  Pr[TallyUniq_Exp(B'(E,Pz,Vz,C(E,SS),SS), A(B'(E,Pz,Vz,C(E,SS),SS)), 
         HRO.RO, GRO.RO,JRO.RO).main() @ &m :  res] <=
  Pr[PoK_Exp(Vz, DecRel'(E, HRO.RO), Ex, PoK_TU(A(B'(E,Pz,Vz,C(E,SS),SS)), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m :  res].
proof.
  cut B1:= tally_uniq_witness A &m.
  cut B2:= tally_uniq_pok A &m.
  cut B3:= tally_uniq_result_uniq' A &m.
  by smt.
qed.

section Test_DREG.
declare module Adreg:  DREG_Adv     {BPS, Ex, SS, BW, BSC, HRO.RO, JRO.RO, GRO.RO, Pz, Vz, C, E}.

local module BTU'(A: DREG_Adv, V: VotingScheme') = BTall'(V,A).

lemma bound_tally_uniq' &m:
   Pr[TallyUniq_Exp (B'(E,Pz,Vz,C(E,SS),SS), BTall'(B'(E,Pz,Vz,C(E,SS),SS),Adreg), 
        HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[PoK_Exp(Vz, DecRel'(E, HRO.RO), Ex, 
             PoK_TU(BTall'(B'(E, Pz, Vz, C(E,SS), SS), Adreg), 
                    HRO.RO, JRO.RO), GRO.RO).main () @ &m :  res].
proof.
  have ->: Pr[TallyUniq_Exp(B'(E,Pz,Vz,C(E,SS),SS), 
                   BTall'(B'(E,Pz,Vz,C(E,SS),SS), Adreg), 
                   HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res]=
           Pr[TallyUniq_Exp(B'(E,Pz,Vz,C(E,SS),SS), 
                   BTU'(Adreg, B'(E,Pz,Vz,C(E,SS),SS)), 
                   HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res];
    first by byequiv =>//=; sim.
  have ->: Pr[PoK_Exp(Vz, DecRel'(E, HRO.RO), Ex, 
                   PoK_TU(BTall'(B'(E,Pz,Vz,C(E,SS),SS), Adreg), 
                   HRO.RO, JRO.RO), GRO.RO).main () @ &m :  res] =
           Pr[PoK_Exp(Vz, DecRel'(E, HRO.RO), Ex, 
                   PoK_TU(BTU'(Adreg, B'(E,Pz,Vz,C(E,SS),SS)), 
                   HRO.RO, JRO.RO), GRO.RO).main () @ &m :  res];
    first by byequiv =>//=; sim. 
  by rewrite (tally_uniq_dreg  (<: BTU'(Adreg)) &m).
qed.

end section Test_DREG.

end section Bound_TallyUniq.


section Bound_Acc.
(* ** abstract algorithms ** *)
declare module C: ValidInd        . 
declare module E: Scheme          {BSC, BW, HRO.RO, GRO.RO, JRO.RO}.
declare module Pz: Prover         {E, BW, BPS, BSC, HRO.RO, GRO.RO, JRO.RO}.
declare module Vz: Verifier       {Pz, E, BPS, BSC, BW, HRO.RO, GRO.RO, JRO.RO}.
declare module SS: SignScheme     {E, Pz, Vz, BW, GRO.RO, HRO.RO, JRO.RO, BSC}.


(* accuracy adversary with access to a voting scheme *)
module type Acc_Adv_2(V: VotingScheme', H : HOracle.ARO, 
                     G : GOracle.ARO, J: JOracle.ARO) = {
  proc main(sk : skey, ul : (ident, upkey*uskey) map, 
            ull : (ident * uskey * upkey) list, hk: hash_key) 
           : (ident * upkey * cipher*sign) list 
}. 

(*declare module BA : Acc_Adv_2       {SS, E, BSC, HRO.RO, GRO.RO, JRO.RO, Pz, Vz}. *)

(* ** ASSUMPTIONS ** *)
(* ** start *)

(* lossless assumptions *)
  axiom Ee_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).enc.
  axiom Ed_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).dec.
  axiom Pz_ll  (G <: GOracle.ARO): 
    islossless G.o => islossless Pz(G).prove.
  axiom Vz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Vz(G).verify.

  axiom Ekgen_extractPk (H<: HOracle.ARO):
    equiv [E(H).kgen ~ E(H).kgen:  ={glob H, glob E} ==> 
          ={glob H, glob E,  res} /\ 
          res{2}.`1 = get_pk res{2}.`2].

(*** *)
local lemma R_ll (H<: HOracle.ARO):
    islossless H.o => islossless DecRel'(E, H).main.
proof.
  move => Ho.
  proc.
  wp; while{1} (true) (size stm.`2 -i); progress.
    wp; call{1} (Ed_ll H Ho).
    by auto; smt.
  by auto; smt.  
qed.   

(*
module BCorr_Adv(E: Scheme, B: Acc_Adv, H: HOracle.Oracle, SS: SignScheme,
                 J: JOracle.Oracle, G: GOracle.ARO) ={
  proc main(): (pkey * (pub_data * hash_out) list * result) * skey  = {
    var ubb, i, b, m, id, bb,  upk, usk, pbb, ull,e;

    H.init();
    J.init();

    (BSC.pk, BSC.sk) <@ E(H).kgen();
    (* assign labels for ids *)
    BSC.uL <- empty;
    ull <- [];
    i <- 0;
    while (i < size (undup Voters)){
      id     <- nth witness (undup Voters) i;
      (usk,upk) <@ SS(J).kgen();
      BSC.uL.[id]<- (upk,usk);
      ull <- ull ++ [(id, (oget BSC.uL.[id]).`2,(oget BSC.uL.[id]).`1)];
      i      <- i + 1;
    }

    bb <@ B(H,G,J).main(BSC.sk, BSC.uL, ull);
  
    ubb  <- [];
    i   <- 0;
     while( i < size (map remi bb)){
      b  <- nth witness (map remi bb) i;
      m  <@ E(H).dec(BSC.sk, b.`1, b.`2);
      e  <@ SS(J).verify(b.`1, b.`2, b.`3);
      (* valid dec and sign *)
      if (m <>None /\ e){
        ubb <- ubb ++ [(b.`1, oget m)];
      }
      i  <- i + 1;
    }
    
    pbb <- map (fun (x: upkey * cipher * sign), 
                    (get_pub_data x, hash x)) (USan (map remi bb));

    return ((BSC.pk, pbb, Count (map snd (Sanitize ubb))), BSC.sk);
  }
}. *)

local lemma bound_acc (BA <: Acc_Adv_2 {SS, E, BSC, HRO.RO, GRO.RO, JRO.RO, Pz, Vz}) &m:
  Pr[Acc_Exp(B'(E,Pz,Vz,C,SS), BA(B'(E,Pz,Vz,C,SS)), 
             HRO.RO, GRO.RO, JRO.RO).main () @ &m : !res]<=
  Pr[PS.Correctness (DecRel'(E,HRO.RO), BCorr_Adv(E,BA(B'(E,Pz,Vz,C,SS)),
                                    HRO.RO, SS, JRO.RO), 
                  Pz, Vz, GRO.RO).main() @ &m: !res].
proof.
  byequiv=>//=.
  proc.
  inline BCorr_Adv(E, BA(B'(E,Pz,Vz,C,SS)), HRO.RO, SS, JRO.RO, GRO.RO).main
         B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).setup
         B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).tally
         B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).verify.

  seq 11 10 :( ={glob BA, glob SS, glob Vz, glob E, glob Pz, bb,
                HRO.RO.m, GRO.RO.m, JRO.RO.m}/\
              pk{1} = BSC.pk{2} /\ sk{1} = BSC.sk{2} /\
              pk{1} = get_pk sk{1}/\
              hk{1} = BSC.hk{2}).
    call(: ={glob HRO.RO, glob GRO.RO, glob JRO.RO});
      first 3 by sim. 
    while( ={i, glob SS, GRO.RO.m}/\
           uL{1} = BSC.uL{2} /\ uLL{1} = ull{2}).
      inline*; wp.
      call(: true). 
      by auto.
    wp; rnd; call{1} (Ekgen_extractPk HRO.RO). 
    swap{2} 1.
    call (_: true ==> ={glob JRO.RO});
      first by sim.
    call (_: true ==> ={glob GRO.RO});
      first by sim.
    call (_: true ==> ={glob HRO.RO});
      first by sim. 
    by auto.

  seq 10 7: (={glob Pz, glob Vz, GRO.RO.m, bb}/\
            pbb{1} = map (fun (x: upkey * cipher * sign), 
                    (get_pub_data x, hash BSC.hk{2} x)) (USan (map remi bb{1}))/\
            (pk1,pbb,r0){1} = s{2} /\ sk1{1} =w{2} /\ !b{2}/\ pk{1} = pk1{1}/\
             hk{1} = BSC.hk{2}).
   wp; call{2} (R_ll HRO.RO HRO.RO_o_ll). wp; progress.

   wp; while (={glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb}/\
              fbb{1} = map remi bb{2} /\ i0{1} = i{2} /\ 
              sk1{1} = BSC.sk{2}). 
     wp; call(: ={glob JRO.RO}); first by sim.
     call(: ={glob HRO.RO}); first by sim.
     by auto.
   by auto=>/>; progress. 
    
  if{2}=>//=.
  + inline*; wp. 
     call(: ={GRO.RO.m}); first by sim.
     wp; call(: ={GRO.RO.m}); first by sim.
     by auto. 
  - wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
    inline*; wp; call{1} (Pz_ll GRO.RO GRO.RO_o_ll).
    by auto.

qed. 

section Test_DREG.

local module BAC(A: DREG_Adv, V: VotingScheme') = BAcc'(V,A).

lemma bound_acc_dreg (A <: DREG_Adv {SS, E, C, BSC, HRO.RO, GRO.RO, JRO.RO, Pz, Vz})&m:
  Pr[Acc_Exp (B'(E,Pz,Vz,C,SS), BAcc'(B'(E,Pz,Vz,C,SS),A), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m: !res]<=
  Pr[PS.Correctness (DecRel'(E,HRO.RO), BCorr_Adv(E,BAcc'(B'(E,Pz,Vz,C,SS),A),
                                    HRO.RO, SS, JRO.RO), 
                  Pz, Vz, GRO.RO).main() @ &m: !res].
proof.
  by rewrite (bound_acc (<: BAC(A)) &m). 
qed.
end section Test_DREG.

end section Bound_Acc.  
