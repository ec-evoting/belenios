require import FMap Int Bool AllCore.

type ident, label, pkey, skey.

(* Global state shared between strong consistency/correctness oracles and game *)
module BSC = {
  var uL    : (ident, label) map
  var valid : bool
  var qt    : int
  var pk    : pkey
  var sk    : skey
  var ibad  : int option
}.