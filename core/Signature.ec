require import Bool AllCore List FSet Finite Distr DInterval List.
require (*  *) ROM FinType Ex_Plug_and_Pray.

(* ---------------------------------------------------------------------- *)
(* Preliminaries *)

(* types *)
type uskey, upkey. (* signing and verification keys *)
type message.      (* message   *)
type string.       (* string     *) 
type ident.

(* input and output for random oracle S *)
type s_in, s_out. 

(* distribution for random oracle S *)
op dout: s_out distr.  
op get_upk: uskey -> upkey.
op Voters: ident list.

(* random oracle *)
clone include ROM.Types with
  type from             <- s_in,
  type to               <- s_out,
  op   dsample(x: s_in) <- dout.

(* ---------------------------------------------------------------------- *)
(* Definitions *)

(* signature scheme *)
module type SignScheme(O: ARO) ={
  proc kgen()      : uskey * upkey { }
  proc sign(usk: uskey, m: message): string {O.o}
  proc verify(upk: upkey, m: message, s: string): bool {O.o}
}.


(* ---------------------------------------------------------------------- *)
(* Security concepts *)

module type Corr_Adv (O: ARO)={
  proc main(): message
}.

(* 1. Correctness property *)
module Correctness(SS: SignScheme, A: Corr_Adv, O : Oracle) = { 

  proc main() : bool = { 
    var usk, upk, s, b, m;
                  O.init();
    (usk, upk) <@ SS(O).kgen();
    m          <@ A(O).main();
    s          <@ SS(O).sign(usk,m);
    b          <@ SS(O).verify(upk,m,s);

    return b;
  }
}.

module WU ={
  var keyL : (uskey * upkey) list
  var sigL : (upkey * message * string) list
  var corL : (uskey * upkey) list
  var ibad : int
  var bupk : upkey option
  var qK   : int
  var uspk : (uskey * upkey) option
  var bad  : bool
}.
  
module type EUF_Oracle ={
  proc o (x: s_in): s_out 
  proc sign (m: message): string option
}.

module EUF_Oracle (SS: SignScheme, O: ARO)={
   proc o = O.o
   proc sign (m: message): string option ={
     var s, s';
     s <- None;
     if (WU.uspk <> None ){
       s' <@ SS(O).sign ((oget WU.uspk).`1, m);
       s <- Some s';
       WU.sigL <- WU.sigL ++ [((oget WU.uspk).`2, m,s')];
     }
     return s;
   }
}.

module type EUF_Adv (O: EUF_Oracle) ={
  proc a1 (): unit
  proc a2(upk: upkey): message * string {O.o O.sign}
}.
 
module EUF_Exp (SS: SignScheme, A: EUF_Adv, O: Oracle) ={
   module OO = EUF_Oracle (SS, O)
   proc main(): bool ={
     var  m, s, ev, usk, upk;
     WU.sigL <- [];
     WU.uspk <- None;

     O.init();
     A(OO).a1();

     (usk, upk) <@ SS(O).kgen();
     WU.uspk <- Some (usk, upk);
     (m,s)   <@  A(OO).a2((oget WU.uspk).`2);

     ev <@ SS(O).verify ((oget WU.uspk).`2,m,s);
     return ev /\ 
            (!((oget WU.uspk).`2,m,s) \in WU.sigL);
     }
}.

(* 3. Existential unforgeable *)
module BU ={
  var keyL : (ident* uskey * upkey) list
  var sigL : (ident* upkey * message * string) list
  var corL : (ident* uskey * upkey) list
  var bad  : bool
  var bow  : bool
  var ibad : int
  var jbad : int
  var upk  : upkey option
  var usk  : uskey option
  var uspk : (ident * uskey * upkey) option
  var qK   : int
  var id   : ident 
  var i    : int
  var qCo  : int
  var ui   : int
}.
op fst3 (x: ident * uskey * upkey) = x.`1.
op thr3 (x: ident * uskey * upkey) = x.`3.
op ft3 (x: ident * uskey * upkey) =  (fst3 x,thr3 x).
op fst4 (x: ident * upkey * message * string) = x.`1.

module type MEUF_Oracle ={
   proc o (x: s_in): s_out (*
   proc keygen(id:ident): upkey option *)
   proc sign (id:ident, m: message): string option
   proc corrupt(id:ident): uskey option
}.

module MEUF_Oracle (SS: SignScheme, O: ARO)={
   proc o = O.o
   proc sign (id: ident, m: message): string option ={
     var s, s', usk, upk',id';
     s <- None;
     if (id \in map fst3 BU.keyL){
       (* there should be some key for this id *)
       (id',usk,upk') <- nth witness BU.keyL (index id (map fst3 BU.keyL));
       s' <@ SS(O).sign (usk, m);
       s <- Some s';
       BU.sigL <- BU.sigL ++ [(id,upk',m,s')];
     }
     return s;
   }
   proc corrupt(id: ident): uskey option ={
     var usk, usk', upk', id';
     usk <- None;
     if (id \in map fst3 BU.keyL /\ !id \in map fst3 BU.corL){
       (id',usk',upk') <- nth witness BU.keyL (index id (map fst3 BU.keyL));
       usk <- Some usk';
       BU.corL <- BU.corL ++ [(id,usk',upk')];
       BU.qCo <- BU.qCo + 1;
       BU.sigL <- filter (predC1 id \o fst4) BU.sigL;
       (* if (!id \in map fst3 BU.corL){
         BU.qCo <- BU.qCo + 1;
       }*)
     }
     return usk;
   }
}.

module type MEUF_Adv (O: MEUF_Oracle) ={
  (* proc a1(): unit {O.keygen} *)
  proc main(ul: (ident * upkey) list): upkey * message * string 
}.

module type MEUF_Adv' (SS: SignScheme, O: MEUF_Oracle) ={
  (* proc a1(): unit {O.keygen} *)
  proc main(ul: (ident * upkey) list): upkey * message * string 
}. 
 
module MEUF_Exp (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, id, i, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.qK  <- 0;
     BU.qCo <- 0;
     i <- 0;

     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(OO).kgen();
       BU.keyL<- BU.keyL ++ [(id, usk,upk)];
       i <- i +1;
     }

     (u,m,s) <@A(OO).main(map ft3 BU.keyL);

     ev <@ SS(O).verify (u,m,s);
     return ev /\ 
            (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                   (x.`2,x.`3,x.`4))
                              BU.sigL) /\                    
            (u \in        (map thr3 BU.keyL)) /\
            (let a = nth witness BU.keyL (index u (map thr3 BU.keyL)) in
              !a.`1 \in       (map fst3 BU.corL));
     }
}.

(* ---------------------------------------------------------------------- *)
(* Typecheck advantage *)

section.
  require import Real.

  declare module O : Oracle.
  declare module SS : SignScheme. 
  declare module A  : MEUF_Adv.
  declare module B  : Corr_Adv.

  local lemma corr  &m: 
    exists eps,
      0%r < eps /\
      Pr[Correctness(SS,B,O).main() @ &m: res ] >= 1%r - eps by [].

  local lemma meuf &m: 
    exists eps,
      0%r < eps /\
      Pr[MEUF_Exp(SS,A,O).main() @ &m: res] < eps by [].

end section.

section.
  require import Real.

  declare module O : Oracle {BU, WU}.
  declare module SS : SignScheme {BU, WU, O}. 
  declare module A  : MEUF_Adv' {BU, WU, SS, O}.

  axiom O_o_ll: islossless O.o.
  axiom SSv_ll (O<: ARO): 
      islossless O.o => islossless SS(O).verify.
  axiom SSk_ll (SS<: SignScheme) (O<: ARO): 
      islossless O.o => islossless SS(O).kgen.
  axiom SSs_ll (SS<: SignScheme) (O<: ARO): 
      islossless O.o => islossless SS(O).sign.
  axiom A_ll (SS<: SignScheme{A}) (O <: MEUF_Oracle{A}):
    islossless O.o =>
    islossless SS(O).kgen =>
    islossless SS(O).sign =>
    islossless SS(O).verify=>
    islossless O.sign => 
    islossless O.corrupt => islossless A(SS,O).main.
  
  axiom size_Voters: 0< size (undup Voters).
  axiom uniq_get_upk (x y: uskey):
     get_upk x = get_upk y => x = y.
  axiom kgen_get_upk (O<: ARO):
    equiv [SS(O).kgen ~ SS(O).kgen:  ={glob O, glob SS} ==> 
          ={glob O, glob SS,  res} /\ 
          res{2}.`2 = get_upk res{2}.`1].
  axiom kgen_get_upk_1 (gs: (glob SS)) (O<: ARO):
    phoare [SS(O).kgen:  
            gs = (glob SS) ==> 
            gs = (glob SS) /\
            res.`2 = get_upk res.`1] = 1%r.
  
lemma get_upk_inj (L : (uskey * upkey) list) a b:
   (forall (x : uskey * upkey), x \in L => x.`2 = get_upk x.`1)=>
   a \in L => b \in L => snd a = snd b => a = b.
proof.
  move => HuL Ha Hb Hab.
  rewrite /snd //= in Hab.
  cut Ha2:= HuL a Ha.
  cut Hb2:= HuL b Hb.
  have ->: a = (a.`1,a.`2) by rewrite pairS.
  have ->: b = (b.`1,b.`2) by rewrite pairS.
  by rewrite (uniq_get_upk a.`1 b.`1 _); 
       first rewrite -Ha2 -Hb2 Hab.
qed. 

lemma undup_no_mem['a] (C:'a list) c:
  !c \in C =>  undup (C ++ [c]) = undup C ++ [c].
proof. 
 by elim: C =>//=; smt.
qed.             

lemma undup_mem (C: 'a list) c:
  c \in C => perm_eq (undup (C ++ [c])) (undup C).
proof. 
  elim: C =>//=.
  move => d C Hp Hor. 
  case (c = d).
  + move => Heq.
    have ->: d \in C ++ [c] by smt.
    simplify.  
    case (d \in C).
    + move => HdC.
      smt.
    - move => HdC.
      have ->: undup (C ++ [c]) = undup C ++ [c]. 
        by rewrite (undup_no_mem C c); first by rewrite Heq HdC. 
      by smt.
  - move => Hdif.
    case (d \in C).           
    + move => HdC.      
      have ->: d \in C ++ [c]. smt.
      smt.
    - move => HdC.
      have ->:! d \in C ++ [c]. smt.
      smt.
qed. 

lemma perm_undup_mem (A B: 'a list) x:
  uniq B => perm_eq (undup A) B => x \in B =>
  perm_eq (undup (A++[x])) B.
proof.
  move => HuB HpAB HxB.
  have HxA: x \in A. smt.
  have HpA: perm_eq (undup (A ++ [x])) (undup A).
    by rewrite (undup_mem A x HxA).  
  by rewrite (perm_eq_trans _ _ _ HpA HpAB).
qed.  

lemma perm_undup_no_mem (A B: 'a list) x:
  uniq B => perm_eq (undup A) B => !x \in B =>
  perm_eq (undup (A++[x])) (B ++[x]).
proof.
  move => HuB HpAB HxB.
  have HxA: !x \in A. smt.
  have ->: (undup (A ++ [x])) = undup A ++ [x]. 
    by rewrite (undup_no_mem A x HxA).
  by rewrite (perm_cat2r [x] (undup A) B) HpAB.
qed. 

lemma nth_index_perm (L1 L2 : (uskey * upkey) list) x:
  uniq L2 => perm_eq (undup L1) L2 =>
  x \in map snd L1 =>
  (forall a, a \in L2 => snd a = get_upk (fst a)) =>
  nth witness L1 (index x (map snd L1)) =
  nth witness L2 (index x (map snd L2)).
proof.
  pose snd:= fun (p : uskey * upkey) => p.`2. 
  move => HuL2 HpL1L2 HxL1 HmL2.
  have HxL2: x \in map snd L2. smt.
  cut := HxL1; rewrite mapP; elim => y [HyL1 Hyx].
  have HyL2: y \in L2. smt.
  cut := HxL2; rewrite mapP; elim => z [HzL2 Hzx].
  cut Hyz:= get_upk_inj L2 y z HmL2 HyL2 HzL2 _.
    by rewrite -/snd -Hzx -Hyx.
  cut Hy_nth:= nth_index_map witness snd L1 y _ HyL1.  
    rewrite /snd.
    move => a b Ha Hb Hab2. 
    rewrite (get_upk_inj L1 a b _ Ha Hb Hab2).  
      smt.
    by done.
  rewrite Hyx Hy_nth -Hyx Hzx.
  cut Hz_nth:= nth_index_map witness snd L2 z _ HzL2.  
    rewrite /snd.
    move => a b Ha Hb Hab2. 
    by rewrite (get_upk_inj L2 a b HmL2 Ha Hb Hab2).  
  by rewrite Hz_nth Hyz.
qed. 

lemma nth_index_filter (L1 L2 : (uskey * upkey) list) x (a: uskey * upkey):
  filter (fun (x : uskey * upkey) => x.`2 <> a.`2) L1 =
  filter (fun (x : uskey * upkey) => x.`2 <> a.`2) L2 =>
  x <> a.`2 =>
  x \in map snd L1 =>
  (forall a, a \in L2 => snd a = get_upk (fst a)) =>
  (forall a, a \in L1 => snd a = get_upk (fst a)) =>
  nth witness L1 (index x (map snd L1)) =
  nth witness L2 (index x (map snd L2)).
proof.
  pose snd:= fun (p : uskey * upkey) => p.`2. 
  move=> Heq_fil Hxa Hmx HaL2 HaL1. 
  have HxL2: x \in map snd L2. 
    move: Hmx; rewrite mapP; elim => y [HyL1 Hyx].
    rewrite mapP. exists y; rewrite Hyx //=. 
    cut := mem_filter (fun (x0 : uskey * upkey) => x0.`2 <> a.`2) y L1.
    rewrite HyL1 //= -/snd -Hyx Hxa //=.
    rewrite Heq_fil. 
    smt.
  cut := Hmx; rewrite mapP; elim => y [HyL1 Hyx].
  cut := HxL2; rewrite mapP; elim => z [HzL1 Hzx].
  cut Hy_nth:= nth_index_map witness snd L1 y _ HyL1.  
    rewrite /snd.
    move => c d Ha Hb Hab2. 
    rewrite (get_upk_inj L1 c d _ Ha Hb Hab2).  
      smt.
    by done.
  rewrite Hyx Hy_nth -Hyx Hzx.
  cut Hz_nth:= nth_index_map witness snd L2 z _ HzL1.  
    rewrite /snd.
    move => c b Ha Hb Hab2. 
    by rewrite (get_upk_inj L2 c b _ Ha Hb Hab2).  
  rewrite Hz_nth. 
  have ->: y = (y.`1, y.`2) by rewrite pairS.
  have ->: z = (z.`1, z.`2) by rewrite pairS.
  rewrite (HaL2 z HzL1) (HaL1 y HyL1).
  smt.
qed. 

lemma perm_mem_1 (L1 L2: ('a * 'b) list) x:
  perm_eq (undup L1) L2 =>
  x \in map snd L2 =>
  x \in map snd L1. 
proof.
  move => Hp HxL2.  
  move: HxL2; rewrite mapP //=. 
  elim => y [HyL2 Hxy]. rewrite mapP.
  exists y. rewrite Hxy //=.  
  smt.
qed. 

lemma perm_mem_2 (L1 L2: ('a * 'b) list) x:
  perm_eq (undup L1) L2 =>
  x \in map snd L1 =>
  x \in map snd L2. 
proof.
  move => Hp HxL1.  
  move: HxL1; rewrite mapP //=. 
  elim => y [HyL1 Hxy]. rewrite mapP.
  exists y. rewrite Hxy //=.  
  smt.
qed. 

lemma perm_no_mem (L1 L2: ('a * 'b) list) x:
  perm_eq (undup L1) L2 =>
  !x \in map snd L2 =>
  !x \in map snd L1. 
proof.
  move => Hp HxL2.  
  move: HxL2; rewrite mapP negb_exists //=; move => HxL2. 
  rewrite mapP negb_exists //=; move => y. 
  cut := HxL2 y.
  smt.
qed. 

lemma perm_no_mem_1 (L1 L2: 'a list) x:
  perm_eq (undup L1) L2 =>
  !x \in  L2 =>
  !x \in  L1. 
proof.
  move => Hp HxL2. smt. 
qed. 

lemma mem_nth_index_snd (L : (uskey * upkey) list) x:
  x \in L =>
  (forall a, a \in L => snd a = get_upk (fst a)) =>
  (nth witness L (index x.`2 (map snd L))).`1 = x.`1.
proof. 
  move =>HxL HaL.
  have ->: nth witness L (index x.`2 (map snd L)) = x.
    cut := nth_index_map witness snd L x _ HxL.
    + move => a b Ha Hb Hab. 
      by rewrite (get_upk_inj L a b HaL Ha Hb).
    by done. 
  by done.
qed. 

(* PLAN:
   - step 1: ensure that keys are uniq
      -> break unforgeability of single scheme
   - step 2: find the key that produces the fake
      -> break unforgeability of single scheme
      +> is important to have uniq keys, otherwise 
         he may corrupt the duplicate and not the key we are targeting
         and win the experiment 
         [this should be impossible-> see step 1] 
*)

(* Step 1: ensure the keys are uniq, otherwise break unforgeability, 
    by just calling the keygen again, to obtain the secret key *)

op rid (x: ident*uskey*upkey) = (x.`2,x.`3).
module MEUF_Exp_pre_bow (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, id, i, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.bow  <- false;
     BU.qK   <- 0;
     BU.ibad <- 0;
     i <- 0;

     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(OO).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       i <- i +1;
     }
     BU.bow <- !uniq (map rid BU.keyL);
              
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s);
     return ev /\ 
            (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                   (x.`2,x.`3,x.`4))
                              BU.sigL) /\                    
            (u \in        (map thr3 BU.keyL)) /\
            (let a = nth witness BU.keyL (index u (map thr3 BU.keyL)) in
              !a.`1 \in       (map fst3 BU.corL));
     }
}.

lemma meuf_pre_bow &m: 
  Pr[MEUF_Exp(SS,A(SS),O).main() @ &m: res] <=
  Pr[MEUF_Exp_pre_bow(SS,A(SS),O).main() @ &m: res /\ !BU.bow] + 
  Pr[MEUF_Exp_pre_bow(SS,A(SS),O).main() @ &m: BU.bow].
proof.
  byequiv (: _ ==> res{1} => res{2}) =>//=; last by smt.
  proc. 
  wp.
  call(: ={glob O}); first by sim.
  call(: ={glob SS, glob O, BU.keyL, BU.corL, BU.sigL});
    first 3 by sim.
  wp; while (={ i, BU.keyL, glob SS}); first by sim.
  by call(: true); auto.  
qed.

module MEUF_Exp_bow (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, id, i, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.bow  <- false;
     BU.qK   <- 0;
     BU.ibad <- 0;
     i <- 0;

     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(OO).kgen();
       if (!(usk, upk) \in map rid BU.keyL){
         BU.keyL <- BU.keyL ++[(id,usk,upk)];
       }elif(!BU.bow){
         BU.bow <- true;
         BU.ibad<- i;
       }
       i <- i +1;
     }
              
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s);
     return ev /\ 
            (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                   (x.`2,x.`3,x.`4))
                              BU.sigL) /\                    
            (u \in        (map thr3 BU.keyL)) /\
            (let a = nth witness BU.keyL (index u (map thr3 BU.keyL)) in
              !a.`1 \in       (map fst3 BU.corL));
     }
}.

lemma meuf_bow &m: 
  Pr[MEUF_Exp_pre_bow(SS,A(SS),O).main() @ &m: BU.bow] <= 
  Pr[MEUF_Exp_bow(SS,A(SS),O).main() @ &m: BU.bow /\ 0<= BU.ibad < size (undup Voters)].
proof.
  byequiv =>/>.
  proc. 
  seq 10 9: ( (BU.bow{1} => BU.bow{2})/\
               0<= BU.ibad{2} < size (undup Voters)).
  wp; while (={i, glob SS} /\ 0 <= i{1}/\
             perm_eq (undup (map rid BU.keyL{1})) 
                     (map rid BU.keyL{2}) /\
             0<= BU.ibad{2} < size (undup Voters)/\
             uniq (map rid BU.keyL{2})/\
             (!uniq (map rid BU.keyL{1}) => BU.bow{2})).
    wp; call(: true); wp.
    auto=>/>; progress.
    + smt. 
    + rewrite !map_cat /rid //= -/rid.
      by rewrite perm_undup_no_mem.
    + by rewrite map_cat /rid //= -/rid cat_uniq H3 //=.
    + cut Hm:= perm_no_mem_1 (map rid BU.keyL{1}) (map rid BU.keyL{2}) _ H0 H6. 
      move: H7; rewrite map_cat /rid //= -/rid //= cat_uniq //=.
      by rewrite negb_and //= Hm //=. 
    + smt. 
      rewrite map_cat /rid //= -/rid.
      by rewrite perm_undup_mem.
    + smt.
    + rewrite map_cat /rid //= -/rid.
      by rewrite perm_undup_mem.
  call(: true); wp.
  auto=>/>; progress; smt.
  call{1} (SSv_ll O O_o_ll); call{2} (SSv_ll O O_o_ll). 
    call{1} (A_ll SS (<: MEUF_Exp(SS, A(SS), O).OO)  
                  O_o_ll (SSk_ll SS (MEUF_Oracle(SS, O)) O_o_ll) 
                  (SSs_ll SS (MEUF_Oracle(SS, O)) O_o_ll) 
                  (SSv_ll (MEUF_Oracle(SS, O)) O_o_ll) _ _). 
    + proc. 
      sp; if =>//=.
      by wp; call{1} (SSs_ll SS O O_o_ll); wp.      
    + by proc; auto.
    call{2} (A_ll SS (<: MEUF_Exp(SS, A(SS), O).OO)  
                  O_o_ll (SSk_ll SS (MEUF_Oracle(SS, O)) O_o_ll) 
                  (SSs_ll SS (MEUF_Oracle(SS, O)) O_o_ll) 
                  (SSv_ll (MEUF_Oracle(SS, O)) O_o_ll) _ _).
    + proc. 
      sp; if =>//=.
      by wp; call{1} (SSs_ll SS O O_o_ll); wp.      
    + by proc; auto.
  by auto; smt. 
qed.


module MEUF_Exp_ibad (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var id, i, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.bow  <- false;
     BU.qK   <- 0;
     BU.ibad <$ [0.. (size (undup Voters))-1];
     i <- 0;

     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(OO).kgen();
       if (!(usk, upk) \in map rid BU.keyL){
         BU.keyL <- BU.keyL ++[(id,usk,upk)];
       }elif(!BU.bow){
         BU.bow <- BU.ibad = i;
       }
       i <- i +1;
     }
     return BU.bow ;
     }
}.


clone  Ex_Plug_and_Pray as EPP with
  type tin  <- unit,
  type tres <- bool,
  op bound  <- size (undup Voters)
proof bound_pos. 
realize bound_pos. by apply size_Voters.  qed. 

(* needs bound on keygen calls qK <n *)
local lemma bow_guess &m:
  1%r/ (size (undup Voters)) %r * 
  Pr[MEUF_Exp_bow(SS,A(SS),O).main() @ &m: BU.bow /\ 0<= BU.ibad < size (undup Voters)] 
  = 
  Pr[EPP.Guess(MEUF_Exp_bow(SS,A(SS),O)).main() 
         @ &m: BU.bow /\ 0 <= BU.ibad < size (undup Voters) /\ fst res = BU.ibad ].
proof.
  (* print glob MEUF_Exp_bow(SS,A(SS),O). *)
  cut := EPP.PBound (MEUF_Exp_bow(SS,A(SS),O))
          (fun g b => let (ibad, bow, corL, sigL, qK, keyL, qCo, O, SS, A)= g in
             bow /\ 0<= ibad < size (undup Voters))
          (fun g b => let (ibad, bow, corL, sigL, qK, keyL, qCo, O, SS, A)= g in
             ibad) () &m. 
  simplify. 
  move => ->. 
  rewrite Pr[mu_eq]; progress; smt. 
qed.


lemma guess_ibad &m: 
  Pr[EPP.Guess(MEUF_Exp_bow(SS,A(SS),O)).main() 
         @ &m: BU.bow /\ 0 <= BU.ibad < size (undup Voters) /\ fst res = BU.ibad ] <=
  Pr[MEUF_Exp_ibad(SS,A(SS),O).main() @ &m: BU.bow].
proof.
  byequiv =>//=.
  proc.
  inline MEUF_Exp_bow(SS, A(SS), O).main.
  wp. 
  call{1} (SSv_ll O O_o_ll). 
  call{1} (A_ll SS (<: MEUF_Exp(SS, A(SS), O).OO)  
                  O_o_ll (SSk_ll SS (MEUF_Oracle(SS, O)) O_o_ll ) 
                  (SSs_ll SS (MEUF_Oracle(SS, O)) O_o_ll) 
                  (SSv_ll (MEUF_Oracle(SS, O)) O_o_ll) _ _).  
    + proc. 
      sp; if =>//=.
      by wp; call{1} (SSs_ll SS O O_o_ll); wp.      
    + by proc; auto.
  progress.
  while ( ={glob SS, BU.keyL} /\ i0{1} = i{2}/\
          (* 
          (BU.bow{1} /\ ={BU.ibad} => BU.bow{2})*)
          (={BU.ibad} => ={BU.bow})).
    seq 2 2: ( ={glob SS, BU.keyL, id, usk, upk}/\ 
               i0{1} = i{2} /\ 
              (={BU.ibad} => ={BU.bow})).
      by call(: true); wp.
    if =>//=.
      by auto.
    if{1} =>//=.
    + auto; progress. smt.
    auto; progress. smt.
  call(: true).
  wp; rnd; wp.
  auto; progress. smt.
qed.

module MEUF_Exp_upto (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var id, i, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.bow  <- false;
     BU.qK   <- 0;
     BU.ibad <$ [0.. (size (undup Voters))-1];
     i <- 0;

     O.init();
     while (i < BU.ibad){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(OO).kgen();
       if (!(usk, upk) \in map rid BU.keyL){
         BU.keyL <- BU.keyL ++[(id,usk,upk)];
       }
       i <- i +1;
     }
     (usk, upk) <@ SS(O).kgen();
     BU.bow <- (usk, upk) \in map rid BU.keyL;
     return BU.bow ;
     }
}.

lemma ibad_upto &m: 
  Pr[MEUF_Exp_ibad(SS,A(SS),O).main() @ &m: BU.bow] <=
  Pr[MEUF_Exp_upto(SS,A(SS),O).main() @ &m: BU.bow].
proof.
  byequiv =>//=.
  proc.
  splitwhile{1} 9: i < BU.ibad.
  seq 9 9 : (={i,glob SS, BU.ibad, BU.keyL, BU.bow}/\
             i{1} = BU.ibad{1}/\!BU.bow{1}/\
             BU.ibad{1} < size (undup Voters)).
    while (={i, glob SS, BU.ibad, BU.keyL, BU.bow}/\
            !BU.bow{1}/\
            0<= i{1}<= BU.ibad{1} /\
            BU.ibad{1}\in [0..size (undup Voters) - 1]).
    + wp; call(: true); wp.
      by auto=>/>; progress; smt. 
    call(: true); wp; rnd; wp.
    by auto=>/>; progress; smt. 
  unroll{1} 1.
  seq 1 2: ( ={BU.bow} /\
             BU.ibad{1} < i{1} ).
    rcondt{1} 1; progress.
      auto=>/>.
    call(: true); wp. 
    by auto; progress; smt. 
  while{1} (={BU.bow} /\
            BU.ibad{1} < i{1}) (size (undup Voters) -i{1}); progress. 
    wp; call{1} (SSk_ll SS (<:MEUF_Exp_ibad(SS, A(SS), O).OO) O_o_ll).
    wp; progress.
    by auto; progress; smt. 
  auto; progress.  smt.
qed.

module MEUF_Exp_corr (SS: SignScheme, C: Corr_Adv, O: Oracle) ={
   module OO = EUF_Oracle (SS, O)
   proc main(): bool ={
     var id, i, usk, upk, m, s;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     WU.sigL <- [];
     BU.bow  <- false;
     WU.uspk <- None;
     BU.qK   <- 0;
     BU.ibad <$ [0.. (size (undup Voters))-1];
     i <- 0;

     O.init();
     while (i < BU.ibad){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(OO).kgen();
       if (!(usk, upk) \in map rid BU.keyL){
         BU.keyL <- BU.keyL ++[(id,usk,upk)];
       }
       i <- i +1;
     }
     (usk, upk) <@ SS(O).kgen();
     BU.bow <- (usk, upk) \in map rid BU.keyL;

     m          <@ C(O).main();
     s          <@ SS(O).sign(usk,m);
     BU.bad     <@ SS(O).verify(upk,m,s);
     
     return BU.bow ;
     }
}.

lemma upto_corr (C<: Corr_Adv {BU}) &m: 
  (islossless C(O).main) =>
  Pr[MEUF_Exp_upto(SS,A(SS),O).main() @ &m: BU.bow]<=
  Pr[MEUF_Exp_corr(SS,C,O).main() @ &m: BU.bow /\ !BU.bad] +
  Pr[MEUF_Exp_corr(SS,C,O).main() @ &m: BU.bow /\ BU.bad].
proof.
  move => C_ll.
  byequiv (_: ={glob SS, glob O} ==> ={BU.bow})=>//=; last by smt. 
  proc.
  call{2} (SSv_ll O O_o_ll); call{2} (SSs_ll SS O O_o_ll).
  call{2} C_ll.
  by wp; sim. 
qed. 

lemma corr_neg (C<: Corr_Adv {BU, O, SS, WU}) &m: 
  (islossless C(O).main) =>
  Pr[MEUF_Exp_corr(SS,C,O).main() @ &m: BU.bow /\ !BU.bad] <= 
  Pr[Correctness(SS,C,O).main() @ &m: !res ].
proof.
  move => C_ll.
  byequiv (_: ={glob SS, glob O, glob C} ==> _)=>/>. 
  proc.
  call (: ={glob O}); first by sim.
  call (: ={glob O}); first by sim.
  call(: ={glob O}); first by sim.
  wp; call(: true).
  exists * (glob SS){1}; elim* => gs.
  while{1} ( gs = (glob SS){1}) (BU.ibad{1} -i{1}); progress.
    wp; call{1} (kgen_get_upk_1 gs (<:MEUF_Exp_corr(SS, C, O).OO)).
    by auto; progress; smt.
  call(: true). 
  by auto; progress; smt.
qed. 


module (BE (SS: SignScheme, A: Corr_Adv): EUF_Adv) (OO: EUF_Oracle) ={
  proc a1 (): unit ={
   var i, id, usk, upk;
   BU.keyL <- [];
   BU.sigL <- [];
   BU.corL <- [];
   BU.bow  <- false;
   BU.ibad <$ [0.. (size (undup Voters))-1];
   i <- 0;

   while (i < BU.ibad){
     id <- nth witness (undup Voters) i;
     (usk,upk)<@SS(OO).kgen();
     if (!(usk, upk) \in map rid BU.keyL){
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
     }
     i <- i +1;
   }
  }
  proc a2(upk: upkey): message * string ={
    var id, usk', upk', m, s;
    (id, usk', upk') <- nth witness BU.keyL 
                        (index upk (map thr3 BU.keyL));
     m <@ A(OO).main();
     s <@ SS(OO).sign(usk', m);
     return (m,s);    

  }
}.

lemma uniq_rid (L : (ident * uskey*upkey) list) y u:
   u \in L => y \in L =>
   rid u = rid y =>
   uniq (map rid L) => u = y.
proof.
  move => HuL HyL Huy HL.
  elim: L HuL HyL HL =>//=.
  move => w L HuyL Hu Hy HL.
  case (! (rid w \in map rid L)). 
    smt.
  simplify. smt.
qed.


lemma corr_bound (C<: Corr_Adv {O, WU, SS, BU}) &m:
 Pr[MEUF_Exp_corr(SS,C,O).main() @ &m: BU.bow /\ BU.bad] <= 
 Pr[EUF_Exp(SS,BE(SS,C),O).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  inline *. 
  seq 13 14 : ( (BU.bow{1} => (usk', upk'){2} = (usk,upk){2}) /\
                ={glob SS, glob O, glob C, BU.keyL, usk, upk} /\
                WU.sigL{2} = [] /\ WU.uspk{2} = Some(usk,upk){2}).
  wp; call(: true). progress. (*
  conseq ( ={glob SS, glob O, glob C, BU.keyL} /\
           WU.sigL{2} = [] /\
           (forall (z: uskey * upkey),
              z \in map rid BU.keyL{1} => 
  progress. *)
  while (={i, BU.keyL, glob SS, glob O, WU.uspk, WU.sigL, BU.ibad}/\
         uniq (map rid BU.keyL{1}) /\
         (forall a, a\in BU.keyL{1} => a.`3 = get_upk a.`2)). 
    wp; call{1} (kgen_get_upk (<:EUF_Oracle(SS, O)) ).  
    auto=>/>;progress. 
    + by rewrite map_cat cat_uniq H //=. 
    + move: H4; rewrite mem_cat //=.
      smt.
  swap{2} 3 6.
  call(: true).
  wp; rnd; wp.
  auto; progress. 
  + rewrite oget_some.
    pose a:= result_R.`1.
    pose b:= result_R.`2.
    have Hs: (index b (map (fun (x : ident * uskey * upkey) => x.`3) keyL_R)) < 
             size ((map (fun (x : ident * uskey * upkey) => x.`3) keyL_R)). 
      rewrite index_mem mapP.
      move: H5; rewrite mapP -/a -/b.
      elim => x [Hxm Hxab].
      exists x; rewrite Hxm //=.
      smt.
    move: H5. rewrite mapP. elim => x [Hxm Hxab].
    cut := nth_index_map witness (fun (x : ident * uskey * upkey) => x.`3) keyL_R x _ Hxm.
    + simplify; move => n m Hn Hm Hnm.
      have Hnm': n.`2 = m.`2. 
        cut Hn2:= H4 n Hn.
        cut Hm2:= H4 m Hm.
        smt. 
      have Hrnm: rid n = rid m by rewrite /rid Hnm Hnm'.      
     by rewrite (uniq_rid keyL_R m n Hn Hm Hrnm H3).
    have ->: ((fun (x0 : ident * uskey * upkey) => x0.`3) x) = b. smt. 
    move => Hx.
    rewrite Hx.    
    smt.
  + rewrite oget_some.
    pose a:= result_R.`1.
    pose b:= result_R.`2.
    have Hs: (index b (map (fun (x : ident * uskey * upkey) => x.`3) keyL_R)) < 
             size ((map (fun (x : ident * uskey * upkey) => x.`3) keyL_R)). 
      rewrite index_mem mapP.
      move: H5; rewrite mapP -/a -/b.
      elim => x [Hxm Hxab].
      exists x; rewrite Hxm //=.
      smt.
    move: H5. rewrite mapP. elim => x [Hxm Hxab].
    cut := nth_index_map witness (fun (x : ident * uskey * upkey) => x.`3) keyL_R x _ Hxm.
    + simplify; move => n m Hn Hm Hnm.
      have Hnm': n.`2 = m.`2. 
        cut Hn2:= H4 n Hn.
        cut Hm2:= H4 m Hm.
        smt. 
      have Hrnm: rid n = rid m by rewrite /rid Hnm Hnm'.      
     by rewrite (uniq_rid keyL_R m n Hn Hm Hrnm H3).
    have ->: ((fun (x0 : ident * uskey * upkey) => x0.`3) x) = b. smt. 
    move => Hx.
    rewrite Hx.    
    smt.

  case (BU.bow{1}).
  + call(: ={glob O}); first by sim.
    wp; call(: ={glob O}); first by sim.
    call(: ={glob O}); first by sim.
    auto; progress; smt.
  - conseq (: !BU.bow{1})=>//=.
    call{1} (SSv_ll O O_o_ll).
    call{2} (SSv_ll O O_o_ll).
    call{1} (SSs_ll SS O O_o_ll).
    wp; call{2} (SSs_ll SS (<: EUF_Oracle(SS, O)) O_o_ll).
    call(: ={glob O}); first by sim.
    by auto.
qed.

lemma meuf_part1 (C<: Corr_Adv {O, WU, SS, BU}) &m: 
  islossless C(O).main =>
  Pr[MEUF_Exp(SS,A(SS),O).main() @ &m: res] 
<=
  Pr[MEUF_Exp_pre_bow(SS,A(SS),O).main() @ &m: res /\ !BU.bow] +
(size (undup Voters))%r *
  Pr[EUF_Exp(SS,BE(SS,C),O).main() @ &m: res]+
(size (undup Voters))%r *
  Pr[Correctness(SS,C,O).main() @ &m: !res ].
proof.
move => C_ll.
have Hab: forall (a b c: real), 0%r < a => b<= c => a * b <= a* c. by smt.

cut := meuf_pre_bow &m.
cut := meuf_bow &m.
cut := bow_guess &m.
cut := guess_ibad &m.
cut := ibad_upto &m.
cut := upto_corr C &m C_ll.
cut:= corr_neg C &m C_ll.
cut:= corr_bound C &m.
  pose L:= Pr[MEUF_Exp(SS,A(SS),O).main() @ &m: res].
  pose R1:= Pr[MEUF_Exp_bow(SS,A(SS),O).main() @ &m: res].
  pose R2:= Pr[EUF_Exp(SS,BE(SS,C),O).main() @ &m: res].
  pose R3:= Pr[Correctness(SS,C,O).main() @ &m: !res ].
  pose A:= Pr[MEUF_Exp_corr(SS, C, O).main() @ &m : BU.bow /\ BU.bad].
  pose B:= Pr[MEUF_Exp_corr(SS, C, O).main() @ &m : BU.bow /\ !BU.bad].
  pose D:= Pr[MEUF_Exp_upto(SS, A(SS), O).main() @ &m : BU.bow].
  pose E:= Pr[MEUF_Exp_ibad(SS, A(SS), O).main() @ &m : BU.bow].
  pose F:= Pr[EPP.Guess(MEUF_Exp_bow(SS, A(SS), O)).main() @ &m :
   BU.bow /\ 0 <= BU.ibad < size (undup Voters) /\ res.`1 = BU.ibad].
  pose G:= Pr[MEUF_Exp_bow(SS, A(SS), O).main() @ &m :
   BU.bow /\ 0 <= BU.ibad < size (undup Voters)].
  move=> H1 H2 H3 H4 H5 H6 H7.
  have Hgf: G = (size (undup Voters))%r * F. 
    have Habc: forall (a b c: real), 0%r<a => 1%r/a * b = c => b = a* c by smt. 
    rewrite (Habc _ _ _ _ H6). smt.
    by done.
  have Hf: F <= (R2 + R3) by smt.
  have Hg: G <= (size (undup Voters))%r * R2 + (size (undup Voters))%r * R3.
    rewrite Hgf. 
    have Habc: forall (a b c: real), a * b + a* c = a * (b +c) by smt.
    rewrite Habc Hab. smt.
    by rewrite Hf.
  by smt.
qed.
   

(* Part 2 : uniq keys, but the adversary fakes a signature *)
module MEUF_Exp_uniq(SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, id, i, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.qK   <-0;
     i <- 0;

     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       BU.qK <- BU.qK + 1;
       i <- i +1;
     }
              
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s);
     return ev /\ uniq (map rid BU.keyL) /\ 
            (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                   (x.`2,x.`3,x.`4))
                              BU.sigL) /\                    
            (u \in        (map thr3 BU.keyL)) /\
            (let a = nth witness BU.keyL (index u (map thr3 BU.keyL)) in
              !a.`1 \in       (map fst3 BU.corL));
     }
}.

lemma meuf_uniq &m:
  Pr[MEUF_Exp_pre_bow(SS,A(SS),O).main() @ &m: res /\ !BU.bow]=
  Pr[MEUF_Exp_uniq(SS,A(SS),O).main() @ &m: res].
proof.
  byequiv =>//=. proc.
  call(: ={glob O}); first by sim.
  call(: ={glob O, glob SS, BU.sigL, BU.corL, BU.keyL}); first 3 by sim. 
  wp; while (={i, BU.keyL, glob SS}); first by sim.
  wp;call(: true); wp.
  by auto=>/>; progress.  
qed.

module MEUF_Exp_bad (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, i, id, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.qK   <-0;
     i <- 0;

     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       BU.qK <- BU.qK + 1;
       i <- i +1;
     }
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s);

     BU.bad <- ev /\ uniq (map rid BU.keyL)/\
              (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                    (x.`2,x.`3,x.`4))
                                BU.sigL) /\                    
              (u \in        (map thr3 BU.keyL)) /\
              (let a = nth witness BU.keyL (index u (map thr3 BU.keyL)) in
                !a.`1 \in  (map fst3 BU.corL));
     BU.ibad<- index u (map thr3 BU.keyL); 
     return BU.bad /\ (0 <= BU.ibad < size (undup Voters));
     }
     
}.

lemma uniq_bad &m: 
  Pr[MEUF_Exp_uniq(SS,A(SS),O).main() @ &m: res ] <=
  Pr[MEUF_Exp_bad(SS,A(SS),O).main() @ &m: BU.bad /\ (0 <= BU.ibad < size (undup Voters))].
proof.
  byequiv =>/>.
  proc.
  wp. 
  call(: ={glob O}); first by sim.
  call(: ={glob O, glob SS, BU.sigL, BU.corL, BU.keyL}); first 3 by sim. 
  while (={i, BU.keyL, glob SS, BU.qK}/\
         size BU.keyL{2} = BU.qK{2} /\
         BU.qK{2} = i{2} /\
         BU.qK{2} <=size (undup Voters)).
    wp;call(: true); wp.
    auto=>/>; progress. smt. smt. 
  call(: true); wp.
  auto=>/>; progress. smt. smt. smt. 
qed.

local lemma bad_guess &m:
  1%r/ (size (undup Voters)) %r * 
  Pr[MEUF_Exp_bad(SS,A(SS),O).main() @ &m: BU.bad /\ 0<= BU.ibad < size (undup Voters)] 
  = 
  Pr[EPP.Guess(MEUF_Exp_bad(SS,A(SS),O)).main() 
         @ &m: BU.bad /\ 0 <= BU.ibad < size (undup Voters) /\ fst res = BU.ibad ].
proof.
  (* print glob MEUF_Exp_bad(SS,A(SS),O). *) 
  cut := EPP.PBound (MEUF_Exp_bad(SS,A(SS),O))
          (fun g b => let (bad, ibad, corL, sigL,  qK, keyL, qCo, O, SS, A)= g in
             bad /\ 0<= ibad <size (undup Voters))
          (fun g b => let (bad, ibad, corL, sigL, qK, keyL,qCo, O, SS, A)= g in
             ibad) () &m. 
  simplify. 
  move => ->. 
  rewrite Pr[mu_eq]; progress; smt. 
qed.

module MEUF_Exp_bad_ibad (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
      var u, m, s, ev, i, id, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.qK   <-0;
     i <- 0;
     BU.ibad <$ [0..size (undup Voters) - 1];
     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       BU.qK <- BU.qK + 1;
       i <- i +1;
     }
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s);
     BU.bad <- ev /\ uniq (map rid BU.keyL) /\
              (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                    (x.`2,x.`3,x.`4))
                                BU.sigL) /\   
              (u \in        (map thr3 BU.keyL)) /\
              (let a = nth witness BU.keyL (index u (map thr3 BU.keyL)) in
                !a.`1 \in  (map fst3 BU.corL))/\
             BU.ibad = index u (map thr3 BU.keyL);  
     return BU.bad;
     }
}.

lemma guess_bad_ibad &m: 
  Pr[EPP.Guess(MEUF_Exp_bad(SS,A(SS),O)).main() 
         @ &m: BU.bad /\ 0 <= BU.ibad < size (undup Voters) /\ fst res = BU.ibad ] <=
  Pr[MEUF_Exp_bad_ibad(SS,A(SS),O).main() @ &m: BU.bad].
proof.
  byequiv =>//=.
  proc.
  inline MEUF_Exp_bad(SS, A(SS), O).main.
  wp. 
  call(: ={glob O}); first by sim.
  call(: ={glob SS, glob O, BU.keyL, BU.corL, BU.sigL}); 
    first 3 by sim.
  swap{2} 6 -5. 
  while (={glob SS, BU.qK, BU.keyL}/\
         i0{1} = i{2}). by sim.
  call(: true); wp; rnd.
  by auto=>/>; progress.
qed.

module MEUF_Exp_ibad_size (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, i, id, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.qK   <-0;
     i <- 0;
     BU.ibad <$ [0..size (undup Voters) - 1];
     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       BU.qK <- BU.qK + 1;
       i <- i +1;
     }
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s);
     BU.bad <- ev /\ uniq (map rid BU.keyL) /\
              (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                    (x.`2,x.`3,x.`4))
                                BU.sigL) /\                    
             BU.ibad < size BU.keyL/\
             (let a = nth witness BU.keyL (index u (map thr3 BU.keyL)) in
                !a.`1 \in  (map fst3 BU.corL))/\
             BU.ibad = index u (map thr3 BU.keyL); 
     return BU.bad;
     }
}.

lemma ibad_ibadsize &m: 
  Pr[MEUF_Exp_bad_ibad(SS,A(SS),O).main() @ &m: BU.bad] <=
  Pr[MEUF_Exp_ibad_size(SS,A(SS),O).main() @ &m: BU.bad].
proof.
  byequiv =>//=.
  proc.
  wp; call(: ={glob O}); first by sim. (*
  conseq (: ={id, u,m,s, glob SS, glob O, BU.keyL, BU.corL, BU.sigL, BU.ibad}/\
           (BU.ibad{1} =  index id{1} (map fst3 BU.keyL{1}) =>
            id{1} = (nth witness BU.keyL{2} BU.ibad{2}).`1))=>//=.
 *)
  call(: ={glob SS, glob O, BU.keyL, BU.corL, BU.sigL}); first 3 by sim.
  while (={i, glob SS, BU.keyL, BU.qK}); first by sim.
  call(: true); rnd.
  auto; progress.
  smt.
qed.


module MEUF_Exp_ibad_nth (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, i, id, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.qK   <-0;
     i <- 0;
     BU.ibad <$ [0..size (undup Voters) - 1];
     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       BU.qK <- BU.qK + 1;
       i <- i +1;
     }
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s); 
     BU.bad <- ev /\ uniq (map rid BU.keyL) /\
              (!(u,m,s) \in map (fun (x: ident * upkey * message * string),
                                    (x.`2,x.`3,x.`4))
                                BU.sigL) /\                    
              (* index u (map thr3 BU.keyL)*) BU.ibad < size BU.keyL/\
              !(nth witness BU.keyL BU.ibad).`1 \in  (map fst3 BU.corL)/\
               (u = (nth witness BU.keyL BU.ibad).`3);
     return BU.bad;
     }
}.

lemma uniq_fst3 (L : (ident * uskey*upkey) list) y u:
   u \in L => y \in L =>
   u.`1 = y.`1  =>
   uniq (map rid L) =>
   uniq (map fst3 L)=> u = y.
proof.
  move => HuL HyL Huy HL HL'.
  case (rid u = rid y).
  + rewrite /rid //=. smt.
  move => Huy_r.   
  elim: L HuL HyL HL HL'=>//=.
  move => w L HuyL Hu Hy HL.
  case ((rid w \in map rid L)). 
    smt.
  move => Hwm.  smt.
qed.

lemma uniq_thr3 (L : (ident * uskey*upkey) list) y u:
   (forall x, x\in L => x.`3 = get_upk x.`2)=>
   u \in L => y \in L =>
   u.`3 = y.`3  =>
   uniq (map rid L) =>
   uniq (map fst3 L)=> u = y.
proof.
  move => HxL HuL HyL Huy HL HL'.
  have Huy_r: rid u = rid y.
    cut Hu2:= HxL u HuL.
    cut Hy2:= HxL y HyL.
    rewrite /rid Hu2 Hy2 //=. smt. 
  smt.
qed.

lemma take_drop_uniq (L :'a list) x i:
   uniq L =>
   x \in take (i+1) L =>
     !x \in drop (i+1) L.
proof.
  move => HL HtL. smt.
qed.

lemma ibad_ibadnth &m: 
  Pr[MEUF_Exp_ibad_size(SS,A(SS),O).main() @ &m: BU.bad] <=
  Pr[MEUF_Exp_ibad_nth(SS,A(SS),O).main() @ &m: BU.bad].
proof.
  byequiv =>//=.
  proc.
  wp; call(: ={glob O}); first by sim. (*
  conseq (: ={id, u,m,s, glob SS, glob O, BU.keyL, BU.corL, BU.sigL, BU.ibad}/\
           (BU.ibad{1} =  index id{1} (map fst3 BU.keyL{1}) =>
            id{1} = (nth witness BU.keyL{2} BU.ibad{2}).`1))=>//=.
 *)
  call(: ={glob SS, glob O, BU.keyL, BU.corL, BU.sigL}); first 3 by sim.

  while (={i, glob SS, glob O, BU.qK, BU.keyL}/\
         0 <= i{2} /\
         (* uniq (map rid BU.keyL{2}) /\ *)
         uniq (map fst3 BU.keyL{2})/\
         (forall x , x \in BU.keyL{2} =>
                       x.`3 = get_upk x.`2) /\ 
         (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BU.keyL{2})) ).
    wp; call{1} (kgen_get_upk O). 
    auto=>/>; progress. 
    + smt. 
    (* + by rewrite map_cat cat_uniq //=. *)
    + rewrite map_cat cat_uniq //= H0 /fst3 //= -/fst3.
      cut := H2 (nth witness (undup Voters) i{2}) _.
        rewrite (drop_nth witness i{2}). smt.    
        by done.
      by done.   
    + rewrite mem_cat in H5; smt. 
    + rewrite map_cat mem_cat //= /fst3 //= -/fst3 //=.
       have Hxm: x \in drop i{2} (undup Voters). 
         rewrite (drop_nth witness i{2}). smt.    
         smt. 
       cut Hxn:= H2 x Hxm.
       rewrite Hxn//=.
       cut := take_drop_uniq (undup Voters) (nth witness (undup Voters) i{2}) i{2}_ _.  
       + smt.       
       + smt.
       smt.

  call(: true).
  rnd; wp.
  auto; progress.
  move: H9 H10 H11 H0 H; 
  pose a:= result_R; move => H9 H10 H11 H0 H. 
   cut:= H10; rewrite -(size_map thr3) index_mem. 
   rewrite mapP; elim => x [Hxm Hxr]. 
   cut Hx1:= nth_index_map witness fst3 keyL_R x _ Hxm.
    + rewrite /fst3 //= ; move => n m Hn Hm Hnm. 
      cut := uniq_fst3 keyL_R n m. smt. 
   cut Hx3:= nth_index_map witness thr3 keyL_R x _ Hxm.
    + rewrite /thr3 //= ; move => n m Hn Hm Hnm. 
      by rewrite (uniq_thr3 keyL_R m n H5). 
   by rewrite Hxr Hx3 /thr3.
qed.

module MEUF_Exp_ibad_nth_2 (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle (SS, O)
   proc main(): bool ={
     var u, m, s, ev, i, id, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.qK   <-0;
     i <- 0;
     BU.ibad <$ [0..size (undup Voters) - 1];
     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       BU.qK <- BU.qK + 1;
       i <- i +1;
     }
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (u,m,s); 
     BU.bad <- ev /\ uniq (map rid BU.keyL) /\
              (!((nth witness BU.keyL BU.ibad).`3,m,s) \in 
                  map (fun (x: ident * upkey * message * string),
                           (x.`2,x.`3,x.`4)) BU.sigL) /\                    
              (*index (nth witness BU.keyL BU.ibad).`3 (map thr3 BU.keyL)*) 
              BU.ibad < size BU.keyL/\
              !(nth witness BU.keyL BU.ibad).`1 \in  (map fst3 BU.corL)/\
              (u = (nth witness BU.keyL BU.ibad).`3);
     return BU.bad;
     }
}.

lemma ibadnth_ibadnth2 &m: 
  Pr[MEUF_Exp_ibad_nth(SS,A(SS),O).main() @ &m: BU.bad] <=
  Pr[MEUF_Exp_ibad_nth_2(SS,A(SS),O).main() @ &m: BU.bad].
proof.
  byequiv =>//=.
  proc.
  wp; conseq (: ={u,m,s, ev, BU.keyL, BU.sigL, BU.ibad, BU.corL})=>//=.
  by sim.
qed.

module MEUF_Exp_keyi (SS: SignScheme, A: MEUF_Adv, O: Oracle) ={
   module OO = MEUF_Oracle(SS, O)
   proc main(): bool ={
     var u, m, s, ev, i, id, usk, upk;
     BU.keyL <- [];
     BU.sigL <- [];
     BU.corL <- [];
     BU.usk  <- None;
     BU.uspk <- None;
     BU.upk <- Some (oget BU.uspk).`3;
     BU.qK   <-0;
     i <- 0;
     BU.ibad <$ [0..size (undup Voters) - 1];
     O.init();
     while (i < size (undup Voters)){
       id <- nth witness (undup Voters) i;
       (usk,upk)<@SS(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk,upk)];
       if (BU.qK = BU.ibad){
         BU.uspk <- Some (id, usk,upk);
         BU.upk <- Some (oget BU.uspk).`3;
       }
       BU.qK <- BU.qK + 1;
       i <- i +1;
     }
     (u,m,s) <@A(OO).main(map ft3 BU.keyL);
     ev <@ SS(O).verify (oget BU.upk,m,s); 
     BU.bad <- ev /\ uniq (map rid BU.keyL) /\
              BU.upk <> None /\
               (!(oget BU.upk,m,s) \in map (fun (x: ident * upkey * message * string),
                                    (x.`2,x.`3,x.`4))
                                    BU.sigL) /\ 
               (! oget BU.upk \in map (fun (x: ident * uskey* upkey),
                                            x.`3) BU.corL);
    
     return BU.bad;
     }
}.


lemma uniq_dif (L: (ident * uskey * upkey) list) x y:
 (forall a , a \in L => a.`3 = get_upk a.`2) =>
  uniq (map fst3 L) =>  
  uniq (map rid L) =>
  x \in L => y \in L => x.`1 <> y.`1 => x.`3 <> y.`3.
proof.
  move=> HaL HfL HrL HxL HyL Hxy1.  
  case (rid x = rid y). smt.
  rewrite /rid. 
  case (x.`3 <> y.`3). smt.
  simplify. move => Hxy3.
  rewrite Hxy3. smt.
qed.

lemma ibadnth_keyi &m: 
  Pr[MEUF_Exp_ibad_nth_2(SS,A(SS),O).main() @ &m: BU.bad] <=
  Pr[MEUF_Exp_keyi(SS,A(SS),O).main() @ &m: BU.bad].
proof.
  byequiv =>//=.
  proc. wp.
  seq 9 12 : ( ={glob SS, glob O, BU.keyL, BU.corL, BU.sigL, BU.ibad, u, m,s}/\
               uniq (map fst3 BU.keyL{2})/\
               0 <= BU.ibad{2} /\
               (forall x , x \in BU.keyL{2} =>
                       x.`3 = get_upk x.`2) /\
               (forall x, uniq (map rid BU.keyL{2}) /\ 
                     x \in BU.keyL{2} =>
                ! (x.`1 \in map fst3 BU.corL{2}) =>
                ! (x.`3 \in map thr3 BU.corL{2}))/\
                size BU.keyL{2} = BU.qK{2} /\
                oget BU.uspk{2} = nth witness BU.keyL{2} BU.ibad{2} /\
                BU.upk{2} = Some (oget BU.uspk{2}).`3).
 
  (* call(: ={glob O}); first by sim. *)
  call(: ={glob SS, glob O, BU.keyL, BU.corL, BU.sigL}/\
         (* uniq (map rid BU.keyL{2}) /\ *)
         uniq (map fst3 BU.keyL{2})/\
         (forall x , x \in BU.keyL{2} =>
                       x.`3 = get_upk x.`2) /\
          (forall x, uniq (map rid BU.keyL{2}) /\ 
                     x \in BU.keyL{2} =>
            ! (x.`1 \in map fst3 BU.corL{2}) =>
            ! (x.`3 \in map thr3 BU.corL{2}))).
  + by proc*; call(:true).
  + proc. 
    sp; if=>//=.
    wp; call(: ={glob O}); first by sim.
    by auto=>/>;smt.    
  + proc.
    auto =>/>; progress. 
    move: H6; rewrite !map_cat !mem_cat /fst3 /thr3 //= -/fst3 -/thr3.
    rewrite !negb_or. 
    move => Hv.
    cut Hnm:= H1 x _ _; first by rewrite H5 H4.  
      by rewrite (andWl _ _ Hv).
    rewrite Hnm //=.
    move: H2; rewrite mapP.
    elim => y [Hym Hyid].
    have ->: nth witness BU.keyL{2} (index id{2} (map fst3 BU.keyL{2})) = y.
      cut := nth_index_map witness fst3 BU.keyL{2} y _ Hym.
        rewrite /fst3; move => a b Ha Hb Hab. 
        by rewrite (uniq_fst3 BU.keyL{2} b a Ha Hb Hab).
      by rewrite Hyid. 
    cut Hd:= uniq_dif BU.keyL{2} x y H0 H H4 H5 Hym _.
      smt.  
   by done.   

  while ( ={glob SS, glob O, i, BU.keyL, BU.corL, BU.sigL, BU.qK}/\
          0 <= i{1} /\ i{1} = BU.qK{1} /\
         (* uniq (map rid BU.keyL{2}) /\ *)
         uniq (map fst3 BU.keyL{2})/\
         (forall x , x \in BU.keyL{2} =>
                       x.`3 = get_upk x.`2) /\
          (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BU.keyL{2}))/\
          size BU.keyL{2} = BU.qK{2} /\
         oget BU.uspk{2} = nth witness BU.keyL{2} BU.ibad{2} /\
         BU.upk{2} = Some (oget BU.uspk{2}).`3).
    wp; call{1} (kgen_get_upk O); wp.
    auto=>/>; progress. 
    + smt. 
    + rewrite map_cat cat_uniq //= H0 /fst3 //= -/fst3.
      cut := H2 (nth witness (undup Voters) (size BU.keyL{2})) _.
        rewrite (drop_nth witness _ ). smt.    
        by done.
      by done.   
    + rewrite mem_cat in H6; smt.
    + rewrite map_cat mem_cat //= /fst3 //= -/fst3 //=.
       have Hxm: x \in drop (size BU.keyL{2}) (undup Voters). 
         rewrite (drop_nth witness (size BU.keyL{2}) ). smt.    
         smt. 
       cut Hxn:= H2 x Hxm.
       rewrite Hxn//=.
       cut := take_drop_uniq (undup Voters) 
                   (nth witness (undup Voters) (size BU.keyL{2})) (size BU.keyL{2}) _ _.  
       + smt.       
       + smt.
       smt.
    + smt. smt. smt. 
    + rewrite map_cat cat_uniq //= H0 /fst3 //= -/fst3.
      cut := H2 (nth witness (undup Voters) (size BU.keyL{2})) _.
        rewrite (drop_nth witness _). smt.    
        by done.
      by done. 
    + rewrite mem_cat in H7; smt.
    + have Hxm: x \in drop (size BU.keyL{2}) (undup Voters). 
         rewrite (drop_nth witness _). smt.    
         smt.
      rewrite map_cat mem_cat //= /fst3 //= -/fst3 //=.
      cut Hxn:= H2 x Hxm.
       rewrite Hxn//=.
       cut := take_drop_uniq (undup Voters) 
                   (nth witness (undup Voters) (size BU.keyL{2})) (size BU.keyL{2}) _ _.  
       + smt.       
       + smt.
       smt.
    + smt. smt. 
  call(: true).
  rnd; wp.
  auto=>/>; progress. smt.
  case (u{1} = oget BU.upk{2}).
  + call(: ={glob O}); first by sim.
    auto=>/>; progress.
     + by rewrite oget_some H9 H6 //=. 
     + rewrite oget_some H9 //= -/thr3. 
       cut := H2 (nth witness BU.keyL{2} BU.ibad{2}) _ H8. 
       rewrite H5 //= mem_nth. smt. 
       by done.
  - (* different *)
   call{1} (SSv_ll O O_o_ll); call{2} (SSv_ll O O_o_ll).
   by auto=>/>; progress; smt. 
qed.

(* reduce to one MEUF *)
module type MEUF_Oracle_one ={
   proc o (x: s_in): s_out 
   proc sign (m: message): string option
   proc corrupt(): uskey option
}.

module MEUF_Oracle_one (SS: SignScheme, O: ARO)={
   proc o = O.o

   proc sign (m: message): string option ={
     var s, s';
     s <- None;
    (*  if (WU.uspk <> None){ *)
       s' <@ SS(O).sign ((oget WU.uspk).`1, m);
       s <- Some s';
       WU.sigL <- WU.sigL ++ [((oget WU.uspk).`2,m,s')];
    (* }*)
     return s;
   }
   proc corrupt(): uskey option ={
     var usk;
     usk <- None;
     if (WU.corL = []){
       usk <- Some (oget WU.uspk).`1;
       WU.corL <- WU.corL ++ [(oget WU.uspk)];
       WU.sigL <- [];
     }
     return usk;
   }
}.

module type MEUF_one_Adv (SS: SignScheme, O: MEUF_Oracle_one) ={
  proc a1 (): unit {O.o}
  proc a2(upk: upkey): message * string {O.o O.sign O.corrupt}
}.
 
module MEUF_one_Exp (SS: SignScheme, A: MEUF_one_Adv, O: Oracle) ={
   module OO = MEUF_Oracle_one (SS, O)
   proc main(): bool ={
     var m, s, ev, usk, upk;
     WU.keyL <- [];
     WU.sigL <- [];
     WU.corL <- [];
     WU.qK  <- 0;
     WU.uspk<- None;

                  O.init();
                  A(SS,OO).a1();
     (usk,upk) <@ SS(O).kgen();
     WU.uspk   <- Some (usk, upk);
     (m,s)     <@ A(SS,OO).a2(upk);

     ev <@ SS(O).verify ((oget WU.uspk).`2,m,s);
     return ev /\ 
            ! (((oget WU.uspk).`2,m,s) \in WU.sigL)/\
            ! (oget WU.uspk \in WU.corL);
     }
}.

(* add signature and verification *)  
module W_one (SS':SignScheme, O: ARO, A: MEUF_Adv,(*: MEUF_one_Adv*) 
       SS: SignScheme, OO: MEUF_Oracle_one)={
  module WO ={
   proc o = O.o
   proc sign (id: ident, m: message): string option ={
     var s, s', usk, upk',id';
     s <- None;
     if (id = BU.id){ 
         s<@ OO.sign(m);
         BU.sigL <- BU.sigL ++ [(id,oget BU.upk,m,oget s)];
     }elif (id \in map fst3 BU.keyL){
         (* I know the keys for this one *)
         (id',usk,upk') <- nth witness BU.keyL (index id (map fst3 BU.keyL));
         s' <@ SS'(O).sign (usk, m);
         s <- Some s';
         BU.sigL <- BU.sigL ++ [(id,upk',m,s')];
      }     
     return s;
   }

   proc corrupt(id: ident): uskey option ={
     var usk, usk', upk', id';
     usk <- None;
     if (id = BU.id){ 
       usk<@ OO.corrupt();
     }elif (id \in map fst3 BU.keyL /\ ! (id \in map fst3 BU.corL)){
       (* I know the keys for this one *)
       (id',usk',upk') <- nth witness BU.keyL (index id (map fst3 BU.keyL));
       usk <- Some usk';
       BU.corL <- BU.corL ++ [(id,usk',upk')];
       BU.sigL <- filter (predC1 id \o fst4) BU.sigL;
     }
     return usk;
   }

  } (* end WO *)

  (* gen keys until ibad *)
  proc a1(): unit ={
    var id, usk, upk;
    BU.keyL <- [];
    BU.sigL <- [];
    BU.corL <- [];
    BU.qK   <-0;
    BU.i <- 0;
    BU.ui  <- 0;
    BU.ibad <$ [0..size (undup Voters) - 1];

    while ( BU.i < size (undup Voters) /\ BU.qK < BU.ibad){
      id <- nth witness (undup Voters) BU.i;
      (usk,upk)<@SS'(O).kgen();
      BU.keyL <- BU.keyL ++[(id,usk,upk)];
      BU.qK <- BU.qK + 1;
      BU.i <- BU.i +1;
    }   
    BU.id <- nth witness (undup Voters) BU.i;
   }
  
  proc a2(upk : upkey): message * string ={
    var u, m, s, id, usk', upk';
     BU.upk <- Some upk;
     BU.i <- BU.i +1;
     BU.qK <- BU.qK + 1; (* this should be the equal to ibad *)
     while (BU.i < size (undup Voters) /\ BU.ibad< BU.qK){
       id <- nth witness (undup Voters) BU.i;
       (usk',upk')<@SS'(O).kgen();
       BU.keyL <- BU.keyL ++[(id,usk',upk')];
       BU.qK <- BU.qK  + 1;
       BU.i <- BU.i +1;
     }

     
     (u,m,s) <@A(WO).main(map ft3 (take BU.ibad BU.keyL) 
                          ++ [(BU.id,oget BU.upk)] ++
                          map ft3 (drop BU.ibad BU.keyL));
     return (m,s);
     }
}.

lemma uniq_id_nth (L1 L2: ('a* 'b *'c) list) (x:'a*'b*'c):
  uniq (map (fun (y: 'a *'b *'c), y.`1) (L1 ++ [x] ++ L2)) =>
  nth witness (L1 ++ [x] ++ L2) (index x.`1 (map (fun (y: 'a *'b *'c), y.`1)
                                                 (L1 ++ [x] ++ L2))) = x.
proof.
  pose fst:= (fun (y : 'a * 'b * 'c) => y.`1).
  move => HuL.
  have ->: (index x.`1 (map fst (L1 ++ [x] ++ L2))) = size L1. 
   rewrite !map_cat !index_cat.
   have ->: x.`1 \in map fst L1 ++ map fst [x] by rewrite /fst mem_cat //=.
   simplify.
   have ->: !x.`1 \in map fst L1.
     move: HuL; rewrite !map_cat !cat_uniq. smt.
   simplify.
   by rewrite /fst size_map //=.

  rewrite !nth_cat. 
  smt.
qed.

lemma uniq_id_nth_eq (L1 L2: ('a* 'b *'c) list) (x :'a*'b*'c) y:
  uniq (map (fun (y: 'a *'b *'c), y.`1) (L1 ++ [x] ++ L2)) =>
  x.`1 <> y =>
  nth witness (L1 ++ [x] ++ L2) 
              (index y (map (fun (y: 'a *'b *'c), y.`1) (L1 ++ [x] ++ L2))) = 
  nth witness (L1 ++ L2) 
              (index y (map (fun (y: 'a *'b *'c), y.`1) (L1 ++ L2))).
proof.
  pose fst:= (fun (y : 'a * 'b * 'c) => y.`1).
  move => HuL Hd.
  rewrite !map_cat !index_cat.
  have ->: y \in map fst L1 ++ map fst [x] =
           y \in map fst L1.
    rewrite /fst //= mem_cat //=. 
    have ->: !y = x.`1 by smt.
    by done.
  rewrite !nth_cat.
  simplify. 
  case (y \in map fst L1 ). smt.
  rewrite !size_cat !size_map //=. smt. 
qed.

lemma keyi_w_one &m: 
  Pr[MEUF_Exp_keyi(SS,A(SS),O).main() @ &m:  BU.bad] <=
  Pr[MEUF_one_Exp(SS,W_one(SS,O,A(SS)),O).main() @ &m:  res].
proof.
  byequiv=>//=.
  proc.
  inline W_one(SS, O, A(SS), SS, MEUF_one_Exp(SS, W_one(SS, O, A(SS)), O).OO).a1 
         W_one(SS, O, A(SS), SS, MEUF_one_Exp(SS, W_one(SS, O, A(SS)), O).OO).a2.
  wp.

  seq 11 22:( ={glob O, glob SS, glob A, BU.corL, BU.sigL, BU.ibad, BU.upk} /\
              BU.corL{2} = []/\
              BU.sigL{2} = []/\
              WU.corL{2} = []/\
              WU.sigL{2} = []/\
              BU.ui{2}   = 0/\
              uniq (map fst3 BU.keyL{1})/\
              BU.uspk{1} = Some 
               (BU.id, (oget WU.uspk).`1, (oget WU.uspk).`2){2}/\
              BU.upk{1} = Some (oget BU.uspk{1}).`3 /\
              BU.keyL{1} = take BU.ibad{1} BU.keyL{2} ++ [oget BU.uspk{1}] ++
                           drop BU.ibad{1} BU.keyL{2}
               
            ).
    splitwhile{1} 11 : BU.qK < BU.ibad. 
    unroll{1} 12.
    seq 11 14:( ={glob O, glob SS, glob A, BU.keyL, BU.corL, BU.sigL, BU.qK, BU.ibad}/\ 
                i{1} = BU.i{2} /\
                i{1} = BU.qK{2}/\
                i{1} = BU.ibad{2}/\
                BU.ibad{1} = size BU.keyL{1}/\
                BU.ibad{1} < size (undup Voters)/\
                BU.corL{2} = []/\
                BU.sigL{2} = []/\
                WU.corL{2} = []/\
                WU.sigL{2} = []/\
                BU.ui{2}   = 0/\
                BU.uspk{1} = None /\
                WU.uspk{2} = None /\
                BU.upk{1} = Some ((oget BU.uspk{1}).`3)/\
                uniq (map fst3 BU.keyL{1})/\
                (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BU.keyL{1}))).
      while ( ={BU.keyL, glob SS, BU.qK, BU.ibad} /\ 
              0 <= i{1} /\
              i{1}= BU.i{2}/\ i{1} = BU.qK{1}/\ 
              i{1} <= BU.ibad{1} /\ 
              size BU.keyL{1} = BU.qK{1}/\
              BU.ibad{1} < size (undup Voters)/\
              BU.uspk{1} = None /\
              WU.uspk{2} = None /\
              BU.upk{1} = Some ((oget BU.uspk{1}).`3)/\
              uniq (map fst3 BU.keyL{1}) /\
              (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BU.keyL{1}))
              ).
      + wp; call(: true).
        auto=>/>; progress. 
        + smt. smt. smt. smt. 
        + rewrite map_cat /fst3 //= -/fst3 cat_uniq H2 //=.
          cut := H3 (nth witness (undup Voters) (size BU.keyL{2})) _ .
             rewrite (drop_nth witness _ ). smt.
             by rewrite mem_head.
          by done.
        + rewrite map_cat mem_cat /fst3 //= -/fst3 //=.
          have Hxm: x \in drop (size BU.keyL{2}) (undup Voters). 
            rewrite (drop_nth witness (size BU.keyL{2}) ). smt.    
            smt. 
          cut Hxn:= H3 x Hxm.
          rewrite Hxn//=.
          cut := take_drop_uniq (undup Voters) 
                   (nth witness (undup Voters) (size BU.keyL{2})) (size BU.keyL{2}) _ _.  
          + rewrite undup_uniq. 
          + smt. 
          smt.       
      swap{2} 6 7.
      call(: true); rnd; wp.
      by auto=>/>; progress; smt. 
    seq 1 7:( ={glob O, glob SS, glob A, BU.corL, BU.sigL, BU.qK, BU.ibad, BU.upk}/\ 
                i{1} = BU.i{2} /\
                BU.qK{2} = BU.ibad{2} +1 /\
                i{1} = BU.qK{1} /\
                size BU.keyL{1} = BU.ibad{2} + 1/\
                BU.ibad{2} < i{1} /\
                BU.corL{2} = []/\
                BU.sigL{2} = []/\
                WU.corL{2} = []/\
                WU.sigL{2} = []/\
                BU.ui{2}   = 0/\
                uniq (map fst3 BU.keyL{1})/\
                (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BU.keyL{1}))/\
                BU.keyL{1} = BU.keyL{2} ++ [oget BU.uspk{1}]/\
                BU.uspk{1} = Some 
                (BU.id, (oget WU.uspk).`1, (oget WU.uspk).`2){2}/\
                BU.upk{1} = Some (oget BU.uspk{1}).`3).   
      rcondt{1} 1; progress.
      rcondt{1} 4; progress.
        wp; call(: true); wp.
        by auto. wp; progress. 
      wp; call(: true); wp. progress.
      auto=>/>; progress. 
      + smt. smt. 
      + rewrite map_cat /fst3 //= -/fst3 cat_uniq H0 //=.
          cut := H1 (nth witness (undup Voters) (size BU.keyL{2})) _ .
             rewrite (drop_nth witness _ ). smt.
             by rewrite mem_head.
          by done.
      + rewrite map_cat mem_cat /fst3 //= -/fst3 //=.
        have Hxm: x \in drop (size BU.keyL{2}) (undup Voters). 
          rewrite (drop_nth witness (size BU.keyL{2}) ). smt.    
          smt. 
        cut Hxn:= H1 x Hxm.
        rewrite Hxn//=.
        cut := take_drop_uniq (undup Voters) 
                   (nth witness (undup Voters) (size BU.keyL{2})) (size BU.keyL{2}) _ _.  
        + rewrite undup_uniq. 
        + smt. 
        smt.  
    unroll{1} 1; unroll{2} 1.
    if =>//=.
    while (={glob SS, BU.qK, BU.ibad}/\
           i{1} = BU.i{2} /\ i{1} = BU.qK{1}/\
           BU.ibad{2} +1 < BU.i{2} /\
           size BU.keyL{1} = i{1} /\
           uniq (map fst3 BU.keyL{1})/\
                (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BU.keyL{1}))/\
           BU.uspk{1} = Some 
                (BU.id, (oget WU.uspk).`1, (oget WU.uspk).`2){2} /\
           BU.upk{1} = Some (oget BU.uspk{1}).`3 /\
           BU.keyL{1} = (take BU.ibad{1} BU.keyL{2}) ++  [oget BU.uspk{1}] ++
                        (drop BU.ibad{1} BU.keyL{2})). 
    + rcondf{1} 4; progress.
        wp; call(: true); wp.
        by auto; smt.
      wp; call(: true); wp.
      auto=>/>; progress. 
      + smt. smt. 
      + rewrite oget_some.
        rewrite map_cat /fst3 //= -/fst3 cat_uniq H0 //=.
        pose L1:= take BU.ibad{2} BU.keyL{2} ++
                  [(BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2)] ++
                  drop BU.ibad{2} BU.keyL{2}.
        cut := H1 (nth witness (undup Voters) (size L1)) _.
          rewrite (drop_nth witness _ ). smt.
          rewrite -/L1. smt.
          by done.
      + rewrite oget_some.
        rewrite map_cat mem_cat /fst3 //= -/fst3 //=.
        pose L1:= take BU.ibad{2} BU.keyL{2} ++
                  [(BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2)] ++
                  drop BU.ibad{2} BU.keyL{2}.
         have Hxm: x \in drop (size L1) (undup Voters). 
          rewrite (drop_nth witness (size L1 )). smt.    
          smt. 
        cut Hxn:= H1 x Hxm.
        rewrite Hxn//=.
        cut := take_drop_uniq (undup Voters) 
                   (nth witness (undup Voters) (size L1)) (size L1) _ _.  
        + rewrite undup_uniq. 
        + smt. 
        smt.  
      + rewrite take_cat.
        have ->: BU.ibad{2} < size BU.keyL{2}. smt.
        smt.
      + smt. 

    rcondf{1} 4; progress.
      wp;call(: true); auto=>/>; smt.    
    wp; call(: true); wp.
    auto=>/>; progress. 
    + smt. smt. 
    + rewrite map_cat /fst3 //= -/fst3 cat_uniq H1 //=.
      cut := H2 (nth witness (undup Voters) (BU.ibad{2} + 1)) _.
          rewrite (drop_nth witness _ ). smt.
          smt.
      by done.
    + rewrite oget_some map_cat mem_cat /fst3 //= -/fst3 //=.
      have Hxm: x \in drop (BU.ibad{2} + 1) (undup Voters). 
        rewrite (drop_nth witness (BU.ibad{2} + 1)). smt.    
        smt. 
      cut Hxn:= H2 x Hxm.
      rewrite Hxn//=.
      cut := take_drop_uniq (undup Voters) 
                   (nth witness (undup Voters) (BU.ibad{2} + 1)) (BU.ibad{2} + 1) _ _.  
      + by rewrite undup_uniq. 
      + rewrite (take_nth witness). smt. 
        smt.
      smt.  
    + smt. smt.  
    (* no more values left *)
    rcondf{1} 1; progress.
    rcondf{2} 1; progress.
      by auto; progress; smt.    
    by auto; progress; smt.
(*
  seq 1 2 :(={glob O, glob SS, BU.ibad, u, m,s}/\   
            oget BU.upk{1} = (oget WU.uspk{2}).`2 /\     
            uniq (map fst3 BU.keyL{1}) /\
            BU.uspk{1} = Some (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2) /\
            BU.keyL{1} = take BU.ibad{1} BU.keyL{2} ++ [oget BU.uspk{1}] ++
                         drop BU.ibad{1} BU.keyL{2} /\
            (forall a, a\in WU.sigL{2} => (BU.id{2}, a.`1,a.`2,a.`3) \in BU.sigL{1})/\
            (forall a, a\in WU.corL{2} => (BU.id{2}, a.`1,a.`2) \in BU.corL{1})).
*)
    call(: ={glob O}); first by sim. 
    wp; call (: ={glob O, glob SS, BU.ibad}/\
              (* add this invariant : to first seq *)
             uniq (map fst3 BU.keyL{1}) /\
             BU.corL{2} = filter ((predC1 BU.id{2})\o fst3) BU.corL{1} /\
            (WU.corL{2} = [] => ! (BU.id{2} \in map fst3 BU.corL{1}))/\
            BU.uspk{1} = Some (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2) /\
            BU.keyL{1} = take BU.ibad{1} BU.keyL{2} ++ [oget BU.uspk{1}] ++
                         drop BU.ibad{1} BU.keyL{2} /\
            (* (forall a, a\in BU.keyL{1} => a.`3 = get_upk a.`2)/\ *)
            (forall a, a\in WU.sigL{2} => (BU.id{2}, a.`1,a.`2,a.`3) \in BU.sigL{1})/\
            (forall a, a\in WU.corL{2} => (BU.id{2}, a.`1,a.`2) \in BU.corL{1})).
    + by proc*; call(: true).
    + proc.
      sp.   
      if{2}=>//=.
      + rcondt{1} 1; progress.
          auto; progress. 
          rewrite !map_cat /fst3 //= -/fst3 oget_some //=. 
          smt.
        inline *; wp. 
        call(: ={glob O}); first by sim.      
        auto=>/>; progress. 
        + rewrite oget_some.
          by rewrite (uniq_id_nth (take BU.ibad{2} BU.keyL{2}) 
                           (drop BU.ibad{2} BU.keyL{2}) 
                           (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2) H) .
        + rewrite oget_some (uniq_id_nth (take BU.ibad{2} BU.keyL{2}) 
                           (drop BU.ibad{2} BU.keyL{2}) 
                           (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2) H) //=.
          move: H4; rewrite !mem_cat //=.
          smt.
      if=>//=.
        progress. 
        + move: H4; rewrite !map_cat oget_some /fst3 //= -/fst3 !mem_cat //= H3. 
          smt.
        + rewrite !map_cat oget_some /fst3 //= -/fst3 !mem_cat //= H3 //=. 
          cut HL2:= cat_take_drop BU.ibad{2} BU.keyL{2}. 
          move: H4; move => H4;
          rewrite -HL2 map_cat mem_cat //= in H4.
      wp; call(: ={glob O}); first by sim. 
      auto=>/>; progress.    
      + rewrite oget_some. 
        pose L1:= take BU.ibad{2} BU.keyL{2}.
        pose L2:= drop BU.ibad{2} BU.keyL{2}.
        pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
        cut := uniq_id_nth_eq L1 L2 x id{2} H _. 
        + by smt. 
        rewrite -/fst3.                  
        have HL1L2: BU.keyL{2} = L1 ++L2 by rewrite /L1 /L2 cat_take_drop.
        by smt.  
      + rewrite oget_some. 
        pose L1:= take BU.ibad{2} BU.keyL{2}.
        pose L2:= drop BU.ibad{2} BU.keyL{2}.
        pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
        cut := uniq_id_nth_eq L1 L2 x id{2} H _. 
        + by smt. 
        rewrite -/fst3.                  
        have HL1L2: BU.keyL{2} = L1 ++L2 by rewrite /L1 /L2 cat_take_drop.
        by smt.  

    + proc. inline *. 
      seq 1 1: ( ={id, usk} /\ usk{1} = None/\
  ={glob O, glob SS, BU.ibad} /\
  uniq (map fst3 BU.keyL{1}) /\
  BU.corL{2} = filter ((predC1 BU.id{2})\o fst3) BU.corL{1} /\
  (WU.corL{2} = [] => ! (BU.id{2} \in map fst3 BU.corL{1})) /\
  BU.uspk{1} = Some (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2) /\
  BU.keyL{1} =
  take BU.ibad{1} BU.keyL{2} ++ [oget BU.uspk{1}] ++
  drop BU.ibad{1} BU.keyL{2} /\
  (forall (a : upkey * message * string),
     a \in WU.sigL{2} => (BU.id{2}, a.`1, a.`2, a.`3) \in BU.sigL{1}) /\
  forall (a : uskey * upkey),
    a \in WU.corL{2} => (BU.id{2}, a.`1, a.`2) \in BU.corL{1}). by wp.
      if{2} =>//=. 
      + auto; progress. 
          + rewrite oget_some ?map_cat /fst3 //= -/fst3 mem_cat //=.  smt. 
          +  smt.
          + move: H2 H; 
           rewrite oget_some.
           pose L1:= take BU.ibad{2} BU.keyL{2}.
           pose L2:= drop BU.ibad{2} BU.keyL{2}.
           pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
           move => H2 H. 
           by rewrite (uniq_id_nth L1 L2 x H) /x //=.
          + move: H3 H; 
           rewrite oget_some.
           pose L1:= take BU.ibad{2} BU.keyL{2}.
           pose L2:= drop BU.ibad{2} BU.keyL{2}.
           pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
           move => H3 H. 
           rewrite (uniq_id_nth L1 L2 x H) /x //=. 
           rewrite filter_cat //= /fst3 /(\o) /predC1 //=. smt.
           + move: H3 H; 
           rewrite oget_some.
           pose L1:= take BU.ibad{2} BU.keyL{2}.
           pose L2:= drop BU.ibad{2} BU.keyL{2}.
           pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
           move => H3 H. 
           rewrite (uniq_id_nth L1 L2 x H) /x //=.
           smt.
          + rewrite oget_some ?map_cat /fst3 ?mem_cat /fst3 //= -/fst3 //= . 
            have He: exists e, e \in WU.corL{2} by smt.
            move: He; elim => e He.
            cut := H2 e He. rewrite mapP. move=> Hee. by exists (BU.id{2}, e.`1, e.`2).
      auto; progress.
      + rewrite oget_some ?map_cat /fst3 //= -/fst3 !mem_cat //= H3 //=. 
        cut HL2:= cat_take_drop BU.ibad{2} BU.keyL{2}. 
        move: H4; move => H4;
          rewrite -HL2 map_cat mem_cat //= in H4.
      + move: H5.
        cut := filter_map fst3 (predC1 BU.id{2}) BU.corL{1}.
        have ->: (preim fst3 (predC1 BU.id{2})) = (predC1 BU.id{2} \o fst3) by rewrite /preim.
        move => Hfm. rewrite -Hfm.
        by rewrite mem_filter /predC1 H3. 
      +  move: H H6; 
         rewrite oget_some.
         pose L1:= take BU.ibad{2} BU.keyL{2}.
         pose L2:= drop BU.ibad{2} BU.keyL{2}.
         pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
         move => H H6.
         rewrite (uniq_id_nth_eq L1 L2 x id{2} H _) /x //=.
          by smt.
        have HL1L2: BU.keyL{2} = L1 ++L2 by rewrite /L1 /L2 cat_take_drop.
        by smt.
      + move: H H6; 
         rewrite oget_some.
         pose L1:= take BU.ibad{2} BU.keyL{2}.
         pose L2:= drop BU.ibad{2} BU.keyL{2}.
         pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
         move => H H6.
         rewrite (uniq_id_nth_eq L1 L2 x id{2} H _) /x //=.
          by smt.
         rewrite filter_cat /predC1 /fst3 //=. smt.
      + rewrite ?map_cat /fst3 //= -/fst3 mem_cat //=. 
        smt.
      +  move: H H6; 
         rewrite oget_some.
         pose L1:= take BU.ibad{2} BU.keyL{2}.
         pose L2:= drop BU.ibad{2} BU.keyL{2}.
         pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
         move => H H6.
         rewrite mem_filter /predC1 /(\o) /fst4 //=.
         have ->: BU.id{2} <> id{2} by smt. simplify. 
         cut := uniq_id_nth_eq L1 L2 x id{2} H _. rewrite /x //=. smt. 
         rewrite (uniq_id_nth_eq L1 L2 x id{2} H _) /x //=.
          by smt. 
         smt.
      + rewrite oget_some. 
        rewrite ?map_cat /fst3 //= -/fst3 ?mem_cat //=. smt. 
      + rewrite oget_some. 
        rewrite ?map_cat /fst3 //= -/fst3 ?mem_cat //= H3 //=.
        move: H4; rewrite negb_and //=; move => H4.
          pose L1:= take BU.ibad{2} BU.keyL{2}.
          pose L2:= drop BU.ibad{2} BU.keyL{2}.
          have HL1L2: BU.keyL{2} = L1 ++L2 by rewrite /L1 /L2 cat_take_drop.
          rewrite -mem_cat -map_cat -HL1L2 negb_and //=.
          have Ho: (id{2} \in map fst3 (filter (predC1 BU.id{2} \o fst3) BU.corL{1})) =>
                    (id{2} \in map fst3 BU.corL{1}).
            cut := filter_map fst3 (predC1 BU.id{2}) BU.corL{1}.
            have ->: (preim fst3 (predC1 BU.id{2})) = (predC1 BU.id{2} \o fst3) by rewrite /preim.
            move => Hfm. rewrite -Hfm.
            by rewrite mem_filter /predC1 H3. 
        by smt.

(*    (*
      + rcondt{1} 1; progress.
         auto=>/>; progress. 
         rewrite oget_some ?map_cat /fst3 //= -/fst3 mem_cat //=. smt. 
         auto; progress. 
         + move: H2 H; 
           rewrite oget_some.
           pose L1:= take BU.ibad{2} BU.keyL{2}.
           pose L2:= drop BU.ibad{2} BU.keyL{2}.
           pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
           move => H2 H. print uniq_id_nth.
           by rewrite (uniq_id_nth L1 L2 x H) /x //=.
         + move: H2 H; 
           rewrite oget_some.
           pose L1:= take BU.ibad{2} BU.keyL{2}.
           pose L2:= drop BU.ibad{2} BU.keyL{2}.
           pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
           move => H2 H. print uniq_id_nth.
           rewrite (uniq_id_nth L1 L2 x H) /x //=.
           move: H3; rewrite mem_cat. smt.
      auto; progress.
      + rewrite oget_some !map_cat !mem_cat /fst3 //= -/fst3. *)

      inline *; wp.
      auto=>/>; progress. 
      + by rewrite oget_some !map_cat !mem_cat /fst3 //= -/fst3.
            
      + move: H2 H; 
         rewrite oget_some.
         pose L1:= take BU.ibad{2} BU.keyL{2}.
         pose L2:= drop BU.ibad{2} BU.keyL{2}.
         pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
         move => H3 H.
         by rewrite (uniq_id_nth L1 L2 x H) /x //=. 
      + rewrite oget_some. 
        move: H2 H; 
         rewrite oget_some.
         pose L1:= take BU.ibad{2} BU.keyL{2}.
         pose L2:= drop BU.ibad{2} BU.keyL{2}.
         pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
         move => H2 H.
        rewrite (uniq_id_nth L1 L2 x H) /x //=.
        move: H3; rewrite !mem_cat //=.
        smt.
      + rewrite oget_some !map_cat !mem_cat /fst3 //= -/fst3 H2 //=.
        cut HL2:= cat_take_drop BU.ibad{2} BU.keyL{2}. 
        move: H3; move => H3;
          rewrite -HL2 map_cat mem_cat //= in H3.
      + rewrite oget_some. 
        move: H2 H; 
          rewrite oget_some.
          pose L1:= take BU.ibad{2} BU.keyL{2}.
          pose L2:= drop BU.ibad{2} BU.keyL{2}.
          pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
          move => H2 H.
        rewrite (uniq_id_nth_eq L1 L2 x id{2} H _) /x //=.
          by smt.
        have HL1L2: BU.keyL{2} = L1 ++L2 by rewrite /L1 /L2 cat_take_drop.
        by smt.
      + rewrite oget_some. 
        move: H2 H; 
          rewrite oget_some.
          pose L1:= take BU.ibad{2} BU.keyL{2}.
          pose L2:= drop BU.ibad{2} BU.keyL{2}.
          pose x:= (BU.id{2}, (oget WU.uspk{2}).`1, (oget WU.uspk{2}).`2).
          move => H2 H.
        rewrite (uniq_id_nth_eq L1 L2 x id{2} H _) /x //=.
          by smt.
        have HL1L2: BU.keyL{2} = L1 ++L2 by rewrite /L1 /L2 cat_take_drop.
        by smt.
      + rewrite oget_some !map_cat !mem_cat /fst3 //= H2 -/fst3 //=.
        by rewrite -mem_cat -map_cat cat_take_drop H3. *)
        
    auto=>/>; progress. 
    + by rewrite ?map_cat ?oget_some /thr3 //= -/thr3.
    + move: H7; rewrite ?oget_some //= .
      pose a:= ((oget WU.uspk{2}).`2, result_R.`2, result_R.`3).
      cut Ha:= H2 a.
      rewrite mapP negb_exists //=.
      move => Hb.
      cut := Hb (BU.id{2}, a.`1, a.`2, a.`3). 
      simplify. 
      smt.
    + move: H8; rewrite ?oget_some //=.
      pose a:= (oget WU.uspk{2}).
      cut Ha:= H3 a.
      rewrite mapP negb_exists //=.
      move => Hb.
      cut := Hb (BU.id{2}, a.`1, a.`2). 
      simplify.
      smt.
qed.

module MEUF_Oracle_one_bad (SS: SignScheme, O: ARO)={
   proc o = O.o

   proc sign (m: message): string option ={
     var s, s';
     s <- None;
     s' <@ SS(O).sign ((oget WU.uspk).`1, m);
     s <- Some s';
     WU.sigL <- WU.sigL ++ [((oget WU.uspk).`2,m,s')];
     return s;
   }
   proc corrupt(): uskey option ={
     var usk;
     usk <- None;
     WU.bad <- true; 
     if (WU.corL = []){
       usk <- Some (oget WU.uspk).`1;
       WU.corL <- WU.corL ++ [(oget WU.uspk)];
       WU.sigL <- [];
     }
     return usk;
   }
}.

module MEUF_one_Exp_bad (SS: SignScheme, A: MEUF_one_Adv, O: Oracle) ={
   module OO = MEUF_Oracle_one_bad (SS, O)
   proc main(): bool ={
     var m, s, ev, usk, upk;
     WU.keyL <- [];
     WU.sigL <- [];
     WU.corL <- [];
     WU.bad  <- false;
     WU.qK  <- 0;
     WU.uspk<- None;

                  O.init();
                  A(SS,OO).a1();
     (usk,upk) <@ SS(O).kgen();
     WU.uspk   <- Some (usk, upk);
     (m,s)     <@ A(SS,OO).a2(upk);

     ev <@ SS(O).verify ((oget WU.uspk).`2,m,s);
     return ev /\ 
            ! (((oget WU.uspk).`2,m,s) \in WU.sigL)/\
            !WU.bad;
     }
}.

module type MEUF_one_Adv' (O: ARO, SS: SignScheme, OO: MEUF_Oracle_one) ={
  proc a1 (): unit {}
  proc a2(upk: upkey): message * string (* { SS(O).kgen SS(O).sign O.o OO.o OO.sign OO.corrupt}*)
}.

lemma meuf_one_bad (B<: MEUF_one_Adv'{SS, O, WU}) &m:
   Pr[MEUF_one_Exp(SS,B(O),O).main() @ &m:  res] <=
   Pr[MEUF_one_Exp_bad(SS,B(O),O).main() @ &m:  res].
proof.
  byequiv =>/>.  
  proc.
  call(: ={glob O}); first by sim.
  call(: ={glob SS, glob O, WU.uspk, WU.sigL, WU.corL}/\
          (!WU.bad{2} <=> WU.corL{1} =[] )/\
          ((oget WU.uspk{1}) \in WU.corL{1} <=> !WU.corL{1} =[])).
  + by proc*; call(: true).
  + proc.
    wp; call(: ={glob O}); first by sim.
    by auto.
  + proc. 
    by auto=>/>; smt.
  + by proc*; call(: true).
  wp; call(: true); call(: true); call(: true).
  by auto=>/>; progress; smt.
qed.

module  BF(SS: SignScheme, B: MEUF_one_Adv, OO : EUF_Oracle) = {
  module CO ={
    proc o = OO.o
    proc sign = OO.sign
    proc corrupt(): uskey option ={
     var usk;
     usk <- None;
     WU.bad <- true;
     return usk;
    }
  } (* end CO *)

  proc a1() : unit ={
    WU.bad <- false;
    B(SS,CO).a1();
  }
  
  proc a2(upk : upkey) : message * string ={
    var m, s;
    (m,s) <@ B(SS,CO).a2(upk);
    return (m,s);
  }
}.

lemma meuf_one_uef (B<: MEUF_one_Adv'{WU, O, SS}) &m:
   (forall  (O<: ARO{B}) (SS<: SignScheme{B}) (O0 <: MEUF_Oracle_one{B}),
      islossless O0.o =>
      islossless O0.sign => 
      islossless O0.corrupt => 
      islossless O.o => 
      islossless B(O,SS,O0).a2)=> 
   Pr[MEUF_one_Exp_bad(SS,B(O),O).main() @ &m:  res] <=
   Pr[EUF_Exp(SS,BF(SS,B(O)),O).main() @ &m:  res].
proof.
  move => B_a2_ll.
  byequiv =>/>.  
  proc.
  inline *.
  seq 11 10 : (!WU.bad{1} =>
                ={m,s, glob SS, glob O, WU.uspk, WU.sigL}
               ).
  wp; call(: WU.bad
         , ={glob SS, glob O, WU.uspk, WU.sigL, WU.bad}/\
           WU.uspk{1} <> None
         , ={WU.bad}).
 
  + proc*; call(: true).
    by auto; progress; smt. 
  + progress. 
    by proc*; call{1} O_o_ll.
  + progress. 
    by proc*; call O_o_ll.
  + proc. 
    rcondt{2} 2; progress.
      by auto.
    wp; call(: ={glob O}); first by sim.
    by auto=>/>; progress.
  + progress; proc.
    wp; call{1} (SSs_ll SS O O_o_ll).
    by auto.
  + progress; proc.
    sp; if=>//=.
    wp; call{1} (SSs_ll SS O O_o_ll).
    by auto.
  + proc.
    by auto=>/>.
  + progress; proc. 
    by auto=>/>; progress; smt. 
  + progress; proc. 
    by auto=>/>; progress; smt.
  + proc*; call(: true).
    by auto; progress; smt. 
  + progress. 
    by proc*; call{1} O_o_ll.
  + progress. 
    by proc*; call O_o_ll.
  wp; call(: true); call(: true); wp; call(: true).
  auto=>/>; progress; smt. 
  case(WU.bad{1}).
  + call{1} (SSv_ll O O_o_ll).
    call{2} (SSv_ll O O_o_ll).
    by auto.
  call(: ={glob O}); first by sim.
  by auto=>/>; progress; smt.
qed.

module WOO (A: MEUF_Adv, O: ARO, SA: SignScheme, OO: MEUF_Oracle_one) = 
       W_one(SA,O,A,SA,OO).


lemma meuf_part2 &m: 
  Pr[MEUF_Exp_pre_bow(SS,A(SS),O).main() @ &m: res /\ !BU.bow] <= 
(size (undup Voters))%r *
  Pr[EUF_Exp(SS, BF(SS,WOO(A(SS),O)), O).main() @ &m : res].
proof.
  have Habc: forall (a b c: real), 0%r < a => b<= c => a * b <= a* c by smt.
  have Habc': forall (a b c: real), 0%r<a => 1%r/a * b = c => b = a* c by smt. 
  rewrite (meuf_uniq &m).
  cut := uniq_bad &m.
  cut := bad_guess &m.
  cut := guess_bad_ibad &m.
  cut := ibad_ibadsize &m.
  cut := ibad_ibadnth &m.
  cut := ibadnth_ibadnth2 &m.
  cut := ibadnth_keyi &m.
  cut := keyi_w_one &m.
  pose L:= Pr[MEUF_Exp_uniq(SS, A(SS), O).main() @ &m : res].
  pose A:= Pr[MEUF_Exp_bad(SS, A(SS), O).main() @ &m :
              BU.bad /\ 0 <= BU.ibad < size (undup Voters)].
  pose B:= Pr[EPP.Guess(MEUF_Exp_bad(SS, A(SS), O)).main() @ &m :
              BU.bad /\ 0 <= BU.ibad < size (undup Voters) /\ res.`1 = BU.ibad].
  pose C:= Pr[MEUF_Exp_bad_ibad(SS, A(SS), O).main() @ &m : BU.bad].
  pose D:= Pr[MEUF_Exp_ibad_size(SS, A(SS), O).main() @ &m : BU.bad].
  pose E:= Pr[MEUF_Exp_ibad_nth(SS, A(SS), O).main() @ &m : BU.bad].
  pose F:= Pr[MEUF_Exp_ibad_nth_2(SS, A(SS), O).main() @ &m : BU.bad].
  pose G:= Pr[MEUF_Exp_keyi(SS, A(SS), O).main() @ &m : BU.bad].
  pose I:= Pr[MEUF_one_Exp(SS, W_one(SS, O, A(SS)), O).main() @ &m : res].
  move => Hgi Hfg Hef Hde Hcd Hbc Hab Hla.
  have Hbi: B <= I by smt.
  cut := meuf_one_bad (WOO(A(SS))) &m. 
    rewrite -/I. 
  have ->: Pr[MEUF_one_Exp(SS, WOO(A(SS), O), O).main() @ &m : res] = I. 
    by rewrite /I; byequiv=>//=; sim.
  pose J:= Pr[MEUF_one_Exp_bad(SS, WOO(A(SS), O), O).main() @ &m : res].
  move => Hij.
  cut := meuf_one_uef (WOO(A(SS))) &m _ .
  + move => OA SA OB OB_o OB_s OB_c OA_o.
    proc.
    call{1} (A_ll SS (<:W_one(SA, OA, A(SS), SA, OB).WO) 
                  OA_o _ _ _ _ _). progress. 
    + by proc*; call{1} (SSk_ll SS (<: W_one(SA, OA, A(SS), SA, OB).WO) OA_o ).
    + by proc*; call{1} (SSs_ll SS (<: W_one(SA, OA, A(SS), SA, OB).WO) OA_o).
    + by proc*; call{1} (SSv_ll (<: W_one(SA, OA, A(SS), SA, OB).WO) OA_o).
    + proc.
      sp; if =>//=.
      + by wp; call{1} OB_s.
      if =>//=.
      by wp; call{1} (SSs_ll SA OA OA_o); wp.
    + proc.
      sp; if =>//=.
      + by call OB_c.      
      by auto.
    while{1} (true) (size (undup Voters) - BU.i).
    + progress.
      wp; call{1} (SSk_ll SA OA OA_o). 
      by auto; smt.
    by auto; smt.
  pose R:= Pr[EUF_Exp(SS, BF(SS,WOO(A(SS), O)), O).main() @ &m : res].
  rewrite -/J.
  move => Hjr.
  have Hbr: B <= R by smt.
  have Hab': A = (size (undup Voters))%r * B. 
    rewrite (Habc' _ _ _ _ Hab). smt.
    by done.
  move: Hla; rewrite Hab'. 
  have Hbr': (size (undup Voters))%r * B <= (size (undup Voters))%r * R.
    rewrite (Habc _ _ _ _ Hbr). smt.
  smt. 
qed.
  
lemma meuf_euf (C<: Corr_Adv {O, WU, SS, BU}) &m: 
  islossless C(O).main =>
  Pr[MEUF_Exp(SS,A(SS),O).main() @ &m: res] 
<=
(size (undup Voters))%r *
  Pr[EUF_Exp(SS,BE(SS,C),O).main() @ &m: res]+
(size (undup Voters))%r *
  Pr[EUF_Exp(SS,BF(SS,WOO(A(SS), O)), O).main() @ &m : res]+
(size (undup Voters))%r *
  Pr[Correctness(SS,C,O).main() @ &m: !res ].
proof.
  move => HC_ll.
  cut P1:= meuf_part1 C &m HC_ll.
  cut P2:= meuf_part2 &m.
  by smt.
qed.

end section.