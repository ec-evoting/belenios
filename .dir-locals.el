((easycrypt-mode .
  ((eval .
    (flet ((pre (s) (concat (locate-dominating-file buffer-file-name ".dir-locals.el") s)))
           (setq easycrypt-load-path
                 `(,(pre ".")
                   ,(pre "core")
                   ,(pre "Voting")
                   ,(pre "Belenios")
                   ,(pre "Belenios/Belenios_DEF")
                   ,(pre "Belenios/Belenios_PROOFS")
                   ,(pre "Belenios_variants")
                   ,(pre "Belenios_variants/Belenios_VAR_DEF")
                   ,(pre "Belenios_variants/Belenios_VAR_PROOFS"))))
    ))))
