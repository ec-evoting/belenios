require import Int List Distr FMap AllCore  Real FSet Mu_mem CyclicGroup.
require import LeftOrRight StdBigop RealExtra. 
require (*  *) Belenios_SEC_PRIV_2. 

  (* basic types and operators used for the encryption scheme space *)
  type fq.           (* plaintext *)
  op Fq_zero: fq.    (* zero element for addition *)
  (* distribution over plaintexts *)
  op dfq : fq distr.
  (* assumption over distribution *)
  axiom dfq_ll: is_lossless dfq.
  (*axiom supp_def: forall (s:fq), in_supp s dfq.*)

(* Labelled Public-Key Encryption Scheme =
   abstract encryption scheme + abstract proof system *)
 
  (* ** abstract  encryption scheme *)
    (* Types and operators *)
    type h_cipher, skey, pkey.
    type plain = fq.
    op dskey: skey distr. 
    axiom dskey_ll: is_lossless dskey.

    (* encryption scheme algorithms *)
    op get_pk: skey -> pkey.
    op decrypt  : skey -> h_cipher -> fq option.
    op encrypt  : pkey -> plain -> t -> h_cipher.

  (* ** abstract Proof System *)
    (* Types and operators *)
    type upkey, h_prf, h_in, h_out, h_data.
    type cipher = h_cipher * h_prf * h_data.
    type h_stm = pkey * upkey * h_cipher.
    type h_wit = plain * t.
    
    op hash: h_cipher -> h_prf -> h_data.

    (* distribution over the output of the hash function *)
    op dh_out: h_out distr.

    (* abstract operators *)
    op verify      : h_stm -> h_prf -> (h_in, h_out) map -> bool.
    (* op to_statement: pkey -> label -> h_cipher -> h_stm.*)
    (* op to_witness  : plain -> t -> h_wit. *)

    clone export ProofSystem as PSpke with
      type stm   <- h_stm,
      type wit   <- h_wit,
      type prf   <- h_prf,
      type g_in  <- h_in,
      type g_out <- h_out,
      op dout    <- dh_out.
      (*proof *. not interested in axiom 
        enum_spec: forall (x : h_wit), count (pred1 x) enum = 1 *)

  (* ** Assumptions on the labelled public-key encryption scheme *)
    (* the zk proof is verified to true, 
       then the decryption will succeed (<> None) *)
    axiom verify_validDec (sk: skey) (b: upkey * cipher) (ro: (h_in,h_out) map) :
      verify ((get_pk sk), b.`1, b.`2.`1) b.`2.`2 ro =>
        (decrypt sk b.`2.`1 <> None).

    (* correctness property based on small values *)
    axiom correctness (sk: skey) (p: fq) (r: t):
      decrypt sk (encrypt (get_pk sk) p r ) = Some p.

 
  (* FIXME: remove the bound, use n from export *)
  op n : { int | 0 < n } as n_pos.     

  (* Import encryption scheme *)
  clone export LPKE as Ipke with
    type label  <- upkey,
    type plain  <- plain,
    type cipher <- cipher,
    type pkey   <- pkey,
    type skey   <- skey,
    type h_in   <- h_in,
    type h_out  <- h_out,
    op   dout   <- dh_out,
    op   n      <- n
    proof *.
    realize n_pos. by apply n_pos. qed.
  
  (* Labelled Public-Key Encryption Scheme  *)
  module IPKE(P: Prover, Ve: Verifier, O:ARO) = {
    proc kgen(): pkey * skey ={
      var sk; 
      sk <$dskey; 
    return (get_pk sk, sk);
    }

    proc enc(pk: pkey, lbl: upkey, p: plain) : cipher ={
      var r, c, pi;

      r <$ FDistr.dt; 
      c <- encrypt pk p r;
      pi<@ P(O).prove((pk, lbl, c), (p, r) );

      return (c, pi, hash c pi);
    }

    proc dec(sk: skey, lbl: upkey, c: cipher) : plain option = {
      var ev, m;

      m <- None; 
      ev <@ Ve(O).verify(((get_pk sk), lbl, c.`1), c.`2);
      if (ev){
        m <- decrypt sk c.`1;
      }
      return m;   
    }
    
  }.

(* ---------------------------------------------------------------------- *)
(* Minivoting with IPKE as an encryption scheme *)

(* decryption operator *)
op dec_cipher (sk: skey, lbl: upkey, c: cipher, ro: (h_in, h_out) map) =
   if(verify (get_pk sk, lbl, c.`1) c.`2 ro) 
      then (decrypt sk c.`1)
      else None.
type ident.
op validInd(pk: pkey, b: ident*upkey*cipher, ro: (h_in,h_out) map) = 
   verify (pk, b.`2, b.`3.`1) b.`3.`2 ro.

clone export Belenios_SEC_PRIV_2 as MV2 with
  (* op Publish  <- Publish,*)
  type ident  <- ident,
  type upkey  <- upkey,  
  type vote   <- plain,  
  (* use LPKE types *)
  type cipher   <- cipher, 
  type PKE.pkey  <- pkey,   
  type PKE.skey     <- skey,   
  type PKE.h_in     <- h_in,   
  type h_out    <- h_out,   
  op dh_out     <- dh_out, 
  op qVo         <- n,            
  op dec_cipher <- dec_cipher,
  op validInd   <- validInd,        
  op get_pk  <- get_pk
  proof *.
  realize qVo_bound. by smt. qed. 

module CIND(Ve: PSpke.Verifier, E: Scheme, SS: SignScheme, H: HOracle.ARO, J: JOracle.ARO) ={
    proc validInd(b: ident*upkey*cipher*sign, pk: pkey): bool ={
      var ev1, ev2;
      ev1 <@ Ve(H).verify((pk,b.`2,b.`3.`1), b.`3.`2);
      ev2 <@ SS(J).verify (b.`2,b.`3,b.`4);
      return ev1 /\ ev2;
    }
}.

(* BPRIV + CONSISTENCY + STRONG VERIFIABILITY PROPERTY *)
(* --------------------------------------------------------------------------*)
section Security.

declare module S : Simulator 
                   { BP, BS, BPS, BSC, BBS, HRO.RO, JRO.RO, GRO.RO, 
                     H.Count, H.HybOrcl, WrapAdv}.
declare module Vz: Verifier
                   { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, JRO.RO, GRO.RO, S}.
declare module Pz : Prover
                   { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, JRO.RO, GRO.RO, S, Vz}.
declare module SS: SignScheme
                   { BP, BS, BPS, BH, BW, WU, BU, GRO.RO, JRO.RO, S, Vz, Pz, HRO.RO, 
                     BSC, BBS, H.Count, H.HybOrcl, WrapAdv}.
declare module Ex: PoK_Extract    {BW, BSC,Pz, BPS, GRO.RO, JRO.RO, Vz, HRO.RO, SS}.

(* ADVERSARIES *)
declare module BPRIV_A: BPRIV_Adv        
                  { BP, BS, BPS, BSC, BBS, HRO.RO, GRO.RO, S, Vz, Pz, SS, JRO.RO,
                    H.Count, H.HybOrcl, WrapAdv}. 
declare module CONSIS2_A: SConsis2_Adv {BP, HRO.RO, JRO.RO, GRO.RO, SS}.
declare module CONSIS3_A: SConsis3_Adv {BP, HRO.RO, JRO.RO, GRO.RO, SS, Pz}.

declare module DBB_A : DBB_Adv    
                  {BS, BPS, BH, BW, WU, BU, BSC, HRO.RO, GRO.RO, JRO.RO,  Pz, Vz, SS, Ex}. 

declare module DREG_A : DREG_Adv    
                  {BS, BPS, BH, BW, WU, BU, BSC, HRO.RO, GRO.RO, JRO.RO,  Pz, Vz, SS, Ex}. 


(* this are needed for the LPKE scheme *)
declare module Ve: PSpke.Verifier
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, 
                    S, Vz, Pz, BPRIV_A, DBB_A, DREG_A, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO, Ex}.
declare module Pe: PSpke.Prover
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO,
                    S, Vz, Pz, Ve, BPRIV_A, DBB_A, DREG_A, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO, Ex}.

(* admit. fixme: link between E and C *)
declare module Ve': PSpke.Verifier
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, 
                    S, Vz, Pz, BPRIV_A, DBB_A, DREG_A, Ve, Pe, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO, Ex}.
(* ** ASSUMPTIONS ** *)
(* ** start *)
  axiom size_Voters:
    0 < size Voters.

  axiom weight_dh_out: weight dh_out = 1%r.
  axiom weight_dg_out: weight dg_out = 1%r. 
  axiom weight_dj_out: weight dj_out = 1%r.
  axiom weight_dk_out: weight dhkey_out = 1%r.
  axiom is_finite_h_in : Finite.is_finite predT <:h_in>.
  axiom distr_h_out    : is_lossless dh_out.
  axiom is_finite_j_in : Finite.is_finite predT <:j_in>.
  axiom distr_j_out    : is_lossless dj_out.
  axiom is_finite_g_in : Finite.is_finite predT <:g_in>.
  axiom distr_g_out    : is_lossless dg_out.

  axiom inj_get_upk (x y : uskey): get_upk x = get_upk y => x = y.

  axiom get_pk_inj (x y: skey):  get_pk x = get_pk y => x = y.

  (* Policy restrictions *)
  axiom San_mem (L: (ident * (plain * upkey * cipher*sign)) list) x:
    mem (Sanitize L) x => mem L x. 

  axiom San_mem'(L : (ident * (upkey * cipher * sign)) list) x:
    x \in Sanitize L => x \in L. 

  axiom San_mem''(L : (upkey * (ident * cipher * sign)) list) x:
    x \in Sanitize L => x \in L. 

  axiom San_map (L: (upkey*cipher*sign)list) 
                (p: upkey * cipher* sign -> plain):
   let g = (fun (x: upkey*cipher*sign), (x.`1, p x)) in
   Sanitize (map g L) = map g (USan L).



  (* axioms restricting possible values for Sanitize *)
  axiom San_uniq (L: (ident * plain * upkey * cipher * sign) list): 
    uniq (Sanitize (map pack4 L)).

  axiom San_uniq_fst (L: (upkey * (ident * cipher * sign)) list):
     uniq (map fst (Sanitize L)).
 
  axiom San_uniq_fst' (L : (ident * (upkey * cipher * sign)) list):
     uniq (map fst (Sanitize L)). 

  axiom San_uniq_snd (S: (ident * (upkey * cipher * sign)) list ):
     uniq (map snd S) =>
     uniq (map snd (Sanitize S)). 
 
  axiom San_uniq' (L : (ident * (upkey * cipher * sign)) list):
      uniq (Sanitize L).


  (* axiom that maintains Sanitize membership w.r.t a filtered list *)
  axiom San_mem_filter (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in Sanitize L => 
     x \in Sanitize (filter f L).

  axiom San_filter_ident (L: (ident * (upkey * cipher * sign)) list) p:
    let f = (fun (x: ident * (upkey * cipher * sign)), p x.`1) in
    Sanitize (filter f L) =  filter f (Sanitize L). 

  axiom uniq_to_San (A: (upkey * (cipher * sign)) list):
    uniq (map fst A) =>
    Sanitize A = A.    
           
  axiom USan_USanitize (A: (ident *upkey*cipher*sign) list):
    map remi (USanitize A) = USan (map remi A). 

  axiom San'_USan (L: (ident*upkey*cipher*sign) list):
    (forall x y, x \in L => y \in L => x.`1 = y.`1 => x.`2 = y.`2) =>
    (forall x y, x \in L => y \in L => x.`2 = y.`2 => x.`1 = y.`1) =>
    Sanitize' L = USanitize L.
 
  (* axiom for determinism of Count, based on L1 is a permutation of L2 *)
  axiom Count_perm (L1 L2: plain list):
    perm_eq L1 L2 => Count L1 = Count L2.

  axiom Count_split (L1 L2: plain list):
    Count (L1 ++ L2) = Count L1 + Count L2.
  axiom add_com (a b : result):
    a + b = b + a. 

(* adversary lossless *)
  axiom Acor_ll (O <: BPRIV_Oracles{BPRIV_A}): 
    islossless BPRIV_A(O).corL.

  axiom BAa1_ll (O <: BPRIV_Oracles { BPRIV_A }):
    islossless O.vote    => islossless O.cast    =>
    islossless O.board   => islossless O.h       =>
    islossless O.g       => islossless O.j       =>
    islossless BPRIV_A(O).a1.
  axiom BAa2_ll (O <: BPRIV_Oracles { BPRIV_A }):
    islossless O.board => islossless O.h     =>
    islossless O.g     => islossless O.j     =>
    islossless BPRIV_A(O).a2.

  axiom CAa_ll (O <: SCons_Oracle { CONSIS2_A }):
    islossless O.h       => islossless O.g       =>
    islossless CONSIS2_A(O).main.

  axiom DAa1_ll (O <: DBB_Oracle { DBB_A }):
    islossless O.corrupt => islossless O.vote => 
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DBB_A(O).a1.
  axiom DAa2_ll (O <: DBB_Oracle { DBB_A }):
    islossless O.check =>  islossless O.h  =>
    islossless O.g     =>  islossless O.j  =>
    islossless DBB_A(O).a2.

  axiom RAa2_ll (O <: DREG_Oracle { DREG_A }):
    islossless O.corrupt => islossless O.vote => 
    islossless O.cast    => islossless O.board =>
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DREG_A(O).a2.
  axiom RAa3_ll (O <: DREG_Oracle { DREG_A }):
    islossless O.check   => islossless O.board =>
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DREG_A(O).a3.
  
  axiom ATU_ll (A<: TallyUniq_Adv') (V <: VotingScheme{A}) 
               (H <: HOracle.ARO{A}) 
              (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
    islossless A(V, H, G, J).main.

  axiom ATU2_ll (A<: TallyUniq_Adv_2) (V <: VotingScheme'{A}) 
               (H <: HOracle.ARO{A}) 
              (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
    islossless A(V, H, G, J).main.

  (* -> Proof system *)
  axiom Pz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Pz(G).prove. 
  axiom Vz_ll (G <: GOracle.ARO):
    islossless G.o => islossless Vz(G).verify.

  (* -> Small proof system, for ciphertexts *)
  axiom Pe_ll (H <: HOracle.ARO):
    islossless H.o =>  islossless Pe(H).prove.
  axiom Ve_ll (H <: HOracle.ARO):
    islossless H.o =>  islossless Ve(H).verify.
  axiom Ve_ll' (H <: HOracle.ARO):
    islossless H.o =>  islossless Ve'(H).verify.

  (* -> Signature *)
  axiom SSk_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).kgen.
  axiom SSs_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).sign. 
  axiom SSv_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).verify. 

  (* -> for ZK simulator *)
  axiom Si_ll: islossless S.init.
  axiom So_ll: islossless S.o.
  axiom Sp_ll: islossless S.prove.

  (* for extractor *)
  axiom Ex_ll (G <: GOracle.ARO):
    islossless G.o =>
    islossless Ex(G).extract.

  axiom Vz_keepstate (gv: (glob Vz)) :
    phoare [Vz(GRO.RO).verify:  
           (glob Vz) = gv ==> (glob Vz) = gv ] = 1%r.

  axiom Verify_to_verify (gv: (glob Ve)) (s: h_stm) (p: h_prf):
    phoare[Ve(HRO.RO).verify: (glob Ve) = gv /\ arg = (s, p)
         ==>
         (glob Ve) = gv /\
         res = verify s p HRO.RO.m] = 1%r.
  axiom Verify_to_verify' (gv: (glob Ve')) (s: h_stm) (p: h_prf):
    phoare[Ve'(HRO.RO).verify: (glob Ve') = gv /\ arg = (s, p)
         ==>
         (glob Ve') = gv /\
         res = verify s p HRO.RO.m] = 1%r.

  axiom Prover_to_verify_one (gp: (glob Pe)) (s: h_stm) (w: h_wit):
    phoare[Pe(HRO.RO).prove: 
          (glob Pe) = gp /\ arg = (s, w)
          ==>
          (glob Pe) = gp /\ verify s res HRO.RO.m]= 1%r.

  (* axiom for transforming a proof in a verification (two-sided) *)
  axiom Prover_to_verify_two (s: h_stm) (w: h_wit):
    equiv[Pe(HRO.RO).prove ~ Pe(HRO.RO).prove: 
         ={glob HRO.RO, glob Pe, arg} /\ arg{2} = (s, w)
         ==>
         ={glob HRO.RO, glob Pe, res} /\
         verify s res{2} HRO.RO.m{2}].

  axiom Kgen_get_upk (J<: JOracle.ARO):
    equiv [SS(J).kgen ~ SS(J).kgen:  
           ={glob J, glob SS} ==> 
           ={glob J, glob SS,  res} /\  res{2}.`2 = get_upk res{2}.`1].
  axiom  Kgen_get_upk' (J1 <: JOracle.ARO) (J2 <: JOracle.ARO):
    equiv [SS(J1).kgen ~ SS(J2).kgen:  
           ={ glob SS} ==>  ={glob SS,  res} /\ res{2}.`2 = get_upk res{2}.`1].

  axiom SSk_eq (gs : (glob SS)) (O <: ARO):
    phoare [SS(O).kgen: 
          (glob SS) = gs ==>  
           (glob SS) = gs /\ res.`2 = get_upk res.`1] = 1%r.

  axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)  ==>
             (glob SS) =gs /\ res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
  (* admit for know: trye to prove using above *)
  axiom ver_Ver_one' (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(MEUF_Oracle(SS,JRO.RO)).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2) ==>
             (glob SS) =gs /\ res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
  axiom Sig_ver_one (gs: (glob SS)) (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   phoare [SS(JRO.RO).sign :
           gs = (glob SS) /\ arg = (usk2,c2) ==>   
           gs = (glob SS) /\ ver_sign upk2 c2 res JRO.RO.m ]=1%r.
   axiom Sig_ver_two (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   equiv [SS(JRO.RO).sign ~ SS(JRO.RO).sign :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (usk2,c2) ==>   
          ={glob JRO.RO, glob SS, res} /\ ver_sign upk2 c2 res{2} JRO.RO.m{2} ].

(* ** end *)


local lemma San_map' (sk: skey, bb: (upkey * cipher*sign) list, mm: (h_in,h_out) map):
  let f   = fun (x : upkey * cipher*sign),
                (x.`1, oget (dec_cipher sk x.`1 x.`2 mm)) in 
  Sanitize (map f bb) =
  map (f \o (fun (x : upkey * (cipher * sign)), (x.`1, x.`2.`1,x.`2.`2))) 
         (Sanitize (map (fun (x : upkey * cipher * sign), (x.`1, (x.`2,x.`3))) bb)).
proof.
  move => f.  
  rewrite map_comp -/upack_2 -/uopen_2 -/USan.
  cut := San_map bb (fun (x : upkey * cipher * sign) => oget (dec_cipher sk x.`1 x.`2 mm)) .
  by rewrite //= -/f. 
qed.

local lemma Ek_ll (H <: HOracle.ARO): 
  islossless H.o => islossless IPKE(Pe,Ve,H).kgen.
proof.
  move => Ho.
  proc. auto=>/>; smt.
qed.

local lemma Ek_ll' (H <: HOracle.ARO): 
  islossless IPKE(Pe,Ve,H).kgen.
proof.
  proc. auto=>/>; smt.
qed.

local lemma Ee_ll(H <: HOracle.ARO): 
    islossless H.o => islossless IPKE(Pe,Ve,H).enc.
proof.
  move => Ho.
  proc.
  call (Pe_ll H Ho).
  by auto; smt.
qed.

local lemma Ed_ll(H <: HOracle.ARO): 
    islossless H.o => islossless IPKE(Pe,Ve,H).dec.
proof.
  move => Ho.
  proc.
  by wp; call (Ve_ll H Ho); wp.
qed.

local lemma Co_ll (H <: HOracle.ARO) (J<: JOracle.ARO):
    islossless H.o =>
    islossless J.o =>
    islossless CIND(Ve',IPKE(Pe, Ve),SS,H,J).validInd.
proof.
  move => Ho Jo.
  proc.
  call (SSv_ll SS J Jo). 
  by call (Ve_ll' H Ho).
qed.

local lemma Ekgen_extractPk (H<: HOracle.ARO):
    equiv [IPKE(Pe,Ve,H).kgen ~ IPKE(Pe,Ve,H).kgen:  
          ={glob H, glob IPKE(Pe,Ve)}  ==> 
          ={glob H, glob IPKE(Pe,Ve),  res} /\ res{2}.`1 = get_pk res{2}.`2].
proof.
  proc.
  by auto=>/>; smt.
qed.

lemma Ekgen_extractPk_one (ge: glob IPKE(Pe,Ve)) (H<: HOracle.ARO):
    phoare [IPKE(Pe,Ve,H).kgen: 
           ge = (glob IPKE(Pe,Ve)) ==> 
           ge = (glob IPKE(Pe,Ve)) /\ 
          res.`1 = get_pk res.`2] = 1%r.
proof.
  by proc; auto; smt.
qed.

local lemma Edec_Odec (ge: (glob IPKE(Pe,Ve))) (sk2: skey)(l2: upkey) (c2: cipher):
    phoare [IPKE(Pe,Ve,HRO.RO).dec:  
           (glob IPKE(Pe,Ve)) =ge /\ arg = (sk2, l2, c2)
           ==>   
           (glob IPKE(Pe,Ve)) =ge /\
           res = dec_cipher sk2 l2 c2 HRO.RO.m ] = 1%r.
proof.
  proc.  
  exists* (glob Ve); elim* => gv.
  wp; call(Verify_to_verify gv ((get_pk sk2), l2, c2.`1) c2.`2).
  by auto=>/>; smt.  
qed.

local lemma Ee_eq (ge: (glob IPKE(Pe,Ve))):
    phoare [IPKE(Pe,Ve,HRO.RO).enc: 
          (glob IPKE(Pe,Ve)) = ge ==> (glob IPKE(Pe,Ve)) = ge] = 1%r.
proof.
  proc.  
  seq 2: (((glob Pe), (glob Ve)) = ge)=>//=.
    by auto=>/>; smt.
  exists * (glob Pe), pk, lbl, c, p, r;
    elim*=> gp pkx lblx cx px rx.
  call (Prover_to_verify_one gp (pkx, lblx, cx) (px,rx)).
  by auto.

  hoare.
  by auto.
qed.

local lemma Eenc_decL (ge: (glob IPKE(Pe,Ve))) (sk2: skey) 
                  (pk2: pkey)(l2: upkey) (p2: plain): 
  (pk2 = get_pk sk2) =>
  phoare [IPKE(Pe,Ve,HRO.RO).enc : 
          (glob IPKE(Pe,Ve)) = ge /\ arg=(pk2, l2, p2) 
          ==> 
          (glob IPKE(Pe,Ve)) = ge /\
          Some p2 = dec_cipher sk2 l2 res HRO.RO.m ]= 1%r.   
proof.
  move => Hkp.
  proc.
  seq 2: (((glob Pe), (glob Ve)) = ge /\ 
           (pk, lbl, p) = (pk2, l2, p2) /\ c = encrypt pk p r)=>//=.
    by auto; smt.
  exists * (glob Pe), pk, lbl, c, p, r;
    elim*=> gp pkx lblx cx px rx.
  call (Prover_to_verify_one gp (pkx, lblx, cx) (px,rx)).
  auto; progress.
  by rewrite /dec_cipher //= -Hkp H Hkp correctness. 

  by hoare; auto.
qed.
  
local lemma Eenc_dec (sk2: skey) (pk2: pkey) (l2: upkey) (p2: plain): 
  (pk2 = get_pk sk2) =>
  equiv [IPKE(Pe,Ve,HRO.RO).enc ~ IPKE(Pe,Ve,HRO.RO).enc : 
         ={glob HRO.RO, glob IPKE(Pe,Ve), arg} /\ arg{2}=( pk2, l2, p2) 
         ==> 
         ={glob HRO.RO, glob IPKE(Pe,Ve),  res} /\
         Some p2 = dec_cipher sk2 l2 res{2} HRO.RO.m{2}]. 
proof.
  move => Hkp. proc.
  seq 2 2: (={ HRO.RO.m, glob Pe, glob Ve, pk, lbl, p, c, r} /\ 
           (pk{1}, lbl{1}, p{1}) = (pk2, l2, p2) /\ 
           c{1} = encrypt pk{1} p{1} r{1})=>//=.
    by auto; smt.
  exists * c{1}, r{1}; elim*=> cx rx.
  call (Prover_to_verify_two (pk2, l2, cx) (p2,rx)).
  auto; progress.
  by rewrite /dec_cipher //= -Hkp H Hkp correctness. 
qed. 
 
local lemma SSs_eq (gss: (glob SS)):
    phoare [SS(JRO.RO).sign: 
          (glob SS) = gss ==> (glob SS) = gss] = 1%r. 
proof.
  proc*. 
  exists* usk, m; elim* => ux mx.
  call (Sig_ver_one gss ux (get_upk ux) mx _); first by done.
  by auto.  
qed.

local lemma validInd_dec_one (gc : (glob Ve'))  (ge: (glob IPKE(Pe,Ve))) (gs: (glob SS))
                      (sk: skey) (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
  (pk = get_pk sk)=>
  phoare [CIND(Ve',IPKE(Pe, Ve),SS,HRO.RO,JRO.RO).validInd : 
          gc = (glob Ve') /\ (glob IPKE(Pe,Ve)) = ge /\ (glob SS) = gs /\ arg=(b,pk) 
          ==> 
          gc = (glob Ve') /\  (glob IPKE(Pe,Ve)) = ge /\ (glob SS) = gs /\
          res = (dec_cipher sk b.`2 b.`3 HRO.RO.m <> None /\
                 ver_sign b.`2 b.`3 b.`4 JRO.RO.m)]=1%r. 
proof.
  move => Hkp.
  proc. 
  call (ver_Ver_one gs b.`2 b.`3 b.`4). 
  exists* (glob Ve'); elim* => gv.
  call (Verify_to_verify' gv (pk,b.`2,b.`3.`1) b.`3.`2).
  auto; progress.
  rewrite eq_iff. progress.
  + rewrite Hkp in H. 
    by rewrite /dec_cipher //=  H (verify_validDec sk (b{hr}.`2, b{hr}.`3) HRO.RO.m{hr} H). 
  - rewrite /dec_cipher in H. 
    smt.
qed.
  
(* Lemma bounding BPRIV  *) 
 
local module C(E: Scheme, SS: SignScheme, H: HOracle.ARO, J: JOracle.ARO) = 
            CIND(Ve', E,SS,H,J).

local lemma validInd_dec_one' (gc : (glob C)) (ge : (glob IPKE(Pe, Ve))) (gs : (glob SS))
    (sk : skey) (pk : pkey) (b : ident * upkey * cipher * sign):
    pk = get_pk sk =>
    phoare[ C(IPKE(Pe, Ve), SS, HRO.RO, JRO.RO).validInd :
             (glob C) = gc /\
             (glob IPKE(Pe, Ve)) = ge /\ (glob SS) = gs /\ arg = (b, pk) ==>
             (glob C) = gc /\
             (glob IPKE(Pe, Ve)) = ge /\
             (glob SS) = gs /\
             res =
             (dec_cipher sk b.`2 b.`3 HRO.RO.m <> None /\
              ver_sign b.`2 b.`3 b.`4 JRO.RO.m)] = 1%r. 
proof.
  move => Hkp. progress.
  proc*. by call (validInd_dec_one gc ge gs sk pk b Hkp). 
qed.

local lemma validInd_ax (gc: (glob C)) (ge: (glob IPKE(Pe, Ve))) (gs: (glob SS))
                    (pk: pkey) (b: ident * upkey * cipher*sign):
  phoare [C(IPKE(Pe, Ve),SS,HRO.RO,JRO.RO).validInd : 
          (glob C) = gc /\ (glob IPKE(Pe, Ve)) = ge /\ (glob SS) = gs /\ arg=(b,pk) 
          ==> 
          (glob C) = gc /\ (glob IPKE(Pe, Ve)) = ge /\ (glob SS) = gs /\
          res = (validInd pk (b.`1,b.`2, b.`3) HRO.RO.m /\
                 ver_sign b.`2 b.`3 b.`4 JRO.RO.m)]=1%r.
proof.
  proc.
  rewrite /validInd.
  call (ver_Ver_one gs b.`2 b.`3 b.`4). 
  exists* (glob Ve'); elim* => gv.
  call (Verify_to_verify' gv (pk,b.`2,b.`3.`1) b.`3.`2).
  by auto.
qed.

  
lemma bpriv &m:
    `|Pr[BPRIV_L(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ]-
      Pr[BPRIV_R(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ]| 
<=
    `|Pr[ZK_L(DecRel(IPKE(Pe, Ve),HRO.RO), Pz, 
                     BZK(IPKE(Pe, Ve),Pz,CIND(Ve',IPKE(Pe, Ve),SS),Vz,SS,BPRIV_A,HRO.RO,JRO.RO), GRO.RO).main() @ &m : res] -
      Pr[ZK_R(DecRel(IPKE(Pe, Ve),HRO.RO), S, 
                     BZK(IPKE(Pe, Ve),Pz,CIND(Ve',IPKE(Pe, Ve),SS),Vz,SS,BPRIV_A,HRO.RO,JRO.RO)).main() @ &m : res]| +
n%r *
    `|Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
                                               BPRIV_A, SS, JRO.RO,S), IPKE(Pe, Ve), HRO.RO), 
               HRO.RO, Left).main() @ &m : 
               res /\ WrapAdv.l <= n /\ size BS.encL <= 1] -
      Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
                                               BPRIV_A, SS, JRO.RO,S), IPKE(Pe, Ve), HRO.RO), 
               HRO.RO, Right).main() @ &m : 
               res /\ WrapAdv.l <= n /\ size BS.encL <= 1]|.
proof. 
  cut H:= bpriv (IPKE(Pe, Ve)) Pz Vz SS S C BPRIV_A weight_dg_out weight_dj_out weight_dk_out
          is_finite_h_in distr_h_out San_map USan_USanitize Acor_ll BAa1_ll BAa2_ll Pz_ll Vz_ll 
          Ek_ll Ee_ll Ed_ll (SSk_ll SS) (SSs_ll SS) (SSv_ll SS) Si_ll So_ll Sp_ll Co_ll 
          Ekgen_extractPk Edec_Odec Ee_eq Eenc_decL Eenc_dec SSs_eq ver_Ver_one 
          Sig_ver_one Sig_ver_two validInd_dec_one' &m. 
  have ->: Pr[BPRIV_L(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                        BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] =
           Pr[BPRIV_L(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
                        BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
    by byequiv=>//=; sim.
  have ->: Pr[BPRIV_R(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                        BPRIV_A, HRO.RO, GRO.RO, JRO.RO, S).main () @ &m : res] =
           Pr[BPRIV_R(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
                        BPRIV_A, HRO.RO, GRO.RO, JRO.RO, S).main () @ &m : res]
     by byequiv=>//=; sim.
  have ->: Pr[ZK_L(DecRel(IPKE(Pe, Ve), HRO.RO), Pz, BZK(IPKE(Pe, Ve), Pz, 
                  CIND(Ve', IPKE(Pe, Ve), SS), Vz, SS, BPRIV_A, HRO.RO, JRO.RO),
                   GRO.RO).main() @ &m : res] =
           Pr[ZK_L(DecRel(IPKE(Pe, Ve), HRO.RO), Pz, BZK(IPKE(Pe, Ve), Pz, 
                   C(IPKE(Pe, Ve), SS), Vz, SS, BPRIV_A, HRO.RO, JRO.RO), 
                   GRO.RO).main() @ &m : res]
    by byequiv=>//=; sim.
  have ->: Pr[ZK_R(DecRel(IPKE(Pe, Ve), HRO.RO), S, BZK(IPKE(Pe, Ve), Pz, 
                   CIND(Ve', IPKE(Pe, Ve), SS), Vz, SS, BPRIV_A, HRO.RO, JRO.RO)
                   ).main() @ &m : res] = 
           Pr[ZK_R(DecRel(IPKE(Pe, Ve), HRO.RO), S, BZK(IPKE(Pe, Ve), Pz, 
                   C(IPKE(Pe, Ve), SS), Vz, SS, BPRIV_A, HRO.RO, JRO.RO)
                   ).main () @ &m : res]
    by byequiv=>//=; sim.
  have ->: Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve), Pz, Vz, 
              CIND(Ve', IPKE(Pe, Ve), SS), SS), BPRIV_A, SS, JRO.RO, S), 
              IPKE(Pe, Ve), HRO.RO), HRO.RO, Left).main () @ &m : 
              res /\ WrapAdv.l <= n /\ size BS.encL <= 1] = 
           Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve), Pz, Vz, 
              C(IPKE(Pe, Ve), SS), SS), BPRIV_A, SS, JRO.RO, S), 
              IPKE(Pe, Ve), HRO.RO), HRO.RO, Left).main() @ &m : 
              res /\ WrapAdv.l <= n /\ size BS.encL <= 1] 
    by byequiv=>//=; sim.
  have ->: Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve), Pz, Vz, 
              CIND(Ve', IPKE(Pe, Ve), SS), SS), BPRIV_A, SS, JRO.RO, S), 
              IPKE(Pe, Ve), HRO.RO), HRO.RO, Right).main() @ &m : 
              res /\ WrapAdv.l <= n /\ size BS.encL <= 1] =
           Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve), Pz, Vz, 
              C(IPKE(Pe, Ve), SS), SS), BPRIV_A, SS, JRO.RO, S), 
              IPKE(Pe, Ve), HRO.RO), HRO.RO, Right).main() @ &m : 
              res /\ WrapAdv.l <= n /\ size BS.encL <= 1]
    by byequiv=>//=; sim.
  by rewrite H.
qed.

lemma consis1(id: ident, v: plain, l: uskey) &m: 
   Pr[PKE.Correctness(IPKE(Pe, Ve), HRO.RO).main(v,get_upk l) @ &m: res] <=
   Pr[SConsis1(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
       CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id,v, l) @ &m: res].
proof.
  cut H:= consis1 HRO.RO GRO.RO (IPKE(Pe,Ve)) S Vz Pz C SS CONSIS2_A CONSIS3_A
  weight_dj_out weight_dk_out is_finite_h_in distr_h_out is_finite_j_in distr_j_out 
  (GRO.RO_init_ll is_finite_g_in distr_g_out) GRO.RO_o_ll
  Co_ll Pz_ll Ek_ll CAa_ll (SSk_ll SS) (SSs_ll SS) Ekgen_extractPk Ekgen_extractPk_one
  validInd_dec_one' validInd_ax  Edec_Odec ver_Ver_one id v l &m.

  have ->:  Pr[SConsis1(B'(IPKE(Pe, Ve), Pz, Vz, 
               CIND(Ve', IPKE(Pe, Ve), SS), SS), CE(IPKE(Pe, Ve)), 
               HRO.RO, GRO.RO, JRO.RO).main (id, v, l) @ &m : res] =
            Pr[SConsis1(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
               CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id, v, l) @ &m : res]
    by byequiv =>/>; sim.
  by rewrite H.
qed.

lemma consis2 &m: 
  Pr[SConsis2(B'(IPKE(Pe,Ve),Pz,Vz,CIND(Ve', IPKE(Pe, Ve),SS),SS), 
                 CIND(Ve',IPKE(Pe,Ve),SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] = 1%r.
proof.
  cut H:= consis2 HRO.RO GRO.RO (IPKE(Pe,Ve)) S Vz Pz C SS CONSIS2_A CONSIS3_A
  weight_dj_out weight_dk_out is_finite_h_in distr_h_out is_finite_j_in distr_j_out 
  (GRO.RO_init_ll is_finite_g_in distr_g_out) GRO.RO_o_ll
  Co_ll Pz_ll Ek_ll CAa_ll (SSk_ll SS) (SSs_ll SS) Ekgen_extractPk Ekgen_extractPk_one
  validInd_dec_one' validInd_ax  Edec_Odec ver_Ver_one &m.
  progress.
  have ->:  Pr[SConsis2(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
               CIND(Ve', IPKE(Pe, Ve), SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res] =
            Pr[SConsis2(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
               C(IPKE(Pe, Ve), SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
    by byequiv =>/>; sim.
  by rewrite H.
qed.

equiv consis3 &m:
    SConsis3_L(B'(IPKE(Pe,Ve),Pz,Vz, CIND(Ve',IPKE(Pe,Ve),SS),SS),  
               CIND(Ve',IPKE(Pe, Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main ~ 
    SConsis3_R(B'(IPKE(Pe,Ve),Pz,Vz, CIND(Ve', IPKE(Pe, Ve),SS),SS), 
               CE(IPKE(Pe,Ve)), CIND(Ve', IPKE(Pe, Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main
    : ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob IPKE(Pe,Ve), 
        glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> ={res}. 
proof.
  cut H:= consis3 HRO.RO GRO.RO (IPKE(Pe,Ve)) S Vz Pz C SS CONSIS2_A CONSIS3_A
  weight_dj_out weight_dk_out is_finite_h_in distr_h_out is_finite_j_in distr_j_out 
  (GRO.RO_init_ll is_finite_g_in distr_g_out) GRO.RO_o_ll
  Co_ll Pz_ll Ek_ll CAa_ll (SSk_ll SS) (SSs_ll SS) Ekgen_extractPk Ekgen_extractPk_one
  validInd_dec_one' validInd_ax  Edec_Odec ver_Ver_one &m.
  transitivity SConsis3_L(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
                         C(IPKE(Pe, Ve), SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main
  ( ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob IPKE(Pe,Ve), 
        glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> ={res})
  ( ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob IPKE(Pe,Ve), 
        glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> ={res})=>//=.
  + progress; smt.
  + proc. sim.
  transitivity SConsis3_R(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
               CE(IPKE(Pe, Ve)), C(IPKE(Pe, Ve), SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main
  ( ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob IPKE(Pe,Ve), 
        glob Ve', glob SS, glob CONSIS3_A} ==> ={res})
  ( ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob IPKE(Pe,Ve), 
        glob Ve', glob SS, glob CONSIS3_A} ==> ={res})=>//=.
  + progress; smt.
  by proc; sim.
qed.

lemma bound_dbb (COR <: Corr_Adv{WU, BU, JRO.RO, SS}) &m:
  islossless COR(JRO.RO).main=>
  Pr[DBB_Exp (B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe,Ve),SS),SS), 
              DBB_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv(B(IPKE(Pe, Ve), Pz, Vz, SS), 
              DBB_A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
n%r ^2 * 
  Pr[Wspread_Exp(IPKE(Pe, Ve), 
                 BWspread(B(IPKE(Pe, Ve), Pz, Vz,SS),GRO.RO,JRO.RO,DBB_A), 
                 HRO.RO).main() @ &m: BS.eq ] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BE(SS, COR), JRO.RO).main() @ &m : res] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(IPKE(Pe, Ve), HRO.RO, GRO.RO, DBB_A, SS), JRO.RO)), 
              JRO.RO).main () @ &m : res] +
(size (undup Voters))%r * 
  Pr[Correctness(SS, COR, JRO.RO).main() @ &m : !res] +
  Pr[PS.Correctness(DecRel(IPKE(Pe, Ve), HRO.RO), 
          BCorr_Adv(IPKE(Pe, Ve), BAcc(B(IPKE(Pe, Ve), Pz, Vz, SS), DBB_A), 
          HRO.RO, SS, JRO.RO), Pz, Vz, GRO.RO).main() @ &m : !res] + 
  Pr[PoK_Exp(Vz, DecRel(IPKE(Pe, Ve), HRO.RO), Ex, 
         PoK_TU(BTall(B(IPKE(Pe, Ve), Pz, Vz, SS), DBB_A), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m : res]. 
proof.
  move => Co. 
  cut Hdbb := bound_dbb (IPKE(Pe,Ve)) Pz Vz SS DBB_A size_Voters n_pos weight_dg_out 
     weight_dh_out weight_dk_out inj_get_upk Pz_ll Vz_ll Ek_ll' Ed_ll Ee_ll SSk_ll SSs_ll 
     (SSv_ll SS) DAa1_ll DAa2_ll Ekgen_extractPk Kgen_get_upk Kgen_get_upk' Edec_Odec 
     Eenc_dec ver_Ver_one ver_Ver_one' Sig_ver_two Vz_keepstate SSs_eq SSk_eq 
     San_mem San_mem' San_mem'' San_map' San_uniq San_uniq_fst San_uniq_snd San_uniq' 
     (* San_uniq_filter *) 
     San_mem_filter San_filter_ident uniq_to_San USan_USanitize Count_perm Count_split 
     add_com COR &m Co.
  have ->: Pr[DBB_Exp(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                DBB_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res] =
           Pr[DBB_Exp(B(IPKE(Pe, Ve), Pz, Vz, SS), 
                DBB_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res]
    by byequiv =>/>; sim.
 
  cut Hacc:= bound_acc_dbb (C(IPKE(Pe,Ve),SS))  (IPKE(Pe,Ve)) Pz Vz SS Ee_ll Ed_ll Pz_ll 
         Vz_ll Ekgen_extractPk  Ee_ll Ed_ll Pz_ll Vz_ll Ekgen_extractPk DBB_A &m.

  cut Htu:= bound_tally_uniq (C(IPKE(Pe,Ve),SS)) (IPKE(Pe,Ve)) Pz Vz Ex SS 
         weight_dh_out weight_dg_out weight_dj_out weight_dk_out is_finite_h_in is_finite_g_in
         is_finite_j_in get_pk_inj Ee_ll Ed_ll Ex_ll Vz_ll ATU_ll Edec_Odec 
         DBB_A
         weight_dh_out weight_dg_out weight_dj_out weight_dk_out is_finite_h_in is_finite_g_in
         is_finite_j_in get_pk_inj Ee_ll Ed_ll Ex_ll Vz_ll ATU_ll Edec_Odec &m.
  smt.
qed. 

(* local module Bacc' (A: DREG_Adv,V: VotingScheme') = BAcc'(V,A).*)

lemma bound_dreg &m:
  Pr[DREG_Exp (B'(IPKE(Pe,Ve),Pz,Vz,CIND(Ve',IPKE(Pe,Ve),SS),SS), 
               DREG_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv'(B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS),
               DREG_A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
n%r ^2 * 
  Pr[Wspread_Exp(IPKE(Pe,Ve), 
                 BWspread'(B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS),SS),
                    GRO.RO,JRO.RO,DREG_A), HRO.RO).main() @ &m: BS.eq ] +
  Pr[PS.Correctness(DecRel(IPKE(Pe, Ve), HRO.RO), 
                    BCorr_Adv(IPKE(Pe, Ve), 
                              BAcc'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe,Ve), SS), SS), 
                              DREG_A), 
          HRO.RO, SS, JRO.RO), Pz, Vz, GRO.RO).main() @ &m : !res] + 
  Pr[PoK_Exp(Vz, DecRel(IPKE(Pe, Ve), HRO.RO), Ex, 
         PoK_TU(BTall'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe,Ve), SS), SS), DREG_A), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m : res].

proof.
  cut H := bound_dreg (IPKE(Pe,Ve)) Pz Vz SS C DREG_A size_Voters n_pos weight_dg_out 
     weight_dh_out weight_dk_out inj_get_upk Pz_ll Vz_ll Ek_ll' Ed_ll Ee_ll SSk_ll SSs_ll 
     (SSv_ll SS) Co_ll RAa2_ll RAa3_ll Ekgen_extractPk Kgen_get_upk Kgen_get_upk' Edec_Odec 
     Eenc_dec ver_Ver_one ver_Ver_one' Sig_ver_two Vz_keepstate SSs_eq SSk_eq 
     validInd_dec_one'  San_mem San_mem' San_mem'' San_map' San_uniq San_uniq_fst San_uniq_fst' 
     San_uniq_snd San_uniq' (* San_uniq_filter *)
     San_mem_filter San_filter_ident USan_USanitize San'_USan Count_perm Count_split add_com &m.
  have ->: Pr[DREG_Exp(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                         DREG_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] =
           Pr[DREG_Exp(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
                         DREG_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res]
    by byequiv =>/>; sim.
  have ->: Pr[Hash_Exp(BHash_Adv'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                      DREG_A, HRO.RO, GRO.RO, JRO.RO)).main () @ &m : res] =
           Pr[Hash_Exp(BHash_Adv'(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
                      DREG_A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res]
    by byequiv =>/>; sim.
  have ->: Pr[Wspread_Exp(IPKE(Pe, Ve), 
                         BWspread'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                            GRO.RO, JRO.RO, DREG_A), HRO.RO).main() @ &m : BS.eq] =
           Pr[Wspread_Exp(IPKE(Pe, Ve), 
                         BWspread'(B'(IPKE(Pe, Ve), Pz, Vz, C(IPKE(Pe, Ve), SS), SS), 
                            GRO.RO, JRO.RO, DREG_A), HRO.RO).main() @ &m : BS.eq]
    by byequiv =>/>; sim.
  cut Hacc:= bound_acc_dreg (C(IPKE(Pe,Ve),SS))  (IPKE(Pe,Ve)) Pz Vz SS Ee_ll Ed_ll Pz_ll 
         Vz_ll Ekgen_extractPk  Ee_ll Ed_ll Pz_ll Vz_ll Ekgen_extractPk DREG_A &m.

  cut Htu:= bound_tally_uniq' C (IPKE(Pe,Ve)) Pz Vz Ex SS 
         weight_dh_out weight_dg_out weight_dj_out is_finite_h_in is_finite_g_in
         is_finite_j_in get_pk_inj Ee_ll Ed_ll Ex_ll Vz_ll ATU2_ll Edec_Odec 
         DREG_A
         weight_dh_out weight_dg_out weight_dj_out is_finite_h_in is_finite_g_in
         is_finite_j_in get_pk_inj Ee_ll Ed_ll Ex_ll Vz_ll ATU2_ll Edec_Odec &m.
  move: H Hacc Htu.
  have ->: Pr[PS.Correctness(DecRel(IPKE(Pe, Ve), HRO.RO), 
                             BCorr_Adv(IPKE(Pe, Ve), BAcc'(B'(IPKE(Pe, Ve), Pz, Vz, 
                             CIND(Ve', IPKE(Pe, Ve), SS), SS), DREG_A), HRO.RO, SS, JRO.RO), 
                             Pz, Vz, GRO.RO).main() @ &m : !res] =
           Pr[PS.Correctness(DecRel(IPKE(Pe, Ve), HRO.RO), 
                             BCorr_Adv(IPKE(Pe, Ve), BAcc'(B'(IPKE(Pe, Ve), Pz, Vz, 
                             C(IPKE(Pe, Ve), SS), SS), DREG_A), HRO.RO, SS, JRO.RO), 
                             Pz, Vz, GRO.RO).main () @ &m : !res];
    first by byequiv =>/>; sim.

  have ->: Pr[PoK_Exp(Vz, DecRel(IPKE(Pe, Ve), HRO.RO), Ex, 
                     PoK_TU(BTall'(B'(IPKE(Pe, Ve), Pz, Vz, 
                         C(IPKE(Pe, Ve), SS), SS), DREG_A),
                     HRO.RO, JRO.RO), GRO.RO).main() @ &m : res] =
           Pr[PoK_Exp(Vz, DecRel(IPKE(Pe, Ve), HRO.RO), Ex, 
                      PoK_TU(BTall'(B'(IPKE(Pe, Ve), Pz, Vz,  
                         CIND(Ve', IPKE(Pe, Ve), SS), SS), DREG_A), 
                      HRO.RO, JRO.RO), GRO.RO).main() @ &m : res];
    first by byequiv =>/>; sim.
  smt.  
qed.

end section Security.