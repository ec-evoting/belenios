require import Int Bool List Distr AllCore FMap.
require import LeftOrRight.
require (*  *) ROM.

(* * Types and operators for voting scheme *)
(* ---------------------------------------------------------------------- *)

type ident, vote, result, ballot, pubBB, 
     pkey, skey, prf, sign, uskey, upkey, hash_key.  
  
op get_upk: uskey -> upkey.

(* filter policy *)
op Sanitize['a 'b]: ('b * 'a) list -> ('b * 'a) list.

(* counting method *)
op Count     : vote list -> result. 

(* list of voters *)
op Voters: ident list.

(* number of honest queries allowed *)
op qVo: {int | 0< qVo} as qVo_pos.


op pack4 (x: ident * vote * upkey * ballot * sign) = 
         (x.`1, (x.`2,x.`3,x.`4,x.`5)).
op open4 (x: ident * (vote * upkey * ballot * sign)) = 
         (x.`1, x.`2.`1,x.`2.`2,x.`2.`3,x.`2.`4).
op pack3 (x: ident * upkey * ballot * sign) = 
         (x.`1, (x.`2,x.`3,x.`4)).
op open3 (x: ident * (upkey * ballot * sign)) = 
         (x.`1, x.`2.`1,x.`2.`2,x.`2.`3).
op fst5  (x: ident * vote * upkey * ballot * sign) = 
         x.`1.
op fst4  (x: ident * upkey * ballot * sign) = 
         x.`1.
op two4  (x: ident * upkey * ballot * sign) = 
         x.`2.
op thr3  (x: ident * uskey * upkey) = 
         x.`3.
op remv (x: ident * vote *upkey * ballot * sign) = 
        (x.`1, x.`3,x.`4,x.`5).
op remi (x: ident * upkey * ballot * sign) = 
        (x.`2, x.`3,x.`4).
op uopen (x: upkey * (ident * ballot * sign)) = 
         (x.`2.`1, x.`1, x.`2.`2,x.`2.`3).
op upack (x: ident * upkey * ballot * sign)   = 
          (x.`2, (x.`1, x.`3,x.`4)).
op uopen_2 (x: upkey * (ballot * sign)) = 
         (x.`1, x.`2.`1,x.`2.`2).
op upack_2 (x: upkey * ballot * sign)   = 
          (x.`1, (x.`2, x.`3)).

lemma uopen_upack x:
  x = uopen (upack x) by rewrite /uopen /upack; smt. 
lemma upack_uopen x:
  x = upack (uopen x) by rewrite /uopen /upack; smt. 

lemma uopen_upack_2 x:
  x = uopen_2 (upack_2 x) by rewrite /uopen /upack; smt. 
lemma upack_uopen_2 x:
  x = upack_2 (uopen_2 x) by rewrite /uopen /upack; smt. 

lemma open_pack_3 x:
  x = open3 (pack3 x) by rewrite /open3 /pack3; smt. 
lemma pack_open_3 x:
  x = pack3 (open3 x) by rewrite /uopen /upack; smt. 

lemma open_pack_4 x:
  x = open4 (pack4 x) by rewrite /open4 /pack4; smt. 
lemma pack_open_4 x:
  x = pack4 (open4 x) by rewrite /uopen /upack; smt. 

op USanitize (L: (ident * upkey * ballot * sign) list)= 
     map uopen (Sanitize (map upack L)).
op USan (L: (upkey * ballot * sign) list)= 
     map uopen_2 (Sanitize (map upack_2 L)).

(* * Random oracles for voting scheme *)
(* ---------------------------------------------------------------------- *)

type h_in, h_out. (* input and output for random oracle H used for ballot *)
type g_in, g_out. (* input and output for random oracle G used for proof  *)
type j_in, j_out. (* input and output for random oracle J used for signature  *)

op dh_out : h_out distr.
op dg_out : g_out distr.
op dj_out : j_out distr.

(** TODO: beware the hidden distribution and axiomatization **)
(*  NOTE: random oracle for voting that cannot be simulated *) 
clone ROM.Types as HOracle with
  type from            <- h_in,
  type to              <- h_out,
  op dsample(x: h_in) <- dh_out.

(** TODO: beware the hidden distribution and axiomatization **)
(* NOTE: random oracle for voting that can be simulated *) 
clone ROM.Types as GOracle with
  type from            <- g_in,
  type to              <- g_out,
  op dsample(x: g_in) <- dg_out.

clone ROM.Types as JOracle with
  type from            <- j_in,
  type to              <- j_out,
  op dsample(x: j_in) <- dj_out.


(* * Voting scheme syntax *)
(* ---------------------------------------------------------------------- *)

module type VotingScheme(H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {

  (* election keys *)
  proc setup()                                                 
    : pkey * skey *hash_key               { }

  (* for each voter, we assign a label and pair of signing keys.
     The label and public sign key are stored for valid test *)
  proc register (id: ident)
    : upkey * uskey { }

  (* create ballot using election key, and sign using signing key *)
  proc vote(id: ident, v: vote, pk: pkey, usk: uskey)
    : (ident * upkey * ballot * sign)                     {H.o J.o}

  (* result and proof of correct computation *)
  proc tally(bb: (ident*upkey * ballot * sign) list, sk: skey,hk: hash_key)
    : result * prf                                        {H.o G.o J.o}

  (* publish something from the ballot box *)
  proc publish(bb: (ident * upkey * ballot * sign) list,
               hk: hash_key)
    : pubBB   {H.o J.o}

  (* alg for voters to verify if their ballot is added to the ballot box *)
  proc verifyvote(b: (ident * upkey * ballot * sign), 
                  pbb: pubBB, hk: hash_key)
    : bool                                                {H.o J.o}

  (* verification for correctness of result *)
  proc verify(st: (pkey * pubBB * result), pi: prf) 
    : bool                                                {G.o}

}.

(* Global state shared between verifiability oracles and game *)
module BW ={
  var pk  : pkey
  var sk  : skey
  var hk  : hash_key
  var qVo : int
  var qCo : int
  var qCa : int
  var uL  : (ident, upkey * uskey) map
  var uLL : (ident *uskey * upkey) list
  var coL : ident list
  var hvote: (ident * vote * upkey * ballot * sign) list
  var check: (ident * vote * upkey * ballot * sign) list
  var bb  : (ident * upkey * ballot * sign) list
  var pbb : pubBB
  var bad : bool
  var bad': bool
  var ball: bool
  var bow : bool
  var ibad: int
  var jbad: int
  var obad: int option
  var idb : ident
  var ido : ident option
  var iu  : (ident * upkey) list
}.


(* dishonest ballot box verifiability oracle types *)
module type DBB_Oracle ={

  (* random oracles: for encryption and proof system *)
  proc h(inp: h_in)                      : h_out
  proc g(inp: g_in)                      : g_out
  proc j(inp: j_in)                      : j_out

  proc corrupt (id: ident)               : uskey option
  proc vote (id: ident, v: vote)         : (ident * upkey * ballot * sign) option
  proc check(id: ident)                  : unit
}.


(* dishonest ballot box  verifiability oracles *) 
module DBB_Oracle(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={
  (* algorithm that removes all entries that use id as identity *)
  proc removeid(id: ident, hvo: (ident * vote * upkey * ballot * sign) list) ={
    var hvo', i, ivb;
    i <- 0;
    hvo' <- [];
    while (i< size hvo){
      ivb <- (nth witness hvo i);
      if (ivb.`1 <> id){
        hvo' <- hvo' ++ [ivb];
      }
      i<- i +1;
    }
    return hvo';
  } 
 
  (* random oracles: for encryption and proof system *)
  proc h = H.o
  proc g = G.o
  proc j = J.o

  (* corrupt oracle *)
  proc corrupt(id: ident): uskey option={
    var usk, usk';
    usk <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL){
      BW.hvote <@ removeid(id, BW.hvote);
      BW.coL<- BW.coL ++ [id];
      BW.qCo <- BW.qCo +1;
      usk' <- (oget BW.uL.[id]).`2;
      usk <- Some usk';
      }
    return usk;
    }
 
  (* honest voter query *)
  proc vote(id: ident, v: vote): (ident * upkey * ballot * sign) option ={
    var b, o;
    o <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL /\ BW.qVo < qVo){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      o <- Some b;
      BW.hvote <- BW.hvote ++ [(id,v,b.`2,b.`3, b.`4)]; 
      BW.qVo <- BW.qVo + 1;
    }
    return o;
  }

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL;
    i <- 0;

    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.hk);
    (* verify all honest ballots *)
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), 
                                     BW.pbb, BW.hk);
      (* take the ballot asigned to id, and if voter has not checked before append *)
      if (id = h.`1 /\ vv /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }
      i <- i +1;
    }
  }

}.

(* weak verifiability adversary *)
module type DBB_Adv(O: DBB_Oracle) ={
  proc a1(sk:skey): (ident * upkey * ballot * sign) list {O.corrupt O.vote O.h O.g O.j}
  proc a2(): result * prf {O.check O.h O.g O.j}
}.

(* weak verifiability experiment *)
module DBB_Exp (V: VotingScheme, A: DBB_Adv,
                   H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id, hC, hNC, hN, fbb;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.uLL  <- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ V(H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ V(H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }
    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of upk *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);

    (* votes of honest voters that checked *)
    hC <- map (fun (b:ident * vote * upkey * ballot * sign), 
                    b.`2) 
               BW.check;
    (* votes of honest voters that didn't check *)
     (* hNC =(id,v,l,c,s)* from Hvote that do not contain Checked
        hN  =v* find the votes in Hvote for the ballots in hNC  *)

    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * ballot * sign), 
                    b.`2) hNC;
    (* testing if the votes of the voters that checked are used to compute the result *)
    if (exists (X D L: vote list), 
          perm_eq X hN /\
          subseq L X /\ 
          size D <= BW.qCo /\ 
          r = Count (hC ++ L ++ D)){
       bool <- false;
    }
    
    return ev /\ bool;
  }
}.

(* #########################################################################*)

(* accuracy adversary *)
module type Acc_Adv(H:HOracle.ARO, G:GOracle.ARO, J: JOracle.Oracle) ={
  proc main(sk: skey, 
            ul: (ident, upkey * uskey) map,
            ull: (ident*uskey *upkey) list,
            hk: hash_key): 
           (ident * upkey * ballot * sign) list
}.

(* accuracy experiment *) 
module Acc_Exp(V: VotingScheme, A: Acc_Adv, 
               H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={
  proc main():bool ={
    var pk,sk,uL,bb,r,pi, ev, i, id, uLL, hk;
     i <- 0;
     uL <- empty;
     uLL<-[];

              H.init();
              G.init();      
              J.init();   

    (pk, sk, hk) <@ V(H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      uL.[id] <@ V(H,G,J).register(id);
      uLL<-uLL ++[(id, (oget uL.[id]).`2,(oget uL.[id]).`1)];
      i<- i + 1;
    }

    bb       <@ A(H,G,J).main(sk, uL, uLL,hk);
    (r,pi)   <@ V(H,G,J).tally(bb,sk,hk);
    BW.pbb   <@ V(H,G,J).publish(bb, hk);
    ev       <@ V(H,G,J).verify((pk, BW.pbb, r),pi);
    return ev;
  }
}.


(* tally uniqueness adversary *)
module type TallyUniq_Adv(H:HOracle.ARO, G: GOracle.ARO, J: JOracle.Oracle) ={
  proc main(): ((ident * upkey * ballot * sign) list) * 
                pkey * result * prf * result * prf *hash_key {H.o G.o J.o}
}.

(* tally uniqueness experiment *)
module TallyUniq_Exp (V: VotingScheme, A: TallyUniq_Adv,
                      H: HOracle.Oracle, G: GOracle.Oracle, J: JOracle.Oracle)={
  proc main(): bool ={
    var bb, pk, r,r', pi, pi', ev, ev', bool, hk;

    bool <- true;

    H.init();
    G.init();
    J.init();

    (bb, pk, 
      r, pi, 
      r', pi',hk) <@ A(H,G,J).main();
    BW.pbb     <@ V(H,G,J).publish(bb,hk);
    ev         <@ V(H,G,J).verify((pk, BW.pbb, r ),pi);
    ev'        <@ V(H,G,J).verify((pk, BW.pbb, r'),pi');

    return ev /\ ev' /\ (r <> r');
  }
}. 



(* -----------------------------------------------------------------------------------------------*)
(* PRIVACY *)
module type VotingScheme'(H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {

  proc setup() : pkey * skey *hash_key                   { }

  proc register (id: ident) : upkey * uskey { }

  proc vote(id: ident, v: vote, pk: pkey, usk: uskey)
               : (ident * upkey * ballot * sign)    {H.o J.o}

  proc valid(bb: (ident * upkey * ballot * sign) list, 
             b : (ident * upkey * ballot * sign),
             pk: pkey)
    : bool    {H.o J.o} 

  proc tally(bb: (ident*upkey * ballot * sign) list, sk: skey, hk: hash_key)
    : result * prf                                        {H.o G.o J.o}

  proc publish(bb: (ident * upkey * ballot * sign) list,
               hk: hash_key)
    : pubBB   {H.o J.o}

  proc verifyvote(b: (ident * upkey * ballot * sign), 
                  pbb: pubBB, hk: hash_key)
    : bool                                                {H.o J.o}

  proc verify(st: (pkey * pubBB * result), pi: prf) 
    : bool                                                {G.o}

}.

(* dishonest registration verifiability oracle types *)
module type DREG_Oracle ={

  (* random oracles: for encryption and proof system *)
  proc h(inp: h_in)                      : h_out
  proc g(inp: g_in)                      : g_out
  proc j(inp: j_in)                      : j_out

  proc corrupt (id: ident)               : unit
  proc vote (id: ident, v: vote)         : (ident * upkey * ballot * sign) option
  proc cast (b: ident*upkey*ballot*sign) : unit
  proc check(id: ident)                  : unit
  proc board()                           : (ident* upkey* ballot* sign) list
}.

module DREG_Oracle(V: VotingScheme',
                   H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={
 
  (* random oracles: for encryption and proof system *)
  proc h = H.o
  proc g = G.o
  proc j = J.o

  (* corrupt oracle *)
  proc corrupt(id: ident): unit={
    if (BW.uL.[id] <> None /\ !id \in BW.coL){
      BW.hvote <- filter ((predC1 id) \o fst5) BW.hvote;
      BW.coL<- BW.coL ++ [id];
      BW.qCo <- BW.qCo +1;
      }
    }
 
  (* honest voter query *)
  proc vote(id: ident, v: vote): (ident * upkey * ballot * sign) option ={
    var b, o, e;
    o <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL /\ BW.qVo < qVo){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      e <@ V(H,G,J).valid(BW.bb,b, BW.pk);
      o <- Some b;
      BW.hvote <- BW.hvote ++ [(id,v,b.`2,b.`3, b.`4)]; 
      if (e){
        BW.bb <- BW.bb ++ [b];
      }
      BW.qVo <- BW.qVo + 1;
    }
    return o;
  }

  proc cast (b: ident *upkey * ballot * sign): unit ={
    var e; 
    if (BW.uL.[b.`1] <> None /\ b.`1 \in BW.coL){
      e <@ V(H,G,J).valid(BW.bb,b, BW.pk);
      if (e){
        BW.bb <- BW.bb ++ [b];
      }
    }
  }

  proc board (): (ident *upkey*ballot*sign) list ={
    return BW.bb;
  }

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL;
    i <- 0;
    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.hk);
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), 
                                     BW.pbb, BW.hk);
      if (id = h.`1 /\ vv /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }
      i <- i +1;
    }
  }

}.

module type DREG_Adv(O: DREG_Oracle) ={
  proc a1(sk: skey): (ident, upkey*uskey) map {O.h O.g O.j}
  proc a2(): unit {O.corrupt O.vote O.cast O.board O.h O.g O.j}
  proc a3(): result * prf {O.check O.board O.h O.g O.j}
}.

(* weak verifiability experiment *)
module DREG_Exp (V: VotingScheme', A: DREG_Adv,
                   H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DREG_Oracle(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, hC, hNC, hN;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.uLL  <- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk, BW.hk) <@ V(H,G,J).setup();

    BW.uL    <@ A(O).a1(BW.sk);
                A(O).a2();
    (* BW.bb <- USanitize BW.bb; *)
    (r,pi)   <@ A(O).a3();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);

    hC <- map (fun (b:ident * vote * upkey * ballot * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * ballot * sign), b.`2) hNC;
    bool <- forall (X D L: vote list), 
                  !perm_eq X hN \/
                  !subseq L X \/ 
                  !size D <= BW.qCo \/ 
                  r <> Count (hC ++ L ++ D);
    return ev /\ bool;
  }
}.


(* * BPRIV security definition *)
(* ---------------------------------------------------------------------- *)

(* ** BPRIV oracles *)

(* nnumber of cast querries *)
op qCa: int.

(* BPRIV oracle types *)
module type BPRIV_Oracles = {
  (* random oracles: for encryption and proof system *)
  proc h(inp: h_in)                      : h_out
  proc g(inp: g_in)                      : g_out
  proc j(inp: j_in)                      : j_out

  (* voting oracles *)
(*  proc corrupt(id: ident)              : uskey option *)
  proc vote(id: ident, vo v1 : vote)     : unit
  proc cast(b: (ident * upkey * ballot * sign)) : unit

  (* adversary view *)
  proc board()                           : pubBB
}.

(* Global state shared between strong consistency/correctness oracles and game *)
module BSC = {
  var uL    : (ident, upkey * uskey) map
  var valid : bool
  var qt    : int
  var pk    : pkey
  var sk    : skey
  var hk    : hash_key
  var ibad  : int option
}.

(* Global state shared between BPRIV oracles and game *)
module BP = {
  var qVo : int
  var qCa : int
  var pk  : pkey
  var sk  : skey
  var hk  : hash_key
  var r   : result
  var pi  : prf
  var b   : bool
  var bb0 : (ident * upkey * ballot * sign) list
  var bb1 : (ident * upkey * ballot * sign) list
  var uL  : (ident, upkey * uskey) map
  var uLL : (ident * upkey) list
  var coL : ident list
}.



(* BPRIV Oracles *) 
module BPRIV_Oracles(V: VotingScheme', 
                     H: HOracle.ARO, G: GOracle.ARO, 
                     J: JOracle.ARO, LR: LorR): BPRIV_Oracles = {
  proc h = H.o
  proc g = G.o
  proc j = J.o

  proc vote(id : ident, v0 v1 : vote) : unit = {
    var b0, b1, l_or_r;
    var ev, b, bb, lps;
   
    if (BP.qVo < qVo) {
      if (BP.uL.[id] <> None /\ !id \in BP.coL) {
        (* upkey, and uskpey *)
        lps      <- oget BP.uL.[id]; 
 
        (* ballots *)
        b0     <@ V(H,G,J).vote(id, v0, BP.pk, lps.`2);
        b1     <@ V(H,G,J).vote(id, v1, BP.pk, lps.`2);

        l_or_r <@ LR.l_or_r();
        b      <- l_or_r?b0:b1;
        bb     <- l_or_r?BP.bb0:BP.bb1;
        ev     <@ V(H,G,J).valid(bb, b, BP.pk);
        if (ev) {
          BP.bb0  <- BP.bb0 ++ [b0];
          BP.bb1  <- BP.bb1 ++ [b1];
        }
      }
      BP.qVo <- BP.qVo + 1;
    }
  }
  
  proc cast(b : (ident * upkey * ballot* sign)) : unit = {
    var ev, bb, l_or_r;

    if (BP.qCa < qCa){
      if (BP.uL.[b.`1] <> None /\ b.`1 \in BP.coL){
        l_or_r <@ LR.l_or_r();
        bb   <- l_or_r?BP.bb0:BP.bb1;
        ev   <@ V(H,G,J).valid(bb, b, BP.pk);
        if (ev) {
          BP.bb0 <- BP.bb0 ++ [b];
          BP.bb1 <- BP.bb1 ++ [b];
        }
      }
      BP.qCa <- BP.qCa + 1;
    }
  }

  proc board(): pubBB = {
    var l_or_r, bb, pbb;

    l_or_r <@ LR.l_or_r();
    bb     <- l_or_r?BP.bb0:BP.bb1;
    pbb    <@ V(H,G,J).publish(bb, BP.hk);
    return pbb;
  }
}.

(* ** BPRIV left *)

(* BPRIV adversary type *)
module type BPRIV_Adv(O: BPRIV_Oracles) = {
  proc corL (pk: pkey, uLL: (ident * upkey) list)
          : ident list { }
  proc a1 (pk: pkey, uL: (ident, upkey*uskey) map)           
          : unit { O.vote O.cast O.board O.h O.g O.j}
  proc a2 (r: result, pi: prf) 
          : bool { O.board O.h O.g O.j}
}.

(* Ballot privacy property: left side *)
module BPRIV_L(V: VotingScheme', A: BPRIV_Adv,  
               H: HOracle.Oracle, G: GOracle.Oracle,
               J: JOracle.Oracle) = {

  module O = BPRIV_Oracles(V, H, G, J, Left)
  module A = A(O)
  module V = V(H,G,J)

  proc main() : bool = {
    var i, id;
    BP.qVo  <- 0;
    BP.qCa  <- 0;
    BP.bb0  <- [];
    BP.bb1  <- [];
    BP.uLL  <- [];
    BP.coL  <- [];
    BP.uL   <- empty;
    i       <- 0;

                      H.init();
                      G.init();
                      J.init();
    (BP.pk, BP.sk,BP.hk) <@ V.setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BP.uL.[id] <@ V.register(id);
      BP.uLL <- BP.uLL ++ [(id, (oget BP.uL.[id]).`1)];
      i<- i + 1;
    }
    BP.coL         <@ A.corL(BP.pk, BP.uLL);
    
                      A.a1(BP.pk, filter (fun (a: ident) (b: upkey * uskey), 
                                          a \in BP.coL) BP.uL);
    (BP.r, BP.pi)  <@ V.tally(BP.bb0, BP.sk,BP.hk); 
    BP.b           <@ A.a2(BP.r, BP.pi);

    return BP.b;
  }
}.

(* BPRIV simulator type *)
module type BPRIV_Simulator = {
  proc init()          : unit
  proc o(x : g_in)     : g_out
  proc prove(st: (pkey * pubBB * result)) : prf 
}.

(* ** BPRIV right *)

(* Ballot privacy property: right side *)
module BPRIV_R(V: VotingScheme', A:BPRIV_Adv, 
               H: HOracle.Oracle, G: GOracle.Oracle, 
               J: JOracle.Oracle,
               S: BPRIV_Simulator) = {

  module O = BPRIV_Oracles(V, H, S, J, Right)
  module A = A(O)
  module V = V(H,G,J)

  proc main() : bool = {
    var pbb, pi, i, id;

    BP.qVo  <- 0;
    BP.qCa  <- 0;
    BP.bb0  <- [];
    BP.bb1  <- [];
    BP.uLL  <- [];
    BP.coL  <- [];
    BP.uL   <- empty;
    i       <- 0;

                      H.init();
                      G.init();
                      J.init();
    (* init simul. *) S.init(); 

    (BP.pk, BP.sk,BP.hk) <@ V.setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BP.uL.[id] <@ V.register(id);
      BP.uLL <- BP.uLL ++ [(id, (oget BP.uL.[id]).`1)];
      i<- i + 1;
    }

    BP.coL         <@ A.corL(BP.pk, BP.uLL);
                      A.a1(BP.pk, filter (fun (a: ident) (b: upkey * uskey), 
                                          a \in BP.coL) BP.uL);
    (BP.r, BP.pi)  <@ V.tally(BP.bb0, BP.sk,BP.hk);
    (* start: simulate proof from public BB for BB1 *)
    pbb            <@ V.publish (BP.bb1, BP.hk);
    pi             <@ S.prove(BP.pk, pbb, BP.r);
    (* end *)
    BP.b           <@ A.a2(BP.r, pi);
    return BP.b;
  }
}.

(* * Strong consistency *)
(* ---------------------------------------------------------------------- *)

(* strong consistency op: extractor *)
module type Extractor(H: HOracle.ARO) = {
  proc extract(b: (ident * upkey * ballot), 
               sk: skey)
       : (upkey * vote option)   {H.o}
}.

(* strong consistency op: validInd *)
module type ValidInd(H: HOracle.ARO, J: JOracle.ARO) = {
  proc validInd(b: (ident * upkey * ballot*sign), 
                pk: pkey)
       : bool  {H.o J.o}
}.

(* strong consistency oracle type *)
module type SCons_Oracle = {
  proc h(inp: h_in) : h_out
  proc g(inp: g_in) : g_out
}.

(* strong consistency oracle instantiation *)
module SCons_Oracle(V: VotingScheme, 
                    H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {
  proc h = H.o
  proc g = G.o
  proc j = J.o
}.

(* strong correctness oracle type *)
module type SCorr_Oracle = {
  (* random oracles *)
  proc h(inp: h_in) : h_out
  proc g(inp: g_in) : g_out
  proc j(inp: j_in) : j_out

  (* strong correctness test *)
  proc test(id:ident, v: vote,
            bb: (ident * upkey * ballot * sign) list)
       : unit
}.

(* strong correctness oracle instantiation *) 
module SCorr_Oracle(V: VotingScheme', 
                    H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {
  proc h    = H.o
  proc g    = G.o
  proc j    = J.o 
 
  proc test(id: ident, v: vote, 
            bb: (ident * upkey * ballot * sign) list): unit = {
    var b, lps, ev;

    if (BSC.qt < 1){   
      if (size bb < qVo + qCa){
        if (BSC.uL.[id] <> None){
          lps   <- oget BSC.uL.[id]; 
          b    <@ V(H,G,J).vote(id, v, BSC.pk, lps.`2);
          ev   <@ V(H,G,J).valid(bb, b, BSC.pk);
          if(!BSC.valid){
            BSC.valid <- !ev;  
          }
        }
      }
      BSC.qt <- BSC.qt + 1;
    }
  }
}.


(* 2. strong consistency part 1 *) 
module SConsis1(V: VotingScheme', C: Extractor, 
                H: HOracle.Oracle, G: GOracle.Oracle, J: JOracle.Oracle) = {
  module V = V(H,G,J)
  module C = C(H)

  proc main (id: ident, v: vote, usk: uskey ): bool = {
    var b, upk', v';

                      H.init();
                      G.init();
                      J.init();
    (BP.pk, BP.sk,BP.hk) <@ V.setup();

    (* Extract of Vote  give id and vote *) 
    b              <@ V.vote(id, v, BP.pk, usk);
    (upk',v')      <@ C.extract((b.`1,b.`2,b.`3), BP.sk);
    BP.b           <- (upk' = get_upk usk) /\ (v' =  Some v);
  
    return BP.b;
  }
}.

(* Strong Consistency part 2 adversary *)
module type SConsis2_Adv(O:SCons_Oracle) = {
  proc main(pk: pkey, uL: (ident, upkey * uskey) map)
       : (ident * upkey * ballot * sign) list * (ident * upkey * ballot * sign)
}.

(* 2. strong consistency part 2 *)
module SConsis2(V:VotingScheme', 
                C:ValidInd, A:SConsis2_Adv, 
                H: HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) = {
 
  module O = SCons_Oracle(V, H, G, J)
  module V = V(H,G, J)
  module A = A(O)

  proc main (): bool = {
    var bb, b, b', i, id;
    
    bb      <- [];
    BP.uL   <- empty;
    i       <- 0;
                      H.init();
                      G.init();
                      J.init();

    (BP.pk, BP.sk,BP.hk) <@ V.setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BP.uL.[id] <@ V.register(id);
      i<- i + 1;
    }
    (bb, b)        <@ A.main(BP.pk, BP.uL);
    (* Valid implies validInd *)
    BP.b           <@ V.valid(bb, b, BP.pk);
    
    b'             <@ C(H,J).validInd(b, BP.pk);
    return (!BP.b || b');
  }
}.

(* Strong consistency part 3 adversary *)
module type SConsis3_Adv(O:SCons_Oracle) = {
  proc main (pk: pkey, uL: (ident, upkey * uskey) map)
       : (ident * upkey * ballot * sign) list
}.


(* 2. strong consistency part 3 left experiment *)
module SConsis3_L(V: VotingScheme, 
                  C2:ValidInd, A: SConsis3_Adv, 
                  H: HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) = {
  module O = SCons_Oracle(V,H,G,J)
  module V = V(H,G, J)
  module A = A(O)

  proc main (m:int) : result option = {
    var bb, ev, i, b, r, id;

    i       <- 0;
    ev      <- true;
    bb      <- [];
    BP.uL   <- empty;

                   H.init();
                   G.init();
                   J.init();

    (BP.pk, BP.sk,BP.hk) <@ V.setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BP.uL.[id] <@ V.register(id);
      i<- i + 1;
    }

    bb             <@ A.main(BP.pk, BP.uL);

    i <- 0;
    while (i < size bb){
      b <- nth witness bb i;
      if (ev){
        ev <@ C2(H,J).validInd(b, BP.pk);
      }
      i <- i + 1;
    }

    (* compute result by tally *)
    r <- None;
    if (ev){
      (BP.r, BP.pi) <@ V.tally(bb, BP.sk,BP.hk);
      r             <- Some BP.r;
    }

    return r;
  }
}.

(* 2. strong consistency part 3 right experiment *)
module SConsis3_R(V: VotingScheme, C1:Extractor, 
                  C2:ValidInd, A: SConsis3_Adv, 
                  H: HOracle.Oracle, G: GOracle.Oracle, J: JOracle.Oracle) = {
  module O = SCons_Oracle(V,H,G,J)
  module V = V(H,G,J)
  module A = A(O)

  proc main () : result option = {
    var bb, ub, ev, i, b, m', r, id, upk;

    i       <- 0;
    ev      <- true;
    bb      <- [];
    ub      <- [];
    BP.uL   <- empty;
                      H.init();
                      G.init();
                      J.init();

    (BP.pk, BP.sk,BP.hk) <@ V.setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BP.uL.[id] <@ V.register(id);
      i<- i + 1;
    }

    bb             <@ A.main(BP.pk, BP.uL);

    i <- 0;
    while (i < size bb){
      b <- nth witness bb i;
      if (ev){
        ev <@ C2(H,J).validInd(b, BP.pk);
      }
      i <- i + 1;
    }

    (* compute result by rho over Extract *)
    r <- None;
    if (ev){
      i <- 0;
      while (i < size bb){
        b  <- nth witness bb i;
        (upk,m') <@ C1(H).extract((b.`1,b.`2,b.`3), BP.sk);
        ub <- ub ++ [(upk, oget m')];
        i <- i + 1;
      }
      r <- Some (Count (map snd (Sanitize ub)));
    }

    return r;
  }
}.

module type SCorr_Adv(H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {
  proc main(pk: pkey, uL: (ident, upkey * uskey) map) 
       : ident * vote * (ident * upkey * ballot * sign) list
}.


(* strong correctness experiment *)
module SCorr(V:VotingScheme', A: SCorr_Adv, 
             H: HOracle.Oracle, G: GOracle.Oracle, J: JOracle.Oracle) = {
  module V = V(H, G, J)
  module A = A(H,G,J)
  
  proc main () : bool = {

    var id, i, v, bb, lps, ev, b, val_id;
    BSC.uL    <- empty;
    BSC.valid <- false;
    i         <- 0;

       H.init();
       G.init();
       J.init();

    (BSC.pk, BSC.sk, BSC.hk) <@ V.setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BSC.uL.[id] <@ V.register(id);
      i<- i + 1;
    }
    
    (id, v, bb) <@ A.main(BSC.pk, BSC.uL);

    if (BSC.uL.[id] <> None){
      lps   <- oget BSC.uL.[id];
      val_id <- forall x, x\in bb => 
                       (x.`1,x.`2) = (id, lps.`1) \/
                       (x.`1 <> id /\ x.`2 <> lps.`1);
      b    <@ V.vote(id, v, BSC.pk, lps.`2);
      ev   <@ V.valid(bb, b, BSC.pk);
      BSC.valid <- val_id /\ !ev;  
    }
    return BSC.valid;
  }
}.

