require import AllCore List Distr.
require import Distr Int FMap Real Mu_mem. 
require import LeftOrRight.
require (*  *) Belenios_DEF_Pre_2.
require (*  *) Bigop.

(* basic types and operators used for the encryption scheme space *)
  type ident, fq.
  op Fq_zero: fq. (* zero element for addition *)
  op (+): fq -> fq -> fq. (* addition *)
  

  (* counting operator that does addition *)
  op Count (ubb: fq list): fq =
    with ubb = "[]" => Fq_zero
    with ubb = (::) idv ubb =>  idv + (Count ubb).
(*
  (* abstract policy *)
  op Sanitize['a]: (ident * 'a) list -> (ident * 'a) list.
  
  (* remove identities *)
  op rem_id (bb: (ident * 'a) list) = map snd bb.
*) 

  (* Helios-hom with a fixed Rho *) 
  clone export Belenios_DEF_Pre_2 as HH with
    type ident  <- ident,
    type MV2.result <- fq,
    type fq         <- fq,
    op Fq_zero      <- Fq_zero,
    op MV2.Count    <- Count,
    op MV2.(+)          <- (+).
(*    op MV2.Sanitize['a] <- Sanitize. *)
  
  
  (* homomorphic property added to the abstract encryption scheme in IPKE*)
  op Add     : (h_cipher list) -> h_cipher option.
  axiom addC  (x y: fq)  : x + y = y + x.
  axiom addA  (x y z: fq): x + (y + z) = (x + y) + z.
  axiom add0f (x: fq)    : Fq_zero + x = x.

  clone Bigop as BigAdd with
    type       t   <- fq,
    op Support.idm <- Fq_zero,
    op Support.(+) <- (+)
    proof Support.Axioms.*.
    realize Support.Axioms.addmA. by apply addA.  qed.
    realize Support.Axioms.addmC. by apply addC.  qed.
    realize Support.Axioms.add0m. by apply add0f. qed.
    import BigAdd.

  (* ** Assumptions on the homomorphic property *)
  axiom hom_val (sk: skey) (clist: h_cipher list):
    (forall c, mem clist c => 
             decrypt sk c <> None) =>
    decrypt sk (oget (Add clist)) <> None.
  
  axiom hom_add (sk: skey) (clist: h_cipher list):
    (forall c, mem clist c => 
             decrypt sk c <> None) =>
    oget (decrypt sk (oget (Add clist))) = 
    BigAdd.big predT (fun (c: h_cipher) => 
                          oget (decrypt sk c)) clist.


(* counting properties  *)
lemma Count_split (L1 L2: fq list):
  Count (L1++L2) = Count L1 + Count L2.
proof.
  by elim: L1 =>//=; smt.
qed.

lemma Count_perm (L1 L2: plain list):
  perm_eq L1 L2 => Count L1 = Count L2.
proof.
  elim: L1 L2=>//=. smt. 
  move => x L1 HL L2. 
  move =>Hp. 
  cut:= perm_eq_mem (x:: L1) L2 Hp.
  move => Hmem.
  cut := Hmem x . rewrite mem_head //=. move => Hx. 
  have HAB: exists A B, L2 = A ++[x]++B by smt.
  move: HAB; elim=> A B HABL2.
  have Hpp: perm_eq L1 (A++B). 
    move: Hp. rewrite HABL2. 
    rewrite perm_eq_sym (perm_catCA A [x] B) cat1s.
    move => Hc.
    have Hc': perm_eq (x :: (A ++ B)) (x :: L1) by smt.
    move: Hc'. 
    by rewrite (perm_cons x (A++B) L1) perm_eq_sym.  
  cut HL1L2:= HL (A++B) Hpp.
  rewrite HL1L2 HABL2 ?Count_split //=.
  smt.
qed. 

lemma add_com (a b : fq):
    a + b = b + a by rewrite addC.

(* Count is the same, even if you split the list in 2 components *) 
lemma Count_filter (L : fq list) p:
    perm_eq (filter p L ++ filter (predC p) L) L => 
    Count L = Count (filter p L ++ filter (predC p) L).
  proof. 
  elim: L =>//=. progress. 
  case (p x). smt. 
  move => Hpx.
  have ->: predC p x by smt.
  progress. 
  have ->: x :: filter (predC p) l = [x]++ filter (predC p) l by smt.
  rewrite ?Count_split. 
  rewrite addA. rewrite (addC _ (Count [x])). 
  have ->: Count [x] = x by smt.
  by smt.
qed. 

(* Count is the same, even if you split the list in 2 components *) 
lemma Count_snd_filter (L : (ident * fq) list) p:
    perm_eq (filter p L ++ filter (predC p) L) L => 
    Count (map snd L) = Count (map snd (filter p L ++ filter (predC p) L)).
  proof. 
  elim: L =>//=. progress. 
  case (p x). smt. 
  move => Hpx.
  have ->: predC p x by smt.
  progress. 
  have ->: x :: filter (predC p) l = [x]++ filter (predC p) l by smt.
  have ->:Count (map snd (filter p l ++ ([x] ++ filter (predC p) l))) =
          Count (map snd (filter p l) ++ map snd([x]) ++ map snd (filter (predC p) l)) by smt.
  rewrite ?Count_split.
  rewrite (addC _ (Count (map snd [x]))).
  smt. 
qed. 

(* ---------------------------------------------------------------------------------------------*)  
(* Helios-hom voting system *) 
module BeleniosHom (Pe: PSpke.Prover, Ve: PSpke.Verifier, Ve': PSpke.Verifier, 
                  Pz: Prover,  Vz: Verifier, SS: SignScheme,
                  H: HOracle.ARO,   G: GOracle.ARO, J: JOracle.ARO)  = {

  proc setup      = B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS, H, G, J).setup
  proc register   = B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS, H, G, J).register
  proc valid      = B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS, H, G, J).valid
  proc vote       = B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS, H, G, J).vote
  proc publish    = B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS, H, G, J).publish
  proc verify     = B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS, H, G, J).verify 
  proc verifyvote = B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS, H, G, J).verifyvote

  (* tally algorithm *)
  proc tally(bb: (ident*upkey * cipher*sign) list, sk: skey, hk: hash_key): fq * prf = {
    var  b, i, pbb, sbb, ev, c, cL, pk, r, pi, r', e;

    sbb <- [];
    i   <- 0;
    pk  <- get_pk sk;

    (* keep only valid ballots *)
    while (i < size (map remi bb)){
      b <- nth witness (map remi bb) i;
      ev <@ Ve(H).verify((pk, b.`1, b.`2.`1), b.`2.`2);
      e  <@ SS(J).verify(b.`1, b.`2, b.`3);
      if(ev /\ e){
        (* keep only id and h_cipher*)
        sbb <- sbb ++ [(b.`1,b.`2.`1)];
      }
      i <- i + 1;
    }
    
    (* apply policy *)
    cL <- map snd (Sanitize sbb);
    (* compute ciphertext by homomorphic addition *)
    c  <- Add cL;

    (* compute result *)
    r <- Fq_zero;
    if (cL <> []){
      r' <- decrypt sk (oget c);
      if(r'<> None){(* decryption doesn't fail *)
        r <- oget r';
      } 
    }

    (* create proof *)
     pbb <- map (fun (x: ident*upkey * cipher * sign), 
                     (may_data x, hash' hk x )) (USanitize bb);
    pi   <@ Pz(G).prove((pk, pbb, r), sk);

    return (r, pi);
  }

  (* intermediary tally algorithm, between Helios-hom.tally and MV.tally *)
  proc tally'(bb: (ident*upkey * cipher*sign) list, sk: skey, hk: hash_key): fq * prf = {
    var ubb, b, i, m, pbb, sbb, ev, c, upk, pk, r, pi, e;

    ubb <- [];
    sbb <- [];
    i   <- 0;
    pk  <- get_pk sk;
    (* keep only valid ballots *)
    while(i < size (map remi bb)){
      b <- nth witness (map remi bb) i;
      ev<@ Ve(H).verify((pk, b.`1, b.`2.`1), b.`2.`2);
      e  <@ SS(J).verify(b.`1, b.`2, b.`3);
      if(ev /\ e){
        (* keep only id and h_cipher*)
        sbb <- sbb ++ [(b.`1, b.`2.`1)];
      }
      i <- i + 1;
    }
 
    (* decrypt ballots *)
    i <- 0;
    while( i < size sbb){
      (upk,c)  <- nth witness sbb i;
      m       <- decrypt sk c;
      ubb     <- ubb ++ [(upk, oget m)];
      i       <- i + 1;
    }
    
    r <- Count (map snd (Sanitize ubb));

    (* create proof *)
    pbb <- map (fun (x: ident*upkey * cipher * sign), 
                     (may_data x, hash' hk x )) (USanitize bb);
    pi    <@ Pz(G).prove((pk, pbb, r), sk);

    return (r, pi);
  }
}.

(* ---------------------------------------------------------------------------------*)
section BeleniosHom_homomorphicAddition.

(* declare module C : ValidInd       {BP, HRO.RO, BSC}. *)
declare module Pe: PSpke.Prover   {BP, HRO.RO, GRO.RO, JRO.RO, SCorr_Oracle, BSC }.
declare module Ve: PSpke.Verifier {Pe, BP, HRO.RO, JRO.RO, GRO.RO}.
declare module Ve': PSpke.Verifier {Ve, Pe, BP, HRO.RO}.

declare module Pz: Prover         {Ve', Pe, Ve, BP, HRO.RO, GRO.RO, JRO.RO }.
declare module Vz: Verifier       {Ve', Pe, Ve, Pz, BP, HRO.RO }.
declare module S : Simulator      {Ve', Pe, Ve, Pz, Vz, GRO.RO, BP, HRO.RO, BSC}.
declare module SS: SignScheme     {Ve', Ve, Pe, Pz, HRO.RO, JRO.RO, GRO.RO}.
declare module A : BPRIV_Adv      {Ve', Pe, Ve, Pz, Vz, GRO.RO, S,SS, HRO.RO, BP }. 

(* ** ASSUMPTIONS ** *)
(* ** start *)
  (* axiom on commutativity of policy and decryption *)
  axiom san_tallymap (sbb: (upkey * h_cipher) list) (sk: skey):
    let f = (fun (b : upkey * h_cipher) =>
                (b.`1, (oget (decrypt sk b.`2)))) in
    Sanitize (map f sbb) = map f (Sanitize sbb).

  (* axiom on membership of policy *)
  axiom san_mem (sbb: (upkey * h_cipher) list) (b: upkey * h_cipher):
    mem (Sanitize sbb) b => mem sbb b.

  (* axiom on transforming Ve.verify in verify operator *)
  axiom Verify_to_verify (gv: (glob Ve)) (s: h_stm) (p: h_prf):
    phoare[Ve(HRO.RO).verify: (glob Ve) = gv  /\ arg = (s, p)
         ==>
         (glob Ve) = gv /\ 
         res = verify s p HRO.RO.m] = 1%r.
  axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)  ==>
             (glob SS) =gs /\ res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
(* ** end *)


(* counting plaintext list is the same as decrypting a homomorphic addition *) 
local lemma fg (sbb: h_cipher list) (sk: skey):
  (forall b, mem sbb b => 
      (decrypt sk b <> None)) =>
  Count (map (fun (b : h_cipher) =>
                  (oget (decrypt sk b)))
         sbb)  =
  oget (decrypt sk (oget (Add sbb))).
proof.
  elim sbb =>//=. smt.
  move => x l Hxl Hb.  
  rewrite hom_add; first by smt. 
  rewrite BigAdd.big_cons /predT //=. 
  rewrite Hxl; first by smt. 
  rewrite hom_add /predT; first by smt. 
  by done.
qed.



(* equivalence between MV.tally and HeliosHom.tally' *)
local equiv MV_Helios_tally1:
  B'( IPKE(Pe,Ve),Pz,Vz,CIND(Ve',IPKE(Pe,Ve),SS),SS,HRO.RO, GRO.RO, JRO.RO).tally ~ 
  BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).tally'
  : ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve,  glob SS, arg}  
      ==>
    ={res, glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS}.
proof.
  proc.
  call(: ={glob GRO.RO}); first by sim.
  wp. 
  while{2} (0<= i{2} /\
            ubb{2} = map (fun (x: upkey * h_cipher),
                              (x.`1, oget (decrypt sk{2} x.`2))) (take i{2} sbb{2}))
           (size sbb{2} - i{2}); progress.
    by auto; smt. 
  wp. 

  while ( ={i, glob SS, glob Ve, sk, HRO.RO.m, JRO.RO.m}/\
          fbb{1} = map remi bb{2} /\
          pk{2} = get_pk sk{2} /\
          0<= i{2} /\
          (let f= fun (b: upkey * cipher* sign), 
                      verify (pk{2}, b.`1, b.`2.`1) 
                                     b.`2.`2 HRO.RO.m{2} /\
                      ver_sign b.`1 b.`2 b.`3 JRO.RO.m{2} in
          let g= fun (b: upkey * cipher*sign) =>
                              (b.`1, b.`2.`1) in
          let k= fun (x: upkey * h_cipher),
                      (x.`1, oget (decrypt sk{2} x.`2)) in
          sbb{2} = map g (filter f (take i{2} (map remi bb{2})))) /\
           let f= fun (b: upkey * cipher* sign), 
                      verify (pk{2}, b.`1, b.`2.`1) 
                                     b.`2.`2 HRO.RO.m{2} /\
                      ver_sign b.`1 b.`2 b.`3 JRO.RO.m{2} in
          let g= fun (b: upkey * cipher*sign) =>
                              (b.`1, b.`2.`1) in
          let k= fun (x: upkey * h_cipher),
                      (x.`1, oget (decrypt sk{2} x.`2)) in
          ubb{1} = map (k \o g) (filter f (take i{1} fbb{1})) ).
    inline*; wp; sp.
    exists* (glob Ve){1}, (glob SS){1}, sk{2}, b{2}; elim* => gv gs skx bx.
    call{1} (ver_Ver_one gs bx.`1 bx.`2 bx.`3).
    call{2} (ver_Ver_one gs bx.`1 bx.`2 bx.`3).
    wp; call{1} (Verify_to_verify gv ((get_pk skx), bx.`1, bx.`2.`1) bx.`2.`2).
    call{2} (Verify_to_verify gv ((get_pk skx), bx.`1, bx.`2.`1) bx.`2.`2).
    auto; progress.
    + smt. 
    + have Hm: 0<= i{2} < size (map remi bb{2}) by smt.
      rewrite (take_nth witness) 1: Hm // filter_rcons. 
      by smt.
    + have Hm: 0<= i{2} < size (map remi bb{2}) by smt.
      rewrite (take_nth witness) 1: Hm // filter_rcons. 
      by rewrite //= H3 H2 //= map_rcons cats1 /(\o) //=.
    + by smt. 
    + have Hm: 0<= i{2} < size (map remi bb{2}) by smt.
      rewrite (take_nth witness) 1: Hm // filter_rcons. 
      by smt.
    + cut Hd:= verify_validDec sk{2} 
                ((nth witness (map remi bb{2}) i{2}).`1, (nth witness (map remi bb{2}) i{2}).`2)
                HRO.RO.m{2} H2.
      smt.
    + smt.
    + have Hm: 0<= i{2} < size (map remi bb{2}) by done.
      rewrite (take_nth witness) 1: Hm // filter_rcons. 
      by rewrite //= H3 H2 //= .  
    + have Hm: 0<= i{2} < size (map remi bb{2}) by done.
      rewrite (take_nth witness) 1: Hm // filter_rcons. 
      by rewrite //= H3 H2 //= .  
    + smt. 
    + have Hm: 0<= i{2} < size (map remi bb{2}) by done.
      rewrite (take_nth witness) 1: Hm // filter_rcons. 
      by rewrite //= H2 //= .  
    + have Hm: 0<= i{2} < size (map remi bb{2}) by done.
      rewrite (take_nth witness) 1: Hm // filter_rcons. 
      by rewrite //= H2 //= . 
  auto=>/>; progress.  
  + by rewrite take0. 
  + by rewrite take0. 
  + by rewrite take0. 
  + smt.   
  + smt.
qed. 

(* equivalence between HeliosHom.tally' and HeliosHom.tally *)
local equiv MV_Helios_tally2:
  BeleniosHom(Pe, Ve, Ve', Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).tally' ~ 
  BeleniosHom(Pe, Ve, Ve', Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).tally
  : ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS, arg}
      ==>
    ={res, glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS}.
proof.
  proc.
  swap{1} 1 4. 
  seq 4 4: (={glob HRO.RO, glob GRO.RO, glob Pz, glob SS, glob JRO.RO,
              glob Ve, bb, sk, pk, sbb, hk} /\
            pk{1} = get_pk sk{1}/\
            (forall b, mem sbb{2} b => 
                 (decrypt sk{2} b.`2 <> None))).
    while( ={ i, bb, sbb, pk, sk, glob Ve, glob SS, glob HRO.RO, glob JRO.RO, sk}/\ 
           0 <= i{2} /\ pk{1} = get_pk sk{1} /\  
              (forall b, mem sbb{2} b => 
                 (decrypt sk{2} b.`2 <> None))).
      inline *; sp; wp.
      exists* (glob Ve){1}, sk{1}, b{2}; elim * => gv sk1 b1.
      call(: ={glob JRO.RO}); first by sim.
      call{1} (Verify_to_verify gv (get_pk sk1, b1.`1, b1.`2.`1) b1.`2.`2).
      call{2} (Verify_to_verify gv (get_pk sk1, b1.`1, b1.`2.`1) b1.`2.`2).
      auto; progress.
      + by smt.
      + move: H5; 
          rewrite cats1 mem_rcons //=;
          move => H10. 
        cut T:= verify_validDec sk{2} ((nth witness (map remi bb{2}) i{2}).`1, 
                                       (nth witness (map remi bb{2}) i{2}).`2) HRO.RO.m{2} _; 
          first by rewrite //=.
        by smt.
      + by smt. 
    by auto.

  call(_: ={glob GRO.RO}); first by sim. 
  wp; while{1} (0 <= i{1} <= size sbb{1} /\
              ubb{1} = map (fun (b: upkey * h_cipher) => 
                            (b.`1, (oget (decrypt sk{1} b.`2))))
                      (take i{1} sbb{1}))
              (size sbb{1} - i{1}); progress.
    by auto; progress; smt.

  auto; progress. 
  + by smt (size_ge0). 
  + by rewrite take0. 
  + by smt. 
  + have ->>: i_L = size sbb{2} by smt.
    rewrite take_size. 
    cut Ht:= fg (map snd (Sanitize sbb{2})) sk{2} _. 
      move => b Hs.  
      have Hex: exists id, (id,b) \in (Sanitize sbb{2}). 
       move: Hs. rewrite /rem_id mapP //=. by smt.
      smt.
      (*cut t:= san_mem sbb{2} b Hs.
      by smt.*)
    cut := san_tallymap sbb{2} sk{2}. 
    simplify; move => Hf.
    by rewrite Hf -Ht -?map_comp /(\o) //=. 
  + have ->>: i_L = size sbb{2} by smt.
    rewrite take_size. print hom_val.
    cut t:= hom_val sk{2} (map snd (Sanitize sbb{2})) _.  
      move => c Hc. 
      have Hex: exists id, (id,c) \in (Sanitize sbb{2}). 
       move: Hc. rewrite /rem_id mapP //=. by smt.
       smt.  
    by smt. 
  + have ->>: i_L = size sbb{2} by smt.
    rewrite take_size. 
    cut :=(san_tallymap sbb{2} sk{2}).
    simplify.
    pose f:= (fun (b : ident * h_cipher) =>
             (b.`1, oget (decrypt sk{2} b.`2))). 
    rewrite //=. 
    have ->: (Sanitize sbb{2}) = [] by smt.
    smt.
qed.

(* equivalence between MV.tally and HeliosHom.tally *)
equiv MV_HA_tally:
  B'(IPKE(Pe,Ve),Pz,Vz,CIND(Ve',IPKE(Pe,Ve),SS),SS, HRO.RO, GRO.RO, JRO.RO).tally ~ 
  BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).tally
  : ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz,  glob Ve, glob SS, arg}  
      ==>
    ={res, glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS}.
proof.

  transitivity BeleniosHom(Pe, Ve, Ve', Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).tally' 
  (={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS, arg} ==> 
   ={res, glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz,  glob Ve, glob SS})
  (={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS, arg} ==> 
   ={res, glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS}) =>//=. 
  by progress; smt.
  by apply (MV_Helios_tally1). 
  by apply (MV_Helios_tally2).
qed.
end section BeleniosHom_homomorphicAddition.
(* BPRIV + CONSISTENCY + + CORRECTNESS + STRONG VERIFIABILITY PROPERTY *)
(* --------------------------------------------------------------------------*)
section Security.

declare module S : Simulator 
                   { BP, BS, BPS, BSC, BBS, HRO.RO, JRO.RO, GRO.RO, 
                     H.Count, H.HybOrcl, WrapAdv}.
declare module Vz: Verifier
                   { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, JRO.RO, GRO.RO, S}.
declare module Pz : Prover
                   { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, JRO.RO, GRO.RO, S, Vz}.
declare module SS: SignScheme
                   { BP, BS, BPS, BH, BW, WU, BU, GRO.RO, JRO.RO, S, Vz, Pz, HRO.RO, 
                     BSC, BBS, H.Count, H.HybOrcl, WrapAdv}.
declare module Ex: PoK_Extract    {BW, BSC,Pz, BPS, GRO.RO, JRO.RO, Vz, HRO.RO, SS}.

declare module BPRIV_A: BPRIV_Adv        
                  { BP, BS, BPS, BSC, BBS, HRO.RO, GRO.RO, S, Vz, Pz, SS, JRO.RO,
                    H.Count, H.HybOrcl, WrapAdv}. 
declare module CONSIS2_A: SConsis2_Adv {BP, HRO.RO, JRO.RO, GRO.RO, SS}.
declare module CONSIS3_A: SConsis3_Adv {BP, HRO.RO, JRO.RO, GRO.RO, SS, Pz}.

declare module DBB_A : DBB_Adv    
                  {BS, BSC, BPS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO,  Pz, Vz, SS, Ex}. 

declare module DREG_A : DREG_Adv    
                  {BS, BSC, BPS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO,  Pz, Vz, SS, Ex}. 


(* this are needed for the LPKE scheme *)
declare module Ve: PSpke.Verifier
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, 
                    S, Vz, Pz, BPRIV_A, DBB_A, DREG_A, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO, Ex}.
declare module Pe: PSpke.Prover
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO,
                    S, Vz, Pz, Ve, BPRIV_A, DBB_A, DREG_A, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO, Ex}.

(* admit. fixme: link between E and C *)
declare module Ve': PSpke.Verifier
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, 
                    S, Vz, Pz, BPRIV_A, DBB_A, DREG_A, Ve, Pe, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO, Ex}.
(* ** ASSUMPTIONS ** *)
(* ** start *)
  axiom size_Voters:
    0 < size Voters.

  axiom weight_dh_out: weight dh_out = 1%r.
  axiom weight_dg_out: weight dg_out = 1%r. 
  axiom weight_dj_out: weight dj_out = 1%r.
  axiom weight_dk_out: weight dhkey_out = 1%r.
  axiom is_finite_h_in : Finite.is_finite predT <:h_in>.
  axiom distr_h_out    : is_lossless dh_out.
  axiom is_finite_j_in : Finite.is_finite predT <:j_in>.
  axiom distr_j_out    : is_lossless dj_out.
  axiom is_finite_g_in : Finite.is_finite predT <:g_in>.
  axiom distr_g_out    : is_lossless dg_out.

  axiom inj_get_upk (x y : uskey): get_upk x = get_upk y => x = y.

  axiom inj_get_pk (x y: skey):  get_pk x = get_pk y => x = y.

  (* Policy restrictions *)
  axiom San_mem (L: (ident * (plain * upkey * cipher*sign)) list) x:
    mem (Sanitize L) x => mem L x. 

  axiom San_mem'(L : (ident * (upkey * cipher * sign)) list) x:
    x \in Sanitize L => x \in L. 

  axiom San_mem''(L : (upkey * (ident * cipher * sign)) list) x:
    x \in Sanitize L => x \in L. 

  axiom san_mem''' (sbb: (upkey * h_cipher) list) (b: upkey * h_cipher):
    mem (Sanitize sbb) b => mem sbb b.

  axiom San_map (L: (upkey*cipher*sign)list) 
                (p: upkey * cipher* sign -> plain):
   let g = (fun (x: upkey*cipher*sign), (x.`1, p x)) in
   Sanitize (map g L) = map g (USan L).


  (* axioms restricting possible values for Sanitize *)
  axiom San_uniq (L: (ident * plain * upkey * cipher * sign) list): 
    uniq (Sanitize (map pack4 L)).

  axiom San_uniq_fst (L: (upkey * (ident * cipher * sign)) list):
     uniq (map fst (Sanitize L)).
 
  axiom San_uniq_fst' (L : (ident * (upkey * cipher * sign)) list):
     uniq (map fst (Sanitize L)). 

  axiom San_uniq_snd (S: (ident * (upkey * cipher * sign)) list ):
     uniq (map snd S) =>
     uniq (map snd (Sanitize S)). 
 
  axiom San_uniq' (L : (ident * (upkey * cipher * sign)) list):
      uniq (Sanitize L).

(*  axiom San_uniq_filter (L: (ident * upkey * cipher * sign) list) 
                        (f: ident * upkey * cipher * sign -> ident):
      uniq (map f L) => uniq (map f (Sanitize' L)). *)

  (* axiom that maintains Sanitize membership w.r.t a filtered list *)
  axiom San_mem_filter (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in Sanitize L => 
     x \in Sanitize (filter f L).

  axiom San_filter_ident (L: (ident * (upkey * cipher * sign)) list) p:
    let f = (fun (x: ident * (upkey * cipher * sign)), p x.`1) in
    Sanitize (filter f L) =  filter f (Sanitize L). 

  axiom uniq_to_San (A: (upkey * (cipher * sign)) list):
    uniq (map fst A) =>
    Sanitize A = A.    
           
  axiom USan_USanitize (A: (ident *upkey*cipher*sign) list):
    map remi (USanitize A) = USan (map remi A). 

  axiom San'_USan (L: (ident*upkey*cipher*sign) list):
    (forall x y, x \in L => y \in L => x.`1 = y.`1 => x.`2 = y.`2) =>
    (forall x y, x \in L => y \in L => x.`2 = y.`2 => x.`1 = y.`1) =>
    Sanitize' L = USanitize L.

  (* axiom on commutativity of policy and decryption *)
  axiom san_tallymap (sbb: (upkey * h_cipher) list) (sk: skey):
    let f = (fun (b : upkey * h_cipher) =>
                (b.`1, (oget (decrypt sk b.`2)))) in
  Sanitize (map f sbb)  =  map f (Sanitize sbb).

(* BPRIV adversary *)
  axiom Acor_ll (O <: BPRIV_Oracles{BPRIV_A}): 
    islossless BPRIV_A(O).corL.

  axiom BAa1_ll (O <: BPRIV_Oracles { BPRIV_A }):
    islossless O.vote    => islossless O.cast    =>
    islossless O.board   => islossless O.h       =>
    islossless O.g       => islossless O.j       =>
    islossless BPRIV_A(O).a1.
  axiom BAa2_ll (O <: BPRIV_Oracles { BPRIV_A }):
    islossless O.board => islossless O.h     =>
    islossless O.g     => islossless O.j     =>
    islossless BPRIV_A(O).a2.

  axiom CAa_ll (O <: SCons_Oracle { CONSIS2_A }):
    islossless O.h       => islossless O.g       =>
    islossless CONSIS2_A(O).main.

  axiom DAa1_ll (O <: DBB_Oracle { DBB_A }):
    islossless O.corrupt => islossless O.vote => 
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DBB_A(O).a1.
  axiom DAa2_ll (O <: DBB_Oracle { DBB_A }):
    islossless O.check =>  islossless O.h  =>
    islossless O.g     =>  islossless O.j  =>
    islossless DBB_A(O).a2.

  axiom RAa2_ll (O <: DREG_Oracle { DREG_A }):
    islossless O.corrupt => islossless O.vote => 
    islossless O.cast    => islossless O.board =>
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DREG_A(O).a2.
  axiom RAa3_ll (O <: DREG_Oracle { DREG_A }):
    islossless O.check   => islossless O.board =>
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DREG_A(O).a3.

  axiom ATU_ll (A<: TallyUniq_Adv') (V <: VotingScheme{A}) 
               (H <: HOracle.ARO{A}) 
              (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
    islossless A(V, H, G, J).main.

  axiom ATU2_ll (A<: TallyUniq_Adv_2) (V <: VotingScheme'{A}) 
               (H <: HOracle.ARO{A}) 
              (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
    islossless A(V, H, G, J).main.

  (* -> Proof system *)
  axiom Pz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Pz(G).prove. 
  axiom Vz_ll (G <: GOracle.ARO):
    islossless G.o => islossless Vz(G).verify.

  (* -> Small proof system, for ciphertexts *)
  axiom Pe_ll (H <: HOracle.ARO):
    islossless H.o =>  islossless Pe(H).prove.
  axiom Ve_ll (H <: HOracle.ARO):
    islossless H.o =>  islossless Ve(H).verify.
  axiom Ve_ll' (H <: HOracle.ARO):
    islossless H.o =>  islossless Ve'(H).verify.

  (* -> Signature *)
  axiom SSk_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).kgen.
  axiom SSs_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).sign. 
  axiom SSv_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).verify. 

  (* -> for ZK simulator *)
  axiom Si_ll: islossless S.init.
  axiom So_ll: islossless S.o.
  axiom Sp_ll: islossless S.prove.

  (* for extractor *)
  axiom Ex_ll (G <: GOracle.ARO):
    islossless G.o =>
    islossless Ex(G).extract.

  axiom Vz_keepstate (gv: (glob Vz)) :
    phoare [Vz(GRO.RO).verify:  
           (glob Vz) = gv ==> (glob Vz) = gv ] = 1%r.

  axiom Verify_to_verify (gv: (glob Ve)) (s: h_stm) (p: h_prf):
    phoare[Ve(HRO.RO).verify: (glob Ve) = gv /\ arg = (s, p)
         ==>
         (glob Ve) = gv /\
         res = verify s p HRO.RO.m] = 1%r.
  axiom Verify_to_verify' (gv: (glob Ve')) (s: h_stm) (p: h_prf):
    phoare[Ve'(HRO.RO).verify: (glob Ve') = gv /\ arg = (s, p)
         ==>
         (glob Ve') = gv /\
         res = verify s p HRO.RO.m] = 1%r.

  axiom Prover_to_verify_one (gp: (glob Pe)) (s: h_stm) (w: h_wit):
    phoare[Pe(HRO.RO).prove: 
          (glob Pe) = gp /\ arg = (s, w)
          ==>
          (glob Pe) = gp /\ verify s res HRO.RO.m]= 1%r.

  (* axiom for transforming a proof in a verification (two-sided) *)
  axiom Prover_to_verify_two (s: h_stm) (w: h_wit):
    equiv[Pe(HRO.RO).prove ~ Pe(HRO.RO).prove: 
         ={glob HRO.RO, glob Pe, arg} /\ arg{2} = (s, w)
         ==>
         ={glob HRO.RO, glob Pe, res} /\
         verify s res{2} HRO.RO.m{2}].

  axiom Kgen_get_upk (J<: JOracle.ARO):
    equiv [SS(J).kgen ~ SS(J).kgen:  
           ={glob J, glob SS} ==> 
           ={glob J, glob SS,  res} /\  res{2}.`2 = get_upk res{2}.`1].
  axiom  Kgen_get_upk' (J1 <: JOracle.ARO) (J2 <: JOracle.ARO):
    equiv [SS(J1).kgen ~ SS(J2).kgen:  
           ={ glob SS} ==>  ={glob SS,  res} /\ res{2}.`2 = get_upk res{2}.`1].

  axiom SSk_eq (gs : (glob SS)) (O <: ARO):
    phoare [SS(O).kgen: 
          (glob SS) = gs ==>  
           (glob SS) = gs /\ res.`2 = get_upk res.`1] = 1%r.

  axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)  ==>
             (glob SS) =gs /\ res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
  (* admit for know: trye to prove using above *)
  axiom ver_Ver_one' (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(MEUF_Oracle(SS,JRO.RO)).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2) ==>
             (glob SS) =gs /\ res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
  axiom Sig_ver_one (gs: (glob SS)) (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   phoare [SS(JRO.RO).sign :
           gs = (glob SS) /\ arg = (usk2,c2) ==>   
           gs = (glob SS) /\ ver_sign upk2 c2 res JRO.RO.m ]=1%r.
   axiom Sig_ver_two (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   equiv [SS(JRO.RO).sign ~ SS(JRO.RO).sign :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (usk2,c2) ==>   
          ={glob JRO.RO, glob SS, res} /\ ver_sign upk2 c2 res{2} JRO.RO.m{2} ].

local lemma MV_HA_bpriv &m:
  `|Pr[BPRIV_L(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ]-
    Pr[BPRIV_R(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ]| =
  `|Pr[BPRIV_L(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ]-
    Pr[BPRIV_R(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ]|.
proof.
  have ->: Pr[BPRIV_L(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
                 BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ] =
           Pr[BPRIV_L(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
                 BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ].
    byequiv (_: ={glob BPRIV_A, glob Pz,
                  glob Pe, glob Ve, glob Ve', glob SS} ==> _ ) =>//=. 
    proc. 
    call (_: ={glob HRO.RO, glob GRO.RO, glob Pz, glob JRO.RO,
               BP.bb1, BP.bb0, BP.sk, BP.uL, BP.pk, BP.hk});
      [1..4: by sim]. 
    call (MV_HA_tally Pe Ve Ve' Pz Vz S SS BPRIV_A san_tallymap 
                      san_mem''' Verify_to_verify ver_Ver_one).
    call (_: ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz,
               glob Pe, glob Ve, glob Ve',
               glob JRO.RO, glob SS,
               BP.bb1, BP.bb0, BP.sk, BP.uL, 
               BP.pk, BP.qVo, BP.qCa, BP.coL, BP.hk});
      [1..6: by sim]. 
    call(: true).
    while (={i, BP.uL, BP.uLL, glob SS}); first by sim.
    call(_: ={glob HRO.RO, glob Pe, glob Ve}); first
      by sim.
    call(_: true ==> ={glob JRO.RO}); first by sim.
    call(_: true ==> ={glob GRO.RO}); first by sim.
    call(_: true ==> ={glob HRO.RO}); first by sim.
    by auto.

  have ->: Pr[BPRIV_R(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ] =
           Pr[BPRIV_R(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ].
    byequiv (_: ={glob BPRIV_A, glob Pz, glob S,
                  glob Pe, glob Ve, glob Ve', glob SS} ==> _ ) =>//=. 
    proc. 
    call (_: ={glob HRO.RO, glob S, glob Pz, glob JRO.RO,
               BP.bb1, BP.bb0, BP.sk, BP.uL, BP.pk,BP.hk});
      [1..4: by sim]. 
    call(: true).
    call(: true); first by auto.
    call (MV_HA_tally Pe Ve Ve' Pz Vz S SS BPRIV_A san_tallymap 
                      san_mem''' Verify_to_verify ver_Ver_one).
    call (_: ={glob HRO.RO, glob S, glob JRO.RO, glob Pz,
               glob Pe, glob Ve, glob Ve',
               glob JRO.RO, glob SS, BP.hk,
               BP.bb1, BP.bb0, BP.sk, BP.uL, 
               BP.pk, BP.qVo, BP.qCa, BP.coL});
      [1..6: by sim]. 
    call(: true).
    while (={i, BP.uL, BP.uLL, glob SS}); first by sim.
    call(_: ={glob HRO.RO, glob Pe, glob Ve}); first
      by sim.
    call(: true).
    call(_: true ==> ={glob JRO.RO}); first by sim.
    call(_: true ==> ={glob GRO.RO}); first by sim.
    call(_: true ==> ={glob HRO.RO}); first by sim.
    by auto.
  by done.   
qed.

(* Lemma bounding bpriv *)
lemma bpriv &m: 
   `|Pr[BPRIV_L(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ]-
    Pr[BPRIV_R(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ]|
<=
   `|Pr[ZK_L(DecRel (IPKE(Pe, Ve),HRO.RO), Pz, 
                     BZK(IPKE(Pe, Ve),Pz,CIND(Ve',IPKE(Pe, Ve),SS),Vz,SS,
                     BPRIV_A,HRO.RO,JRO.RO), GRO.RO).main() @ &m : res] -
      Pr[ZK_R(DecRel (IPKE(Pe, Ve),HRO.RO), S, 
                     BZK(IPKE(Pe, Ve),Pz,CIND(Ve',IPKE(Pe, Ve),SS),Vz,SS,
                     BPRIV_A,HRO.RO,JRO.RO)).main() @ &m : res]| +
n%r *
    `|Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
                                               BPRIV_A, SS, JRO.RO,S), IPKE(Pe, Ve), HRO.RO), 
               HRO.RO, Left).main() @ &m : 
               res /\ WrapAdv.l <= n /\ size BS.encL <= 1] -
      Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
                                               BPRIV_A, SS, JRO.RO,S), IPKE(Pe, Ve), HRO.RO), 
               HRO.RO, Right).main() @ &m : 
               res /\ WrapAdv.l <= n /\ size BS.encL <= 1]|.
proof.
  rewrite -(MV_HA_bpriv &m).  
  by rewrite (bpriv S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out weight_dk_out
             is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan Count_perm Count_split add_com Acor_ll 
              BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll 
              Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll So_ll Sp_ll Ex_ll  
              Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
           
qed. 

lemma consis1(id: ident, v: plain, l: uskey) &m: 
   Pr[PKE.Correctness(IPKE(Pe, Ve), HRO.RO).main(v,get_upk l) @ &m: res] <=
   Pr[SConsis1(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
       CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id,v, l) @ &m: res].
proof.
  have ->:  Pr[SConsis1(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id, v, l) @ &m : res] =
            Pr[SConsis1(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve',IPKE(Pe, Ve), SS), SS), 
               CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id, v, l) @ &m : res].
    by byequiv =>/>; sim.
   by rewrite (consis1 S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan Count_perm Count_split add_com Acor_ll 
              BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll 
              Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll So_ll Sp_ll Ex_ll
              Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two id v l &m).
qed.

lemma consis2 &m: 
  Pr[SConsis2(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
                 CIND(Ve',IPKE(Pe,Ve),SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] = 1%r.
proof.
  have ->:  Pr[SConsis2(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               CIND(Ve', IPKE(Pe, Ve), SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res] =
            Pr[SConsis2(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
               CIND(Ve', IPKE(Pe, Ve), SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
    by byequiv =>/>; sim.
  by rewrite (consis2 S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan Count_perm Count_split add_com Acor_ll 
              BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll 
              Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll So_ll Sp_ll Ex_ll 
              Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
qed.

(* left side of strong consistency part 3 *)
local equiv consis_tally_L :
  SConsis3_L(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), CIND(Ve',IPKE(Pe,Ve),SS), 
             CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).V.tally ~ 
  SConsis3_L(B'(IPKE(Pe,Ve),Pz,Vz, CIND(Ve',IPKE(Pe,Ve),SS),SS),  
               CIND(Ve',IPKE(Pe, Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).V.tally
   : ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS, arg}  
      ==>
    ={res, glob HRO.RO, glob GRO.RO, glob JRO.RO,glob Pz,  glob Ve, glob SS}.
proof.
  symmetry =>//=.
  conseq (_: ={glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz,  glob Ve, glob SS, arg} ==>
   ={res, glob HRO.RO, glob GRO.RO, glob JRO.RO, glob Pz, glob Ve, glob SS})=>//=. 
  by rewrite (MV_HA_tally Pe Ve Ve' Pz Vz S SS BPRIV_A san_tallymap 
                          san_mem''' Verify_to_verify ver_Ver_one).  
qed. 

(* Lemma bounding strong consistency part 3 *)
equiv consis3 &m:
    SConsis3_L(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), CIND(Ve',IPKE(Pe,Ve),SS), 
               CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main ~ 
    SConsis3_R(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), CE(IPKE(Pe,Ve)),
               CIND(Ve',IPKE(Pe,Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main
    : ={glob HRO.RO,glob GRO.RO, glob JRO.RO, glob IPKE(Pe,Ve),glob Ve',glob Pz, glob SS,glob CONSIS3_A}
     ==> ={res}.
proof.
  (* Because of the tally from BeleniosHom to B', glob Pz (used to create the proof) 
     is needed *)
  transitivity SConsis3_L(B'(IPKE(Pe,Ve),Pz,Vz, CIND(Ve',IPKE(Pe,Ve),SS),SS),  
               CIND(Ve',IPKE(Pe, Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main 
  (={glob HRO.RO,glob GRO.RO,glob JRO.RO,glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})
  (={glob HRO.RO,glob GRO.RO,glob JRO.RO,glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})=>//=. 

  + by progress; smt.

  + proc. 
    seq 13 13: (={ev, r, bb, BP.sk, glob HRO.RO, glob Ve', glob SS, BP.hk,
                glob GRO.RO, glob Pz, glob JRO.RO, glob IPKE(Pe, Ve)})=>//=. 
      by sim. 
    if; auto. 
    by call (consis_tally_L). 

  transitivity SConsis3_R(B'(IPKE(Pe,Ve),Pz,Vz, CIND(Ve', IPKE(Pe, Ve),SS),SS), 
               CE(IPKE(Pe,Ve)), CIND(Ve', IPKE(Pe, Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main 
  (={glob HRO.RO, glob GRO.RO,glob JRO.RO, glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})
  (={glob HRO.RO, glob GRO.RO,glob JRO.RO, glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})=>//=. 

  + by progress; smt.

  + by rewrite (consis3 S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan Count_perm Count_split add_com Acor_ll 
              BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll 
              Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll So_ll Sp_ll Ex_ll 
              Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
  - by sim. 
qed.

lemma bound_dbb (COR <: Corr_Adv{WU, BU, JRO.RO, SS}) &m:
  islossless COR(JRO.RO).main=>
  Pr[DBB_Exp (BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), 
              DBB_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv(B(IPKE(Pe, Ve), Pz, Vz, SS), 
              DBB_A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
n%r ^2 * 
  Pr[Wspread_Exp(IPKE(Pe, Ve), 
                 BWspread(B(IPKE(Pe, Ve), Pz, Vz,SS),GRO.RO,JRO.RO,DBB_A), 
                 HRO.RO).main() @ &m: BS.eq ] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BE(SS, COR), JRO.RO).main() @ &m : res] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(IPKE(Pe, Ve), HRO.RO, GRO.RO, DBB_A, SS), JRO.RO)), 
              JRO.RO).main () @ &m : res] +
(size (undup Voters))%r * 
  Pr[Correctness(SS, COR, JRO.RO).main() @ &m : !res] +
  Pr[PS.Correctness(DecRel (IPKE(Pe, Ve), HRO.RO), 
          BCorr_Adv(IPKE(Pe, Ve), BAcc(B(IPKE(Pe, Ve), Pz, Vz, SS), DBB_A), 
          HRO.RO, SS, JRO.RO), Pz, Vz, GRO.RO).main() @ &m : !res] + 
  Pr[PoK_Exp(Vz, DecRel (IPKE(Pe, Ve), HRO.RO), Ex, 
         PoK_TU(BTall(B(IPKE(Pe, Ve), Pz, Vz, SS), DBB_A), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m : res]. 
proof.
  move => Co. 
  have ->: Pr[DBB_Exp(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), 
                DBB_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] =
           Pr[DBB_Exp(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                DBB_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res]. 
    by byequiv =>/>; sim.
  by rewrite (bound_dbb S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan Count_perm Count_split add_com Acor_ll 
              BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll 
              Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll So_ll Sp_ll Ex_ll 
              Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two COR &m Co).
qed.

lemma bound_dreg &m:
  Pr[DREG_Exp (BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), 
               DREG_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv'(B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS),
               DREG_A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
n%r ^2 * 
  Pr[Wspread_Exp(IPKE(Pe,Ve), 
                 BWspread'(B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS),SS),
                    GRO.RO,JRO.RO,DREG_A), HRO.RO).main() @ &m: BS.eq ] +
  Pr[PS.Correctness(DecRel (IPKE(Pe, Ve), HRO.RO), 
                    BCorr_Adv(IPKE(Pe, Ve), 
                              BAcc'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe,Ve), SS), SS), 
                              DREG_A), 
          HRO.RO, SS, JRO.RO), Pz, Vz, GRO.RO).main() @ &m : !res] + 
  Pr[PoK_Exp(Vz, DecRel (IPKE(Pe, Ve), HRO.RO), Ex, 
         PoK_TU(BTall'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe,Ve), SS), SS), DREG_A), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m : res]. 
proof.
  have ->: Pr[DREG_Exp(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), 
                         DREG_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] =
           Pr[DREG_Exp(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe, Ve), SS), SS), 
                         DREG_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
    by byequiv =>/>; sim.
  by rewrite (bound_dreg S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan Count_perm Count_split add_com Acor_ll 
              BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll 
              Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll So_ll Sp_ll Ex_ll
              Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
qed.
end section Security.
