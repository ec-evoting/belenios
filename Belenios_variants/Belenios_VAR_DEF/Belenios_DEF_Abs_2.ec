require import Int Bool Real FMap.
require import Distr List.
require import LeftOrRight.
require (*  *) LPKE ProofSystem Signature DEF_VERIF_PRIV_2.


type vote, h_out, result, g_out, j_in, j_out, sign, upkey.
op dh_out : h_out distr.
op dg_out : g_out distr.
op dj_out : j_out distr.

op qVo : {int | 0< qVo} as qVo_bound.

(* import encryption scheme *)
clone export LPKE as PKE with
  type label<- upkey,
  type plain <- vote,
  type h_out<- h_out,
  op dout <- dh_out,
  op n    <- qVo
  proof*.
  realize n_pos. by exact qVo_bound. qed.

type hash_out, ident, maybe_id, hash_key.
type pub_data = maybe_id * upkey * cipher * sign.
op get_id: ident -> maybe_id.

op hash: hash_key ->maybe_id * upkey * cipher * sign -> hash_out.
op remi (x: ident * upkey * cipher * sign) = (x.`2,x.`3,x.`4).
op may_data (x: ident * upkey * cipher * sign) = 
            (get_id x.`1, x.`2, x.`3, x.`4).
op hash' (hk: hash_key) = (hash hk \o may_data).

op dhkey_out: hash_key distr.

(* import proof system *)
clone export ProofSystem as PS with
  type stm   <- pkey  * (pub_data * hash_out) list * result,
  type wit   <- skey,
  type g_out <- g_out,
  op   dout  <- dg_out
  proof *. 

op Voters : ident list.
(* import signature system *) 
clone export Signature as S with
  type ident   <- ident,
  type upkey   <- upkey,
  type message <- cipher, (* fixme: maybe add pk *)
  type string  <- sign,
  type s_in    <- j_in,
  type s_out   <- j_out,
  op dout      <- dj_out,
  op Voters    <- Voters
  proof *.

clone include DEF_VERIF_PRIV_2 with
  type ident  <- ident,
  type vote   <- vote,
  type result <- result,
  type pubBB  <- (pub_data * hash_out) list,
  type ballot <- cipher,
  type hash_key <- hash_key,
  type pkey   <- pkey,
  type skey   <- skey,
  type prf    <- prf,
  type upkey  <- upkey,
  type uskey  <- uskey,
  type sign   <- sign,
  type h_in   <- h_in,
  type h_out  <- h_out,
  type g_in   <- g_in,
  type g_out  <- g_out,
  type j_in   <- j_in,
  type j_out  <- j_out,
  op dh_out   <- dh_out,
  op dg_out   <- dg_out,
  op dj_out   <- dj_out,
  op thr3      <- thr3,
  op Voters   <- Voters,
  op qVo      <- qVo,
  op remi     <- remi,
  op get_upk  <- get_upk
  proof *. 
  realize qVo_pos by exact qVo_bound. 

op get_pk : skey -> pkey. 

           
(* abstract model of Belenios *)
module (B (E: Scheme, P: Prover, Ve: Verifier, SS: SignScheme ): VotingScheme) 
       ( H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {

  (* setup algorithm *)
  proc setup(): pkey * skey *hash_key = {
    var pk, sk,hk;

    (pk, sk) <@ E(H).kgen();
    hk       <$ dhkey_out;
    return (pk, sk, hk);
  }

  (* register algorithm *)
  proc register(id: ident): upkey * uskey = {
    var upk, usk;

    (usk,upk) <@ SS(J).kgen();
    return (upk, usk);
  }
  
 
  (* voting algorithm *)
  proc vote(id: ident, v: vote, pk: pkey, usk: uskey)
         : (ident * upkey * cipher * sign) = {
    var c,s, upk;

    upk <- get_upk usk;
    c <@ E(H).enc(pk, upk, v);
    s <@ SS(J).sign(usk,c);
    return (id, upk, c, s);
  }

  (* tally algorithm *)
  proc tally(bb: (ident* upkey * cipher * sign) list, sk: skey, hk: hash_key): result * prf = {
    var ubb, b, i, m, e, pbb, pk, r, pi, fbb;

    ubb  <- [];
    i   <- 0;
    pk  <- get_pk sk;
 
    fbb <- map remi bb;
    (* keep only what is valid and verified *)
    while( i < size fbb){
      b  <- nth witness fbb i;
      m  <@ E(H).dec(sk, b.`1, b.`2);
      e  <@ SS(J).verify(b.`1, b.`2, b.`3);
      (* valid dec and sign *)
      if (m <>None /\ e){
        ubb <- ubb ++ [(b.`1, oget m)];
      }
      i  <- i + 1;
    }
    
    r    <- Count (map snd (Sanitize ubb));
    
    pbb <- map (fun (x: ident* upkey * cipher * sign), 
                    (may_data x, hash' hk x )) (USanitize bb);
    pi   <@ P(G).prove((pk, pbb, r), sk);

    return (r, pi);
  } 
  
  (* publish algorithm *)
  proc publish (bb: (ident * upkey * cipher * sign) list, hk: hash_key) ={
    var pbb;
    pbb <- map ((fun (x: ident*upkey * cipher * sign), 
                    (may_data x, hash' hk x))) (USanitize bb);
    return pbb;
  }

  (* verify algorithm *)
  proc verify(st: (pkey * (pub_data * hash_out) list * result), pi: prf): bool = {
    var b;
    b <@ Ve(G).verify(st, pi);
    return b;
  }
 
  (* vote verification algorithm *)
  proc verifyvote(b: (ident * upkey * cipher * sign), 
                  pbb: (pub_data * hash_out) list, 
                  hk: hash_key): bool ={
    var v;
    (* checks the hash is in the list of available hashes *)
    v<- (hash' hk b) \in (map snd pbb);
    return v;
  }
}.

(* relation for proof system of correct tabulation *)
module DecRel (E: Scheme, H: HOracle.ARO)  ={
  proc main(stm: (pkey * ((maybe_id * upkey * cipher*sign) * hash_out) list * result), 
            wit: skey ): bool ={
    var ubb, b, i, m, r, ev1, ev2, h;

    (* 1. the pk is obtained from the sk *)
    ev1 <- (stm.`1 = get_pk wit);

    (* 2. the result matches the dec of the valid board *)
    ubb  <- [];
    i   <- 0;

    while( i < size stm.`2){
      (b,h)  <- nth witness stm.`2 i;
      m  <@ E(H).dec(wit, b.`2, b.`3);
      (*if (m <>None){ *)
        ubb <- ubb ++ [oget m];
      (*}*)
      i  <- i + 1;
    }
    
    r    <- Count ubb;
    ev2  <- stm.`3 = r;
    return (ev1 /\ ev2);
  }
}. 


(* abstract Belenios with validity check *) 
module (B' (E: Scheme, P: Prover, Ve: Verifier, C: ValidInd, SS: SignScheme): VotingScheme') 
       ( H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {

  proc setup    = B(E,P,Ve,SS,H,G,J).setup
  proc register = B(E,P,Ve,SS,H,G,J).register
  proc vote     = B(E,P,Ve,SS,H,G,J).vote
  proc tally    = B(E,P,Ve,SS,H,G,J).tally
  proc publish  = B(E,P,Ve,SS,H,G,J).publish
  proc verify   = B(E,P,Ve,SS,H,G,J).verify
  proc verifyvote =  B(E,P,Ve,SS,H,G,J).verifyvote

  proc valid(bb: (ident * upkey * cipher * sign) list, 
             b : (ident * upkey * cipher * sign),
             pk: pkey) : bool ={
    var  ev1,  ev2;
    ev1 <- (forall x, x\in bb => 
                       (x.`1,x.`2) = (b.`1,b.`2) \/
                       (x.`1 <> b.`1 /\ x.`2 <> b.`2));
    (* check ciphertext and signature validity *)
    ev2 <@ C(H,J).validInd(b, pk); (*
    (* check signature validity *)
    ev3 <@ SS(J).verify(b.`2,b.`3,b.`4); *)
    return ev1 /\ ev2;
  }
  
}.
