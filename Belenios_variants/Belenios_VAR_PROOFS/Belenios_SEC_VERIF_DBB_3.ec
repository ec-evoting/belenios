require import FMap List Int FSet Real LeftOrRight AllCore.
require import IntExtra RealExtra Distr.
require (*  *) Belenios_DEF_Abs_3.
require (*  *) Ex_Plug_and_Pray.


clone include Belenios_DEF_Abs_3. 

(* old logic lemmas, useful *)
lemma nosmt andEl: forall (a b:bool), (a /\ b) => a by [].
lemma nosmt andEr: forall (a b:bool), (a /\ b) => b by [].

module type Hash_Adv ={
  proc find(hk: hash_key): (maybe_id*upkey * cipher * sign) * 
               (maybe_id*upkey * cipher * sign)
}.

(* general hash targeted collision resistance *)
module Hash_Exp (A: Hash_Adv) ={
  proc main(): bool ={
    var ml, mr,hk;
    hk <$ dhkey_out; 
    (ml,mr) <@ A.find(hk);
    return (ml <> mr /\ hash hk ml = hash hk mr);
  }
}.

module BH ={
  var l: maybe_id*upkey * cipher * sign
  var r: maybe_id*upkey * cipher * sign
  var sl: ident * upkey * cipher * sign
  var sr: ident * upkey * cipher * sign
}.

op dec_cipher: skey -> upkey -> cipher -> (h_in,h_out) map -> vote option. 
op ver_sign  : upkey -> cipher -> sign -> (j_in,j_out) map -> bool.

(* ** 
  eager random oracle for the proof system
  over the correctness of the election result
** *)
clone ROM.Eager as HRO with
  type from    <- h_in,
  type to      <- h_out,
  op dsample(inp: h_in) <- dh_out
  proof *.

clone ROM.Eager as GRO with
  type from    <- g_in,
  type to      <- g_out,
  op dsample(inp: g_in) <- dg_out
  proof *.

clone ROM.Eager as JRO with
  type from    <- j_in,
  type to      <- j_out,
  op dsample(inp: j_in) <- dj_out
  proof *. 

section DBB.

(* ** abstract algorithms ** *) 
declare module E: Scheme      {BS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO}.
declare module Pz: Prover     {BS, BH, BW, BU, JRO.RO, HRO.RO, GRO.RO, E}.
declare module Vz: Verifier   {BS, BH, BW, BU, JRO.RO, HRO.RO, GRO.RO, E, Pz}.
declare module SS: SignScheme {BS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, E, Pz, Vz}.
declare module A : DBB_Adv    {BS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, E, Pz, Vz, SS}. 

(* ** ASSUMPTIONS ** *)
(* ** start *)

  axiom size_Voters:
    0 < size Voters.
  axiom size_qVo:
    0 < qVo.

  axiom dg_weight: 
     weight dg_out = 1%r.
  axiom dh_weight: 
     weight dh_out = 1%r.
  axiom dk_weight:
     weight dhkey_out = 1%r.
  axiom inj_get_upk (x y : uskey):
    get_upk x = get_upk y => x = y.

(* lossless assumptions *)
  axiom Pz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Pz(G).prove.
  axiom Vz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Vz(G).verify. 
  axiom Ek_ll(H <: HOracle.ARO): 
    islossless E(H).kgen.
  axiom Ed_ll(H <: HOracle.ARO): 
    islossless H.o => islossless E(H).dec.
  axiom Ee_ll(H <: HOracle.ARO): 
    islossless H.o => islossless E(H).enc. 
  axiom SSk_ll (SS<: SignScheme) (O<: JOracle.ARO): 
      islossless O.o => islossless SS(O).kgen.
  axiom SSs_ll (SS<: SignScheme) (O<: JOracle.ARO): 
      islossless O.o => islossless SS(O).sign.
  axiom SSv_ll (O<: JOracle.ARO): 
      islossless O.o => islossless SS(O).verify.

  axiom Aa1_ll (O <: DBB_Oracle { A }):
    islossless O.corrupt => 
    islossless O.vote => 
    islossless O.h   =>
    islossless O.g   =>
    islossless O.j   =>
    islossless A(O).a1.
  axiom Aa2_ll (O <: DBB_Oracle { A }):
    islossless O.check => 
    islossless O.h   =>
    islossless O.g   =>
    islossless O.j  =>
    islossless A(O).a2.

(* axioms that link algorithms to operators *)

  (* axiom for stating that the keys are generated *)
  axiom Kgen_get_pk (H<: HOracle.ARO):
    equiv [E(H).kgen ~ E(H).kgen:  ={glob H, glob E} ==> 
          ={glob H, glob E,  res} /\ 
          res{2}.`1 = get_pk res{2}.`2].

  (* axiom for stating that the keys are generated *)
  axiom Kgen_get_upk (J<: JOracle.ARO):
    equiv [SS(J).kgen ~ SS(J).kgen:  ={glob J, glob SS} ==> 
          ={glob J, glob SS,  res} /\ 
          res{2}.`2 = get_upk res{2}.`1].

  axiom  Kgen_get_upk' (J1 <: JOracle.ARO) (J2 <: JOracle.ARO):
    equiv [SS(J1).kgen ~ SS(J2).kgen:  ={ glob SS} ==> 
          ={glob SS,  res} /\ 
          res{2}.`2 = get_upk res{2}.`1].

  (* axiom for linking E.dec to dec_cipher operator *)   
  axiom dec_Dec_one (ge: (glob E)) (sk2: skey)(l2: upkey) (c2: cipher):
    phoare [E(HRO.RO).dec:  
           (glob E) =ge /\ arg = (sk2, l2, c2)
           ==>   
           (glob E) =ge /\
           res = dec_cipher sk2 l2 c2 HRO.RO.m ] = 1%r.

  (* axiom for transforming an encryption into decryption (two-sided) *)
  axiom Enc_dec_two (sk2: skey) (pk2: pkey) (l2: upkey) (p2: vote): 
    (pk2 = get_pk sk2) =>
    equiv [E(HRO.RO).enc ~ E(HRO.RO).enc : 
          ={glob HRO.RO, glob E, arg} /\ arg{2}=( pk2, l2, p2) 
          ==> 
          ={glob HRO.RO, glob E,  res} /\
          Some p2 = dec_cipher sk2 l2 res{2} HRO.RO.m{2}]. 

   axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)
             ==>
            (glob SS) =gs /\
            res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
   axiom ver_Ver_one' (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(MEUF_Oracle(SS,JRO.RO)).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)
             ==>
            (glob SS) =gs /\
            res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.

  axiom Sig_ver_two (usk2: uskey) (upk2: upkey) (c2: cipher):
  (upk2 = get_upk usk2) =>
  equiv [SS(JRO.RO).sign ~ SS(JRO.RO).sign :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (usk2,c2)
           ==>   
           ={glob JRO.RO, glob SS, res} /\
           ver_sign upk2 c2 res{2} JRO.RO.m{2} ].

  (* axiom for state of verifier Vz w.r.t to an eager radom oracle *)   
  axiom Vz_keepstate (gv: (glob Vz)) :
    phoare [Vz(GRO.RO).verify:  
           (glob Vz) = gv ==> (glob Vz) = gv ] = 1%r.

  axiom SSs_eq (gss: (glob SS)):
    phoare [SS(JRO.RO).sign: 
          (glob SS) = gss ==> (glob SS) = gss] = 1%r.
  axiom SSk_eq (gs : (glob SS)) (O <: ARO):
    phoare [SS(O).kgen: 
          (glob SS) = gs ==>  
           (glob SS) = gs /\ res.`2 = get_upk res.`1] = 1%r.

  (* axiom for membership of Sanitize *)
  axiom San_mem (L: (ident * (vote * upkey * cipher*sign)) list) x:
    mem (Sanitize L) x => mem L x. 

  axiom San_mem'(L : (ident * (upkey * cipher * sign)) list) x:
    x \in Sanitize L => x \in L.
  axiom San_mem''(L : (upkey * (ident * cipher * sign)) list) x:
    x \in Sanitize L => x \in L. 


  axiom San_map' (sk: skey, 
                 bb: (upkey * cipher*sign) list, 
                 mm: (h_in,h_out) map):
    let f   = fun (x : upkey * cipher*sign),
                  (x.`1, oget (dec_cipher sk x.`1 x.`2 mm)) in 
    Sanitize (map f bb) = 
    map (f \o (fun (x : upkey * (cipher * sign)), (x.`1, x.`2.`1,x.`2.`2))) 
         (Sanitize (map (fun (x : upkey * cipher * sign), (x.`1, (x.`2,x.`3))) bb)). 

  (* axioms restricting possible values for Sanitize *)
  axiom San_uniq (L: (ident * vote * upkey * cipher * sign) list): 
    uniq (Sanitize (map pack4 L)).

  axiom San_uniq_fst (L: (upkey * (ident * cipher * sign)) list):
     uniq (map fst (Sanitize L)).
 
  axiom San_uniq_snd (S: (ident * (upkey * cipher * sign)) list ):
     uniq (map snd S) =>
     uniq (map snd (Sanitize S)). 
 
  axiom San_uniq' (L : (ident * (upkey * cipher * sign)) list):
      uniq (Sanitize L).


  (* axiom that maintains Sanitize membership w.r.t a filtered list *)
  axiom San_mem_filter (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in Sanitize L => 
     x \in Sanitize (filter f L).

  axiom San_filter_ident (L: (ident * (upkey * cipher * sign)) list) p:
    let f = (fun (x: ident * (upkey * cipher * sign)), p x.`1) in
    Sanitize (filter f L) = filter f (Sanitize L). 

  axiom uniq_to_San (A: (upkey * (cipher * sign)) list):
    uniq (map fst A) =>
    Sanitize A = A.    
       
  axiom USan_USanitize (A: (ident *upkey*cipher*sign) list):
    map remi (USanitize A) = USan (map remi A). 

  (* axiom for determinism of Count, based on L1 is a permutation of L2 *)
  axiom Count_perm (L1 L2: vote list):
    perm_eq L1 L2 =>
    Count L1 = Count L2.

  op (+): result -> result -> result.
  axiom Count_split (L1 L2: vote list):
    Count (L1 ++ L2) = Count L1 + Count L2.
  axiom add_com (a b : result):
    a + b = b + a. 
  
(* ** end*)

(* decryption should not change the state of an eager random oracle *) 
local lemma dec_Dec_two (sk2: skey)(l2: upkey) (c2: cipher):
  equiv [E(HRO.RO).dec ~ E(HRO.RO).dec :
          ={glob HRO.RO, glob E, arg}/\ arg{2} = (sk2, l2, c2)
           ==>   
           ={glob HRO.RO, glob E, res} /\
           res{2} = dec_cipher sk2 l2 c2 HRO.RO.m{2} ].
proof.
  proc*=>//=. 
  exists* (glob E){1}; elim* => ge.
  call{1} (dec_Dec_one ge sk2 l2 c2 ).
  call{2} (dec_Dec_one ge sk2 l2 c2 ). 
  by auto.
qed.

local lemma ver_Ver_two (upk2: upkey) (c2: cipher) (s2: sign):
  equiv [SS(JRO.RO).verify ~ SS(JRO.RO).verify :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (upk2,c2,s2)
           ==>   
           ={glob JRO.RO, glob SS, res} /\
           res{2} = ver_sign upk2 c2 s2 JRO.RO.m{2} ].
proof.
  proc*=>//=. 
  exists* (glob SS){1}; elim* => gs.
  call{1} (ver_Ver_one gs upk2 c2 s2).
  call{2} (ver_Ver_one gs upk2 c2 s2). 
  by auto.
qed.

local lemma uniq_perm_eq_filter['a] (L S: (ident * 'a) list):
  uniq S =>
  uniq L =>
  (forall b, mem L b => mem S b)=>
  perm_eq L (filter (mem L) S).
proof.
  rewrite perm_eqP.
  move => HuS HuL Hmem p.
  elim: L S HuL Hmem HuS =>//=. smt.
  move => x L Hcount S Hnmem.
  rewrite count_filter.
  have Hc: forall (s: (ident * 'a) list) p1 p2,
    count (predU p1 p2) s = count p1 s + count p2 s - count (predI p1 p2) s.
    move => s p1 p2.
    elim: s=>//=.
    move => y s Hcs.
    rewrite Hcs //=.
    have Hk: b2i (predU p1 p2 y)  =
             b2i (p1 y) + b2i (p2 y) - b2i (predI p1 p2 y).
      by smt.
    by smt.
  have Hc': forall (s: (ident * 'a) list) p1 p2,
    count (predI p1 p2) s = count p1 s + count p2 s - count (predU p1 p2) s.
    move => s p1 p2.
    rewrite Hc //=.
    by smt.

  have Hc'': forall (s: (ident * 'a) list) p x,
    !mem s x =>
    count (predI p (transpose (=) x)) s =0.
    move => s p0.
    elim: s =>//=.
    progress. 
    have Hnn: x1 <> x0  by smt.
    have Hnmm: ! mem l x1 by smt. 
    rewrite (H x1 Hnmm).
    rewrite /predI //=.
    smt.
    
  have ->: (predI p (mem (x :: L))) =
           (fun y, (p y /\ y = x) \/ (p y /\ mem L y)). 
    rewrite -cat1s /predI. 
    rewrite fun_ext /(==).
    move => y.
    rewrite !mem_cat. 
    smt. 

  have ->: (fun (y : ident * 'a) => p y /\ y = x \/ p y /\ mem L y) =
           predU (predI p (transpose (=) x)) (predI p (mem L)). 
    by rewrite /predU /predI //=.
  rewrite Hc. 

  move => Hmemb HuS. 
  rewrite (Hcount S _ _ HuS); first 2 by smt.
  rewrite count_filter.
  have ->: (b2i (p x) + count (predI p (mem L)) S =
             count (predI p (transpose (=) x)) S + 
             count (predI p (mem L)) S -
             count (predI (predI p (transpose (=) x)) (predI p (mem L))) S) =
           (b2i (p x)  =
            count (predI p (transpose (=) x)) S  -
            count (predI (predI p (transpose (=) x)) (predI p (mem L))) S).
    by smt.
  have ->: count (predI (predI p (transpose (=) x)) (predI p (mem L))) S = 0.
  have ->: (predI (predI p (transpose (=) x)) (predI p (mem L))) = 
           (fun y, false).
    rewrite /predI //=.
    rewrite fun_ext /(==).
    move => y. smt.
    smt.
  simplify.
  have HmemS: mem S x by smt.

  
  have Hs: forall (s: (ident * 'a) list),
      uniq s =>
      mem s x =>
      b2i (p x) = count (predI p (transpose (=) x)) s.
    elim =>//=. 
    progress. 
    case(x0 = x). 
    + move => Heq.
      have ->: x0 = x by smt.
      rewrite Hc''. smt.      
      rewrite /predI //=. 
    - move => Hneq.
      have ->: b2i (predI p (transpose (=) x) x0) = 0 by rewrite /predI //=; smt.
      have Hmm: mem l x by smt.
      by rewrite (H H1 Hmm).
  
  by rewrite (Hs S HuS HmemS).
qed.

(* similar to above, but no id *)
local lemma uniq_perm_eq_filter_two['a] (L S: 'a list):
  uniq S =>
  uniq L =>
  (forall b, mem L b => mem S b)=>
  perm_eq L (filter (mem L) S).
proof.
  rewrite perm_eqP.
  move => HuS HuL Hmem p.
  elim: L S HuL Hmem HuS =>//=. smt.
  move => x L Hcount S Hnmem.
  rewrite count_filter.
  have Hc: forall (s: 'a list) p1 p2,
    count (predU p1 p2) s = count p1 s + count p2 s - count (predI p1 p2) s.
    move => s p1 p2.
    elim: s=>//=.
    move => y s Hcs.
    rewrite Hcs //=.
    have Hk: b2i (predU p1 p2 y)  =
             b2i (p1 y) + b2i (p2 y) - b2i (predI p1 p2 y).
      by smt.
    by smt.
  have Hc': forall (s: 'a list) p1 p2,
    count (predI p1 p2) s = count p1 s + count p2 s - count (predU p1 p2) s.
    move => s p1 p2.
    rewrite Hc //=.
    by smt.

  have Hc'': forall (s: 'a list) p x,
    !mem s x =>
    count (predI p (transpose (=) x)) s =0.
    move => s p0.
    elim: s =>//=.
    progress. 
    have Hnn: x1 <> x0  by smt.
    have Hnmm: ! mem l x1 by smt. 
    rewrite (H x1 Hnmm).
    rewrite /predI //=.
    smt.
    
  have ->: (predI p (mem (x :: L))) =
           (fun y, (p y /\ y = x) \/ (p y /\ mem L y)). 
    rewrite -cat1s /predI. 
    rewrite fun_ext /(==).
    move => y.
    rewrite !mem_cat. 
    smt. 

  have ->: (fun (y : 'a) => p y /\ y = x \/ p y /\ mem L y) =
           predU (predI p (transpose (=) x)) (predI p (mem L)). 
    by rewrite /predU /predI //=.
  rewrite Hc. 

  move => Hmemb HuS. 
  rewrite (Hcount S _ _ HuS); first 2 by smt.
  rewrite count_filter.
  have ->: (b2i (p x) + count (predI p (mem L)) S =
             count (predI p (transpose (=) x)) S + 
             count (predI p (mem L)) S -
             count (predI (predI p (transpose (=) x)) (predI p (mem L))) S) =
           (b2i (p x)  =
            count (predI p (transpose (=) x)) S  -
            count (predI (predI p (transpose (=) x)) (predI p (mem L))) S).
    by smt.
  have ->: count (predI (predI p (transpose (=) x)) (predI p (mem L))) S = 0.
  have ->: (predI (predI p (transpose (=) x)) (predI p (mem L))) = 
           (fun y, false).
    rewrite /predI //=.
    rewrite fun_ext /(==).
    move => y. smt.
    smt.
  simplify.
  have HmemS: mem S x by smt.

  
  have Hs: forall (s: 'a list),
      uniq s =>
      mem s x =>
      b2i (p x) = count (predI p (transpose (=) x)) s.
    elim =>//=. 
    progress. 
    case(x0 = x). 
    + move => Heq.
      have ->: x0 = x by smt.
      rewrite Hc''. smt.      
      rewrite /predI //=. 
    - move => Hneq.
      have ->: b2i (predI p (transpose (=) x) x0) = 0 by rewrite /predI //=; smt.
      have Hmm: mem l x by smt.
      by rewrite (H H1 Hmm).
  
  by rewrite (Hs S HuS HmemS).
qed.


module DBB_Exp_Keys (V: VotingScheme, A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id, hC, hNC, hN, fbb;
    BW.bad  <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.uLL  <- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ V(H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ V(H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }


    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), 
                    b.`2) 
               BW.check;


    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), 
                    b.`2) hNC;

   bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ r <> Count (hC ++ L ++ D));
    return ev /\ bool ;
  }
}.

local lemma dbb_dbbkeys &m:
  Pr[DBB_Exp (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DBB_Exp_Keys (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  wp.
  conseq (_: _ ==> ={BW.check, BW.hvote, BW.bb, BW.qCo, ev, bool, r})=>//=. 
  + progress. 
    cut := H.
    rewrite negb_exists //=.
    move => Hx.
    cut := Hx X.
    rewrite negb_exists //=.
    move => Hy.
    cut := Hy D.
    rewrite negb_exists //=.
    move => Hz.
    cut := Hz L.  
    by  rewrite ?negb_and.
  by sim. 
qed.


module DBB_Exp_tally (V: VotingScheme, A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id, hC, hNC, hN, r', pi', ev', fbb;
    BW.bad  <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ V(H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ V(H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ V(H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ V(H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    (* votes of honest voters that checked *)
    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2)  BW.check;
    (* votes of honest voters that didn't check *)
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), 
                    b.`2) hNC;

   bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ r <> Count (hC ++ L ++ D));
    return ev /\ bool ;
  }
}.

local lemma dbb_dbbtally &m:
  Pr[DBB_Exp_Keys (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] =
  Pr[DBB_Exp_tally (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  sim.
  seq 22 22: (={r, ev, bool, BW.check, BW.qCo, BW.hvote, BW.bb}/\ 
              bool{2}).
    call(: ={glob Vz, glob GRO.RO}); first by sim. 
    call(: true); first by sim. 
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, 
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check,BW.hk}); first 4 by sim.
    wp; 
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}); first 5 by sim.
    wp; while (={i, BW.uL, BW.uLL, glob SS, glob JRO.RO}); 
      first by sim.
    call (: ={glob E, glob HRO.RO}); first by sim.
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim.
    by auto.
  
  inline*; wp.
  call{2} (Vz_ll GRO.RO GRO.RO_o_ll).
  wp; call{2} (Pz_ll GRO.RO GRO.RO_o_ll).
  wp; while{2} (true) (size fbb0{2} -i0{2}). 
  + move => _ z.
    wp; call{2} (SSv_ll JRO.RO JRO.RO_o_ll).
    call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  by auto=>/>; smt (@Int @IntExtra). 
qed.

op preha = may_data. 

module DBB_Oracle_check(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc removeid = DBB_Oracle(V,H,G,J).removeid
  proc h        = DBB_Oracle(V,H,G,J).h
  proc g        = DBB_Oracle(V,H,G,J).g
  proc j        = DBB_Oracle(V,H,G,J).j
  proc corrupt  = DBB_Oracle(V,H,G,J).corrupt
  proc vote     = DBB_Oracle(V,H,G,J).vote

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL, ve;
    i <- 0;

    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.hk);
    (* verify all honest ballots *)
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      (* this checks hash membership *)
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), 
                                     BW.pbb, BW.hk);
      (* this checks hash input membership *)
      ve <- (remv h) \in 
            map open3 (Sanitize (map pack3 BW.bb));
      (* take the ballot asigned to id, and if voter has not checked before append *)
      if (id = h.`1 /\ vv /\ ve /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }else{
        if (vv /\ !ve){
            BW.bad <- vv /\ !ve;
            BH.l  <- preha (remv h);
            BH.r  <- nth witness (map (preha \o open3) (Sanitize (map pack3 BW.bb)))
                      (find (fun x, hash BW.hk BH.l = hash BW.hk x /\ BH.l <> x) 
                           (map (preha \o open3) (Sanitize (map pack3 BW.bb))));
                   
        }
      }

      i <- i +1;
    }
  }

}.


module DBB_Exp_check (V: VotingScheme, A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle_check(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id, hC, hNC, hN, r', pi', ev', fbb;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ V(H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ V(H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ V(H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ V(H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;

    bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ r <> Count (hC ++ L ++ D));
    return ev /\ bool;
  }
}.

local lemma dbbtally_dbbcheck &m:
  Pr[DBB_Exp_tally (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DBB_Exp_check (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] +
  Pr[DBB_Exp_check (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad].
proof.
  byequiv =>/>.
  proc.
  auto; progress. 
  conseq (: _ ==> ={ev, r, BW.hvote, BW.bb, BW.qCo}/\
                  (!BW.bad{2} => ={BW.check}))=>/>; first by smt.
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim. 
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim.   
  (* call A.a2 *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO,  
           BW.bb, BW.pk, BW.hvote, BW.uL,BW.hk}/\
          (!BW.bad{2} => ={BW.check})). 
  + proc. 
    while (={i, pL, id, BW.bb, BW.pbb, BW.uL, BW.pk,BW.hk, 
             glob HRO.RO}/\
           BW.pbb{2} = map (fun (x: ident*upkey * cipher * sign), 
                                (x, hash' BW.hk{2} x)) BW.bb{2}/\
           (!BW.bad{2} => ={BW.check})).  
      by inline*;auto; progress; smt.
    inline*; wp.
    by auto.

  + by proc.  
    by proc.
    by proc. 

  (* call A.a1 *)
  wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}); first 5 by sim.
  wp; while (={i, BW.uL, glob SS, glob JRO.RO, BW.uLL}); first by sim.
  call (: ={glob E, glob HRO.RO}); first by sim.
  call (: true ==> ={glob JRO.RO}); first by sim.
  call (: true ==> ={glob GRO.RO}); first by sim.
  call (: true ==> ={glob HRO.RO}); first by sim. 
  by auto. 
qed.

module DBB_Oracle_check'(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc removeid = DBB_Oracle(V,H,G,J).removeid
  proc h        = DBB_Oracle(V,H,G,J).h
  proc g        = DBB_Oracle(V,H,G,J).g
  proc j        = DBB_Oracle(V,H,G,J).j
  proc corrupt  = DBB_Oracle(V,H,G,J).corrupt
  proc vote     = DBB_Oracle(V,H,G,J).vote

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL, veh;
    i <- 0;

    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.hk);
    (* verify all honest ballots *)
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), BW.pbb, BW.hk);
      veh <- exists id, (get_id id, (remv h).`2,(remv h).`3, (remv h).`4) \in 
             map preha (USanitize BW.bb);

      if (id = h.`1 /\ vv /\ veh /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }else{
        if (!BW.bad){
            BW.bad <- vv /\ !veh ;
            BH.l  <- preha (remv h);
            BH.r  <- nth witness (map preha (USanitize BW.bb))
                      (find (fun (x: maybe_id*upkey * cipher * sign), 
                                 hash BW.hk BH.l = hash BW.hk x /\ 
                                 (BH.l.`2,BH.l.`3,BH.l.`4) <> (x.`2,x.`3,x.`4)) 
                           (map preha (USanitize BW.bb)));
                   
        }
      }
      i <- i +1;
    }
  }

}.


module DBB_Exp_check' (V: VotingScheme, A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle_check'(V,H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id, hC, hNC, hN, r', pi', ev', fbb;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ V(H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ V(H,G,J).register(id); 
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }
    
    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ V(H,G,J).publish(BW.bb, BW.hk);
    ev       <@ V(H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ V(H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ V(H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;

    bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ r <> Count (hC ++ L ++ D));
    return ev /\ bool;
  }
}.

local lemma dbbtally_dbbcheck_prime &m:
  Pr[DBB_Exp_tally (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DBB_Exp_check' (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] +
  Pr[DBB_Exp_check' (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad].
proof.
  byequiv =>/>.
  proc.
  auto; progress. 
  conseq (: _ ==> ={ev, r, BW.hvote, BW.bb, BW.qCo}/\
                  (!BW.bad{2} => ={BW.check}))=>/>; first by smt.
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim. 
  call(: ={glob Vz, glob GRO.RO}); first by sim. 
  call(: ={glob E, glob SS, glob Pz, glob HRO.RO, 
           glob GRO.RO, glob JRO.RO}); first by sim.   
  (* call A.a2 *)
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO,  
           BW.bb, BW.pk, BW.hvote, BW.uL,BW.hk}/\
          (!BW.bad{2} => ={BW.check})). 
  + proc.
    while (={i, pL, id, BW.bb, BW.pbb, BW.uL, BW.pk,BW.hk,
             glob HRO.RO}/\
           BW.pbb{2} = map (fun (x: ident*upkey * cipher * sign), 
                                  (x, hash' BW.hk{2} x)) BW.bb{2}/\
           (!BW.bad{2} => ={BW.check}) ).  
      inline*; auto; progress. 
      + smt. + smt. smt. smt.

    inline*; wp. 
    by auto.

  + by proc.  
    by proc.
    by proc. 

  (* call A.a1 *)
  wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}); first 5 by sim.
  wp; while (={i, BW.uL, glob SS, glob JRO.RO, BW.uLL}); first by sim.
  call (: ={glob E, glob HRO.RO}); first by sim.
  call (: true ==> ={glob JRO.RO}); first by sim.
  call (: true ==> ={glob GRO.RO}); first by sim.
  call (: true ==> ={glob HRO.RO}); first by sim.
  by auto. 
qed.


module BHash_Adv(V: VotingScheme, A: DBB_Adv,
                H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={
  module O = DBB_Oracle_check'(V,H,G,J)

  proc find(hk: hash_key): (maybe_id*upkey * cipher * sign) * 
               (maybe_id*upkey * cipher * sign) ={

    var r, pi, bool, i, id, fbb,hk';
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.uLL  <- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,hk') <@ V(H,G,J).setup();
    BW.hk <- hk;             
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ V(H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }
    
    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();

    return (BH.l, BH.r);
  }
}.


local lemma dbbcheck_dbbhash &m:
  Pr[DBB_Exp_check' (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad]<=
  Pr[Hash_Exp(BHash_Adv(B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  inline BHash_Adv(B(E, Pz, Vz, SS), A, HRO.RO, GRO.RO, JRO.RO).find.
  seq 20 23: ( ={BW.bad,BW.hk} /\ BW.hk{2} = hk{2} /\
               (BW.bad{2} => BH.l{2} <> BH.r{2} /\ 
                             hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})). 
    (* call A.a2 *)
    wp.
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.hk,
           BW.bb, BW.pk, BW.hvote, BW.uL, BW.bad, BW.check}/\
          (BW.bad{2} => (BH.l{2}.`2,BH.l{2}.`3,BH.l{2}.`4) <> 
                        (BH.r{2}.`2,BH.r{2}.`3,BH.r{2}.`4) /\ 
                         hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})).
    + proc. inline *.
      while ( ={ i, pL, id, BW.pbb, BW.pk, BW.bb, BW.check, BW.bad, BW.hk,
                glob HRO.RO}/\ 0<= i{2}/\
             BW.pbb{2} = map (fun (x: ident*upkey * cipher * sign), 
                                  (x, hash' BW.hk{2} x)) BW.bb{2}/\
             (BW.bad{2} => (BH.l{2}.`2,BH.l{2}.`3,BH.l{2}.`4) <> 
                           (BH.r{2}.`2,BH.r{2}.`3,BH.r{2}.`4) /\ 
                           hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})/\
             (!BW.bad{2}=> forall x, x \in (take i{2} pL{2}) =>
                            !((hash' BW.hk{2} (remv x) \in map (hash' BW.hk{2}) (USanitize BW.bb{2})) /\  
                              !(remi \o remv) x \in map remi (USanitize BW.bb{2})
                             ))
             ).
            
auto=>/>; progress. 
+ smt. 
+ move: H1 H3 H4 H5.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
  pose BB:= (USanitize BW.bb{2}).
  have ->: map (fun (p : pub_data * hash_out) => p.`1)
          (map (fun (x0 : ident * upkey * cipher * sign) =>
                    (x0, (\o) (hash BW.hk{2}) may_data x0)) BW.bb{2}) =
           BW.bb{2}
    by rewrite -?map_comp /(\o) map_id //=. 
  rewrite -/BB.
  move => H1 H3 H4 H5.
  move: H7; rewrite (take_nth witness); first by smt.
  rewrite -cats1 mem_cat //=; move => H7.
  case (x \in take i{2} pL{2}).
    move => Hmem.
    cut := H1 H6 x Hmem. 
    by done.
  move => Hnmem. 
  have Hx: x = nth witness pL{2} i{2} by smt.
  rewrite Hx -/b.
  rewrite negb_and H3 //=.
  move: H4.
  rewrite mapP.
  elim => y. simplify. 
  elim => Hmy [Hyeq1 Hyeq2 Hyeq3 Hyeq4]. 
  rewrite mapP.
  exists y. 
  by rewrite Hmy /remi //=.
+ smt. 
+ move: H1 H3 H4 H5 H6.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
   pose BB:= (USanitize BW.bb{2}).
  have ->: map (fun (p : pub_data * hash_out) => p.`1)
          (map (fun (x0 : ident * upkey * cipher * sign) =>
                    (x0, (\o) (hash BW.hk{2}) may_data x0)) BW.bb{2}) =
           BW.bb{2}.
    by rewrite -?map_comp /(\o) map_id //=. 
  rewrite -/BB.
  move => H1 H3 H4 H5 H6.
  pose c:= may_data (remv b).
  move: H6 H5; rewrite /(\o) -/c; move => H6.
  rewrite mapP. elim => y [HyBB Hcy].
  cut := nth_find witness (fun (x : maybe_id * upkey * cipher * sign) =>
                      hash BW.hk{2} c = hash BW.hk{2} x /\
                      ! (c.`2 = x.`2 && c.`3 = x.`3 && c.`4 = x.`4)) 
                         (map may_data BB) _. 
  + rewrite hasP.
    rewrite //=. 
    have Hy: may_data y \in map may_data BB.
      by rewrite mapP; exists y.
    exists (may_data y).
    rewrite Hy Hcy /may_data //=. 
    move: H6; rewrite negb_exists //=; move => H6.
    cut := H6 y.`1.
    rewrite mapP negb_exists //=. 
    smt. (* c and y mem BB *)   
  by done. 
+ move : H1 H3 H5 H6.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
  pose BB:= (USanitize BW.bb{2}).
  have ->: map (fun (p : pub_data * hash_out) => p.`1)
          (map (fun (x0 : ident * upkey * cipher * sign) =>
                    (x0, (\o) (hash BW.hk{2}) may_data x0)) BW.bb{2}) =
           BW.bb{2}.
    by rewrite -?map_comp /(\o) map_id //=. 
  rewrite -/BB. 
  move => H1 H3 H5 H6.
  pose c:= may_data (remv b).
  move: H6 H5; rewrite /(\o) -/c; move => H6.
  rewrite mapP. elim => y [HyBB Hcy].
  cut := nth_find witness (fun (x : maybe_id*upkey * cipher * sign) =>
                         hash BW.hk{2} c = hash BW.hk{2} x /\ ! (c.`2 = x.`2 && c.`3 = x.`3 && c.`4 = x.`4)) 
                         (map may_data BB) _. 
  + rewrite hasP.
    rewrite //=. 
    have Hy: may_data y \in map may_data BB.
      by rewrite mapP; exists y.
    exists (may_data y).
    rewrite Hy Hcy /may_data //=. 
    move: H6; rewrite negb_exists //=; move => H6.
    cut := H6 y.`1.
    rewrite mapP negb_exists //=. 
    move => Hcx.
    cut := Hcx (remv b).
    smt. (* c and y mem BB *)   
  by done. 

+ move : H1 H3 H5 H6.
  rewrite /hash' /preha.
  pose b := (nth witness pL{2} i{2}).
  have ->: (b.`1, b.`3, b.`4, b.`5) = remv b by rewrite /remv.
   pose BB:= (USanitize BW.bb{2}).
  have ->: map (fun (p : pub_data * hash_out) => p.`1)
          (map (fun (x0 : ident * upkey * cipher * sign) =>
                    (x0, (\o) (hash BW.hk{2}) may_data x0)) BW.bb{2}) =
           BW.bb{2}.
    by rewrite -?map_comp /(\o) map_id //=. 
  rewrite -/BB.
  move => H1 H3 H5 H6.
  move: H6; rewrite (take_nth witness); first by smt.
  rewrite -cats1 mem_cat //=; move => H6.
  case (x \in take i{2} pL{2}).
    move => Hmem.
    cut := H1 H4 x Hmem. 
    by done.
  move => Hnmem. 
  have Hx: x = nth witness pL{2} i{2} by smt.
  rewrite Hx -/b.
  case (! ((\o) (hash BW.hk{2}) may_data (remv b) \in map ((hash BW.hk{2}) \o may_data) BB)).
 + smt.
 + simplify. move => Hmem.
   move: H5; rewrite Hmem.
   simplify.
   elim => id.
   rewrite mapP.
   elim => y. rewrite /preha //=. elim => Hmy Heqy.
   rewrite mapP.
   exists y.
   rewrite Hmy //=. 
   smt.
+ smt. 

  auto; progress. smt (take0).

    + by proc.
    + by proc.
    + by proc.
    conseq (: _ ==> ={glob A, GRO.RO.m, HRO.RO.m, JRO.RO.m, (* , glob Pp, *)
                      BW.bb, BW.pk, BW.hvote, BW.uL, BW.bad, BW.check,
                      BW.hk}/\ BW.hk{2} =hk{2}/\
                    (BW.bad{2} => (BH.l{2}.`2,BH.l{2}.`3,BH.l{2}.`4) <> 
                                  (BH.r{2}.`2,BH.r{2}.`3,BH.r{2}.`4) /\
                                   hash BW.hk{2} BH.l{2} = hash BW.hk{2} BH.r{2})) =>//=.
    progress. smt. smt.
   
    (* call A.a1 *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo, (* glob Pp, *)
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo,BW.hk}); first 5 by sim.
    wp; while (={i, BW.uL, BW.uLL, glob SS, glob JRO.RO}); first by sim.
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd{2}. swap{1} 17 -16. 
    call (: ={glob HRO.RO}). 
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt.
  
  wp.
  inline *; wp.
  call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
  wp; call{1} (Pz_ll GRO.RO GRO.RO_o_ll).
  wp; while{1} (true) (size fbb0{1} -i0{1}); progress. 
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll).
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto; progress; smt.
  wp;  call{1} (Vz_ll GRO.RO GRO.RO_o_ll).  
  by auto; progress; smt. 
qed. 


(* .....................*)
local lemma ssd['a] (p1 p2 : 'a -> bool) (s : 'a list):
  perm_eq (filter p1 s ++ 
           filter (predI p2 (predC p1)) s ++ 
           filter (predI (predC p2) (predC p1)) s) s.
proof.
  cut := perm_filterC p2 (filter (predC p1) s).
  cut := perm_filterC p1 s.
  rewrite -?filter_predI. 
  pose a := filter p1 s.
  pose b:= filter (predC p1) s.
  pose b1:= filter (predI p2 (predC p1)) s.
  pose b2:=filter (predI (predC p2) (predC p1)) s.
  move => Hab Hb.
  smt (@List).
qed.


local lemma nth_vote (S: (ident * upkey * cipher * sign) list) 
               (L: (ident * vote * upkey * cipher * sign) list) f:
  (forall x, x\in S => x \in (map remv L))=>
  (forall x, x \in L => x.`2 =  (f\o remv) x      )=>
  map (fun (b : ident * upkey * cipher * sign),
           (nth witness L (index b (map remv L))).`2)
       S =
  map f S. 
proof.
  move => HmemSL Hxf; move: HmemSL.
  elim: S =>//=.
  move => x S Hp Hmem. 
  have : x \in map remv L. 
    smt. 
  rewrite mapP. 
  elim =>z Hz.
  split.
  + cut := Hxf z _; first by rewrite (andEl _ _ Hz).
    have ->: (nth witness L (index x (map remv L))) = z. 
    + cut := nth_index_map witness remv L z _ _. 
      + rewrite /remv; move => a b Ha Hb Hab. 
        cut := Hxf a Ha.
        cut := Hxf b Hb.
        rewrite /remv /(\o). 
        move => Hb2 Hba.
        by smt.
      + by rewrite (andEl _ _ Hz).
      by rewrite (andEr _ _ Hz).
    by rewrite (andEr _ _ Hz) /(\o).
  - cut := Hp _. 
      by smt.
    by done.
qed.   

local lemma filter_simplify (S: (ident * upkey * cipher * sign) list) 
                      (C H: (ident * vote * upkey * cipher * sign) list):

  filter (mem (map remv (filter (predC (mem (map fst5 C)) \o fst5) H))) S =
  filter (fun (x: ident * upkey * cipher * sign), 
              (mem (map remv H) x) /\ (!mem (map fst5 C) x.`1)) S.
proof.
  cut := filter_map remv  (fun (x: ident * upkey * cipher * sign),
                               predC (mem (map fst5 C)) x.`1) H .
  rewrite /preim //=.
  move => Hmm.
  have ->: (predC (mem (map fst5 C)) \o fst5) =
           (fun (x : ident * vote * upkey * cipher * sign) =>
                predC (mem (map fst5 C)) x.`1).
    by rewrite fun_ext /(==) /predC /fst5 /(\o). 
  rewrite -Hmm.


  have ->: (mem (filter (fun (x : ident * upkey * cipher * sign) =>
                             predC (mem (map fst5 C)) x.`1)
                        (map remv H))) =
           (fun (x : ident * upkey * cipher * sign) =>
                (x \in (map remv H) /\ ! (x.`1 \in map fst5 C))). 
    rewrite fun_ext /(==). 
    move => z. 
    rewrite /predC /fst5 //=. 
    smt.
  by done.
qed. 


local lemma Hsub_map['a 'b] (S L: 'a list) 
               (f: 'a -> 'b):
  subseq S L => subseq (map f S) (map f L).
proof. 
  elim: L S=>//=. smt.
  move=> x S Hp L. 
  case (L = []). smt.
  move => HL_ne.
  have ->: L = head witness L :: behead L by smt.
  case (x = head witness L).
  + move => Heq. 
    rewrite Heq //=.
    by cut := Hp (behead L).
  - move => Hneq.
    have ->: !x=head witness L by smt.
    case (!f x = f (head witness L)). smt. 
    simplify. 
    move => Hfeq.
    rewrite Hfeq //=. 
    smt.
qed.


local lemma eq_map_mem['a 'b] (f1 f2 : 'a -> 'b) (s: 'a list):
     (forall x, x \in s => f1 x = f2 x)
  =>  map f1 s = map f2 s.
proof. 
  elim: s=>//=.
  move => x s Hm Hor.
  smt.
qed.


local lemma nth_vote' (L: (ident * vote * upkey * cipher * sign) list) f:
  (forall x, x\in L => x.`2 = f (remv x))=>
  map (fun (b : ident * upkey * cipher * sign),
           (nth witness L (index b (map remv L))).`2)
      (map remv L) =
  map (fun (b : ident * vote * upkey * cipher * sign), 
                                  b.`2)  L. 
proof.
  move => Hmem_f. 
  rewrite -map_comp. 
  rewrite /(\o). 
  have Ho: forall x, x\in L =>
             (nth witness L (index (remv x) (map remv L))).`2 =
             x.`2.
    move => x Hmem_x.
    cut:= nth_index_map witness remv L x _ Hmem_x.
      move => a b Ha Hb. 
      rewrite /remv. 
      move => Hab. 
      cut := Hmem_f a Ha.
      cut := Hmem_f b Hb.
      move => Hb2 Hba.
      smt.
    by smt.
  by rewrite (eq_map_mem _ _ L Ho).
qed.   

local lemma uniq_filter_fst (L S: (ident * (upkey * cipher * sign)) list):
  let fst4= fun (x: ident * (upkey * cipher * sign)), x.`1 in
  uniq L =>
  (forall x, x\in S /\ fst4 x \in (map fst4 L) => x \in L)=>
  filter (mem L) S =
  filter ((mem (map fst4 L))\o fst4) S.
proof.
  move => fst4  HuL HmLS.
  cut := eq_in_filter (mem L) (mem (map fst4 L) \o fst4) S _ . 
  + move => x Hx_S.
    progress.
    + rewrite /(\o) /fst4 mapP.
      exists x.     
      by rewrite H.     
    - move : H.
      rewrite /(\o) mapP.
      elim => y Hmem_y.
      cut Hl:= andEl _ _ Hmem_y. 
      have Hr: x.`1 = y.`1 by move: Hmem_y; rewrite /fst //=. 
      cut := HmLS x _. 
        rewrite Hx_S (andEr _ _ Hmem_y). 
        rewrite mapP. 
        by exists y; rewrite Hl //=.
      by done.
  by done.
qed.


module DBB_Exp_dish (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle_check'(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id,  hCN, r', 
        pi', ev', vbb, b,m,e, fbb, j, c,
        dL, hon_mem, val_bb, hC, hNC, hN;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (remi \o remv) BW.hvote));
    val_bb  <- map remi vbb; (* (map remi (USanitize vbb)); *)
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    (* size of dishonest valid ballots *)
    BW.ball <- size dL <= BW.qCo;

    bool <- (forall (X D L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/
         ! size D <= BW.qCo \/ 
         r <> Count (hC ++ L ++ D));

    return ev /\ bool /\ BW.ball;
  }
}.



local lemma dbb_dish &m:
  Pr[DBB_Exp_check' (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DBB_Exp_dish(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res]+
  Pr[DBB_Exp_dish(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : !BW.ball].
proof.
  byequiv =>/>.
  proc.
  conseq (: ={ev, bool})=>//.
  swap{2} 41 -12. 
  seq 28 29 : ( ={ev, bool}); first by sim. 
  wp; while{2} (true) ( size(filter (predC hon_mem) val_bb){2} - j{2}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).    
    by auto=>/>; smt.
  wp;while{2} (true) (size (filter hon_mem val_bb){2} -j{2}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).    
    by auto=>/>; smt.
  wp; while{2} (true) (size BW.bb{2} - i{2}); progress.
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll);
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).    
    by auto=>/>; smt. 
  by auto=>/>; smt.
qed.

module DBB_Exp_ball (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle_check'(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var bool, i, id, vbb, b,m,e, fbb,
        hon_mem, val_bb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb)); 

    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (remi \o remv) BW.hvote));
    val_bb  <- map remi vbb; 

    BW.ball <- BW.qCo < 
               size (filter ((predC hon_mem) \o remi) vbb);
    return BW.ball;
  }
}.

local lemma dbbdish_dbbball &m:
  Pr[DBB_Exp_dish(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : !BW.ball] =
  Pr[DBB_Exp_ball(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  byequiv =>/>.  
  proc.
  seq 31 23 : (={BW.qCo,vbb, BW.sk, BW.bb, BW.hvote, glob E, glob HRO.RO, glob SS, glob JRO.RO} /\
               let f = fun (x: ident * upkey * cipher * sign),
                       dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} 
               in vbb{2} = filter f BW.bb{2}). 
    seq 20 20 : ( ={BW.qCo, BW.sk, BW.bb, BW.hvote, glob E, glob SS, 
                    HRO.RO.m, JRO.RO.m}). 
      by sim.
    seq 3 0: (={BW.qCo,BW.sk, BW.bb, BW.hvote, glob E, glob SS, HRO.RO.m, JRO.RO.m}). 
      inline *; wp.   
      call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
      wp; call{1} (Aa2_ll (<:DBB_Exp_dish(E, Pz, Vz, SS, A, 
                             HRO.RO, GRO.RO, JRO.RO).O) _ 
                          HRO.RO_o_ll GRO.RO_o_ll JRO.RO_o_ll).      
        proc.
        inline *; wp.
        while{1} (true) (size pL - i); progress.
          auto=>/>; smt.
        by auto=>/>; smt.
      by auto.
    seq 2 0: (={BW.qCo, BW.sk, BW.bb, BW.hvote, glob E, glob SS, HRO.RO.m, JRO.RO.m}).
      inline *; wp.
      call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
      wp; call{1} (Pz_ll GRO.RO GRO.RO_o_ll).
      wp; sp.
      exists * (glob E){1}, (glob SS){1}; elim*=> ge gs.
      while{1} (ge = (glob E){1} /\ gs = (glob SS){1}) 
               (size fbb0{1} - i0{1}); progress.
        wp; sp.
        exists * b0, sk; elim* => x sk.
        call{1} (ver_Ver_one gs x.`1 x.`2 x.`3).
        call{1} (dec_Dec_one ge sk x.`1 x.`2).   
        by auto =>/>; smt.    
      by auto=>/>; smt. 
     while (={i, vbb, BW.bb, BW.sk, glob E, glob SS, HRO.RO.m, JRO.RO.m}/\
         0<=i{2} <= size BW.bb{2}/\
         let f = fun (x: ident * upkey * cipher * sign),
                     dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                      ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).        
      wp; sp. 
      exists* BW.sk{1}, b{1}; elim* => skx bx. 
      call{1} (ver_Ver_two bx.`2 bx.`3 bx.`4).
      call{1} (dec_Dec_two skx bx.`2 bx.`3).
      auto=>/>; progress.
      + smt. smt. smt. smt. smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite filter_rcons H2.   
     by auto=>/>; smt. 

  wp; while{1} ( size dL{1} = size (take j{1} (filter (predC hon_mem{1}) val_bb{1})) /\
                 0 <= j{1})
               (size (filter (predC hon_mem{1}) val_bb{1}) - j{1}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).               
    auto=>/>; smt.
  wp; while{1} (true) (size (filter hon_mem{1} val_bb{1}) - j{1}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    auto=>/>; smt.
  auto=>/>; progress.
  + smt. 
  + by rewrite take0. 
  + smt. 
  + move: H3; rewrite H1.
    rewrite take_oversize; first by smt.
    rewrite filter_map. 
    have ->: (preim remi (predC (mem (map (remi \o remv) BW.hvote{2})))) = 
             (predC (mem (map (remi \o remv) BW.hvote{2})) \o remi).
      by rewrite /preim.
    rewrite size_map. 
    pose f:= (fun (x : ident * upkey * cipher * sign) =>
           dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
           ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}). 
    smt.
  + move: H3; rewrite H1. 
    rewrite take_oversize; first by smt.
    rewrite !filter_map. 
    have ->: (preim remi (predC (mem (map (remi \o remv) BW.hvote{2})))) = 
             (predC (mem (map (remi \o remv) BW.hvote{2})) \o remi).
      by rewrite /preim.
    rewrite size_map.
    smt. 
qed.  

module DBB_Exp_ball2 (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle_check'(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var bool, i, id, vbb, b,m,e, fbb,
        hon_mem, val_bb, c, co_bb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BU.keyL <- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));

    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (remi \o remv) BW.hvote));
    val_bb  <- (map remi vbb);
    co_bb  <- (filter ((predC hon_mem) \o remi) vbb);
  
    (* find an element for which the id has not been corrupted,
            and the upkey (assigned to same/or different id) has also not been corrupted *)
    c <- nth witness co_bb
             (find (fun (x: ident * upkey * cipher * sign),
                        !(nth witness BW.uLL (index x.`2 (map thr3 BW.uLL))).`1 
                               \in BW.coL)
                   co_bb);
    BW.ball <- !(remi c \in map (remi \o remv) BW.hvote)/\
               (let o = nth witness BW.uLL (index c.`2 (map thr3 BW.uLL)) in
                 !o.`1 \in  BW.coL)/\
               c.`2 \in (map thr3 BW.uLL)/\
               (find (fun (x: ident * upkey * cipher * sign),
                        !(nth witness BW.uLL (index x.`2 (map thr3 BW.uLL))).`1 
                               \in BW.coL)
                   co_bb) < size co_bb;
    return BW.ball;
  }
}.

local lemma uniq_size_has['a] (L1 L2 :'a list):
  uniq L2 =>
  size L1 < size L2 => has (predC (mem L1)) L2.
proof.
  move => HuL HsL.
  elim : L2 L1 HuL HsL =>//=. 
  smt.
  move => x L2 HL1 L1 Hand Hs.
  case (!x \in L1). smt.
  + simplify. 
    move => HxL1.
    have ->: !predC (mem L1) x by rewrite /predC.
    simplify.   
    have := cat_take_drop (index x L1) L1.
    rewrite (drop_nth x). smt. 
    rewrite -cat1s.  
    have ->: nth x L1 (index x L1) = x. smt.
    pose A:= take (index x L1) L1.
    pose B:= drop (index x L1 + 1) L1.
    move => HdL1.
    move : Hs.
    have ->: size L1 = 1 + size (A++B). smt.
    move => Hs.
    have HABL: size (A++B) < size L2. smt.
    cut := HL1 (A++B) _ HABL. smt.
    rewrite !hasP. 
    elim => y [HyL2 HyL1].
    exists y; rewrite HyL2 //= -HdL1 //=.
    rewrite /predC -cat1s !mem_cat //=.
    move: HyL1; rewrite /predC mem_cat. smt.
qed. 

local lemma uniq_size_has_2 (L1 : ident list) (L2 : (ident * upkey * cipher * sign) list) 
                    (f: ident * upkey * cipher * sign -> ident):
  uniq (map f L2) =>
  size L1 < size L2 => has (predC (mem L1) \o f) L2.
proof.
  move => HuL HsL.
  elim : L2 L1 HuL HsL =>//=. 
  smt.
  move => x L2 HL1 L1 Hand Hs. 
  case (!f x \in L1). 
  + rewrite /(\o) /predC //=. 
    smt. 
  + simplify. 
    move => HxL1.
    have ->: !(\o) (predC (mem L1)) f x by rewrite /predC /(\o).
    simplify.   
    have := cat_take_drop (index (f x) L1) L1.
    rewrite (drop_nth (f x)). smt. 
    rewrite -cat1s.  
    have ->: nth (f x) L1 (index (f x) L1) = (f x). smt.
    pose A:= take (index (f x) L1) L1.
    pose B:= drop (index (f x) L1 + 1) L1.
    move => HdL1.
    move : Hs.
    have ->: size L1 = 1 + size (A++B). smt.
    move => Hs.
    have HABL: size (A++B) < size L2. smt.
    cut := HL1 (A++B) _ HABL. smt.
    rewrite !hasP. 
    elim => y [HyL2 HyL1].
    exists y; rewrite HyL2 //= -HdL1 //=.
    rewrite /predC -cat1s /(\o) ?mem_cat //=.
    move: HyL1; rewrite /predC /(\o) mem_cat. 
    rewrite !negb_or.
    elim => Hl Hr.
    rewrite Hl Hr //=.
    
    have Hx: !(f x \in map f L2). smt.
    have Hy:  (f y \in map f L2). 
      by rewrite (map_f f L2 y HyL2).
    smt.
qed. 


local lemma uniq_filter['a 'b] (L: 'a list) (f:'a -> 'b) p:
  uniq (map f L) =>
  uniq (map f (filter p L)).
proof.
  elim: L =>//=.
  move=> x L HL Hf.
  case (p x). smt.
  smt.
qed.

local lemma uniq_mem_map['a 'b] (A: 'a list) a b (f:'a -> 'b):
  uniq (map f A) => a \in A => b \in A => f a = f b =>
  a = b.
proof.
  elim: A =>//=. 
  move => x A HuA HaA. smt.
qed. 

local lemma uniq_nomem_map['a 'b] (A: 'a list) a b (f:'a -> 'b):
  uniq (map f A) => a \in A => b \in A => a <> b => f a <> f b.
proof.
  elim: A =>//=. 
  move => x A HuA HaA. smt.
qed. 

local lemma ddd (L: (ident * upkey * cipher * sign) list) 
          (S: (ident * uskey * upkey) list):
 let f= fun (x : ident * upkey * cipher * sign) =>
           (nth witness S (index x.`2 (map thr3 S))).`1 in
 let g= fun (x : ident * upkey * cipher * sign) => x.`2 in
 (* L = Usanitize of upk *)
 uniq (map (fun (x: ident * uskey * upkey), x.`1) S)=>
 (forall x, x \in map g L =>
            x \in map thr3 S)=>
 uniq (map g L) =>
 uniq (map f L).
proof.
  move => f g HuS HLS HuL.  
  pose f':= fun (x : upkey) =>
           (nth witness S (index x (map thr3 S))).`1.
  have ->: f = f'\o g by rewrite /(\o) /f /g /f'.
  rewrite map_comp. 
  pose L':= map g L.
  move: HLS HuL; rewrite -/L'; move => HLS HuL.
  have HH: forall (x y : upkey), x \in L' => y \in L' => f' x = f' y => x = y.
  + move => x y Hx Hy Hxy. 
    move: Hxy. rewrite /f'. 
    have Hsx: (index x (map thr3 S)) < size (map thr3 S). smt.
    have Hsy: (index y (map thr3 S)) < size (map thr3 S). smt.
    have HiS: forall a b, a\in S => b \in S => a.`1 = b.`1 => a = b. smt.
    move => Hxy.
    have Hxy': nth witness S (index x (map thr3 S)) =
               nth witness S (index y (map thr3 S)). smt.
    have Hxx: exists (id:ident) (z: uskey), 
              nth witness S (index x (map thr3 S)) = (id,z,x). smt.
    have Hyy: exists (id:ident) (z: uskey), 
              nth witness S (index y (map thr3 S)) = (id,z,y). smt.
    smt.
  by rewrite (map_inj_in_uniq  f' L' HH) HuL.
qed.

local lemma ddd'['a] (L: (upkey * 'a) list) 
                (S: (ident * uskey * upkey) list):

 let f= fun (x : upkey) =>
            (nth witness S (index x (map thr3 S))).`1 in
 (* L = Usanitize of upk *)
 uniq (map (fun (x: ident * uskey * upkey), x.`1) S)=>
 (forall x, x \in map fst L =>  x \in map thr3 S)=>
 uniq (map fst L) => uniq (map f (map fst L)).
proof.
  move => f HuS.
  pose L':= map fst L.
  move => HLS HuL. 
  have HH: forall (x y : upkey), x \in L' => y \in L' => f x = f y => x = y.
  + move => x y Hx Hy Hxy. 
    move: Hxy. rewrite /f. 
    have Hsx: (index x (map thr3 S)) < size (map thr3 S). smt.
    have Hsy: (index y (map thr3 S)) < size (map thr3 S). smt.
    have HiS: forall a b, a\in S => b \in S => a.`1 = b.`1 => a = b. smt.
    move => Hxy.
    have Hxy': nth witness S (index x (map thr3 S)) =
               nth witness S (index y (map thr3 S)). smt.
    have Hxx: exists (id:ident) (z: uskey), 
              nth witness S (index x (map thr3 S)) = (id,z,x). smt.
    have Hyy: exists (id:ident) (z: uskey), 
              nth witness S (index y (map thr3 S)) = (id,z,y). smt.
    smt.
  by rewrite (map_inj_in_uniq f L' HH) HuL.
qed.

local lemma take_drop_uniq' (L :'a list) x i:
   uniq L =>
   x \in take (i+1) L =>
     !x \in drop (i+1) L.
proof.
  cut HL:= cat_take_drop (i+1) L.
  move=> HuL; rewrite -HL in HuL.
  move: HuL. rewrite cat_uniq. 
  elim => Hut [Hhas Hud].
  move: Hhas; rewrite hasPn; move => Hhas. smt.
qed. 

op rem_id = remi.

local lemma dbbball_two &m:
  Pr[DBB_Exp_ball(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DBB_Exp_ball2(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  byequiv =>/>.
  proc. 
  wp. progress.
  conseq (: ={BW.coL, vbb, BW.hvote} /\
            BW.qCo{1} = size BW.coL{1} /\
            uniq (map fst3 BW.uLL{2})/\
            (forall x, x \in map (fun (y : ident * upkey * cipher * sign), y.`2) vbb{2} =>
                      x \in map thr3 BW.uLL{2})/\
            uniq (map (fun (x : ident * upkey * cipher * sign), x.`2) vbb{2})).
  progress.
  + pose L:= map (rem_id \o remv) hvote_R.
    pose S:= vbb_R.
    pose i:= (find (fun (x : ident * upkey * cipher * sign) =>
              ! ((nth witness uLL_R (index x.`2 (map thr3 uLL_R))).`1 \in
                 coL_R)) (filter (predC (mem L) \o rem_id) S)).
    pose x:= (nth witness (filter (predC (mem L) \o rem_id) S) i).          
    have Hi: i < size (filter (predC (mem L) \o rem_id) S).          
      rewrite /i -/S. search find map.
      rewrite -has_find.
      move: H2. rewrite -/S -/L. 
      pose A:= (filter (predC (mem L) \o rem_id) S).
      have ->: (fun (x0 : ident * upkey * cipher * sign) =>
               ! ((nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1 \in coL_R)) =
              (predC (mem coL_R) \o (fun (x0 : ident * upkey * cipher * sign) =>
                                         (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1)).
        rewrite /(\o) //=. 
      pose f:= fun (x0 : ident * upkey * cipher * sign) =>
                   (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1. 
      move => Hs.
      cut := uniq_size_has_2 coL_R A f _ Hs.
      + rewrite /A uniq_filter. 
        rewrite /S.
        have ->: f = (fun (x0 : upkey) =>
                       (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1) \o 
                     (fun (x : ident * upkey * cipher * sign) => x.`2)
          by rewrite /(\o).          
        rewrite ?map_comp. 
        pose fo:= (fun (x0 : upkey) => (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1). 
        cut Ho:= San_uniq_fst (map upack vbb_R). 
        cut Hu:= ddd' (map upack vbb_R) uLL_R H _ _. (* use H0 *)
        + move => a Ha.
          cut := H0 a _.
            move: Ha. 
            by rewrite -map_comp /upack /(\o).
          by done.
        + by rewrite -map_comp /upack /(\o).
        by move: Hu; rewrite /fo -?map_comp /upack /(\o). 
      by done. 
    cut := mem_nth witness (filter (predC (mem L) \o rem_id) S) i _.
      by rewrite Hi /i find_ge0.
    rewrite -/x.
    rewrite (mem_filter (predC (mem L) \o rem_id) x S).
    by rewrite /predC //=.
  + pose L:= map (rem_id \o remv) hvote_R.
    pose S:= vbb_R.
    pose i:= (find (fun (x : ident * upkey * cipher * sign) =>
              ! ((nth witness uLL_R (index x.`2 (map thr3 uLL_R))).`1 \in
                 coL_R)) (filter (predC (mem L) \o rem_id) S)).
    pose x:= (nth witness (filter (predC (mem L) \o rem_id) S) i).
    have Hi: i < size (filter (predC (mem L) \o rem_id) S).          
      rewrite /i -/S. search find map.
      rewrite -has_find.
      move: H2. rewrite -/S -/L. 
      pose A:= (filter (predC (mem L) \o remi) S).
      have ->: (fun (x0 : ident * upkey * cipher * sign) =>
               ! ((nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1 \in coL_R)) =
              (predC (mem coL_R) \o (fun (x0 : ident * upkey * cipher * sign) =>
                                         (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1)).
        rewrite /(\o) //=. 
      pose f:= fun (x0 : ident * upkey * cipher * sign) =>
                   (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1. 
      move => Hs. 
      cut := uniq_size_has_2 coL_R A f _ Hs.
      + rewrite /A uniq_filter. 
        rewrite /S.
        have ->: f = (fun (x0 : upkey) =>
                       (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1) \o 
                     (fun (x : ident * upkey * cipher * sign) => x.`2)
          by rewrite /(\o).          
        rewrite ?map_comp. 
        pose fo:= (fun (x0 : upkey) => (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1). 
        cut Ho:= San_uniq_fst (map upack vbb_R). 
        cut Hu:= ddd' (map upack vbb_R) uLL_R H _ _. (* use H0 *)
        + move => a Ha.
          cut := H0 a _.
            move: Ha. 
            by rewrite -map_comp /upack /(\o).
          by done.
        + by rewrite -map_comp /upack /(\o).
        by move: Hu; rewrite /fo -?map_comp /upack /(\o). 
      by done. 

      move: Hi; rewrite /i. smt. 
  + pose L:= map (rem_id \o remv) hvote_R.
    pose S:= vbb_R. 
    pose i:= (find (fun (x : ident * upkey * cipher * sign) =>
              ! ((nth witness uLL_R (index x.`2 (map thr3 uLL_R))).`1 \in
                 coL_R)) (filter (predC (mem L) \o rem_id) S)). 
    pose x:= (nth witness (filter (predC (mem L) \o rem_id) S) i).
    have Hi: i < size (filter (predC (mem L) \o rem_id) S).          
      rewrite /i -/S. search find map.
      rewrite -has_find.
      move: H2. rewrite -/S -/L. 
      pose A:= (filter (predC (mem L) \o rem_id) S).
      have ->: (fun (x0 : ident * upkey * cipher * sign) =>
               ! ((nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1 \in coL_R)) =
              (predC (mem coL_R) \o (fun (x0 : ident * upkey * cipher * sign) =>
                                         (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1)).
        rewrite /(\o) //=. 
      pose f:= fun (x0 : ident * upkey * cipher * sign) =>
                   (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1. 
      move => Hs.
      cut := uniq_size_has_2 coL_R A f _ Hs.
      + rewrite /A uniq_filter. 
        rewrite /S.
        have ->: f = (fun (x0 : upkey) =>
                       (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1) \o 
                     (fun (x : ident * upkey * cipher * sign) => x.`2)
          by rewrite /(\o).          
        rewrite ?map_comp. 
        pose fo:= (fun (x0 : upkey) => (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1). 
        cut Ho:= San_uniq_fst (map upack vbb_R). 
        cut Hu:= ddd' (map upack vbb_R) uLL_R H _ _. (* use H0 *)
        + move => a Ha.
          cut := H0 a _.
            move: Ha. 
            by rewrite -map_comp /upack /(\o).
          by done.
        + by rewrite -map_comp /upack /(\o).
        by move: Hu; rewrite /fo -?map_comp /upack /(\o). 
      by done. 
      move: Hi; rewrite /i //=.
      pose p:= (fun (x0 : ident * upkey * cipher * sign) =>
                ! ((nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1 \in coL_R)).
      pose S':= (filter (predC (mem L) \o rem_id) S).
      move => Hfs.
      have Hx: x \in S'.
        rewrite /x /i -/S' -/p. smt.
      have Hx2: x \in vbb_R. 
        move: Hx; rewrite /S' mem_filter /S /USanitize. elim => Hfx. 
        by done.
      cut Hxu:= H0 x.`2 _. 
        by rewrite mapP; exists x; rewrite Hx2 //=.  
      by rewrite Hxu.
  + pose L:= map (rem_id \o remv) hvote_R.
    pose S:= vbb_R.
    pose i:= (find (fun (x : ident * upkey * cipher * sign) =>
              ! ((nth witness uLL_R (index x.`2 (map thr3 uLL_R))).`1 \in
                 coL_R)) (filter (predC (mem L) \o rem_id) S)).         
    rewrite /i -/S. 
    rewrite -has_find.
      move: H2. rewrite -/S -/L. 
      pose A:= (filter (predC (mem L) \o rem_id) S).
      have ->: (fun (x0 : ident * upkey * cipher * sign) =>
               ! ((nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1 \in coL_R)) =
              (predC (mem coL_R) \o (fun (x0 : ident * upkey * cipher * sign) =>
                                         (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1)).
        rewrite /(\o) //=. 
      pose f:= fun (x0 : ident * upkey * cipher * sign) =>
                   (nth witness uLL_R (index x0.`2 (map thr3 uLL_R))).`1. 
      move => Hs.
      cut := uniq_size_has_2 coL_R A f _ Hs.
      + rewrite /A uniq_filter. 
        rewrite /S.
        have ->: f = (fun (x0 : upkey) =>
                       (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1) \o 
                     (fun (x : ident * upkey * cipher * sign) => x.`2)
          by rewrite /(\o).          
        rewrite ?map_comp. 
        pose fo:= (fun (x0 : upkey) => (nth witness uLL_R (index x0 (map thr3 uLL_R))).`1). 
        cut Ho:= San_uniq_fst (map upack vbb_R). 
        cut Hu:= ddd' (map upack vbb_R) uLL_R H _ _. (* use H0 *)
        + move => a Ha.
          cut := H0 a _.
            move: Ha. 
            by rewrite -map_comp /upack /(\o).
          by done.
        + by rewrite -map_comp /upack /(\o).
        by move: Hu; rewrite /fo -?map_comp /upack /(\o). 
      by done. 

  while (={i, vbb, BW.bb, BW.sk, glob E, glob SS, HRO.RO.m, JRO.RO.m}/\
         0<=i{2} <= size BW.bb{2}/\
         let f = fun (x: ident * upkey * cipher * sign),
                     dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                      ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).        
    wp; sp. 
    exists* BW.sk{1}, b{1}; elim* => skx bx. 
    call{1} (ver_Ver_two bx.`2 bx.`3 bx.`4).
    call{1} (dec_Dec_two skx bx.`2 bx.`3).
    auto=>/>; progress.
    + smt. smt. smt. smt. smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons H2.      
  wp.   
  call (: ={glob SS, glob E, glob JRO.RO, glob HRO.RO, glob GRO.RO, 
            BW.coL, BW.hvote, BW.qCo, BW.uL, BW.qVo, BW.pk}/\
          BW.qCo{1} = size BW.coL{1} ).
  + proc.          
    auto=>/>; progress. inline *. 
    sp; if=>//=.
    wp; while (={i, hvo, hvo', id0}); first by sim.
    by auto=>/>; smt.
  + proc.
    sp; if =>//=. 
    inline*; wp. 
    call(: ={glob JRO.RO}); first by sim.
    call(: ={glob HRO.RO}); first by sim.  
    auto=>/>; progress.
  + by proc. by proc. by proc.
  while (={i, BW.uL, glob SS, BW.uLL}/\
         0<= i{2} /\
         uniq (map fst3 BW.uLL{2})/\
         (* (forall e, e\in BW.uLL{2} => BW.uL{1}.[e.`1] = Some (e.`3,e.`2))/\ *)
         (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BW.uLL{1}))). 
  + inline *; wp.
    call(: true); wp.
    auto =>/>; progress. 
    + smt.
    + rewrite get_set_eq oget_some //=  map_cat cat_uniq H0 //= /fst3 //=.
      cut := H1 (nth witness (undup Voters) i{2}) _. 
      + rewrite (drop_nth witness i{2}); first by smt.
        by rewrite mem_head. 
    + by rewrite -/fst3.
    + rewrite map_cat mem_cat /fst3 //= -/fst3. 
      have Hxm: x \in drop i{2} (undup Voters). 
         rewrite (drop_nth witness i{2}). smt.    
         smt. 
       cut Hxn:= H1 x Hxm.
       rewrite Hxn//=. 
       cut := take_drop_uniq' (undup Voters) (nth witness (undup Voters) i{2}) i{2} _ _.  
       + smt.       
       + smt.
       smt.

  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
  wp; rnd; call(: true).
  call(: true ==> ={glob JRO.RO}); first by sim.
  call(: true ==> ={glob GRO.RO}); first by sim.
  call(: true ==> ={glob HRO.RO}); first by sim.
  auto =>/>; progress.
  + by rewrite size_ge0. 
    by rewrite take0. 
  + move: H9; rewrite take_oversize. smt.
    rewrite mapP. elim => y. rewrite mem_filter.
    elim => [[Hig Hmust] Hxy].
    move: Hmust; rewrite mem_filter. smt.
  + rewrite take_oversize; first by smt. 
    rewrite -filter_predI.
    pose f:= (predI
           (fun (x : ident * upkey * cipher * sign) =>
              dec_cipher result_R.`2 x.`2 x.`3 m_R <> None /\
              ver_sign x.`2 x.`3 x.`4 m_R1)
           (mem (map thr3 uLL_R) \o two4)).
    rewrite uniq_filter /USanitize. 
    by rewrite -map_comp /uopen /(\o) //= San_uniq_fst.
qed.

module BMEUF (E: Scheme, 
              H:HOracle.Oracle, G:GOracle.Oracle, 
              A: DBB_Adv, SS: SignScheme,
              O: MEUF_Oracle) ={

  module DO ={
    proc h        = H.o
    proc g        = G.o
    proc j        = O.o
    proc check (id: ident): unit ={ }
    proc corrupt(id: ident): uskey option={
      var usk;
      usk <@ O.corrupt(id);
      if (usk <> None /\ ! (id \in BW.coL)){
        BW.hvote <- filter (predC1 id \o fst5) BW.hvote;
        BW.coL <- BW.coL ++ [id];
      }
      return usk;
    }

    proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
      var o, c, s, e;
      o <- None;
      if (id \in map fst BW.iu /\ !id \in BW.coL /\ BW.qVo <qVo){
        e <- nth (ft3 witness) BW.iu (index id (map fst BW.iu));
        c <@ E(H).enc(BW.pk, e.`2, v);
        s <@ O.sign(id,c);
        if (s <> None){
          o <- Some (id, e.`2, c, oget s);
          BW.hvote <- BW.hvote ++ [(id,v, e.`2,c,oget s)]; 
        }
        BW.qVo <- BW.qVo + 1; 

      }
      return o;
    }

  } (* end DO *)

  proc main(ul: (ident * upkey) list): upkey * cipher * sign ={
    var fbb, i, vbb, m, b, e, hon_mem, val_bb, co_bb, c;

    BW.coL <- [];
    BW.qVo <- 0;
    BW.hvote <- [];  
    BW.iu    <- ul;
    (BW.pk,BW.sk) <@ E(H).kgen();
    BW.hk <$ dhkey_out;
    H.init();
    G.init();
    fbb    <@ A(DO).a1(BW.sk);
    BW.bb  <- (filter ((mem (map snd ul))\o two4) (USanitize fbb));

    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(O).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id vbb);
    co_bb  <- (filter ((predC hon_mem) \o rem_id) vbb);
    c <- nth witness co_bb
             (find (fun (x: ident * upkey * cipher * sign),
                        !(nth (ft3 witness) ul (index x.`2 (map snd ul))).`1 
                               \in BW.coL)
                   co_bb);
    return (rem_id c);
  }

}.

local lemma dbbballtwo_meuf &m:
  Pr[DBB_Exp_ball2(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <= 
  Pr[MEUF_Exp(SS,BMEUF(E, HRO.RO, GRO.RO,A,SS),JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc. 
  inline BMEUF(E, HRO.RO, GRO.RO, A, SS, MEUF_Exp(SS, BMEUF(E, HRO.RO, GRO.RO, A,SS), JRO.RO).OO).main. 
  seq 24 22 : ( ={vbb, JRO.RO.m, BW.hvote, BW.coL} /\
                BW.uLL{1} = BU.keyL{2} /\
                BW.coL{1} = map fst3 BU.corL{2} /\
               uniq (map fst3 BU.keyL{2}) /\
               ul{2} = map ft3 BU.keyL{2}/\
                (* map (rem_id \o remv) BW.hvote{1} = map rem_id BU.sigL{2} /\*)
                (forall x, 
                  x \in map rem_id BU.sigL{2}
                  => x \in map (rem_id \o remv) BW.hvote{1}) /\  
                (forall x, 
                  x \in BU.sigL{2} 
                  => x \in map (remv) BW.hvote{1}) /\
                (forall x, x \in vbb{1} => ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2})). 
  while (={vbb, glob E, glob SS, HRO.RO.m, JRO.RO.m, BW.sk, BW.bb}/\
         0<= i{1} /\
         i{1} = i0{2} /\
         (forall x, x \in vbb{1} => ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2})).
    wp; sp.
    seq 1 1: (={b, vbb, glob E, glob SS, HRO.RO.m, JRO.RO.m, BW.bb, BW.sk} /\
              0 <= i{1} /\  i{1} = i0{2} /\
             (forall x, x \in vbb{1} => ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
             i{1} < size BW.bb{1} /\ i0{2} < size BW.bb{2} /\ 
             b{2} = nth witness BW.bb{2} i0{2}/\
             m{1} = m0{2}).
      by call(: ={glob HRO.RO}); first by sim.   
      exists* (glob SS){1}, b{1}; elim* => gs x. 
      call{1} (ver_Ver_one gs x.`2 x.`3 x.`4).
      call{2} (ver_Ver_one' gs x.`2 x.`3 x.`4).
      auto=>/>; progress.  
      + smt. 
      + rewrite mem_cat mem_seq1 //= in H4. 
        smt.
      + smt.
  wp; progress. 
  call (: ={glob E, glob SS, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.coL, 
            BW.qVo, BW.pk, BW.hvote}/\
          BW.uLL{1} = BU.keyL{2} /\
          BW.coL{1} = map fst3 BU.corL{2} /\
          BW.iu{2} = map ft3 BU.keyL{2}/\ 
          (forall x, 
                  x \in map rem_id BU.sigL{2} 
                  => x \in map (rem_id \o remv) BW.hvote{1}) /\  
          (forall x, 
                  x \in BU.sigL{2} 
                  => x \in map (remv) BW.hvote{1}) /\ 
          (forall id, BW.uL{1}.[id] <> None =>
             nth witness BU.keyL{2} (index id (map fst3 BU.keyL{2})) =
             (id, (oget BW.uL{1}.[id]).`2, (oget BW.uL{1}.[id]).`1))/\ 
          (forall id, BW.uL.[id]{1} <> None <=> 
                      id \in map fst3 BU.keyL{2})/\
          (forall id, BW.uL.[id]{1} <> None =>
              (oget BW.uL.[id]{1}).`1 = get_upk  (oget BW.uL.[id]{1}).`2)).
  + (* corrupt *)
    proc.
    inline *.
    seq 1 2: (={id, glob E, glob SS, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.coL, 
                BW.qVo, BW.pk, BW.hvote} /\
              BW.uLL{1} = BU.keyL{2} /\
              BW.coL{1} = map fst3 BU.corL{2} /\ 
              BW.iu{2} = map ft3 BU.keyL{2}/\ 
             (forall x, 
                  x \in map rem_id BU.sigL{2} 
                  => x \in map (rem_id \o remv) BW.hvote{1}) /\ 
             (forall x, 
                  x \in BU.sigL{2} 
                  => x \in map (remv) BW.hvote{1})/\
              (forall id, BW.uL{1}.[id] <> None=>
             nth witness BU.keyL{2} (index id (map fst3 BU.keyL{2})) =
             (id, (oget BW.uL{1}.[id]).`2, (oget BW.uL{1}.[id]).`1))/\
              (forall id, BW.uL.[id]{1} <> None <=> 
                      id \in map fst3 BU.keyL{2})/\
              usk{1} = None /\ id0{2} = id{2} /\ usk0{2} = None/\
             (forall id, BW.uL.[id]{1} <> None =>
              (oget BW.uL.[id]{1}).`1 = get_upk  (oget BW.uL.[id]{1}).`2)).    
      by wp.
    if =>//=.    
      progress. smt. smt. 
    rcondt{2} 7; progress.
      by auto=>/>; progress. 
    wp; while{1} (0 <= i{1} /\
               hvo'{1} = filter ((predC1 id0{1}) \o fst5) (take i{1} hvo{1}))
              (size hvo{1} - i{1}); progress.
       auto=>/>; progress. 
       + smt. 
       + rewrite (take_nth witness); first by smt. 
         by rewrite filter_rcons /fst5 /predC1 /(\o) //= H1 cats1 //=. 
       + smt. smt. 
       + rewrite (take_nth witness); first by smt. 
         by rewrite filter_rcons /fst5 /predC1 /(\o) //=. 
       + smt.     
    auto=>/>; progress.  
    + by rewrite take0.
    + smt. smt. smt. 
    + by rewrite map_cat /fst3 //= -/fst3. 
    + rewrite map_comp.
      rewrite take_oversize; first by smt.
      cut Hfm:= filter_map remv (predC1 id{2} \o fst4) BW.hvote{2}.      
      have ->: (predC1 id{2} \o fst5) = (preim remv (predC1 id{2} \o fst4)).
         by rewrite /preim /remv /fst4 /fst5 //=.       
       rewrite -Hfm.
       have ->: Top.fst4 = S.fst4 by rewrite //=.
       
       cut := H x _. rewrite mapP. 
         + move: H8; rewrite mapP; elim => y [Hyf Hym]. exists y. smt. 
       rewrite map_comp.
       move => HxH.
       move: H8; rewrite mapP; elim => y [Hyf Hyx]. 
       move: Hyf. rewrite mem_filter. elim => Hyf Hym. 
       rewrite mapP. exists y. rewrite mem_filter Hyf Hyx //=.
       smt.
     + cut Hfm:= filter_map remv (predC1 id{2} \o fst4) BW.hvote{2}.   
       rewrite take_oversize; first by smt.   
      have ->: (predC1 id{2} \o fst5) = (preim remv (predC1 id{2} \o fst4)).
         by rewrite /preim /remv /fst4 /fst5 //=.       
       rewrite -Hfm.
       have ->: Top.fst4 = S.fst4 by rewrite //=.
       smt.
       
    
    by auto.
  + (* vote *)
    proc.
    sp.
    if =>//=.  
      progress. 
      + rewrite /ft3 -map_comp /(\o) //=. smt. 
      + move: H4; rewrite /ft3 -map_comp /(\o) //=. 
        smt.
    inline *. 
    seq 6 2: (o{2} = None /\
   o{1} = None /\
   ={id, v} /\
   ={glob E, glob SS, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.coL, BW.qVo, BW.pk, BW.hvote} /\
   BW.uLL{1} = BU.keyL{2} /\
   BW.coL{1} = map fst3 BU.corL{2} /\
   BW.iu{2} = map ft3 BU.keyL{2}/\ 
   (forall (x : upkey * cipher * sign),
      x \in map rem_id BU.sigL{2} => x \in map (rem_id \o remv) BW.hvote{1}) /\
   (forall x,  x \in BU.sigL{2} 
              => x \in map (remv) BW.hvote{1})/\
   (forall (id1 : ident),
      BW.uL{1}.[id1] <> None =>
      nth witness BU.keyL{2} (index id1 (map fst3 BU.keyL{2})) =
      (id1, (oget BW.uL{1}.[id1]).`2, (oget BW.uL{1}.[id1]).`1)) /\
   (forall (id1 : ident),
     BW.uL{1}.[id1] <> None <=> id1 \in map fst3 BU.keyL{2} )/\
  BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\ BW.qVo{1} < qVo/\
   ={c} /\ id0{1}= id{1} /\ v0{1} = v{1}/\ pk{1} = BW.pk{1}/\
   usk{1} = (oget BW.uL.[id]).`2{1} /\ upk{1} = get_upk usk{1} /\
   e{2} = nth (ft3 witness) BW.iu{2} (index id{2} (map fst BW.iu{2}))/\
   (forall id, BW.uL.[id]{1} <> None =>
              (oget BW.uL.[id]{1}).`1 = get_upk  (oget BW.uL.[id]{1}).`2)).
    call(: ={glob HRO.RO}); first by sim.
    auto=>/>; progress.
    + rewrite -map_comp. 
      have ->: fst \o ft3 = fst3 by rewrite /fst3.
      have ->: nth (ft3 witness) (map ft3 BU.keyL{2}) (index id{2} (map fst3 BU.keyL{2})) =
               ft3 (nth witness BU.keyL{2} (index id{2} (map fst3 BU.keyL{2}))).
        have Ho: forall (A: (ident * uskey * upkey) list) j,
                   ft3 (nth witness A j) = 
                   nth (ft3 witness) (map ft3 A) j.
           move => A a; elim : A=>//=. 
           smt.
        by rewrite Ho.
      smt.        
    rcondt{2} 4; progress.
        auto=>/>; progress. smt. 
      wp; call(: ={glob JRO.RO}); first by sim.
      auto=>/>; progress. 
      + smt. 
      + move: H7. rewrite -?map_comp.
        have ->: fst \o ft3 = fst3 by rewrite /fst3.
        have ->: nth (ft3 witness) (map ft3 BU.keyL{2}) (index id{2} (map fst3 BU.keyL{2})) =
               ft3 (nth witness BU.keyL{2} (index id{2} (map fst3 BU.keyL{2}))).
        have Ho: forall (A: (ident * uskey * upkey) list) j,
                   ft3 (nth witness A j) = 
                   nth (ft3 witness) (map ft3 A) j.
           move => A a; elim : A=>//=. 
           smt.
          by rewrite Ho. 
        smt. 
      + move: H7; rewrite -?map_comp.
        have ->: fst \o ft3 = fst3 by rewrite /fst3.
        have ->: nth (ft3 witness) (map ft3 BU.keyL{2}) (index id{2} (map fst3 BU.keyL{2})) =
               ft3 (nth witness BU.keyL{2} (index id{2} (map fst3 BU.keyL{2}))).
        have Ho: forall (A: (ident * uskey * upkey) list) j,
                   ft3 (nth witness A j) = 
                   nth (ft3 witness) (map ft3 A) j.
           move => A a; elim : A=>//=. 
           smt.
        by rewrite Ho. smt.  
      + rewrite map_cat /remv //= -/remv mem_cat map_comp. 
        move: H8; rewrite map_cat mem_cat /rem_id //= -/rem_id.
        smt.
      + rewrite map_cat /remv //= -/remv mem_cat.
        move: H8; rewrite mem_cat /rem_id //= -/rem_id. 
        smt.
    
  + by proc. by proc. by proc.
  
  wp; swap{2} [10..12] -3.
  swap{2} [14..17] -4.
  wp; while (={i, glob SS} /\ BW.uLL{1} = BU.keyL{2} /\
             0 <= i{1} /\
             (forall id, BW.uL{1}.[id] <> None =>
             nth witness BU.keyL{2} (index id (map fst3 BU.keyL{2})) =
             (id, (oget BW.uL{1}.[id]).`2, (oget BW.uL{1}.[id]).`1))/\ 
          (forall id, BW.uL.[id]{1} <> None <=> 
                      id \in map fst3 BU.keyL{2})/\
          (forall id, BW.uL.[id]{1} <> None =>
              (oget BW.uL.[id]{1}).`1 = get_upk  (oget BW.uL.[id]{1}).`2)/\
          uniq (map fst3 BW.uLL{1})/\
         (forall x, x \in drop i{1} (undup Voters) =>
                   !x \in (map fst3 BW.uLL{1}))).
    inline *.
    sp; wp.
    call{1} (Kgen_get_upk' JRO.RO (<:MEUF_Exp(SS, BMEUF(E, HRO.RO, GRO.RO, A,SS), JRO.RO).OO)).
    auto=>/>; progress.
    + smt. smt.
    + pose a:= (nth witness (undup Voters) i{2}, result_R.`1, result_R.`2).
      case (id1 = nth witness (undup Voters) i{2}).
      + move => Heq. rewrite get_set Heq //= oget_some //=. 
        rewrite -/a -Heq.
        have ->: (index id1 (map fst3 (BU.keyL{2} ++ [a]))) = size BU.keyL{2}.
          rewrite map_cat index_cat.
          have ->: !id1 \in map fst3 BU.keyL{2}.
            cut := H4 id1 _.
              rewrite Heq (drop_nth witness); first by smt.
              by rewrite mem_head.
            by done.
          rewrite Heq /a /fst3 //=. 
          by smt.
        smt.
      - move => Hneq.               
        rewrite map_cat index_cat.
        case (!id1 \in map fst3 BU.keyL{2}). smt.
        simplify. move => HmL.
        rewrite HmL //=.
        rewrite nth_cat -(size_map fst3) index_mem HmL //=. smt.
    + rewrite map_cat mem_cat /fst3 //= -/fst3. smt.
    + move: H7; rewrite map_cat mem_cat /fst3 //= -/fst3. smt.
    + case (id1 = nth witness (undup Voters) i{2}).
      + smt.
      - move => Hneq. 
        move: H7; rewrite get_set_neq. smt.
        smt.
    + rewrite map_cat /fst3 //= -/fst3 cat_uniq H3 //=.
      smt.
    + rewrite map_cat /fst3 //= -/fst3 mem_cat. 
      have Hxm: x \in drop i{2} (undup Voters). 
         rewrite (drop_nth witness i{2}). smt.    
         smt. 
       cut Hxn:= H4 x Hxm.
       rewrite Hxn//=. 
        cut := take_drop_uniq' (undup Voters) (nth witness (undup Voters) i{2}) i{2}_ _.  
       + smt.       
       + smt.
       smt.
  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
  swap{2} [10..11] 3; wp; rnd; call(: true).
  call(: true ==> ={glob JRO.RO}); first by sim.
  wp; call(: true ==> ={glob GRO.RO}); first by sim.
  call(: true ==> ={glob HRO.RO}); first by sim.
  auto=>/>; progress. 
  + smt. smt. smt. 
  + by rewrite /ft3 -map_comp /(\o) //=.
  + by rewrite /ft3 -map_comp /(\o).
  + by move: H10; rewrite /ft3 -map_comp /(\o).

  seq 4 4: ( ={vbb, JRO.RO.m, BW.hvote, co_bb} /\
             BW.uLL{1} = BU.keyL{2} /\
             BW.coL{1} = map fst3 BU.corL{2} /\
             (forall (x : upkey * cipher * sign),
     x \in map rem_id BU.sigL{2} => x \in map (rem_id \o remv) BW.hvote{1}) /\
  (forall (x : ident * upkey * cipher * sign),
     x \in BU.sigL{2} => x \in map remv BW.hvote{1}) /\
             (forall (x : ident * upkey * cipher * sign),
                x \in vbb{1} => ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2})/\
             ={c} /\ 
             ((find (fun (x: ident * upkey * cipher * sign),
                    !(nth witness BW.uLL{1} (index x.`2 (map thr3 BW.uLL{1}))).`1 
                      \in BW.coL{1}) co_bb{1}) < 
              size co_bb{1}  => ver_sign c{2}.`2 c{2}.`3 c{2}.`4 JRO.RO.m{2})).  
    auto=>/>; progress. 
    + pose L:= (filter (predC (mem (map (rem_id \o remv) BW.hvote{2})) \o rem_id) vbb{2}).
      pose C:= map fst3 BU.corL{2}.
      pose K:= BU.keyL{2}.
      rewrite -map_comp.
      have ->: (fun (p : ident * upkey) => p.`2) \o ft3 = thr3 by rewrite /ft3 /(\o).
      have ->: (fun (x : ident * upkey * cipher * sign) =>
                  ! ((nth witness K (index x.`2 (map thr3 K))).`1 \in C)) = 
             (fun (x : ident * upkey * cipher * sign) =>
                  ! ((nth (ft3 witness) (map ft3 K) (index x.`2 (map thr3 K))).`1 \in C)).
        rewrite fun_ext /(==). move => x.
        pose j:= index x.`2 (map thr3 K).
      have ->: (nth witness K j).`1 = (nth (ft3 witness) (map ft3 K) j).`1.
        have Ho: forall (A: (ident * uskey * upkey) list) j,
                   ft3 (nth witness A j) = 
                   nth (ft3 witness) (map ft3 A) j.
           move => A a; elim : A=>//=. 
           smt.
         cut Hoo:= Ho K j.
         rewrite -Hoo. 
         by rewrite /ft3 /thr3 /fst3 //=. 
      by done.
      by done.   
    + move: H3.
      pose L:= (filter (predC (mem (map (rem_id \o remv) BW.hvote{2})) \o rem_id) vbb{2}).
      pose C:= map fst3 BU.corL{2}.
      pose K:= BU.keyL{2}.
      rewrite -map_comp.
      have ->: (fun (p : ident * upkey) => p.`2) \o ft3 = thr3 by rewrite /ft3 /(\o).  
      have ->: (fun (x : ident * upkey * cipher * sign) =>
                  ! ((nth witness K (index x.`2 (map thr3 K))).`1 \in C)) = 
             (fun (x : ident * upkey * cipher * sign) =>
                  ! ((nth (ft3 witness) (map ft3 K) (index x.`2 (map thr3 K))).`1 \in C)).
      rewrite fun_ext /(==). move => x.
      pose j:= index x.`2 (map thr3 K).
      have ->: (nth witness K j).`1 = (nth (ft3 witness) (map ft3 K) j).`1.
        have Ho: forall (A: (ident * uskey * upkey) list) j,
                   ft3 (nth witness A j) = 
                   nth (ft3 witness) (map ft3 A) j.
           move => A a; elim : A=>//=. 
           smt.
         cut Hoo:= Ho K j.
         rewrite -Hoo. 
         by rewrite /ft3 /thr3 /fst3 //=. 
      by done.     
      pose i:= (find
        (fun (x : ident * upkey * cipher * sign) =>
           ! ((nth (ft3 witness) (map ft3 K) (index x.`2 (map thr3 K))).`1 \in
              C)) L). 
      move => H3. 
      cut := H2 (nth witness L i) _.
        have Hm: nth witness L i \in L. smt. 
        move: Hm; pose a:= nth witness L i.
         rewrite /L mem_filter.
        by done. 
      by done.

  wp; sp.
  exists* (glob SS){2}, u{2}, m{2}, s{2}; elim* => gs ux mx sx.
  call{2} (ver_Ver_one gs ux mx sx).
  auto=>/>; progress. smt. rewrite -/rem_id.
  cut := H (rem_id c{2}). rewrite H3 //=.
 
qed.

local module XS(SS:SignScheme, O: MEUF_Oracle) = BMEUF (E,HRO.RO,GRO.RO,A, SS, O). 


local lemma meuf_euf (C<: Corr_Adv {JRO.RO, WU, SS, BU}) &m:
  islossless C(JRO.RO).main =>
  Pr[MEUF_Exp(SS,BMEUF(E, HRO.RO, GRO.RO,A,SS),JRO.RO).main() @ &m: res] 
<= 
 (size (undup Voters))%r * 
    Pr[EUF_Exp(SS, BE(SS, C), JRO.RO).main() @ &m : res] +
 (size (undup Voters))%r *
    Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO)), JRO.RO).main() @ &m : res] +
 (size (undup Voters))%r * 
    Pr[Correctness(SS, C, JRO.RO).main() @ &m : !res].
proof.
  move => C_ll.
  cut := meuf_euf JRO.RO SS XS JRO.RO_o_ll _ _ _ _ _ _ _ _ C &m C_ll. 
  + by move => O Oo; conseq (SSv_ll O Oo).
  + by move => SA OA OAo; conseq (SSk_ll SA OA OAo).
  + by move => SA OA OAo; conseq (SSs_ll SA OA OAo).
  + move => SA OA OAo SAk SAs SAv OAs OAc.
    proc.
    wp; while(true) (size BW.bb -i); progress.
      wp; call{1} SAv.   
      wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll). 
      by auto=>/>; smt.
    wp. 
    call{1} (Aa1_ll (<: BMEUF(E, HRO.RO, GRO.RO, A, SA, OA).DO)
                    _ _ HRO.RO_o_ll GRO.RO_o_ll OAo).
    + by proc; wp; call OAc.
    + proc.
      sp; if=>//=.
      wp; call OAs; call (Ee_ll HRO.RO HRO.RO_o_ll).
      by auto.
    inline*; wp.
    while (true) (card work0); progress.
      by auto=>/>; smt.
    wp; while (true) (card work); progress.
      by auto=>/>; smt.
    wp; rnd; call(Ek_ll HRO.RO).
    by auto=>/>; smt.
  + smt.
  + exact inj_get_upk.
  + move => O. by proc*; call{1} (Kgen_get_upk  O).
  + by move => gs O; proc*; call{1} (SSk_eq gs O).
  have ->: Pr[MEUF_Exp(SS, XS(SS), JRO.RO).main() @ &m : res] =
           Pr[MEUF_Exp(SS, BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO).main() @ &m : res].
    by byequiv=>/>; sim.
  have ->: Pr[EUF_Exp(SS, BF(SS, WOO(XS(SS), JRO.RO)), JRO.RO).main() @ &m : res]= 
           Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(E, HRO.RO, GRO.RO, A,SS), JRO.RO)), 
               JRO.RO).main() @ &m : res].
    by byequiv =>/>; sim.  
  by smt.
qed. 


(* ....... DO reduction to <= qCo *) 
module DBB_Oracle_vote(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc removeid = DBB_Oracle(V,H,G,J).removeid
  proc h        = DBB_Oracle(V,H,G,J).h
  proc g        = DBB_Oracle(V,H,G,J).g
  proc j        = DBB_Oracle(V,H,G,J).j
  
  (* corrupt oracle *)
  proc corrupt(id: ident): uskey option={
    var usk, usk';
    usk <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL){
      BW.hvote <- filter ((predC1 id) \o fst5) BW.hvote;
      BW.coL <- BW.coL ++ [id];
      BW.qCo <- BW.qCo +1;
      usk' <- (oget BW.uL.[id]).`2;
      usk <- Some usk';
      }
    return usk;
    }

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL /\ BW.qVo <qVo){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      o <- Some b;
      if (!(b.`2,b.`3) \in (map (fun (x: ident * vote * upkey * cipher*sign),
                         (x.`3,x.`4))
                     BW.hvote)){
          BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)]; 
        }elif(!BW.bow){
         BW.bow <- true;
         BW.ibad <- BW.qVo;
        }
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }

  (* dishonest voters query *)
  proc check (id: ident): unit ={
    var i, h, vv, pL, veh;
    i <- 0;

    BW.pbb <@ V(H,G,J).publish(BW.bb, BW.hk);
    (* verify all honest ballots *)
    pL <- map open4 (Sanitize (map pack4 BW.hvote));
    while (i < size pL){
      h      <- nth witness pL i;
      vv     <@ V(H,G,J).verifyvote ((h.`1,h.`3,h.`4,h.`5), 
                                     BW.pbb, BW.hk);
      veh <- exists id, (get_id id, (remv h).`2,(remv h).`3, (remv h).`4) \in 
             map preha (USanitize BW.bb);
      if (id = h.`1 /\ vv /\ veh /\ !id \in (map fst5 BW.check)){
        BW.check <- BW.check ++ [h];
      }
      i <- i +1;
    }
  }

}.

module DBB_Exp_vote (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id, hC, hNC, hN, r', pi', ev', fbb,
        vbb, b, m, e, hon_mem, val_bb, hCN, j, c, dL;
    BW.bad  <- false;
    BW.bow  <- false;
    BW.ibad  <$ [0..qVo-1];
    BW.bb   <- [];
    BW.coL  <- [];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));

    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    hC <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) BW.check;
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;

    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id vbb);
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }

    bool <- (forall (X L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/ r <> Count (hC ++ L ++ dL));
    return ev /\ bool /\size dL <= BW.qCo ;
  }
}.

local lemma dbb_wspread &m:
  (* Pr[DBB_Exp_check' (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] *)
  Pr[DBB_Exp_dish(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DBB_Exp_vote (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] +
  Pr[DBB_Exp_vote (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: 
     BW.bow /\ 0 <= BW.ibad < qVo].
proof.
  byequiv =>/>.
  proc.
  wp. simplify.

  seq 25 26: (  0 <= BW.ibad{2} < qVo /\
             ={ev, bool, r, BW.bb, BW.qCo, glob E, glob SS, 
               HRO.RO.m, JRO.RO.m, BW.sk}/\
             (!BW.bow{2} => 
                ={BW.hvote, BW.check}))=>/>.
(*    progress.
    cut Hof:= H4 X D L.    
    by rewrite -(andEl _ _ (H1 H2)) -(andEr _ _ (H1 H2)) Hof. 
*)
  wp; call (: ={BW.pk, BW.pbb, glob Vz, glob HRO.RO, GRO.RO.m});
    first by sim.
  call (: ={BW.sk, BW.bb, glob Pz, glob E, glob SS, 
            HRO.RO.m, GRO.RO.m, JRO.RO.m});
    first by sim.
  call (: ={BW.pk, BW.pbb, glob Vz, glob HRO.RO, GRO.RO.m});
    first by sim.
  call (: ={BW.sk, BW.bb, glob Pz, glob E, glob SS, 
            HRO.RO.m, GRO.RO.m, JRO.RO.m});
    first by sim.
   (* A.a2 call *)
  call (: ={BW.bb, BW.uL, BW.pk, HRO.RO.m, JRO.RO.m, GRO.RO.m,BW.hk}/\
          (!BW.bow{2} =>  ={BW.hvote, BW.check})).
  + proc.
    inline *.
    case (!BW.bow{2}).
    + sim. smt.
    - conseq (: BW.bow{2})=>/>.
      while{1} (true) (size pL{1} - i{1}); progress.    
        auto=>/>; smt.
      while{2} (true) (size pL{2} - i{2}); progress.    
        auto=>/>; smt.
      auto=>/>; smt.
  + by proc.
  + by proc. 
  + by proc.

  (* A.a1 call *)
  wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
            glob SS, glob E, BW.bb, BW.pk, BW.coL, 
            BW.uL, BW.qCo}/\
          (!BW.bow{2} => ={BW.hvote})/\
          0<= BW.ibad{2} <qVo /\ 
          0 <= BW.qVo{2}).
   + proc. 
     seq 1 1: (={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
                glob SS, glob E, BW.bb, BW.pk, BW.coL, 
                BW.uL, BW.qCo, usk, id}/\
               (!BW.bow{2} => ={BW.hvote})/\
               0<= BW.ibad{2} <qVo /\ 
               0 <= BW.qVo{2}/\
               usk{1} = None).
       by wp.
     if =>//=. 
     inline*; wp. 
     while{1} (0 <= i{1} /\
               hvo'{1} = filter ((predC1 id0{1}) \o fst5) (take i{1} hvo{1}))
              (size hvo{1} - i{1}); progress.
       auto=>/>; progress. 
       + smt. 
       + rewrite (take_nth witness); first by smt. 
         by rewrite filter_rcons /fst5 /predC1 /(\o) //= H1 cats1 //=. 
       + smt. smt. 
       + rewrite (take_nth witness); first by smt. 
         by rewrite filter_rcons /fst5 /predC1 /(\o) //=. 
       + smt.        
     auto=>/>; smt. 

   + proc.
     sp; if=>//=.
     inline *; wp.
     call (: ={glob JRO.RO}); first by sim.
     call (: ={glob HRO.RO}); first by sim.     
     auto=>/>; smt.

   + by proc.
   + by proc.
   + by proc.

   wp; while( ={i, BW.uL, BW.uLL, glob SS});
     first by sim.
   call(: ={glob E}); first by sim.
   call (: true ==> ={glob JRO.RO}); first by sim.
   call (: true ==> ={glob GRO.RO}); first by sim.
   call (: true ==> ={glob HRO.RO}); first by sim.
   wp; rnd{2}; wp.
   auto=>/>; progress. smt. smt. smt. 
   case (!BW.bow{2}).
     while (={j, dL, glob E, glob HRO.RO, hon_mem, val_bb, BW.sk});
      first by sim.
     wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, hCN, BW.sk});
      first by sim.
     wp; while (={i, BW.bb, glob E, glob SS, HRO.RO.m, JRO.RO.m, BW.sk, vbb});
      first by sim.
      by auto=>/>; try rewrite /rem_id; smt. 
   wp; while{1} (true) (size (filter (predC hon_mem) val_bb){1} - j{1}); progress.
     wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{2} (true) (size (filter (predC hon_mem) val_bb){2} - j{2}); progress.
     wp; call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{1} (true) (size (filter hon_mem val_bb){1} - j{1}); progress.
     wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{2} (true) (size (filter hon_mem val_bb){2} - j{2}); progress.
     wp; call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{1} (true) (size BW.bb{1} - i{1}); progress.
     wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll).
     wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   wp; while{2} (true) (size BW.bb{2} - i{2}); progress.
     wp; call{2} (SSv_ll JRO.RO JRO.RO_o_ll).
     wp; call{2} (Ed_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   by auto=>/>; smt.
qed.


module DBB_Exp_check_two (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id,  hC, hNC, hN, r', 
        pi', ev', vbb, b,m,e, fbb, j, c, hon_mem, val_bb, dL, hCN;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id vbb);
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }

    hC <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.check)) 
                            (map rem_id vbb))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.check)) 
                               (map rem_id vbb)) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hC<- hC ++ [oget m];
      j <- j + 1;
    }
    
    hNC<- filter (predC (mem BW.check)) BW.hvote;
    hN <- map (fun (b:ident * vote * upkey * cipher * sign), b.`2) hNC;
    bool <- (forall (X L : vote list),
         ! perm_eq X hN \/
         ! subseq L X \/ 
         r <> Count (hC ++ L ++ dL));
    
    return ev /\ bool /\ size dL <= BW.qCo ;
  }
}.
              
                      

local lemma dbbcheck_dbbcheck_two &m:
  Pr[DBB_Exp_vote (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DBB_Exp_check_two (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  
  seq 19 17: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
                glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
                BW.uL, BW.qCo, BW.pk, BW.sk, bool, BW.bad, BW.uLL,BW.hk}/\ 
              BW.bb{1} =[] /\ 
              BW.coL{1} =[] /\ 
              BW.hvote{1} =[] /\
              BW.check{1} =[]/\
              BW.qVo{1} = 0/\
              bool{2} /\ BW.pk{2} = get_pk BW.sk{2}/\
              (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )). 
    wp; while (={i, BW.uL, glob SS, glob JRO.RO, BW.uLL}/\
               (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )).
      inline*; wp.
      call{1} (Kgen_get_upk JRO.RO).
      auto=>/>; progress.
      + case (id1 = nth witness (undup Voters) i{2}). smt.
        move => Hneq. rewrite get_set_eq oget_some //=.
        move: H2; rewrite get_set_neq. smt.
        move => H2. 
        smt.
      + case (id1 = nth witness (undup Voters) i{2}). smt.
        move => Hneq. rewrite get_set_neq. smt.
        smt.
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt.

  seq 7 7: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
              glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
              BW.uL, BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi', BW.hk,
              BW.uLL, fbb}/\ 
            bool{2}/\
            (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
            BW.bb{2} = (filter ((mem (map thr3 BW.uLL{2}))\o two4) (USanitize fbb{2}))/\
            (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.check{2})).
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: ={glob E, glob SS, glob Pz, 
             glob JRO.RO, glob HRO.RO, glob GRO.RO}); first by sim.
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: true); first by sim.
    (* A.a2 call *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.hk,  
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in map remv BW.check{2} => 
                exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
           (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
           (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb,BW.hk}/\
             0 <= i{2} /\
             (* uniq (map id BW.check) *)
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in BW.check{2} => 
                        x \in BW.hvote{2})/\
             (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
             BW.pbb{2} = map (fun (x: ident*upkey * cipher * sign), 
                                  (x, hash' BW.hk{2} x))  BW.bb{2}/\
             (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
        auto =>/>; progress.
        + smt. 
        + move: H7; rewrite mem_cat mem_seq1; move => Hmor.
          case (x \in BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.   
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt. 
            rewrite -Hx. 
            move => HmS. 
            cut := San_mem (map pack4 BW.hvote{2}) (pack4 x) _. 
              + move: HmS; rewrite mapP.
                elim=> y HymS.     
                rewrite (andEr _ _ HymS).       
                have ->: pack4 (open4 y) = y by rewrite /open4 /pack4; smt.        
                rewrite (andEl _ _ HymS).
            rewrite mapP //=; elim => y HymS.
            have ->: x = y by  cut:= (andEr _ _ HymS); rewrite /pack4 ; smt.
            rewrite (andEl _ _ HymS).
        + move: H7; rewrite map_cat //= mem_cat mem_seq1; move => Hmem.
          case (x \in map remv BW.check{2}). smt.
          (* use the exists id *)
          move => Hn.
          have Hx: x = remv (nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2})
            by move :Hmem; rewrite Hn.
          move: H5. 
          rewrite -Hx /preha mapP.
          (*rewrite -USan_USanitize. *)
          elim => y.       
          elim => Hym.  
            rewrite /remi /(\o) -?Hx. 
            elim => Hy1 Hy2 Hy3 Hy4.
          exists y.`1; rewrite  Hy2 Hy3 Hy4.
          smt.                           
    
        + rewrite map_cat //=.  
          rewrite cats1 rcons_uniq (H2 H7) //=.
          move: H3 H4 H5 H6;
            pose L:= map open4 (Sanitize (map pack4 BW.hvote{2}));
            pose x:= nth witness L i{2}.
          move => H3 H4 H5 H6.      
          rewrite mapP negb_exists //=.
          move => a. rewrite negb_and.           
          case (!a \in BW.check{2}). smt.
          simplify. move => HmaC. 
          have HmaH: a \in BW.hvote{2} by rewrite H0.
          rewrite /(\o) /rem_id /remv //=.  
          have HmxH: x \in BW.hvote{2}. 
            rewrite /x /L. search mem Sanitize. 
            cut := San_mem (map pack4 BW.hvote{2}) 
                           (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) _. smt.
            rewrite mapP.
            elim => y Hymem. 
            have ->: nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} = 
                      open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}). smt.
            have ->: open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) = y. 
              by rewrite (andEr _ _ Hymem) /open4 /pack4 //=; smt.
            rewrite (andEl _ _ Hymem).
          
          move: H6. rewrite mapP negb_exists /fst5 //=.
          move => H6.
          cut := H6 x. rewrite //=.
          move => HmxC. rewrite ?andabP. 
          have Hmust: forall (X: (ident * upkey * cipher * sign) list) a b,
                       uniq (map rem_id X) => 
                       a \in X => 
                       b \in X => 
                      a <> b =>
                      rem_id a <> rem_id b. 
          + move => X c b HuX Hma Hmb Hneq. 
            cut Hia:= nth_index witness c X Hma.
            cut Hib:= nth_index witness b X Hmb.
            case (index c X < index b X).
             + move => Hiab.
               have Hea: exists A B C, X = A ++ [c] ++ B ++ [b] ++ C. 
                cut := take_nth witness (index c X) X _. smt.
                rewrite -cats1 Hia.
                move => HA. 
                exists (take (index c X) X).
                rewrite -HA.
                cut := take_nth witness (index b X) X _ . smt.
                rewrite -cats1 Hib.
                move => HB. 
                exists (drop (index c X +1) (take (index b X) X)). 
                have ->: take (index c X + 1) X ++ drop (index c X + 1) (take (index b X) X) ++ [b]
                 = take (index b X + 1) X.
                  rewrite HB.
                have ->: take (index c X + 1) X = take (index c X + 1) (take (index b X) X). 
                  pose i:= index c X + 1.
                  pose j:= index b X.
                  cut := cat_take_drop j X . 
                  smt.
                smt.
                exists (drop (index b X +1) X).
                by rewrite cat_take_drop.
               move: Hea. elim => A B C HX.
               move: HuX. rewrite HX. 
               rewrite ?map_cat ?cat_uniq.
               simplify. 
               case( rem_id b  = rem_id c).  
                  move => Heq.
                  have ->: rem_id b \in map rem_id A ++ [rem_id c] ++ map rem_id B. smt.
                  by done.               
               by smt.  
             - move => Hiab.   
               have Hi_dif: index c X <> index b X. smt.
               have Hiab': index b X < index c X by smt.
               have Hea: exists A B C, X = A ++ [b] ++ B ++ [c] ++ C. 
                cut := take_nth witness (index b X) X _. smt.
                rewrite -cats1 Hib.
                move => HB. 
                exists (take (index b X) X).
                rewrite -HB.
                cut := take_nth witness (index c X) X _ . smt.
                rewrite -cats1 Hia.
                move => HA. 
                exists (drop (index b X +1) (take (index c X) X)). 
                have ->: take (index b X + 1) X ++ drop (index b X + 1) (take (index c X) X) ++ [c]
                 = take (index c X + 1) X.
                  rewrite HA.
                have ->: take (index b X + 1) X = take (index b X + 1) (take (index c X) X). 
                  pose i:= index b X + 1.
                  pose j:= index c X.
                  cut := cat_take_drop j X . 
                  smt.
                smt.
                exists (drop (index c X +1) X).
                by rewrite cat_take_drop.
               move: Hea. elim => A B C HX.
               move: HuX. rewrite HX. 
               rewrite ?map_cat ?cat_uniq.
               simplify. 
               case(rem_id b  = rem_id c). 
                 move => Heq.
                 have ->: rem_id c \in map rem_id A ++ [rem_id b] ++ map rem_id B. smt.
                 by done.               
               by smt.
            cut := Hmust (map remv BW.hvote{2}) (remv a) (remv x) _ _ _ _.
             + rewrite -map_comp H7. 
             + rewrite mapP. exists a. rewrite HmaH //=.
             + rewrite mapP. exists x. rewrite HmxH //=.
             + smt.
            rewrite /rem_id /remv //= ?andabP.
            by smt.
        + smt.


      by auto=>/>; progress. 

    (* H, G, J *)
    + by proc. 
    + by proc. 
    + by proc.

    (* A.a1 call *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}/\
             BW.pk{2} = get_pk BW.sk{2}/\
             (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                           (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                           ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                          (\o) (mem (map thr3 BW.uLL{2})) two4 x in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
             (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.hvote{2})).
    + proc. 
      auto=>/>; progress. smt. smt. smt. smt. 
      have ->: forall (X: (ident * vote * upkey * cipher * sign) list) f,
                      let g = (rem_id \o remv) in
                      uniq (map g X) => uniq (map g (filter f X)).
      + move => X f g.      
        elim: X=>//=.     
        move => x X Hp Hand.
        case (!f x).
        + move => Hfn.   
          by rewrite Hfn //= Hp; first by rewrite (andEr _ _ Hand). 
        - simplify. 
          move => Hfx.
          rewrite Hfx //=.
          split. 
          + smt.
          + smt.
      + by done.
      by done.

    + proc. 
      sp; if =>//=.
      exists * BW.pk{2}, BW.sk{2}; elim* => pkx skx.
      pose Kp:= pkx = get_pk skx.
      have Hp: Kp \/ !Kp by smt.
      elim Hp.
      + move => Hp.
        inline*; wp; sp.
        seq 1 1: ( ={id0, v0, pk, usk, upk, o}/\
                 (pkx = BW.pk{2} /\ skx = BW.sk{2}) /\
                 id0{2} = id{2} /\ 
                 v0{2} = v{2} /\
                 pk{2} = BW.pk{2} /\ 
                 usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
                 upk{2} = get_upk usk{2} /\
                 o{2} = None /\
                 ={id, v, GRO.RO.m, HRO.RO.m, JRO.RO.m,  
                   glob SS, glob E, BW.bb, BW.pk, BW.qVo,
                   BW.hvote, BW.coL, BW.uL, BW.qCo, c} /\
                 BW.pk{2} = get_pk BW.sk{2} /\
                 (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
                 (forall (y : ident * vote * upkey * cipher * sign),
                     y \in BW.hvote{2} =>
                       (dec_cipher BW.sk{2} y.`3 y.`4 HRO.RO.m{2} <> None /\
                       ver_sign y.`3 y.`4 y.`5 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 (y.`1,y.`3,y.`4,y.`5))/\
                 (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
                 uniq (map (rem_id \o remv) BW.hvote{2}) /\
                 BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1})/\
                 dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2} <> None /\
                 v0{2} = oget (dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2})).
          exists* v0{1}, upk{2}; 
            elim* => vx upkx. 
          call{1} (Enc_dec_two skx pkx upkx vx Hp).
          by auto; progress; smt. 
        exists* usk{2}, upk{2}, c{2}; 
          elim* => uskx upkx cx.
        pose Kup:= upkx = get_upk uskx.
        have Hup: Kup \/ !Kup by smt.
        elim Hup.
        + move => Hup.
          call{2} (Sig_ver_two uskx upkx cx Hup ).
          auto; progress.
          + smt. smt. 
          + move: H8; rewrite mem_cat //=. 
            move => H8.
            case (y \in BW.hvote{2}). smt. 
            move => HnyH.
            move: H8; rewrite HnyH //=; move => H8.
            rewrite /two4 H8 /(\o) //= .
            cut := H id{2} H3. smt.
          + smt.
          + rewrite map_cat //=.
            have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H7. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt. 
        - move => Hup_n.
          conseq (: _ ==> !upk{2} = get_upk usk{2}) =>/>.
          call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
          call{2} (SSs_ll SS JRO.RO JRO.RO_o_ll). 
          by auto; progress; smt.
              
      - move => Hp_n.
        conseq (: _ ==> !BW.pk{2} = get_pk BW.sk{2}) =>/>.
        inline *; wp.
        call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
        call{2} (SSs_ll SS JRO.RO JRO.RO_o_ll). 
        call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
        call{2} (Ee_ll HRO.RO HRO.RO_o_ll).
        by auto; progress; smt.        

    (* H, G, J *)
    + by proc.     
    + by proc.
    + by proc.
    by auto => />; progress; smt. 
    
  swap{1} [1..3] 11.
  seq 11 11: (={dL, glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
      glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, BW.uL,
      BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi', BW.uLL, fbb} /\
  bool{2} /\
  (forall (id0 : ident),
     BW.uL{1}.[id0] <> None =>
     ((id0, (oget BW.uL{1}.[id0]).`2, (oget BW.uL{1}.[id0]).`1) \in BW.uLL{2}) /\
     (oget BW.uL{1}.[id0]).`1 = get_upk (oget BW.uL{1}.[id0]).`2) /\
  (let fd =
     (fun (x : ident * upkey * cipher * sign) =>
        (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
         ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
        (\o) (mem (map thr3 BW.uLL{2})) two4 x) in
   (forall (y : ident * vote * upkey * cipher * sign),
      y \in BW.hvote{2} => fd (y.`1, y.`3, y.`4, y.`5))) /\
  BW.bb{2} = filter (mem (map thr3 BW.uLL{2}) \o two4) (USanitize fbb{2}) /\
  (forall (x : ident * upkey * cipher * sign),
     x \in map remv BW.check{2} =>
     exists (id0 : ident), (id0, x.`2, x.`3, x.`4) \in USanitize BW.bb{2}) /\
  (forall (x : ident * vote * upkey * cipher * sign),
     x \in BW.check{2} => x \in BW.hvote{2}) /\
  (let f =
     (fun (x : ident * upkey * cipher * sign) =>
        oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) in
   (forall (x : ident * vote * upkey * cipher * sign),
      x \in BW.hvote{2} => x.`2 = f (remv x))) /\
  uniq (map (rem_id \o remv) BW.check{2})/\
  (let f = fun (x : ident * upkey * cipher * sign) =>
                     (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                     ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                     (\o) (mem (map thr3 BW.uLL{2})) two4 x in
            vbb{2} = filter f (USanitize fbb{2}) /\
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))).
     while (={j, dL, glob E, glob HRO.RO, hon_mem, val_bb, BW.sk});
      first by sim.
     wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, hCN, BW.sk});
      first by sim.
     wp; while (={vbb, glob E, glob SS, HRO.RO.m, JRO.RO.m, BW.bb, i, BW.sk}/\
                0<=i{2} <= size BW.bb{2}/\
            let f = fun (x: ident * upkey * cipher * sign),
                        dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).
    wp; sp. 
    exists* (glob SS){1}, (glob E){1}, BW.sk{1}, b{1}; elim* => gs ge skx bx.
    call{1} (ver_Ver_two bx.`2 bx.`3 bx.`4).
    call{1} (dec_Dec_two skx bx.`2 bx.`3).
    auto; progress.
    + smt. smt. smt. smt. smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons H3.      
    
    auto; progress. 
    + smt. smt. 
    + rewrite take_oversize; first by smt.
      by rewrite -filter_predI /predI. 
    
      

  seq 1 3: (={dL,r, ev, bool, BW.hvote, BW.qCo, BW.bb, BW.check} /\
            perm_eq hC{1} hC{2}).
    wp; while{2} ( 0 <= j{2} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id vbb{2}) in
                   hC{2} = map g (take j{2} xbb))
                 (size (filter (mem (map (rem_id \o remv) BW.check{2})) 
                               (map rem_id vbb{2})) - j{2}); progress.
      wp; sp.
      exists* (glob E), BW.sk, c; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
      + smt.
    auto =>/>; progress.
    + smt. smt.
    + pose g:= fun (x : upkey * cipher * sign) =>
              oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}).
      pose fd:= (fun (x : ident * upkey * cipher * sign) =>
              (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
              ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
              (\o) (mem (map thr3 BW.uLL{2})) two4 x).
      move: H6; rewrite -/fd; move => Hsize.
      rewrite take_oversize. smt.
      cut := nth_vote' BW.check{2} (fun (x : ident * upkey * cipher * sign) =>
                                        oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) _ .
      + move => x HxmC.
        cut := H4 x _; first by rewrite (H3 x HxmC). 
        by done.
      move => Heq.
      rewrite -Heq. 
      cut Hc:= nth_vote (map remv BW.check{2}) BW.check{2} 
                        (fun (x : ident * upkey * cipher * sign) =>
                                        oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) _ _.
      + done.
      + move => x HxmC.
        cut := H4 x _; first by rewrite (H3 x HxmC). 
        by done.
      rewrite Hc.
      rewrite -map_comp.
      have ->: ((fun (x : ident * upkey * cipher * sign) =>
                oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2})) \o
                remv) = (g \o rem_id \o remv).
        by rewrite  /g /rem_id /remv /(\o) //=.       
      pose S:= (filter fd (USanitize fbb{2})).
      pose L:= map (rem_id \o remv) BW.check{2}.
      have ->: (map (g \o rem_id \o remv) BW.check{2}) = map g L 
         by rewrite /L ?map_comp.
      have Hp: perm_eq L (filter (mem L) (map rem_id S)).
      have Hcheck: forall x, x \in L =>
                   x \in (map rem_id S).
         rewrite /L. 
         move => x. rewrite map_comp mapP.
         elim => y HymC. 
         cut := H2 y _; first by rewrite (andEl _ _ HymC).
         rewrite /S mapP.
         elim => id HmS.
         exists (id, y.`2, y.`3, y.`4).
         split.
         + rewrite /USanitize. 
           move: HmS. 
           pose a:= (id, y.`2, y.`3, y.`4).
           pose p:= (mem (map thr3 BW.uLL{2}) \o two4).
           pose dv:= fun (x : ident * upkey * cipher * sign) =>
                         (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                          ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}).
           have Hfd: fd = predI dv p.
             rewrite /predI /fd /dv /p fun_ext /(==). smt.
           rewrite Hfd filter_predI. 
           pose X:= (filter p (USanitize fbb{2})).
           rewrite /Sanitize'.
           rewrite mapP. elim => b [HbS Hab].
           cut Hb:= San_mem'' _ b HbS.
           have Ha: a \in X.
             move: Hb; rewrite mapP.
             elim => c [HcX Hbc].
             by rewrite Hab Hbc -uopen_upack HcX.
           rewrite mem_filter Ha //=. 
           cut := (andEl _ _ HymC).
           rewrite mapP. elim => z HzmC.
           cut := H3 z _; first by rewrite (andEl _ _ HzmC).
           move => HzmH.
           cut := H1 z HzmH. 
           rewrite /dv.  
           by rewrite /a (andEr _ _ HzmC).
         - by rewrite (andEr _ _ HymC) /rem_id.

      cut Hperm:= uniq_perm_eq_filter_two L (map rem_id S) _ _ _.
       + rewrite /S / Sanitize' /USanitize. (*
         cut := filter_map pack3 (fd \o open3) (map uopen (Sanitize (map upack fbb{2}))). 
         have ->: (preim pack3 (fd \o open3)) = fd by rewrite /preim.
         rewrite -?map_comp. 
         move => Hfm_mf.
         rewrite -Hfm_mf.*)
         rewrite map_inj_in_uniq . 
         + move => x y Hxm Hym. 
           rewrite /rem_id /remi /open3 /(\o) //=.          
           elim => Hxy2 [Hxy3 Hxy4].
           case (x.`1 = y.`1). smt. 
           (* show that is not possible *)
           move => Hxy1.
           move: Hxm. rewrite mem_filter. move => Hx.
           move: Hym. rewrite mem_filter. move => Hy.
           have Hxs: upack x \in Sanitize (map upack fbb{2}).
           + move: Hx; rewrite mapP.
             elim => Hlx.
             elim => a Hma. 
             rewrite (andEr _ _ Hma) -upack_uopen.
             by rewrite (andEl _ _ Hma).
           have Hys: upack y \in Sanitize (map upack fbb{2}).
           + move: Hy; rewrite mapP.
             elim => Hlx.
             elim => a Hma. 
             rewrite (andEr _ _ Hma) -upack_uopen.
             by rewrite (andEl _ _ Hma).


           have Hmust: forall (X: (upkey * (ident * cipher * sign)) list) a b,
                     uniq (map fst X) => a \in X => b \in X => a.`1 = b.`1 => a = b.
           + move => X a b HuX Hma Hmb Heqab. 
             case (b.`2 = a.`2). smt.
             move => Hneq.  
             cut Hia:= nth_index witness a X Hma.
             cut Hib:= nth_index witness b X Hmb.
             case (index a X < index b X).
             + move => Hiab.
               have Hea: exists A B C, X = A ++ [a] ++ B ++ [b] ++ C. 
                cut := take_nth witness (index a X) X _. smt.
                rewrite -cats1 Hia.
                move => HA. 
                exists (take (index a X) X).
                rewrite -HA.
                cut := take_nth witness (index b X) X _ . smt.
                rewrite -cats1 Hib.
                move => HB. 
                exists (drop (index a X +1) (take (index b X) X)). 
                have ->: take (index a X + 1) X ++ drop (index a X + 1) (take (index b X) X) ++ [b]
                 = take (index b X + 1) X.
                  rewrite HB.
                have ->: take (index a X + 1) X = take (index a X + 1) (take (index b X) X). 
                  pose i:= index a X + 1.
                  pose j:= index b X.
                  cut := cat_take_drop j X . 
                  smt.
                smt.
                exists (drop (index b X +1) X).
                by rewrite cat_take_drop.
               move: Hea. elim => A B C HX.
               move: HuX. rewrite HX. 
               pose f:= (fun (p : upkey * (ident * cipher * sign)) => p.`1).
               rewrite ?map_cat ?cat_uniq.
               simplify. have ->: f b  = f a by rewrite /f Heqab //=. 
               have ->: f a \in map f A ++ [f a] ++ map f B. smt.
                by done.                 
             - move => Hiab.   
               have Hi_dif: index a X <> index b X. smt.
               have Hiab': index b X < index a X by smt.
               have Hea: exists A B C, X = A ++ [b] ++ B ++ [a] ++ C. 
                cut := take_nth witness (index b X) X _. smt.
                rewrite -cats1 Hib.
                move => HB. 
                exists (take (index b X) X).
                rewrite -HB.
                cut := take_nth witness (index a X) X _ . smt.
                rewrite -cats1 Hia.
                move => HA. 
                exists (drop (index b X +1) (take (index a X) X)). 
                have ->: take (index b X + 1) X ++ drop (index b X + 1) (take (index a X) X) ++ [a]
                 = take (index a X + 1) X.
                  rewrite HA.
                have ->: take (index b X + 1) X = take (index b X + 1) (take (index a X) X). 
                  pose i:= index b X + 1.
                  pose j:= index a X.
                  cut := cat_take_drop j X . 
                  smt.
                smt.
                exists (drop (index a X +1) X).
                by rewrite cat_take_drop.
               move: Hea. elim => A B C HX.
               move: HuX. rewrite HX. 
               pose f:= (fun (p : upkey * (ident * cipher * sign)) => p.`1).
               rewrite ?map_cat ?cat_uniq.
               simplify. have ->: f b  = f a by rewrite /f Heqab //=. 
               have ->: f a \in map f A ++ [f a] ++ map f B. smt.
                by done.               
                           
           cut := (Hmust (Sanitize (map upack fbb{2})) _ _ _ Hxs Hys _ ). 
           + by rewrite San_uniq_fst. 
           + by done. 
           by smt. 
         rewrite filter_uniq map_inj_in_uniq.
         + smt. 
         + cut := San_uniq_fst (map upack fbb{2}).
           by exact uniq_map.
         
       + by rewrite /L H5.
       + by exact Hcheck.        

      by rewrite Hperm.       
      by rewrite (perm_eq_map _ _ _ Hp). 


  auto =>/>; progress. 
  cut := H1 X L.
  by smt.
qed.    
  
 

module DBB_Exp_hvote (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id,  hC, hNC, hN, r', 
        pi', ev', vbb, b,m,e, fbb, j, c,dL, hon_mem, val_bb, hCN;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id vbb);
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }

    hC <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.check)) 
                            (map rem_id vbb))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.check)) 
                               (map rem_id vbb)) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hC<- hC ++ [oget m];
      j <- j + 1;
    }

    (* votes of honest voters that didn't check *)
    hNC<- filter (predC (mem (map (rem_id \o remv) BW.check))) 
                 (map (rem_id \o remv) BW.hvote);
    j <- 0;
    hN <- [];
    while (j < size hNC){
      c <- nth witness hNC j;
      m <@ E(H).dec (BW.sk, c.`1,c.`2);
      hN <- hN ++ [oget m];
      j <- j + 1;
    }
    bool <- (forall (L: vote list),
             ! subseq L hN \/ 
             r <> Count (hC ++ L ++ dL));
    
    return ev /\ bool /\size dL <= BW.qCo  ;
  }
}.


local lemma subseq_perm_exists['a] (A B a: 'a list):
  perm_eq A B =>
  subseq a A =>
  exists b, perm_eq a b => subseq b B.
proof.
  move => HpAB HsaA.
  smt.
qed. 
  

          
local lemma dbbcheck_two_dbbhvote &m:
  Pr[DBB_Exp_check_two (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DBB_Exp_hvote (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  
  seq 17 17: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
                glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
                BW.uL, BW.qCo, BW.pk, BW.sk, bool, BW.bad, BW.uLL,BW.hk}/\ 
              BW.bb{1} =[] /\ 
              BW.coL{1} =[] /\ 
              BW.hvote{1} =[] /\
              BW.check{1} =[]/\
              BW.qVo{1} = 0/\
              bool{2} /\ BW.pk{2} = get_pk BW.sk{2}/\
              (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )). 
    wp; while (={i, BW.uL, glob SS, glob JRO.RO, BW.uLL}/\
               (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )).
      inline*; wp.
      call{1} (Kgen_get_upk JRO.RO).
      auto=>/>; progress.
      + case (id1 = nth witness (undup Voters) i{2}). smt.
        move => Hneq. rewrite get_set_eq oget_some //=.
        move: H2; rewrite get_set_neq. smt.
        move => H2. 
        smt.
      + case (nth witness (undup Voters) i{2} = id1). smt.
        move => Hneq; rewrite get_set_neq 1: Hneq //=.
        smt.        
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt.

  seq 7 7: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
              glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
              BW.uL, BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi', BW.hk, 
              fbb}/\ 
            bool{2}/\
            (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
            (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
            BW.bb{2} = (filter ((mem (map thr3 BW.uLL{2}))\o two4) (USanitize fbb{2}))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.check{2})/\
             uniq (map (rem_id \o remv) BW.hvote{2})).
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: ={glob E, glob SS, glob Pz, 
             glob JRO.RO, glob HRO.RO, glob GRO.RO}); first by sim.
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: true); first by sim.
    (* A.a2 call *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.hk,  
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
           (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb,BW.hk}/\
             0 <= i{2} /\
             (* uniq (map id BW.check) *)
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in BW.check{2} => 
                        x \in BW.hvote{2}) /\
             BW.pbb{2} = map (fun (x: ident * upkey * cipher * sign), 
                                  (x, hash' BW.hk{2} x)) BW.bb{2}/\
             (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
        auto =>/>; progress.
        + smt. 
        + move: H6; rewrite mem_cat mem_seq1; move => Hmor.
          case (x \in BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.   
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt. 
            rewrite -Hx. 
            move => HmS. 
            cut := San_mem (map pack4 BW.hvote{2}) (pack4 x) _. 
              + move: HmS; rewrite mapP.
                elim=> y HymS.     
                rewrite (andEr _ _ HymS).       
                have ->: pack4 (open4 y) = y by rewrite /open4 /pack4; smt.        
                rewrite (andEl _ _ HymS).
            rewrite mapP //=; elim => y HymS.
            have ->: x = y by  cut:= (andEr _ _ HymS); rewrite /pack4 ; smt.
            rewrite (andEl _ _ HymS). 
        + rewrite map_cat //=.  
          rewrite cats1 rcons_uniq (H1 H6) //=.
          move: H3 H4 H5 H6;
            pose L:= map open4 (Sanitize (map pack4 BW.hvote{2}));
            pose x:= nth witness L i{2}.
          move => H3 H4 H5 H6.      
          rewrite mapP negb_exists //=.
          move => a. rewrite negb_and.           
          case (!a \in BW.check{2}). smt.
          simplify. move => HmaC. 
          have HmaH: a \in BW.hvote{2} by rewrite H0.
          rewrite /(\o) /rem_id /remv //=.  
          have HmxH: x \in BW.hvote{2}. 
            rewrite /x /L. search mem Sanitize. 
            cut := San_mem (map pack4 BW.hvote{2}) 
                           (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) _. smt.
            rewrite mapP.
            elim => y Hymem. 
            have ->: nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} = 
                      open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}). smt.
            have ->: open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) = y. 
              by rewrite (andEr _ _ Hymem) /open4 /pack4 //=; smt.
            rewrite (andEl _ _ Hymem).
          
          move: H5. rewrite mapP negb_exists /fst5 //=.
          move => H5.
          cut := H5 x. rewrite //=.
          move => HmxC. rewrite ?andabP. 
            cut := uniq_nomem_map (* Hmust *) (map remv BW.hvote{2}) (remv x) (remv a) rem_id _ _ _ _.
             + rewrite -map_comp H6. 
             + rewrite mapP. exists x. rewrite HmxH //=.
             + rewrite mapP. exists a. rewrite HmaH //=.
             + smt.
            by rewrite /rem_id /remv //= ?andabP.
        + smt.
        auto=>/>; progress; try rewrite -USan_USanitize /hash' -map_comp /(\o) /preha; smt.

    + by proc. by proc. by proc.
     (* A.a1 call *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}/\
             BW.pk{2} = get_pk BW.sk{2}/\
             (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
             (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.hvote{2})).
    + proc. 
      auto=>/>; progress. smt. smt. smt. smt.
      have ->: forall (X: (ident * vote * upkey * cipher * sign) list) f,
                      let g = (rem_id \o remv) in
                      uniq (map g X) => uniq (map g (filter f X)).
      + move => X f g.      
        elim: X=>//=.     
        move => x X Hp Hand.
        case (!f x).
        + move => Hfn.   
          by rewrite Hfn //= Hp; first by rewrite (andEr _ _ Hand). 
        - simplify. 
          move => Hfx.
          rewrite Hfx //=.
          split. 
          + smt.
          + smt.
      + by done.
      by done.

    + proc. 
      sp; if =>//=.
      exists * BW.pk{2}, BW.sk{2}; elim* => pkx skx.
      pose Kp:= pkx = get_pk skx.
      have Hp: Kp \/ !Kp by smt.
      elim Hp.
      + move => Hp.
        inline*; wp; sp.
        seq 1 1: ( ={id0, v0, pk, usk, upk, o}/\
                 (pkx = BW.pk{2} /\ skx = BW.sk{2}) /\
                 id0{2} = id{2} /\ 
                 v0{2} = v{2} /\
                 pk{2} = BW.pk{2} /\ 
                 usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
                 upk{2} = get_upk usk{2} /\
                 o{2} = None /\
                 ={id, v, GRO.RO.m, HRO.RO.m, JRO.RO.m,  
                   glob SS, glob E, BW.bb, BW.pk, BW.qVo,
                   BW.hvote, BW.coL, BW.uL, BW.qCo, c} /\
                 BW.pk{2} = get_pk BW.sk{2} /\
                 (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
                 (forall (y : ident * vote * upkey * cipher * sign),
                     y \in BW.hvote{2} =>
                       (dec_cipher BW.sk{2} y.`3 y.`4 HRO.RO.m{2} <> None /\
                        ver_sign y.`3 y.`4 y.`5 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 (y.`1, y.`3, y.`4, y.`5))/\
                 (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
                 uniq (map (rem_id \o remv) BW.hvote{2}) /\
                 BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1})/\
                 dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2} <> None /\
                 v0{2} = oget (dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2})).
          exists* v0{1}, upk{2}; 
            elim* => vx upkx. 
          call{1} (Enc_dec_two skx pkx upkx vx Hp).
          by auto; progress; smt. 
        exists* usk{2}, upk{2}, c{2}; 
          elim* => uskx upkx cx.
        pose Kup:= upkx = get_upk uskx.
        have Hup: Kup \/ !Kup by smt.
        elim Hup.
        + move => Hup.
          call{2} (Sig_ver_two uskx upkx cx Hup ).
          auto; progress.
          + smt. smt. 
          + move: H8; rewrite mem_cat //=. 
            move => H8.
            case (y \in BW.hvote{2}). smt. 
            move => HnyH.
            move: H8; rewrite HnyH //=; move => H8.
            rewrite /two4 H8 /(\o) //= .
            cut := H id{2} H3. smt.
          + smt. 
          + rewrite map_cat //=.
            have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H7. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt. 
        - move => Hup_n.
          conseq (: _ ==> !upk{2} = get_upk usk{2}) =>/>.
          call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
          call{2} (SSs_ll SS JRO.RO JRO.RO_o_ll). 
          by auto; progress; smt.
              
      - move => Hp_n.
        conseq (: _ ==> !BW.pk{2} = get_pk BW.sk{2}) =>/>.
        inline *; wp.
        call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
        call{2} (SSs_ll SS JRO.RO JRO.RO_o_ll). 
        call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
        call{2} (Ee_ll HRO.RO HRO.RO_o_ll).
        by auto; progress; smt.        

    (* H, G, J *)
    + by proc.     
    + by proc.
    + by proc.  
   
    by auto => />; progress. 
    
     

  seq 14 14: (={dL,bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
              fbb, hC} /\ 
            bool{2}/\
            (* (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in  Sanitize' BW.bb{2})/\*)
            BW.bb{2} = filter (mem (map thr3 BW.uLL{2}) \o two4) (USanitize fbb{2})/\
            (let f = (fun (x : ident * upkey * cipher * sign) =>
                     (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                      ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                      (\o) (mem (map thr3 BW.uLL{2})) two4 x) in
            vbb{2} = filter f (USanitize fbb{2}) /\
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})).

    inline *; wp. 
    while( ={glob HRO.RO, glob JRO.RO, glob E, glob SS, BW.sk, 
             BW.check, vbb, hC, j}/\
            0 <= j{2} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id vbb{2}) in
                   hC{2} = map g (take j{2} xbb)).
                 
      wp; sp.
      exists* (glob E){1}, BW.sk{1}, c{1}; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      call{2} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
    wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, BW.sk, dL});
      first by sim.
    wp; while (={j, hon_mem, val_bb, glob E, glob HRO.RO, BW.sk, hCN});
      first by sim.
    wp; while ( ={ i, BW.bb, BW.sk, glob E, glob SS, glob HRO.RO, glob JRO.RO, vbb}/\
            0<=i{2} <= size BW.bb{2}/\
            let f = fun (x: ident * upkey * cipher * sign),
                        dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).
      wp; sp. 
      exists* (glob SS){1}, (glob E){1}, BW.sk{1}, b{1}; elim* => gs ge skx bx.
      call{1} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{2} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{1} (dec_Dec_one ge skx bx.`2 bx.`3).
      call{2} (dec_Dec_one ge skx bx.`2 bx.`3).
      auto=>/>; progress.
      + smt. smt. smt. smt. smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite filter_rcons H2. 
    auto=>/>; progress. 
    + smt. smt. smt. 
    + rewrite take_oversize; first by smt.
      by rewrite -filter_predI /predI.

  seq 2 4: ( ={dL,ev, bool, hC, BW.qCo, r}/\ 
             perm_eq hN{1} hN{2}). 

     
  wp; while{2} (0 <= j{2} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   hN{2} = map g (take j{2} hNC{2}))
               (size hNC{2} - j{2})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.            
  auto=>/>; progress.
  + by rewrite take0.  + smt.
  + pose f:= fun (x : ident * upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}).
    pose g:= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}).
    move: H5; rewrite -/g; move => Hg.
    have H3': forall y, y \in BW.hvote{2} => y.`2 = f (remv y). smt.
    have ->: (take j_R (filter (predC (mem (map (rem_id \o remv) BW.check{2})))
           (map (rem_id \o remv) BW.hvote{2}))) =
             (filter (predC (mem (map (rem_id \o remv) BW.check{2})))
           (map (rem_id \o remv) BW.hvote{2})) by smt.
    
    cut Hk:= nth_vote' (filter (predC (mem BW.check{2})) BW.hvote{2}) f _.
    + move => x Hxm.
      smt.
    rewrite -Hk. 
    cut Hc:= nth_vote (map remv (filter (predC (mem BW.check{2})) BW.hvote{2}))
                       (filter (predC (mem BW.check{2})) BW.hvote{2}) f _ _.
    + by done.
    + move => x Hxm.
      smt.
    rewrite Hc.
    have ->: f = g \o rem_id by rewrite /(\o) /f /g /rem_id.
    rewrite filter_map -?map_comp. 
    have ->: preim (rem_id \o remv) 
                     (predC (mem (map (rem_id \o remv) BW.check{2}))) = 
               (predC (mem (map (rem_id \o remv) BW.check{2}))) \o rem_id \o remv .
        by rewrite /preim.
    rewrite (perm_eq_map (g \o rem_id \o remv)).
    pose riv:= (rem_id \o remv).
    have ->: (predC (mem (map riv BW.check{2})) \o rem_id \o remv) =
             (predC (mem (map riv BW.check{2})) \o riv) by rewrite /riv.
    have eq_filter_mem: forall (A : (ident * vote * upkey * cipher * sign) list) p1 p2,
               (forall x, x\in A => (p1 x <=> p2 x)) => filter p1 A = filter p2 A
               by move => A p1 p2; elim: A => //= x l Ha; smt.      
    rewrite (eq_filter_mem BW.hvote{2} (predC (mem (map riv BW.check{2})) \o riv) 
                                           (predC (mem BW.check{2})) _ ).
          move => x HxmH.
          progress. 
          + move: H5. rewrite /(\o) /predC. 
            rewrite mapP negb_exists //=. 
            by move => Hac; cut := Hac x; rewrite //=.
          - move: H5. rewrite /(\o) /predC.
            move => Hpc.
            rewrite mapP negb_exists //=.
            move => a.
            rewrite negb_and.
            case (riv x <> riv a). done.
            simplify. move => Heq_xa.
            move: H4; rewrite -/riv; move => Hu4.
            case (a = x).
            + move => Heq. 
              by rewrite Heq Hpc. 
            move => Hneq.
            case (a \in BW.hvote{2}).          
            + move => HmaH. 
              cut Heq:= uniq_mem_map BW.hvote{2} a x riv Hu4 HmaH HxmH _ ; first by rewrite Heq_xa.
              by rewrite Heq Hpc.
            move => Hnm. 
            cut Himp:= H2 x.  
            by smt.
          by rewrite perm_eq_refl.

   auto=>/>; progress. 
     cut := H1 hN{2} L. 
     by rewrite perm_eq_sym H.
qed.      


module DBB_Exp_hvote_two (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id,  hC, hN, r', 
        pi', ev', vbb, b,m,e, fbb, j, c, dL, hon_mem, val_bb;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hC <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.check)) 
                            (map rem_id vbb))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.check)) 
                               (map rem_id vbb)) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hC<- hC ++ [oget m];
      j <- j + 1;
    }
   
    hN <- [];
    j <- 0;
    while (j < size (filter (predI (predC (mem (map (rem_id \o remv) BW.check)))
                                   (mem (map (rem_id \o remv) BW.hvote)))
                            (map rem_id vbb))
          ){
      c <- nth witness (filter (predI (predC (mem (map (rem_id \o remv) BW.check)))
                                      (mem (map (rem_id \o remv) BW.hvote)))
                               (map rem_id vbb)) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hN<- hN ++ [oget m];
      j <- j + 1;
    }
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id vbb);

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    bool <- r <> Count (hC ++ hN ++ dL);

    return ev /\ bool /\ size dL <= BW.qCo ;
  }
}.

local lemma exists_two_elem['a] (X : 'a list) c b:
  c \in X => b \in X =>
  index c X < index b X =>
  exists A B C, X = A ++ [c] ++ B ++ [b] ++ C.
proof.
  move => Hma Hmb Hiab.
  cut Hia:= nth_index witness c X Hma.
  cut Hib:= nth_index witness b X Hmb.
  cut := take_nth witness (index c X) X _; 
    first by smt.
  rewrite -cats1 Hia.
  move => HA. 
  exists (take (index c X) X).
  rewrite -HA.
  cut := take_nth witness (index b X) X _ . smt.
  rewrite -cats1 Hib.
  move => HB. 
  exists (drop (index c X +1) (take (index b X) X)). 
  have ->: take (index c X + 1) X ++ drop (index c X + 1) (take (index b X) X) ++ [b] = 
           take (index b X + 1) X.
    rewrite HB.
    have ->: take (index c X + 1) X = take (index c X + 1) (take (index b X) X). 
      cut := cat_take_drop (index b X) X.
      by smt. 
    by smt.
  exists (drop (index b X +1) X).
  by rewrite cat_take_drop.
qed.

local lemma index_diff['a] (X: 'a list) c b:
  c \in X => b \in X => c <> b =>
  index c X <> index b X.
proof. smt. qed.


local lemma dbbhvote_dbbhvote_two &m:
  Pr[DBB_Exp_hvote (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[DBB_Exp_hvote_two (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  
  seq 17 17: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
                glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
                BW.uL, BW.qCo, BW.pk, BW.sk, bool, BW.bad, BW.uLL,BW.hk}/\ 
              BW.bb{1} =[] /\ 
              BW.coL{1} =[] /\ 
              BW.hvote{1} =[] /\
              BW.check{1} =[]/\
              BW.qVo{1} = 0/\
              bool{2} /\ BW.pk{2} = get_pk BW.sk{2}/\
              (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )). 
    wp; while (={i, BW.uL, glob SS, glob JRO.RO, BW.uLL}/\
               (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )).
      inline*; wp.
      call{1} (Kgen_get_upk JRO.RO).
      auto=>/>; progress.
      + case (id1 = nth witness (undup Voters) i{2}). smt.
        move => Hneq. rewrite get_set_eq oget_some //=.
        move: H2; rewrite get_set_neq. smt.
        move => H2. 
        smt.
      + case (nth witness (undup Voters) i{2} = id1). smt.
        move => Hneq; rewrite get_set_neq 1: Hneq //=.
        smt.
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt.

  seq 7 7: (={glob A, glob Pz, glob SS, glob E, glob Vz, glob HRO.RO, BW.qVo,
              glob GRO.RO, glob JRO.RO, BW.bb, BW.coL, BW.hvote, BW.check, 
              BW.uL, BW.qCo, BW.pk, BW.sk, bool, r, ev, ev', r', pi', BW.hk,
              fbb}/\ 
            bool{2}/\
            (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
            BW.bb{2} = filter (mem (map thr3 BW.uLL{2}) \o two4) (USanitize fbb{2}) /\
            (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.check{2})/\
             uniq (map (rem_id \o remv) BW.hvote{2})).
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: ={glob E, glob SS, glob Pz, 
             glob JRO.RO, glob HRO.RO, glob GRO.RO}); first by sim.
    call(: ={glob Vz, glob GRO.RO}); first by sim.
    call(: true); first by sim.
    (* A.a2 call *)
    call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.hk, 
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in map remv BW.check{2} => 
                exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
           (forall x, x \in BW.check{2} => x \in BW.hvote{2})/\
           (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb,BW.hk}/\
             0 <= i{2} /\
             (* uniq (map id BW.check) *)
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in BW.check{2} => 
                        x \in BW.hvote{2})/\
             (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
             BW.pbb{2} = map (fun (x: ident * upkey * cipher * sign), 
                                  (x, hash' BW.hk{2} x)) BW.bb{2}/\
             (uniq (map (rem_id \o remv) BW.hvote{2}) => 
                  uniq (map (rem_id \o remv) BW.check{2}))).
        auto =>/>; progress.
        + smt. 
        + move: H7; rewrite mem_cat mem_seq1; move => Hmor.
          case (x \in BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.   
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt. 
            rewrite -Hx. 
            move => HmS. 
            cut := San_mem (map pack4 BW.hvote{2}) (pack4 x) _. 
              + move: HmS; rewrite mapP.
                elim=> y HymS.     
                rewrite (andEr _ _ HymS).       
                have ->: pack4 (open4 y) = y by rewrite /open4 /pack4; smt.        
                rewrite (andEl _ _ HymS).
            rewrite mapP //=; elim => y HymS.
            have ->: x = y by  cut:= (andEr _ _ HymS); rewrite /pack4 ; smt.
            rewrite (andEl _ _ HymS).
        + move: H7; rewrite map_cat //= mem_cat mem_seq1; move => Hmem.
          case (x \in map remv BW.check{2}). smt.
          (* use the exists id *)
          move => Hn.
          have Hx: x = remv (nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2})
            by move :Hmem; rewrite Hn.
          move: H5.
          (* rewrite -USan_USanitize. *)
          rewrite /preha //= mapP.
          elim => y [Hym [Hy1 Hy2 Hy3 Hy4]].      
          exists y.`1.
          rewrite Hx Hy2 Hy3 Hy4 //=.    
          smt.    
        + rewrite map_cat //=.  
          rewrite cats1 rcons_uniq (H2 H7) //=.
          move: H3 H4 H5 H6;
            pose L:= map open4 (Sanitize (map pack4 BW.hvote{2}));
            pose x:= nth witness L i{2}.
          move => H3 H4 H5 H6.      
          rewrite mapP negb_exists //=.
          move => a. rewrite negb_and.           
          case (!a \in BW.check{2}). smt.
          simplify. move => HmaC. 
          have HmaH: a \in BW.hvote{2} by rewrite H0.
          rewrite /(\o) /rem_id /remv //=.  
          have HmxH: x \in BW.hvote{2}. 
            rewrite /x /L. search mem Sanitize. 
            cut := San_mem (map pack4 BW.hvote{2}) 
                           (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) _. smt.
            rewrite mapP.
            elim => y Hymem. 
            have ->: nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} = 
                      open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}). smt.
            have ->: open4 (nth witness (Sanitize (map pack4 BW.hvote{2})) i{2}) = y. 
              by rewrite (andEr _ _ Hymem) /open4 /pack4 //=; smt.
            rewrite (andEl _ _ Hymem).
          
          move: H6. rewrite mapP negb_exists /fst5 //=.
          move => H6.
          cut := H6 x. rewrite //=.
          move => HmxC. rewrite ?andabP. 
          have Hmust: forall (X: (ident * upkey * cipher * sign) list) a b,
                       uniq (map rem_id X) => 
                       a \in X => 
                       b \in X => 
                      a <> b =>
                      rem_id a <> rem_id b. 
          + move => X c b HuX Hma Hmb Hneq. 
            case (index c X < index b X).
            + move => Hiab.
              cut Hea:= exists_two_elem X c b Hma Hmb Hiab.
              move: Hea. elim => A B C HX.
              move: HuX. rewrite HX. 
              rewrite ?map_cat ?cat_uniq.
              simplify. 
              case(rem_id b  = rem_id c).  
              + move => Heq.
                have ->: rem_id b \in map rem_id A ++ [rem_id c] ++ map rem_id B. smt.
                by done.               
              by smt.  
            - move => Hiab.   
              cut Hi_dif:= index_diff X c b Hma Hmb Hneq.
              have Hiab': index b X < index c X by smt.
              cut Hea:= exists_two_elem X b c Hmb Hma Hiab'.
              move: Hea. elim => A B C HX.
              move: HuX. rewrite HX. 
              rewrite ?map_cat ?cat_uniq.
              simplify. 
              case(rem_id b  = rem_id c). 
              + move => Heq.
                have ->: rem_id c \in map rem_id A ++ [rem_id b] ++ map rem_id B. smt.
                by done.               
              by smt.
            cut := Hmust (map remv BW.hvote{2}) (remv a) (remv x) _ _ _ _.
             + rewrite -map_comp H7. 
             + rewrite mapP. exists a. rewrite HmaH //=.
             + rewrite mapP. exists x. rewrite HmxH //=.
             + smt.
            rewrite /rem_id /remv //= ?andabP.
            by smt.
        + smt.

      by auto; progress; try rewrite -USan_USanitize /hash' -map_comp /preha /(\o). 

    (* H, G, J *)
    + by proc. 
    + by proc. 
    + by proc.

    (* A.a1 call *)
    wp; call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}/\
             BW.pk{2} = get_pk BW.sk{2}/\
             (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
             (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x in
             forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
             (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
              forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
             uniq (map (rem_id \o remv) BW.hvote{2})).
    + proc. 
      auto=>/>; progress. 
      + smt. smt. smt. smt. 
      have ->: forall (X: (ident * vote * upkey * cipher * sign) list) f,
                      let g = (rem_id \o remv) in
                      uniq (map g X) => uniq (map g (filter f X)).
      + move => X f g.      
        elim: X=>//=.     
        move => x X Hp Hand.
        case (!f x).
        + move => Hfn.   
          by rewrite Hfn //= Hp; first by rewrite (andEr _ _ Hand). 
        - simplify. 
          move => Hfx.
          rewrite Hfx //=.
          split. 
          + smt.
          + smt.
      + by done.
      by done.

    + proc. 
      sp; if =>//=.
      exists * BW.pk{2}, BW.sk{2}; elim* => pkx skx.
      pose Kp:= pkx = get_pk skx.
      have Hp: Kp \/ !Kp by smt.
      elim Hp.
      + move => Hp.
        inline*; wp; sp.
        seq 1 1: ( ={id0, v0, pk, usk, upk, o}/\
                 (pkx = BW.pk{2} /\ skx = BW.sk{2}) /\
                 id0{2} = id{2} /\ 
                 v0{2} = v{2} /\
                 pk{2} = BW.pk{2} /\ 
                 usk{2} = (oget BW.uL{2}.[id{2}]).`2 /\
                 upk{2} = get_upk usk{2} /\
                 o{2} = None /\
                 ={id, v, GRO.RO.m, HRO.RO.m, JRO.RO.m,  
                   glob SS, glob E, BW.bb, BW.pk, BW.qVo,
                   BW.hvote, BW.coL, BW.uL, BW.qCo, c} /\
                 BW.pk{2} = get_pk BW.sk{2} /\
                 (forall id, BW.uL.[id]{1} <> None =>
                         (id, (oget BW.uL.[id]{1}).`2, (oget BW.uL.[id]{1}).`1) \in BW.uLL{2} /\
                         (oget BW.uL.[id]{1}).`1 = get_upk (oget BW.uL.[id]{1}).`2 )/\
                 (let fd = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x in
                  forall y, y \in BW.hvote{2} => fd (y.`1,y.`3,y.`4,y.`5))/\
                 (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
                 uniq (map (rem_id \o remv) BW.hvote{2}) /\
                 BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1})/\
                 dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2} <> None /\
                 v0{2} = oget (dec_cipher BW.sk{2} upk{2} c{2} HRO.RO.m{2})).
          exists* v0{1}, upk{2}; 
            elim* => vx upkx. 
          call{1} (Enc_dec_two skx pkx upkx vx Hp).
          by auto; progress; smt. 
        exists* usk{2}, upk{2}, c{2}; 
          elim* => uskx upkx cx.
        pose Kup:= upkx = get_upk uskx.
        have Hup: Kup \/ !Kup by smt.
        elim Hup.
        + move => Hup.
          call{2} (Sig_ver_two uskx upkx cx Hup ).
          auto; progress.
          + smt. smt. 
          + move: H8; rewrite mem_cat //=. 
            move => H8.
            case (y \in BW.hvote{2}). smt. 
            move => HnyH.
            move: H8; rewrite HnyH //=; move => H8.
            rewrite /two4 H8 /(\o) //= .
            cut := H id{2} H3. smt.
          + smt. 
          + rewrite map_cat //=.
            have ->: [(\o) rem_id remv 
                      (id{2}, oget (dec_cipher BW.sk{2} (get_upk (oget BW.uL{2}.[id{2}]).`2) 
                                    c{2} HRO.RO.m{2}), 
                      get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)] =
                     [(get_upk (oget BW.uL{2}.[id{2}]).`2, c{2}, result_R)]
             by rewrite /rem_id /remv /(\o) //=.
            rewrite cats1 rcons_uniq H2 /rem_id /remv /(\o) //=. 
            move: H7. rewrite mapP. rewrite negb_exists //=.
            move => Hpp.
            rewrite mapP.
            rewrite negb_exists //=.
            move => a.
            cut := Hpp a. rewrite ?negb_and. move => Hpa.
            smt. 
        - move => Hup_n.
          conseq (: _ ==> !upk{2} = get_upk usk{2}) =>/>.
          call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
          call{2} (SSs_ll SS JRO.RO JRO.RO_o_ll). 
          by auto; progress; smt.
              
      - move => Hp_n.
        conseq (: _ ==> !BW.pk{2} = get_pk BW.sk{2}) =>/>.
        inline *; wp.
        call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
        call{2} (SSs_ll SS JRO.RO JRO.RO_o_ll). 
        call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
        call{2} (Ee_ll HRO.RO HRO.RO_o_ll).
        by auto; progress; smt.        

    (* H, G, J *)
    + by proc.     
    + by proc.
    + by proc.  
   
    by auto => />; progress.  
        
  seq 5 3: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
              fbb, vbb, BW.sk, HRO.RO.m, glob E} /\ 
            bool{2}/\
            (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
            BW.bb{2} = filter ((mem (map thr3 BW.uLL{2}))\o two4) (USanitize fbb{2})/\
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x  in
            vbb{2} = filter f (USanitize fbb{2}) /\
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id vbb{1}).
     wp; while ( ={ i, BW.bb, BW.sk, glob E, glob SS, glob HRO.RO, glob JRO.RO, vbb}/\
            0<=i{2} <= size BW.bb{2}/\
            let f = fun (x: ident * upkey * cipher * sign),
                        dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
            vbb{2} = filter f (take i{2} BW.bb{2})).
      wp; sp. 
      exists* (glob SS){1}, (glob E){1}, BW.sk{1}, b{1}; elim* => gs ge skx bx.
      call{1} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{2} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
      call{1} (dec_Dec_one ge skx bx.`2 bx.`3).
      call{2} (dec_Dec_one ge skx bx.`2 bx.`3).
      auto=>/>; progress.
      + smt. smt. smt. smt. smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite filter_rcons H2.      
     auto=>/>; progress. 
     + smt. smt.  
     + rewrite take_oversize; first by smt.
       by rewrite -filter_predI /predI.

seq 3 0: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
              fbb, vbb, BW.sk, HRO.RO.m, glob E} /\ 
            bool{2}/\
            (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
            BW.bb{2} = filter ((mem (map thr3 BW.uLL{2}))\o two4) (USanitize fbb{2})/\
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x  in
            vbb{2} = filter f (USanitize fbb{2}) /\
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id vbb{1}).
   exists*  (glob E){1}; elim* => ge.
   while{1} (ge =(glob E){1}) (size (filter hon_mem val_bb){1} - j{1}); progress.
      sp; exists* BW.sk, c; elim* => skx bx.
      wp; call{1} (dec_Dec_one ge skx bx.`1 bx.`2).
      auto=>/>;  smt.
   auto=>/>; progress. smt. 

seq 3 0: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
              fbb, vbb, BW.sk, HRO.RO.m, glob E} /\ 
            bool{2}/\
            (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
            BW.bb{2} = filter ((mem (map thr3 BW.uLL{2}))\o two4) (USanitize fbb{2})/\
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x  in
            vbb{2} = filter f (USanitize fbb{2}) /\
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id vbb{1}/\
            dL{1} = map (fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) 
                        (filter (predC hon_mem) val_bb){1}).
   exists* (glob E){1}; elim* => ge.
   wp; while{1} (={HRO.RO.m, BW.sk}/\ (glob E){1}=ge/\
                 0<=j{1}<= size (filter (predC hon_mem) val_bb){1} /\
                 (let g= fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in 
                  dL{1} = map g (take j{1} (filter (predC hon_mem) val_bb){1})))
                 (size (filter (predC hon_mem) val_bb){1} - j{1}); progress.
     sp; exists* BW.sk, c; elim* => skx bx.
     wp; call{1} (dec_Dec_one ge skx bx.`1 bx.`2).
     auto=>/>; progress.  
     + smt. smt. 
     + rewrite (take_nth witness). smt. 
       smt.
     + smt.
   by auto=>/>; progress; smt.
seq 3 3: (={bool, ev, BW.qCo, r, BW.hvote, BW.check, BW.bb, 
              fbb, vbb, BW.sk, HRO.RO.m, glob E, hC} /\ 
            bool{2}/\
            (forall x, x \in map remv BW.check{2} => 
                        exists id, (id,x.`2,x.`3,x.`4) \in USanitize BW.bb{2})/\
            BW.bb{2} = filter ((mem (map thr3 BW.uLL{2}))\o two4) (USanitize fbb{2})/\
            (let f = fun (x : ident * upkey * cipher * sign) =>
                       (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                       ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
                       (\o) (mem (map thr3 BW.uLL{2})) two4 x  in
            vbb{2} = filter f (USanitize fbb{2}) /\
            (forall (y : ident * vote * upkey * cipher * sign),
                    y \in BW.hvote{2} => f (y.`1, y.`3, y.`4, y.`5)))/\
            (forall x, x \in BW.check{2} => x \in BW.hvote{2}) /\
            (let f= fun (x : ident * upkey * cipher * sign) =>
                          oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}) in
                  forall x, x \in BW.hvote{2} => x.`2 = f (remv x))/\
            uniq (map (rem_id \o remv) BW.check{2})/\
            uniq (map (rem_id \o remv) BW.hvote{2})/\
            hon_mem{1} = mem (map (rem_id \o remv) BW.hvote){1} /\
            val_bb{1} = map rem_id vbb{1}/\
            dL{1} = map (fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) 
                        (filter (predC hon_mem) val_bb){1}).
   while( ={glob HRO.RO, glob E, BW.sk, 
             BW.check, vbb, hC, j}/\
            0 <= j{2} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id vbb{2}) in
                   hC{2} = map g (take j{2} xbb)).
                 
      wp; sp.
      exists* (glob E){1}, BW.sk{1}, c{1}; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      call{2} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
   auto=>/>; progress; smt. 

  seq 4 5: ( ={ev, bool, hC, BW.qCo, r} /\  
             (exists X, perm_eq X hN{2} /\ subseq X hN{1})/\
            dL{1} = map (fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) 
                        (filter (predC hon_mem) val_bb){2}
             (* hN{1} = hN{2}*)). 
    
  wp; while{2} (0 <= j{2} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                let xbb = filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                         (mem (map (rem_id \o remv) BW.hvote{2})))
                                 (map rem_id vbb{2}) in
                   hN{2} = map g (take j{2} xbb))
               (size (filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                   (mem (map (rem_id \o remv) BW.hvote{2})))
                            (map rem_id vbb{2})) - j{2})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.     
  wp; while{1} ( ={BW.sk, HRO.RO.m} /\ 0 <= j{1} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   hN{1} = map g (take j{1} hNC{1}))
               (size hNC{1} - j{1})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.            

  auto=>/>; progress. 
  + smt. smt. rewrite take0. smt. smt. 
  pose fd:= (fun (x : ident * upkey * cipher * sign) =>
              (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
              ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}) /\
              (\o) (mem (map thr3 BW.uLL{2})) two4 x).
    pose f:= fun (x : ident * upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2}).
    pose g:= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}).
    pose pc:= predC (mem (map (rem_id \o remv) BW.check{2})).
    pose mH:= (map (rem_id \o remv) BW.hvote{2}).
  
    move: H8 H6; rewrite -/fd -/pc -/mH -/g; move => H8 H6.
    have H1': forall y, y \in BW.hvote{2} => fd (remv y). smt.
    have H3': forall y, y \in BW.hvote{2} => y.`2 = (g \o rem_id \o remv) y. 
        by rewrite /g /rem_id /remv /(\o) //=. 
    rewrite !take_oversize; first 2 by smt.
    pose BB:= (map rem_id (filter fd (USanitize fbb{2}))). 
    (* is uniq, because every (u,c,s) has a uniq u set by USanitize *)
    (* rewrite /p. *)

    cut := uniq_perm_eq_filter_two (filter (mem BB) (filter pc mH)) BB _ _ _.
    + rewrite /BB /Sanitize' /USanitize. 
      rewrite uniq_filter -map_comp.      
      cut := San_uniq_fst (map upack fbb{2}).
      pose L:= (Sanitize (map upack fbb{2})).
      rewrite /rem_id /remi /uopen /(\o) //=.
      have Ho: forall (A: (upkey * (ident * cipher * sign)) list), 
               uniq (map fst A) =>
               uniq (map (fun (x : upkey * (ident * cipher * sign)) => 
                                     (x.`1, x.`2.`2, x.`2.`3)) A).
         elim =>//=. 
         move => x A HuA Hand.
         rewrite (HuA _); first  by rewrite (andEr _ _ Hand).
         cut := andWl _ _ Hand. 
         rewrite //=.
         rewrite ?mapP ?negb_exists //=.
         smt.
      by exact (Ho L).
      
    + by rewrite !filter_uniq H5.
    + move => b.
      by rewrite mem_filter. 
    have ->: (filter (mem (filter (mem BB) (filter pc mH))) BB) = 
             filter (predI pc (mem mH)) BB. 
      have eq_filter_mem: forall (A : (upkey * cipher * sign) list) p1 p2,
            (forall x, x\in A => (p1 x <=> p2 x)) => filter p1 A = filter p2 A.
         move => A p1 p2. 
         elim: A => //= x l Hp. 
         by smt. 
      have ->: forall (A B : (upkey * cipher * sign) list),
               filter (mem (filter (mem A) B)) A = filter (mem B) A. 
      + move => A B.
        rewrite (eq_filter_mem A (mem (filter (mem A) B)) (mem B) _ ).
        + move => x HxA. 
          by smt.
        by done.
      - rewrite (eq_filter (mem (filter pc mH)) (predI pc (mem mH)) BB).
        + move => x. 
          by smt.
        by done.
    move => Hp. 
    have Hpp:= filter_subseq (mem BB) (filter pc mH).
    exists (map g (filter (mem BB) (filter pc mH))).
    rewrite perm_eq_map 1: Hp.
    by rewrite Hsub_map 1: Hpp.

wp; while{2} (0<=j{2}<= size (filter (predC hon_mem) val_bb){2} /\
                 (let g= fun (x : upkey * cipher * sign) =>
                         oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in 
                  dL{2} = map g (take j{2} (filter (predC hon_mem) val_bb){2})))
                 (size (filter (predC hon_mem) val_bb){2} - j{2}); progress.
     sp; exists*  (glob E), BW.sk, c; elim* => ge skx bx.
     wp; call{1} (dec_Dec_one ge skx bx.`1 bx.`2).
     auto=>/>; progress.  
     + smt. smt. 
     + rewrite (take_nth witness). smt. 
       smt.
     + smt.
auto=>/>; progress. 
  smt. smt. smt.
  rewrite take_oversize; first by smt.
  cut := H5 X. rewrite H0 //= ?Count_split. smt.
  smt. 
qed. 


(*  *)
module DBB_Exp_hvote_three (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id,  hCN, r', 
        pi', ev', vbb, b,m,e, fbb, j, c, hon_mem, val_bb, dL;
    BW.bad <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];
    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hCN <- [];
    j <- 0;
    while (j < size (filter (mem (map (rem_id \o remv) BW.hvote)) 
                            (map rem_id vbb))
          ){
      c <- nth witness (filter (mem (map (rem_id \o remv) BW.hvote)) 
                               (map rem_id vbb)) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id vbb);

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    bool <- r <> Count (hCN ++ dL);
    
    return ev /\ bool /\ size dL <= BW.qCo ;
  }
}.


local lemma dbbhvotetwo_dbbhvotethree &m:
  Pr[DBB_Exp_hvote_two (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] =
  Pr[DBB_Exp_hvote_three (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  seq 27 27 : ( ={glob E, glob HRO.RO, BW.check, BW.hvote, BW.qCo, 
                 BW.sk, vbb, ev, bool, r, BW.uLL,BW.hk} /\
                (forall x, x \in map (rem_id \o remv) BW.check{2} =>
                           x \in map (rem_id \o remv) BW.hvote{2})).
  while (={i, BW.bb, BW.sk, vbb, glob E, glob SS, glob JRO.RO, glob HRO.RO});
    first by sim.
  wp; call (: ={glob Vz, glob GRO.RO}); first by sim.
  wp; call (: ={glob E, glob SS, glob Pz, glob HRO.RO, glob GRO.RO, glob JRO.RO});
    first by sim.
  wp; call (: ={glob Vz, glob GRO.RO}); first by sim.
  wp; call (: true); first by sim.
  call(: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.hk,  
             BW.bb, BW.pk, BW.hvote, BW.uL, BW.check, BW.bad}/\
           (forall x, x \in map (rem_id \o remv) BW.check{2} =>
                           x \in map (rem_id \o remv) BW.hvote{2})).
    + proc.
      inline *; wp.
      while( ={i, pL, id, BW.check, BW.pbb, BW.bb,BW.hk}/\
             0 <= i{2} /\
             (* uniq (map id BW.check) *)
             pL{2} = map open4 (Sanitize (map pack4 BW.hvote{2}))/\
             (forall x, x \in map (rem_id \o remv) BW.check{2} =>
                           x \in map (rem_id \o remv) BW.hvote{2})).
        auto =>/>; progress.
        + smt. 
        + move: H5; rewrite map_cat mem_cat //=; move => Hmor. 
          case (x \in map (rem_id \o remv) BW.check{2}).
          + move => Hmem.
            by rewrite (H0 x Hmem).
          + move =>Hnmem.
            move: Hmor; rewrite Hnmem //=; move => Hx.         
            cut :=mem_nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2} _ . 
              + by smt.
            pose y:= nth witness (map open4 (Sanitize (map pack4 BW.hvote{2}))) i{2}.
            move => HyS.
            have HH: y \in BW.hvote{2}.
             
              cut := San_mem (map pack4 BW.hvote{2}) (pack4 y) _.
                move: HyS; rewrite mapP. smt.
              rewrite mapP. smt.
            rewrite Hx -/y.
            rewrite mapP. exists y. 
            by rewrite HH //=.
        + smt.
      auto=>/>; smt.
    + by proc. by proc. by proc.    
    wp; call (: ={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo}); first 5 by sim.
    wp; while (={BW.uLL, i, BW.uL, glob SS, glob JRO.RO}). sim.
    call (: ={glob E}); first by sim.
    call (: true ==> ={glob JRO.RO}); first by sim.
    call (: true ==> ={glob GRO.RO}); first by sim.
    call (: true ==> ={glob HRO.RO}); first by sim.
    by auto =>/>.

  wp; while (={j, hon_mem, val_bb, BW.sk, glob E, HRO.RO.m, dL}); first by sim.  

  wp; while{1} ( ={glob E, glob HRO.RO, BW.sk, vbb, BW.check, BW.hvote}/\
                0 <= j{1} /\
                let g= fun (x : upkey * cipher * sign) =>
                           oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                let xbb = filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                         (mem (map (rem_id \o remv) BW.hvote{2})))
                                 (map rem_id vbb{2}) in
                   hN{1} = map g (take j{1} xbb))
               (size (filter (predI (predC (mem (map (rem_id \o remv) BW.check{2})))
                                   (mem (map (rem_id \o remv) BW.hvote{2})))
                            (map rem_id vbb{2})) - j{1})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.              

    wp; while{1} ( ={glob HRO.RO, glob E, BW.sk, 
                 BW.check, vbb}/\
               0 <= j{1} /\
                   let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                   let xbb = filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id vbb{2}) in
                   hC{1} = map g (take j{1} xbb))
              (size (filter (mem (map (rem_id \o remv) BW.check{2})) 
                                     (map rem_id vbb{2})) -j{1}); progress.
                 
      wp; sp.
      exists* (glob E), BW.sk, c; elim* => ge skx cx.
      call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite map_rcons cats1 //=. 
      + smt.
    
  wp; while{2} (={glob E, glob HRO.RO} /\0 <= j{2} /\
                let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
                let xbb = filter (mem (map (rem_id \o remv) BW.hvote{2}))
                                 (map rem_id vbb{2}) in
                   hCN{2} = map g (take j{2} xbb))
               (size (filter (mem (map (rem_id \o remv) BW.hvote{2}))
                            (map rem_id vbb{2})) - j{2})=>/>; progress.
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge skx cx.
    call{1} (dec_Dec_one ge skx cx.`1 cx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite map_rcons cats1 //=. 
    + smt.            
  auto=>/>; progress.
  + smt. smt. smt. smt. smt. smt.
  + pose BB:= (map rem_id vbb{2}).
    pose C:= (map (rem_id \o remv) BW.check{2}).
    pose HO:= (map (rem_id \o remv) BW.hvote{2}).
    pose g:= (fun (x : upkey * cipher * sign) =>
            oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})).
    move: H1 H3 H H0;
      rewrite -/BB -/C -/HO; move => H1 H3 H H0.
    have ->: (take j_L (filter (mem C) BB)) = filter (mem C) BB by smt.
    have ->: (take j_L0 (filter (predI (predC (mem C)) (mem HO)) BB)) =
             (filter (predI (predC (mem C)) (mem HO)) BB) by smt.
    have ->: (take j_R (filter (mem HO) BB)) = (filter (mem HO) BB) by smt. 
    rewrite -?map_cat.
    have Hperm: perm_eq (map g (filter (mem C) BB ++
                                filter (predI (predC (mem C)) (mem HO)) BB))
                        (map g (filter (mem HO) BB)).
      rewrite perm_eq_map. 
      have ->: filter (mem C) BB = filter (predI (mem C) (mem HO)) BB.
        rewrite (eq_filter (predI (mem C) (mem HO)) (mem C) BB).
          move => x. rewrite /predI. smt.
        by done.
      cut := perm_filterC (mem C) (filter (mem HO) BB).
      by rewrite -?filter_predI //=.
    by rewrite ?Count_split (Count_perm _ _ Hperm).
 qed. 

local lemma dbb_dbbvotethree (C <: Corr_Adv{WU, BU, JRO.RO, SS}) &m:
  islossless C(JRO.RO).main =>
  Pr[DBB_Exp (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv(B(E, Pz, Vz, SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
  (* go to wspread *) 
  Pr[DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow /\ 0 <= BW.ibad < qVo]  +
  (* go to tally uniqness+ accuracy *)
  Pr[DBB_Exp_hvote_three(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res]+
  (size (undup Voters))%r * 
     Pr[EUF_Exp(SS, BE(SS, C), JRO.RO).main() @ &m : res] +
  (size (undup Voters))%r *
     Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO)), 
                    JRO.RO).main () @ &m : res] +
  (size (undup Voters))%r * Pr[Correctness(SS, C, JRO.RO).main() @ &m : !res].
proof.
  move => C_ll.
  cut := dbb_dbbkeys &m. 
  (* cut := dbbkeys_keyspace &m. *)
  cut := dbb_dbbtally &m.
  (* admit. not needed *)
  (* cut := dbbtally_dbbcheck &m.*)
  cut := dbbtally_dbbcheck_prime &m.
  cut := dbbcheck_dbbhash &m.
  cut := dbb_wspread &m.
  cut := dbbcheck_dbbcheck_two &m.
  cut := dbbcheck_two_dbbhvote &m.
  cut := dbbhvote_dbbhvote_two &m.
  cut := dbbhvotetwo_dbbhvotethree &m.
  cut := dbb_dish &m.
  cut := dbbdish_dbbball &m.
  cut := dbbball_two &m.
  cut := dbbballtwo_meuf &m.
  cut := meuf_euf C &m C_ll.
  pose L := Pr[DBB_Exp (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose A1:= Pr[DBB_Exp_Keys (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose A2:= Pr[DBB_Exp_Keys (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: !BW.bad'].
  pose C1:= Pr[DBB_Exp_tally (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose E1:= Pr[DBB_Exp_check' (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose E2:= Pr[DBB_Exp_check' (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: BW.bad].
  pose F1:= Pr[Hash_Exp(BHash_Adv(B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m: res].
  pose G1:= Pr[DBB_Exp_vote   (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose G2:= Pr[DBB_Exp_vote (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: 
               BW.bow /\ 0 <= BW.ibad < qVo].
  pose H1:= Pr[DBB_Exp_check_two (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose I1:= Pr[DBB_Exp_hvote (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]. 
  pose J1:= Pr[DBB_Exp_hvote_two (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
  pose K1:=  Pr[DBB_Exp_hvote_three (E,Pz,Vz,SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].

  pose W1:= Pr[DBB_Exp_dish(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
  pose W2:= Pr[DBB_Exp_dish(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m :
   !BW.ball].
  pose W3:= Pr[DBB_Exp_ball(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
  pose W4:= Pr[DBB_Exp_ball2(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
  pose W5:= Pr[MEUF_Exp(SS, BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO).main() @ &m : res].
  pose W6:= (size (undup Voters))%r * 
               Pr[EUF_Exp(SS, BE(SS, C), JRO.RO).main() @ &m : res] +
            (size (undup Voters))%r *
               Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO)), 
                    JRO.RO).main () @ &m : res] +
            (size (undup Voters))%r * Pr[Correctness(SS, C, JRO.RO).main() @ &m : !res].
  move => Hw56 Hw45 Hw34 Hw23 Hew.
  have He: E1 <= W1+ W6 by smt.
  move => Hjk Hij Hhi Hgh Heg Hef Hce (* Hcd *) Hac Hab .
  by smt.
qed. 

(* reduction to well spreadness *)
(* guess bad oracle *)
clone  Ex_Plug_and_Pray as WV with
  type tin  <- unit,
  type tres <- bool,
  op bound  <- qVo
proof bound_pos. 
realize bound_pos. by apply size_qVo. qed. 

local lemma dbbvote_guess &m:
  1%r/ qVo %r * 
  Pr[DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow /\ 0 <= BW.ibad < qVo]
  =
  Pr[WV.Guess(DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO)).main()
         @ &m: BW.bow /\ 0<= BW.ibad <qVo /\ fst res = BW.ibad].
proof.
  (* FIXME: rrr. every time I compile this file the order of ibad and bad changes *)
   print glob DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO). 
  cut := WV.PBound (DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO))
          (fun g b => let (ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, 
                           gv8, gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16, gv17,
                           gE, gPz, gVz, gSS, gA)= g in
             (bow /\ 0<=  ibad < qVo))
          (fun g b => let (ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, 
                           gv8, gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16, gv17,
                           gE, gPz, gVz, gSS, gA)= g in
           ibad) () &m. 
  simplify. 
  move => ->. 
  by rewrite Pr[mu_eq]; progress; smt. 
qed.

module DBB_Oracle_wspread(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc removeid = DBB_Oracle_vote(V,H,G,J).removeid
  proc h        = DBB_Oracle_vote(V,H,G,J).h
  proc g        = DBB_Oracle_vote(V,H,G,J).g
  proc j        = DBB_Oracle_vote(V,H,G,J).j
  proc corrupt = DBB_Oracle_vote(V,H,G,J).corrupt
  proc check = DBB_Oracle_vote(V,H,G,J).check

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.uL.[id] <> None /\ !id \in BW.coL /\ BW.qVo <qVo){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      o <- Some b;
      if (!(b.`2,b.`3) \in (map (fun (x: ident * vote * upkey * cipher*sign),
                         (x.`3,x.`4))
                     BW.hvote)){
          BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)]; 
        }elif(!BW.bow){
         BW.bow <- BW.ibad = BW.qVo;
        }
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DBB_Exp_wspread (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DBB_Oracle_wspread(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var bool, i, id, fbb;
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk); 
    return bool;
  }
}.


local lemma guess_dbbwspread &m:
  Pr[WV.Guess(DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO)).main()
         @ &m: BW.bow /\ 0<= BW.ibad <qVo /\ fst res = BW.ibad] <=
  Pr[DBB_Exp_wspread(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow].
proof.
  byequiv =>/>.
  proc.
  inline DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main.
  seq 22 20 : ( BW.bow{1} /\ 
          0 <= BW.ibad{1} < qVo /\
          i{1} = BW.ibad{1} => BW.bow{2})=>/>.
    call (_: ={glob E, glob HRO.RO, glob GRO.RO,
               glob SS, glob JRO.RO,
               BW.sk, BW.pk, BW.bb, BW.hvote, BW.coL, BW.uL, 
               BW.qVo, BW.qCo, BW.qCa}/\
             (BW.bow{1} /\ BW.ibad{2} = BW.ibad{1} =>
                 BW.bow{2})).
    + by proc; auto=>/>. 
    + proc. 
      inline *; sp.
      if =>//=.   
      wp; call (: ={glob JRO.RO}); first by sim.
      call (: ={glob HRO.RO}); first by sim.
      by auto; progress; smt. 
    + by proc. by proc. by proc.
    wp; while (={BW.uL, glob SS, JRO.RO.m}/\
               i0{1} = i{2});
      first by sim.
    call (_: ={glob E, glob HRO.RO});
      first by sim. 
    call (_: true ==> ={glob JRO.RO});
      first by sim.
    call (_: true ==> ={glob GRO.RO});
      first by sim.
    call (_: true ==> ={glob HRO.RO});
      first by sim.
    
    by wp; rnd{1}; auto; progress; smt. 

  inline *; wp.
  while{1} (true) (size (filter (predC hon_mem) val_bb){1} - j{1}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; while{1} (true) (size (filter hon_mem val_bb){1} - j{1}); progress.
    wp; call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; while{1} (true) (size BW.bb{1} - i0{1}); progress.
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll);
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll). 
  wp; call{1} (Pz_ll GRO.RO GRO.RO_o_ll).
  wp; while{1} (true) (size fbb0{1} - i1{1}); progress.
    wp; call{1} (SSv_ll JRO.RO JRO.RO_o_ll);
    call{1} (Ed_ll HRO.RO HRO.RO_o_ll).
    by auto=>/>; smt.
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll). 
  wp; call{1} (Aa2_ll ( <: DBB_Exp_vote(E, Pz, Vz, SS, A, 
                                          HRO.RO, GRO.RO, JRO.RO).O) 
                       _ HRO.RO_o_ll GRO.RO_o_ll JRO.RO_o_ll).
  + proc. 
    while{1} (true) (size pL - i); progress.
      by inline *; auto=>/>; smt.
    by inline *; auto =>/>; smt.    
  by auto=>/>; smt.
qed.

op get_uc (x: ident * vote * upkey * cipher*sign) = (x.`3,x.`4).
op get_uc' (x: ident * upkey * cipher*sign) = (x.`2,x.`3).


module DBB_Oracle_wspread_two(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc removeid = DBB_Oracle_vote(V,H,G,J).removeid
  proc h        = DBB_Oracle_vote(V,H,G,J).h
  proc g        = DBB_Oracle_vote(V,H,G,J).g
  proc j        = DBB_Oracle_vote(V,H,G,J).j
  proc corrupt = DBB_Oracle_vote(V,H,G,J).corrupt
  proc check = DBB_Oracle_vote(V,H,G,J).check

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
        
      BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      BW.bow <- (get_uc' b) \in (map get_uc  BW.hvote);
      BW.obad <- None;
      if ((get_uc' b) \in (map get_uc  BW.hvote)){
        BW.obad <- Some (index (get_uc' b) (map get_uc BW.hvote));
      }
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DBB_Exp_wspread_two (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DBB_Oracle_wspread_two(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var bool, i, id, fbb;
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    BW.obad <- None;
    i <- 0;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk); 
    return BW.bow;
  }
}.

local lemma dbbwspread_dbbwspreadtwo &m:
  Pr[DBB_Exp_wspread(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow] <=
  Pr[DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.obad <> None /\ 0 <= oget BW.obad < qVo].
proof.
  byequiv =>/>.
  proc.
  call (: BW.ibad < BW.qVo
          ,={glob GRO.RO, glob HRO.RO, glob JRO.RO, BW.qVo,
             glob SS, glob E, BW.bb, BW.pk, BW.hvote, BW.coL, 
             BW.uL, BW.qCo, BW.bow, BW.ibad} /\
             BW.ibad{2} < qVo/\
             BW.qVo{2} <= qVo/\
             size BW.hvote{2} <= BW.qVo{2}/\
             (BW.qVo{2} <= BW.ibad{2} => !BW.bow{2})/\
             (!BW.bow{2} => BW.obad{2} = None)
          ,={BW.bow}/\
           (BW.bow{2} => BW.obad{2} <> None)/\
           BW.ibad{1} < BW.qVo{1}/\
           (BW.obad{2} <> None => 
             0 <= oget BW.obad{2} <= BW.ibad{2})).
   + by exact Aa1_ll.

   (* A.a1 corrupt: oracle, lossless, preserves bow *)
   + by proc; auto =>/>; smt.
   + by progress; proc; auto =>/>.
   + by progress; proc; auto =>/>.

   (* A.a1 vote: oracle, lossless, preserves bow *)
   + proc.
     sp; if{1} =>//=.  
     + if{2} =>//=.
       + (* BW.qVo <= BW.ibad *)
         (* rcondt{2} 1; progress.*)
         inline *; wp.
         call(: ={glob JRO.RO}); first by sim.
         call(: ={glob HRO.RO}); first by sim.
         by auto=>/>; smt.
       - (* BW.ibad <= BW.qVo *)
         if{2} =>//=.
         (* BW.ibad = BW.qVo *)
         + inline *. wp.
           call(: ={glob JRO.RO}); first by sim.
           call(: ={glob HRO.RO}); first by sim.
           rewrite /get_uc' /get_uc //=. 
           auto=>/>; progress. 
           + smt. smt. 
           + rewrite oget_some; smt. 
           + smt. smt. smt. 
           + rewrite oget_some; smt.
           + smt. smt.
         - (* BW.ibad < BW.qVo *)
           inline *; wp.
           call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
           call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
           by auto=>/>; smt. 
     - rcondf{2} 1; progress.
         by auto =>/>; smt.
       rcondf{2} 1; progress. 
         by auto=>/>; smt. 
   + progress; proc.  
     inline*; wp.    
     sp; if =>//=.      
     wp; 
     call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
     call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
     by auto=>/>; smt.
   + progress; proc.
     inline *; wp.
     sp; if =>//=.
     + wp; 
       call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
       call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
       by auto=>/>; smt.
     - if =>//=.
       wp; 
       call{1} (SSs_ll SS JRO.RO JRO.RO_o_ll).
       call{1} (Ee_ll HRO.RO HRO.RO_o_ll).
       by auto=>/>; smt.

   + by proc.
   + by move=> _ _; proc.
   + by move=>  _ ; proc.

   + by proc.
   + by move=> _ _; proc.
   + by move=>  _ ; proc.
   
   + by proc.
   + by move=> _ _; proc.
   + by move=>  _ ; proc.

   wp; while (={i, BW.uL, glob SS}); first by sim.
   call(: ={glob E}); first by sim. 
   call(: true ==> ={glob JRO.RO}); first by sim.
   call(: true ==> ={glob GRO.RO}); first by sim.
   call(: true ==> ={glob HRO.RO}); first by sim.
   by wp; rnd; auto=>/>; smt.
qed.

local lemma dbbwspreadtwo_guess  &m:
  1%r/ qVo %r * 
  Pr[DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.obad <> None/\ 0 <= oget BW.obad < qVo] =
  Pr[WV.Guess(DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO)).main()  
               @ &m: BW.obad <> None /\ 0 <= oget BW.obad < qVo /\
                     fst res = oget BW.obad ].
proof. 
  print glob DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).
  cut := WV.PBound (DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO))
          (fun g b => let (obad, ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, gv8, 
                           gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16,
                           gE, gSS, gA)= g in
             (obad<>None /\ 0<= oget obad <qVo))
          (fun g b => let (obad, ibad, bow, gv1, gv2, gv3, gv4, gv5, gv6, gv7, gv8, 
                           gv9, gv10, gv11, gv12, gv13, gv14, gv15, gv16,
                           gE, gSS, gA)= g in
           oget obad) () &m. 
  simplify; move => ->. 
  by rewrite Pr[mu_eq]; progress; smt. 
qed.


module DBB_Oracle_wspread_four(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc removeid = DBB_Oracle_vote(V,H,G,J).removeid
  proc h        = DBB_Oracle_vote(V,H,G,J).h
  proc g        = DBB_Oracle_vote(V,H,G,J).g
  proc j        = DBB_Oracle_vote(V,H,G,J).j
  proc corrupt = DBB_Oracle_vote(V,H,G,J).corrupt
  proc check = DBB_Oracle_vote(V,H,G,J).check

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
        
      BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      (* BW.bow <- false; *)
      if ((get_uc' b) \in (map get_uc  BW.hvote)){
        BW.bow <- BW.obad = Some (index (get_uc' b) (map get_uc BW.hvote));
      }
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DBB_Exp_wspread_four (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DBB_Oracle_wspread_four(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var bool, i, id, fbb, il;
    il <$ [0..qVo -1];
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.obad <- Some il;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk); 
    return BW.bow;
  }
}.

local lemma dbbwspreadthree_dbbwspread_four &m:
  Pr[WV.Guess(DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO)).main()  
               @ &m: BW.obad <> None /\ 0 <= oget BW.obad < qVo /\
                     fst res = oget BW.obad ] <=
  Pr[DBB_Exp_wspread_four(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow].
proof.
  byequiv =>/>.
  proc.
  inline DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main.

  wp; call (: ={ BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
                 HRO.RO.m, JRO.RO.m, GRO.RO.m} /\
              (BW.obad{1} <> None /\ ={BW.obad} => BW.bow{2})/\
              (BW.qVo{2} <= BW.ibad{2} => !BW.bow{1} /\ !BW.bow{2})).
  + proc.
    auto=>/>; smt.
  + proc.
    sp; if =>//=.   
    + inline *; wp.
      call(: ={JRO.RO.m}); first by sim.
      call(: ={HRO.RO.m}); first by sim.
      auto=>/>; smt. 
    if =>//=.
    inline *.
    wp; call(: ={JRO.RO.m}); first by sim.
    call(: ={HRO.RO.m}); first by sim.   
    by auto=>/>; smt. 
  + by proc. by proc. by proc.
  wp; while ( ={glob SS, BW.uL}/\i0{1} = i{2});
    first by sim.   
  call (_: ={ glob E, glob HRO.RO}); first by sim.
  call (_: true ==> ={glob JRO.RO});
      first by sim.
  call (_: true ==> ={glob GRO.RO});
      first by sim.
  call (_: true ==> ={glob HRO.RO});
      first by sim. 
  wp; progress. rnd; wp; progress. rnd; auto; progress. 
  by rewrite some_oget 1: H7 in H5.
qed. 
     
module DBB_Oracle_wspread_five(V: VotingScheme,
              H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) ={

  proc removeid = DBB_Oracle_vote(V,H,G,J).removeid
  proc h        = DBB_Oracle_vote(V,H,G,J).h
  proc g        = DBB_Oracle_vote(V,H,G,J).g
  proc j        = DBB_Oracle_vote(V,H,G,J).j
  proc corrupt = DBB_Oracle_vote(V,H,G,J).corrupt
  proc check = DBB_Oracle_vote(V,H,G,J).check

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
        
      BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      b <@ V(H,G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
      BW.bow <- ( (nth witness BW.hvote (oget BW.obad)).`3 = b.`2/\
                  (nth witness BW.hvote (oget BW.obad)).`4 = b.`3);
      
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
}.

module DBB_Exp_wspread_five (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  
  module O = DBB_Oracle_wspread_five(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var bool, i, id, fbb, il;
    il <$ [0..qVo -1];
    BW.bad <- false;
    BW.bow <- false;
    BW.ibad <$ [0..qVo -1];
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL <- empty;
    bool    <- true;
    i <- 0;
    BW.obad <- Some il;

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk); 
    return BW.bow;
  }
}.

local lemma dbbwspreadfour_dbbwspreadfive &m:
  Pr[DBB_Exp_wspread_four(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow] <=
  Pr[DBB_Exp_wspread_five(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow].
proof.
  byequiv =>/>.
  proc.   
  call(: ={BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
                 HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad} /\
              (BW.bow{1} => BW.bow{2})/\
              (!BW.ibad{1}< BW.qVo{1} => !BW.bow{1})). 
  + proc. auto=>/>; smt.
  + proc.
    sp; if =>//=.
    + inline *; wp. 
      call(: ={JRO.RO.m}); first by sim.
      call(: ={HRO.RO.m}); first by sim.
      auto=>/>; smt. 
    if=>//=. 
    seq 1 1: ((o{2} = None /\ o{1} = None /\
                ={id, v} /\
                ={BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
                  HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad} /\
                (BW.bow{1} => BW.bow{2}) /\
                  ! (BW.qVo{1} < BW.ibad{1} /\
                  BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}))) /\
                  BW.ibad{1} = BW.qVo{1} /\
                  BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1})/\
                  ={b} /\
                (!BW.ibad{1}< BW.qVo{1} => !BW.bow{1})). inline *; wp.
    call(: ={JRO.RO.m}); first by sim.
    call(: ={HRO.RO.m}); first by sim.
    auto=>/>; progress. 
    if{1} =>//=.
    + auto; progress. 
      + move: H4; rewrite oget_some /get_uc /get_uc'.  
        pose f:= fun (x0 : ident * vote * upkey * cipher * sign) => (x0.`3, x0.`4).
        rewrite /index.
        move => Hmem.
        have ->: (nth witness BW.hvote{2}
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`3 =
               (nth witness (map f BW.hvote{2})
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`1.
        + rewrite (nth_map witness witness). 
          + cut := find_ge0 (pred1 (b{2}.`2, b{2}.`3)) 
                        (map f BW.hvote{2}). 
            smt.
          smt. 
        smt.
      + move: H4; rewrite oget_some /get_uc /get_uc'.  
        pose f:= fun (x0 : ident * vote * upkey * cipher * sign) => (x0.`3, x0.`4).
        rewrite /index.
        move => Hmem.
        have ->: (nth witness BW.hvote{2}
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`4 =
               (nth witness (map f BW.hvote{2})
                     (find (pred1 (b{2}.`2, b{2}.`3)) (map f BW.hvote{2}))).`2.
        + rewrite (nth_map witness witness). 
          + cut := find_ge0 (pred1 (b{2}.`2, b{2}.`3)) 
                        (map f BW.hvote{2}). 
            smt.
          smt. 
        smt.
      + move: H4; rewrite /get_uc /get_uc'.  
        pose f:= fun (x0 : ident * vote * upkey * cipher * sign) => (x0.`3, x0.`4).
        rewrite /index.
        move => Hmem. smt.
    auto; progress.  
 
  + by proc. by proc. by proc.   

  wp; while ( ={glob JRO.RO, glob SS, i, BW.uL});
    first by sim.
  call (_: ={ glob E, glob HRO.RO}); first by sim.
  call (_: true ==> ={glob JRO.RO});
    first by sim.
  call (_: true ==> ={glob GRO.RO});
    first by sim.
  call (_: true ==> ={glob HRO.RO});
    first by sim.
  by auto; progress; smt. 
qed.
 
module WO_H (WO: Wspread_Oracle) ={
  proc o = WO.h
}.


module BWspread(V: VotingScheme, G: GOracle.Oracle, J: JOracle.Oracle, 
                B: DBB_Adv, WO:Wspread_Oracle) ={
  module O ={
    
    proc h = WO.h
    proc j = DBB_Oracle_vote(V,WO_H(WO),G,J).j
    proc g = DBB_Oracle_vote(V,WO_H(WO),G,J).g
    proc corrupt = DBB_Oracle_vote(V,WO_H(WO),G,J).corrupt
    proc check = DBB_Oracle_vote(V,WO_H(WO),G,J).check

  proc vote(id: ident, v: vote): (ident * upkey * cipher * sign) option ={
    var b, o;
    o <- None;
    if (BW.qVo < BW.ibad /\ BW.uL.[id] <> None /\ !id \in BW.coL){
        b <@ V(WO_H(WO),G,J).vote(id, v, BW.pk, (oget BW.uL.[id]).`2);
        o <- Some b;
        if (! (get_uc' b) \in (map get_uc BW.hvote)){
            BW.hvote <- BW.hvote ++ [(id,v, b.`2,b.`3,b.`4)];
        }
        
      BW.qVo <- BW.qVo + 1; 
    }elif(BW.ibad = BW.qVo /\ BW.uL.[id] <> None /\ !id \in BW.coL){
      WO.test(v, (oget BW.uL.[id]).`1, 
                 (nth witness BW.hvote (oget BW.obad)).`4);
      BW.qVo <- BW.qVo + 1; 
    }
    return o;
  }
 
  }

  proc main(sk : skey):unit ={
    var i,j, id;
    BW.hk <$ dhkey_out;
    G.init();
    J.init();
    BW.ibad <$ [0..qVo - 1];
    BW.coL <- [];
    BW.hvote <- [];
    BW.qVo <- 0;
    BW.sk <- sk;
    BW.pk <- get_pk sk;
    BW.uL <- empty;
    i <$ [0..(qVo-1)];
    BW.obad <- Some i;
    j <- 0;
    while (j < size (undup Voters)){
      id <- nth witness (undup Voters) j;
      BW.uL.[id] <@ V(WO_H(WO),G,J).register(id);
      j<- j + 1;
    }
    B(O).a1(sk);
  }
}.

local lemma dbbwspreadfive_wspread &m:
  Pr[DBB_Exp_wspread_five(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ 
            &m : BW.bow] <=
  Pr[Wspread_Exp(E, BWspread(B(E, Pz, Vz,SS),GRO.RO,JRO.RO,A), HRO.RO).main() 
         @ &m: BS.eq ].
proof.
  byequiv =>/>.
  proc.
  inline BWspread(B(E, Pz, Vz, SS), GRO.RO, JRO.RO, A, Wspread_Exp(E, BWspread(B(E, Pz, Vz, SS), GRO.RO, JRO.RO, A), HRO.RO).O).main.
  call(: ={BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
                 HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad}/\
         (forall id, BW.uL{2}.[id] <> None =>
              get_upk (oget BW.uL{2}.[id]).`2 = (oget BW.uL{2}.[id]).`1)/\
         BW.pk{1} = BS.pk{2}/\
         0<= BS.qt{2} /\
         (BW.qVo{1} <= BW.ibad{1} => BS.qt{2} <1)/\
         (BS.qt{2} < 1=> !BW.bow{1})/\
         (BW.bow{1} => BS.eq{2})).
  + proc. auto=>/>; smt.
  + proc.
    sp; if =>//=.    
    + inline *; wp.
      call(: ={glob JRO.RO}); first by sim.
      call(: ={glob HRO.RO}); first by sim.
      by auto =>/>; smt.
    if =>//=.
    inline *; wp.
    rcondt{2} 4; progress; first by auto.
    sp.
    seq 1 1: (v0{2} = v{2} /\
  l{2} = (oget BW.uL{2}.[id{2}]).`1 /\
  c{2} = (nth witness BW.hvote{2} (oget BW.obad{2})).`4 /\
  id0{1} = id{1} /\
  v0{1} = v{1} /\
  pk{1} = BW.pk{1} /\
  usk{1} = (oget BW.uL{1}.[id{1}]).`2 /\
  upk{1} = get_upk usk{1} /\
  ((o{2} = None /\
    o{1} = None /\
    ={id, v} /\
    ={BW.hvote, BW.qVo, BW.coL, BW.uL, BW.pk, BW.ibad, glob E, glob SS,
        HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.obad} /\
    BW.pk{1} = BS.pk{2} /\
    0 <= BS.qt{2} /\
    (BW.qVo{1} <= BW.ibad{1} => BS.qt{2} < 1) /\
    (BS.qt{2} < 1 => !BW.bow{1}) /\ (BW.bow{1} => BS.eq{2})) /\
   ! (BW.qVo{1} < BW.ibad{1} /\
      BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}))) /\
  BW.ibad{1} = BW.qVo{1} /\
  BW.uL{1}.[id{1}] <> None /\ ! (id{1} \in BW.coL{1}) /\
  (forall id, BW.uL{2}.[id] <> None =>
              get_upk (oget BW.uL{2}.[id]).`2 = (oget BW.uL{2}.[id]).`1)/\
  c{1} = c'{2}).
    call(: ={glob HRO.RO}); first by sim.
    auto. progress. smt. smt. 
    wp; exists* (glob SS){1}; elim* => gs. 
    call{1} (SSs_eq gs).
    by auto =>/>; progress; smt.

  + by proc. by proc. by proc. 
  wp; while (={BW.uL, glob SS, JRO.RO.m}/\ i{1} = j{2} /\
             (forall id, BW.uL{2}.[id] <> None =>
              get_upk (oget BW.uL{2}.[id]).`2 = (oget BW.uL{2}.[id]).`1)).
    inline *; wp.
    call{1} (Kgen_get_upk JRO.RO).
    by auto=>/>; smt.
  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
  wp. 
  swap{2} 16 -15.
  swap{2} 10 -6.
  swap{2} [6..8] 3.
  wp; rnd; wp; call{1} (Kgen_get_pk HRO.RO).
  wp; call (: true ==> ={glob JRO.RO}); first by sim.
  call (: true ==> ={glob GRO.RO}); first by sim.
  call (: true ==> ={glob HRO.RO}); first by sim.
  wp; rnd; wp; rnd.
  by auto=>/>; smt.
qed. 

local lemma dbb_dbbwspread (C <: Corr_Adv{WU, BU, JRO.RO, SS}) &m:
  islossless C(JRO.RO).main =>
  Pr[DBB_Exp (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  (* final *) 
  Pr[Hash_Exp(BHash_Adv(B(E, Pz, Vz, SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
  qVo%r ^2 * Pr[Wspread_Exp(E, BWspread(B(E, Pz, Vz,SS),GRO.RO,JRO.RO,A), HRO.RO).main() 
         @ &m: BS.eq ] +
  (size (undup Voters))%r *
   Pr[EUF_Exp(SS, BE(SS, C), JRO.RO).main() @ &m : res] +
  (size (undup Voters))%r *
   Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO)), JRO.RO).main
      () @ &m : res] +
  (size (undup Voters))%r * Pr[Correctness(SS, C, JRO.RO).main() @ &m : !res]+
  (* go to tally uniq+ acc *)
  Pr[DBB_Exp_hvote_three(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  move => C_ll.
  cut := dbb_dbbvotethree C &m C_ll.
  cut := dbbvote_guess &m.
  cut := guess_dbbwspread &m.
  cut := dbbwspread_dbbwspreadtwo &m.
  cut := dbbwspreadtwo_guess  &m.
  cut := dbbwspreadthree_dbbwspread_four &m.
  cut := dbbwspreadfour_dbbwspreadfive &m.
  cut := dbbwspreadfive_wspread &m.

  pose L:= Pr[DBB_Exp (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
 (* pose R1:= (size Voters)%r ^ 2 * Pr[KeySpace(SS, JRO.RO).main() @ &m : res]+
           Pr[Hash_Exp(BHash_Adv(B(E, Pz, Vz, SS), A, HRO.RO, GRO.RO, JRO.RO)).main() 
               @ &m : res]. *)
  pose R2:=Pr[DBB_Exp_hvote_three(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () 
              @ &m : res].
  pose R3:= Pr[Wspread_Exp(E, BWspread(B(E, Pz, Vz, SS), GRO.RO, JRO.RO, A), HRO.RO).main
               () @ &m : BS.eq].
  pose A:= Pr[DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() @ &m :
              BW.bow /\ 0 <= BW.ibad < qVo].
  pose B:= Pr[WV.Guess(DBB_Exp_vote(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO)).main
               () @ &m : BW.bow /\ 0 <= BW.ibad < qVo /\ res.`1 = BW.ibad].
  pose CD:= Pr[DBB_Exp_wspread(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main() 
               @ &m : BW.bow].
  pose D:= Pr[DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main
               () @ &m : BW.obad <> None /\ 0 <= oget BW.obad < qVo].
  pose E:= Pr[WV.Guess(DBB_Exp_wspread_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO)).main
               () @ &m : BW.obad <> None /\ 0 <= oget BW.obad < qVo /\ res.`1 = oget BW.obad].
  pose F:= Pr[DBB_Exp_wspread_four(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main
               () @ &m : BW.bow].
  pose G:= Pr[DBB_Exp_wspread_five(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main
               () @ &m : BW.bow].

  move => Hg3 Hfg Hef Hde Hcd Hbc Hab Hla.
  have Hxyz: forall (x y z: real), 0%r < x => (1%r/ x) * y = z <=> y = x * z; 
    first by progress; smt.
  have ->: qVo%r ^2 = qVo%r * qVo%r. 
    have ->: 2 = 1+1 by smt. 
    rewrite powrS; first by smt.
    smt.

  have Hab': A= qVo%r * B.  
    move: Hab; rewrite Hxyz; first by smt.
    by done.
  have Hbd: B <= D by smt.
  have Had: A <= qVo%r * D.
    rewrite Hab' StdOrder.RealOrder.ler_pmul2l. 
      by rewrite RealExtra.lt_fromint size_qVo. 
    by done.
  have Hde': D = qVo%r * E.
    move: Hde; rewrite Hxyz; first by smt.
    by done.
  have He3: E <= R3 by smt.
  have Hd3: D <= qVo%r *R3. 
    rewrite Hde'. 
    cut Hp:= size_qVo. 
    cut:= StdOrder.RealOrder.ler_wpmul2l qVo%r _ E R3 He3.
      rewrite RealExtra.le_fromint. 
      smt. 
    by done.
  have HLR: A <= qVo%r * (qVo%r * R3).
    move: Had. rewrite Hde' //=. 
    cut := StdOrder.RealOrder.ler_wpmul2l qVo%r _ (qVo%r * E) (qVo%r * R3) _. 
      rewrite RealExtra.le_fromint.
      smt.
      smt.  
    smt.    
  smt. 
qed. 

(*  *)
module DBB_Exp_dish_two (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id,  hCN, r', 
        pi', ev', vbb, b,m,e, fbb, j, c,
        dL, hon_mem, val_bb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');


    i <- 0;
    vbb <- [];
    (* keep only what is valid and verified *)
    while( i < size BW.bb){
      b  <- nth witness BW.bb i;
      m  <@ E(H).dec(BW.sk, b.`2, b.`3);
      e  <@ SS(J).verify(b.`2, b.`3, b.`4);
      if (m <>None /\ e){
        vbb <- vbb ++ [b];
      }
      i  <- i + 1;
    }
    
    hon_mem <- (mem (map (rem_id \o remv) BW.hvote));
    val_bb  <- (map rem_id vbb);
    hCN <- [];
    j <- 0;
    while (j < size (filter hon_mem val_bb)){
      c <- nth witness (filter hon_mem val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      hCN<- hCN ++ [oget m];
      j <- j + 1;
    }

    dL <- [];
    j <- 0;
    while (j < size (filter (predC hon_mem) val_bb)){
      c <- nth witness (filter (predC hon_mem) val_bb) j;
      m <@ E(H).dec(BW.sk, c.`1, c.`2);
      dL<- dL ++ [oget m];
      j <- j + 1;
    }
    (* size of dishonest valid ballots *)
    BW.ball <- size dL <= BW.qCo;

    bool <- r <> Count (hCN ++ dL);
    
    return ev /\ bool /\BW.ball ;
  }
}.


local lemma dbbdish_dbbdishtwo &m:
  Pr[DBB_Exp_hvote_three(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DBB_Exp_dish_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  byequiv =>/>.  
  proc.
  seq 27 28 : (={BW.bb, glob E, glob SS, HRO.RO.m, JRO.RO.m, 
                 vbb, BW.sk, BW.hvote, BW.qCo, ev, r}). 
    by sim.
  wp; while (={j, glob E, HRO.RO.m, dL, BW.sk, hon_mem, val_bb}); first by sim.
  wp; while (={j, glob E, HRO.RO.m, BW.sk, hCN}/\
             hon_mem{2} = mem (map (rem_id \o remv) BW.hvote{1})/\
             val_bb{2}  = map rem_id vbb{1}). 
    wp; call(: ={glob HRO.RO}); first by sim.
    by auto=>/>; smt.
  by auto=>/>.
qed.

(*  *)
module DBB_Exp_result (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, bool, i, id,  r', 
        pi', ev', fbb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    bool    <- true;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally ( BW.bb,BW.sk,BW.hk);
    ev'      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');

    bool <- r <> r';
    
    return ev /\ bool ;
  }
}.

local lemma dbbdishtwo_dbbresult &m:
  Pr[DBB_Exp_dish_two(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DBB_Exp_result(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
proof.
  byequiv =>/>.  
  proc.
  seq 23 23 : (={ r, ev, BW.pbb, BW.bb, fbb, BW.uL, BW.sk, glob E, BW.hk,
                 glob SS, glob Pz, glob Vz, HRO.RO.m, JRO.RO.m, GRO.RO.m, BW.pk}/\
               BW.bb{2} = filter ((mem (map thr3 BW.uLL{2}))\o two4) (USanitize fbb{2})).
    inline *; wp.
    call(: ={GRO.RO.m}); first by sim.
    wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.hvote, BW.check, 
               BW.bb, BW.pk, BW.uL,BW.hk});
    first 4 by sim.
    wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.uL, BW.qVo, BW.pk, 
               BW.coL, glob SS, glob E, BW.hvote});
    first 5 by sim.
    conseq (: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.uL, BW.qVo, BW.pk, 
                BW.sk, BW.coL, glob SS, glob E, BW.hvote, BW.uLL,
                BW.check,BW.hk})=>//=. 
    by sim.

  inline *; wp.     
  while{1} ( ={BW.sk, HRO.RO.m} /\ 0<= j{1} /\
             let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
            dL{1} = map g (take j{1} (filter (predC hon_mem{1}) val_bb{1})))
           (size (filter (predC hon_mem{1}) val_bb{1}) - j{1}); progress. 
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge sk x.
    call{1} (dec_Dec_one ge sk x.`1 x.`2). 
    auto=>/>; progress. 
    + smt.
    + rewrite (take_nth witness) ; first by smt.
      by rewrite -cats1 map_cat. 
    + smt.
  wp;     
  while{1} ( ={BW.sk, HRO.RO.m} /\ 0<= j{1} /\
             let g= fun (x : upkey * cipher * sign) =>
                   oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2}) in
            hCN{1} = map g (take j{1} (filter hon_mem{1} val_bb{1})))
           (size (filter hon_mem{1} val_bb{1}) - j{1}); progress. 
    wp; sp.
    exists* (glob E), BW.sk, c; elim* => ge sk x.
    call{1} (dec_Dec_one ge sk x.`1 x.`2). 
    auto=>/>; progress. 
    + smt.
    + rewrite (take_nth witness) ; first by smt.
      by rewrite -cats1 map_cat. 
    + smt.
  wp; while{1} ( ={ JRO.RO.m, HRO.RO.m, BW.sk, BW.bb} /\ 0<= i{1}/\
                 let fd= fun (x : ident * upkey * cipher * sign) =>
                     dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                     ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2} in
                 vbb{1} = filter fd (take i{1} BW.bb{2}))
                (size BW.bb{2} - i{1}); progress.
    wp; sp.
    exists* (glob SS), (glob E), BW.sk, b; elim* => gs ge skx bx.
    call{2} (ver_Ver_one gs bx.`2 bx.`3 bx.`4).
    call{1} (dec_Dec_one ge skx bx.`2 bx.`3).
    auto=>/>; progress.
    + smt. smt. smt. smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons H1.      
    + smt.

  wp; call(: ={GRO.RO.m}); first by sim.
  wp; call(: ={glob GRO.RO}); first by sim.
  wp; while (={i0, fbb0, glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb, sk}/\ 
             0<=i0{1}/\ BW.sk{2} = sk{1}/\
             let f= fun (x : upkey * cipher * sign) =>
                   (x.`1, oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})) in
             let fd= fun (x : upkey * cipher * sign) =>
                     dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2} <> None /\
                     ver_sign x.`1 x.`2 x.`3 JRO.RO.m{2} in
             ubb{1} = map f (filter fd (take i0{1} fbb0{2}))).
    wp; sp.             
    exists* BW.sk{2}, b{2}; elim* => skx bx.
    call{1} (ver_Ver_two bx.`1 bx.`2 bx.`3).
    call{1} (dec_Dec_two skx bx.`1 bx.`2).
    auto=>/>; progress.
    + smt. 
    + rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons //= H1 H2 //= map_rcons //= -cats1.      
    + smt.
      rewrite (take_nth witness); first by smt.
      by rewrite filter_rcons //= H1 //= map_rcons //= -cats1. 
  auto=>/>; progress.
  + by rewrite take0 //=. 
  + by rewrite take0. 
  + smt. 
  + by rewrite take0. 
  + smt.  
  + by rewrite take0.
  + smt.
  + rewrite take_oversize. smt.
    move: H9. rewrite ?take_oversize; first 4 by smt.
    pose fd:= (fun (x : upkey * cipher * sign) =>
                 dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2} <> None /\
                 ver_sign x.`1 x.`2 x.`3 JRO.RO.m{2}).  
    pose fd':= (fun (x : ident * upkey * cipher * sign) =>
                 dec_cipher BW.sk{2} x.`2 x.`3 HRO.RO.m{2} <> None /\
                 ver_sign x.`2 x.`3 x.`4 JRO.RO.m{2}).  
    pose k:=  (mem (map thr3 BW.uLL{2}))\o two4.
    pose g:= (fun (x : upkey * cipher * sign) =>
              oget (dec_cipher BW.sk{2} x.`1 x.`2 HRO.RO.m{2})).
    cut := filter_map remi fd (filter k (USanitize fbb{2})).
      have ->: (preim remi fd) = fd' by rewrite /fd /fd' /remi /preim /(\o).
    move => Hfm.
    rewrite Hfm.
    rewrite /rem_id.
    pose L:= (filter fd' (filter k (USanitize fbb{2}))).
    pose p:= (mem (map (remi \o remv) BW.hvote{1})).
    rewrite -map_cat. 
    cut Hp:= perm_filterC p (map remi L).
    have ->: Count (map g
                   (filter p (map remi L) ++
                    filter (predC p) (map remi L))) =
             Count (map (g\o remi) L). 
     cut Hmp:= perm_eq_map g _ _ Hp.
     by rewrite (Count_perm _ _ Hmp) -map_comp.
     rewrite /rem_id. print San_map'.
    cut := (San_map' BW.sk{2} (map remi L) HRO.RO.m{2}).
      rewrite //=.
    move => Hsm.
    rewrite Hsm.
    rewrite -map_comp. 
    have ->: ((fun (p0 : upkey * vote) => p0.`2) \o
               fun (x : upkey * (cipher * sign)) =>
              (x.`1, oget (dec_cipher BW.sk{2} x.`1 x.`2.`1 HRO.RO.m{2}))) = 
              g \o (fun (x : upkey * (cipher * sign)) => (x.`1, x.`2.`1, x.`2.`2)).
             by rewrite /(\o) /g /remi.
    rewrite map_comp.
    have Ho: map remi L =
            (map (fun (x : upkey * (cipher * sign)) => (x.`1, x.`2.`1, x.`2.`2))
            (Sanitize (map (fun (x : upkey * cipher * sign) => (x.`1, (x.`2, x.`3)))
                      (map remi L)))).
    rewrite /L -?filter_predI.
    rewrite uniq_to_San.
    + rewrite /remi -?map_comp /(\o) //=.
      rewrite uniq_filter. 
      rewrite /USanitize -map_comp /uopen /(\o) //=.
      by rewrite San_uniq_fst.
    by rewrite -?map_comp /(\o) //=. 
    
    have ->: forall (A: (upkey * (cipher * sign)) list),
             (map (g \o fun (x : upkey * (cipher * sign)) => (x.`1, x.`2.`1, x.`2.`2)) A) 
             = map g (map (fun (x : upkey * (cipher * sign)) => (x.`1, x.`2.`1, x.`2.`2)) A)
      by move => A; rewrite -map_comp.
    by rewrite -Ho.
qed.

module DBB_Exp_vf (E: Scheme, P: Prover, Ve: Verifier, 
                       SS: SignScheme(*, Pp: PP*), A: DBB_Adv,
                      H:HOracle.Oracle, G:GOracle.Oracle, J: JOracle.Oracle) ={

  (* module V = B(E,P,Ve,SS,Pp) *)
  module O = DBB_Oracle_vote(B(E,P,Ve,SS),H,G,J)

  proc main(): bool ={
    var r, pi, ev, i, id,  r', 
        pi', fbb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    i <- 0;
    BW.uLL <- [];

    H.init();
    G.init();
    J.init();

    (BW.pk, BW.sk,BW.hk) <@ B(E,P,Ve,SS,H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ B(E,P,Ve,SS,H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    BW.pbb   <@ B(E,P,Ve,SS,H,G,J).publish(BW.bb, BW.hk);
    ev       <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r),pi);


    (r',pi') <@ B(E,P,Ve,SS,H,G,J).tally (BW.bb,BW.sk,BW.hk);
    BW.ball      <@ B(E,P,Ve,SS,H,G,J).verify((BW.pk, BW.pbb, r'),pi');
    
    return (ev /\ BW.ball) /\ r <> r' ;
  }
}.

local lemma dbbresult_dbbvf &m:
  Pr[DBB_Exp_result(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[DBB_Exp_vf(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res]+
  Pr[DBB_Exp_vf(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : !BW.ball].
proof.
  byequiv => //=.
  proc.
  wp.
  inline  B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).verify
          B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).tally 
          B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  wp; call(: ={GRO.RO.m}); first by sim.
  wp; call(: ={GRO.RO.m}); first by sim.
  wp; while (={i0, fbb0, glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb, sk});
    first by sim.
  wp; call(: ={GRO.RO.m}); first by sim.
  wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.hvote, BW.check, 
               BW.bb, BW.pk, BW.uL,BW.hk});
    first 4 by sim.
  
  wp; call(: ={HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.uL, BW.qVo, BW.pk, 
               BW.coL, glob SS, glob E, BW.hvote});
    first 5 by sim.
  wp; while (={i, BW.uL, BW.uLL, glob SS}); first by sim.
  call(: ={glob E}); first by sim.

  call(: true ==> ={glob JRO.RO}); first by sim.
  call(: true ==> ={GRO.RO.m}); first by sim.
  call(: true ==> ={HRO.RO.m}); first by sim.
  by auto.
qed.

(* accuracy adversary that calls the weak verifiability adversary *)
module BAcc(V: VotingScheme, A: DBB_Adv,
            H:HOracle.ARO, G:GOracle.ARO, J: JOracle.ARO) ={
  module O = DBB_Oracle_vote(V,H,G,J)

  proc main (sk : skey, 
             ul : (ident, upkey * uskey) map,
             uLL: (ident * uskey*upkey) list,
             hk: hash_key) 
           : (ident * upkey * cipher * sign) list ={
    var i,r, pi, fbb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    i <- 0;
    BW.uL <- ul;
    BW.sk <- sk;
    BW.pk <- get_pk BW.sk;
    BW.hk <- hk;
    
    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();

    return BW.bb;
  }
}.

(* lemma that bounds *)
local lemma verifreplacevf_accuracy &m:
  Pr[DBB_Exp_vf(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : !BW.ball] <=
  Pr[Acc_Exp (B(E,Pz,Vz,SS), BAcc(B(E,Pz,Vz,SS),A), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m: !res].
proof.
  byequiv =>/>.
  proc.
  inline BAcc(B(E, Pz, Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main.  
  wp. progress.
  seq 20 30: ( ={glob E, glob SS, glob Vz, glob Pz, 
                 HRO.RO.m, GRO.RO.m, JRO.RO.m, BW.bb, 
                 BW.pk, BW.sk,BW.hk}/\
               BW.hk{2} =hk{2} /\
               r{1} = r0{2} /\ pi{1} = pi0{2} /\
               BW.sk{2} = sk{2} /\
               BW.pk{1} = get_pk BW.sk{1}/\
               BW.pk{2} = pk{2} ).
    call (: ={JRO.RO.m, GRO.RO.m, HRO.RO.m, BW.check, 
              BW.bb, BW.pk, BW.hvote, BW.uL,BW.hk}); 
      first 4 by sim.
    wp; call (: ={JRO.RO.m, HRO.RO.m, GRO.RO.m, BW.pk, BW.qVo, 
                  BW.coL, BW.uL, glob SS, glob E, BW.coL, BW.uL, BW.hvote}); 
      first 5 by sim. 
    wp; while (={glob SS, i}/\ BW.uL{1} = uL{2} /\ BW.uLL{1} = uLL{2}); 
      first by sim.  
             
    inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO). 
    call(: true ==> ={JRO.RO.m}); first by sim.
    call(: true ==> ={GRO.RO.m}); first by sim.
    call(: true ==> ={HRO.RO.m}); first by sim. 
    by auto=>/>; progress; smt. 

  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  seq 5 0: ( ={glob E, glob SS, glob Vz, glob Pz, HRO.RO.m, 
              GRO.RO.m, JRO.RO.m, BW.bb, BW.pk, BW.sk,BW.hk} /\
             BW.hk{2} = hk{2} /\
             r{1} = r0{2} /\ pi{1} = pi0{2} /\
             bb{1}= BW.bb{1} /\ pk{2} = BW.pk{2} /\
             BW.sk{2} = sk{2} /\
              BW.pk{1} = get_pk BW.sk{1}/\
             BW.pbb{1} = pbb{1} /\
             pbb{1} = map (fun (x:  ident*upkey * cipher * sign)=>
                               (x, hash' BW.hk{2} x))  bb{1}).
    inline *; wp; sp.
    exists * (glob Vz){1}; elim *=> gv.
    call{1} (Vz_keepstate  gv).
    by auto=>/>.
  inline *; wp.
  call(: ={glob GRO.RO}); first by sim.
  wp; call(: ={glob GRO.RO}); first by sim. 
  wp; while (={fbb0, glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb}/\ 
             i0{1} = i1{2}/\ sk{1} = sk1{2}); first by sim.
  by auto=>/>. 
qed. 

module BTall(V: VotingScheme, A: DBB_Adv,
                   H:HOracle.ARO, G:GOracle.ARO, J: JOracle.ARO) ={
  module O = DBB_Oracle_vote(V,H,G,J)

  proc main ()
       : ((ident * upkey * cipher*sign) list) * 
         pkey * result * prf * result * prf *hash_key={
    var r, pi, r', pi', id, i, fbb;
    BW.bad  <- false;
    BW.ball <- false;
    BW.bb   <-[];
    BW.coL  <-[];
    BW.hvote<- [];
    BW.check<- [];
    BW.qVo  <- 0;
    BW.qCo  <- 0;
    BW.qCa  <- 0;
    BW.uL   <- empty;
    i <- 0;
    BW.uLL <- [];

    (BW.pk, BW.sk,BW.hk) <@ V(H,G,J).setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BW.uL.[id] <@ V(H,G,J).register(id);
      BW.uLL <- BW.uLL ++[(id, (oget BW.uL.[id]).`2, (oget BW.uL.[id]).`1)];
      i<- i + 1;
    }

    fbb    <@ A(O).a1(BW.sk);
    (* remove duplicates of the same upkey *)
    BW.bb  <- (filter ((mem (map thr3 BW.uLL))\o two4) (USanitize fbb));
    (r,pi)   <@ A(O).a2();
    (r',pi') <@ V(H,G,J).tally (BW.bb,BW.sk,BW.hk);
    
    return (BW.bb, BW.pk, r, pi, r', pi',BW.hk);
  }

}.

local lemma dbbvf_tallyuniq &m:
  Pr[DBB_Exp_vf(E, Pz, Vz, SS, A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] <=
  Pr[TallyUniq_Exp (B(E,Pz,Vz,SS), BTall(B(E,Pz,Vz,SS),A), 
        HRO.RO, GRO.RO, JRO.RO).main() @ &m: res].
proof.
  byequiv =>/>.
  proc.
  inline BTall(B(E, Pz, Vz, SS), A, HRO.RO, GRO.RO, JRO.RO).main.
  swap{1} 22 1.
  conseq (: ={ev, r, r'}/\ BW.ball{1} = ev'{2})=>//=.
  call (: ={glob Vz, GRO.RO.m}); first by sim.
  call (: ={glob Vz, GRO.RO.m}); first by sim.
  swap{1} 21 1.
  inline B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  wp; call (: ={JRO.RO.m, GRO.RO.m, HRO.RO.m, glob SS, 
                glob Pz, glob E}); first by sim.
  wp; call (: ={JRO.RO.m, GRO.RO.m, HRO.RO.m, BW.check, 
              BW.bb, BW.pk, BW.hvote, BW.uL,BW.hk}); 
      first 4 by sim.
  wp; call (: ={JRO.RO.m, HRO.RO.m, GRO.RO.m, BW.pk, BW.qVo, 
               BW.coL, BW.uL, glob SS, glob E, BW.coL, BW.uL, BW.hvote}); 
      first 5 by sim.
  wp; while( ={i, BW.uL, glob SS, BW.uLL}); first by sim.
  call (: ={glob E}); first by sim.
  swap{1} [13..15] -12.
  wp; call(: true ==> ={JRO.RO.m}); first by sim.
  call(: true ==> ={GRO.RO.m}); first by sim.
  call(: true ==> ={HRO.RO.m}); first by sim.
  by auto=>/>; smt.
qed. 

lemma bound_dbb (C <: Corr_Adv{WU, BU, JRO.RO, SS}) &m:
   islossless C(JRO.RO).main=>
  Pr[DBB_Exp (B(E,Pz,Vz,SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  (* final *) 
  Pr[Hash_Exp(BHash_Adv(B(E, Pz, Vz, SS), A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
qVo%r ^2 * 
  Pr[Wspread_Exp(E, BWspread(B(E, Pz, Vz,SS),GRO.RO,JRO.RO,A), HRO.RO).main() @ &m: BS.eq ] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BE(SS, C), JRO.RO).main() @ &m : res] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(E, HRO.RO, GRO.RO, A, SS), JRO.RO)), JRO.RO).main () @ &m : res] +
(size (undup Voters))%r * 
  Pr[Correctness(SS, C, JRO.RO).main() @ &m : !res] +
  Pr[Acc_Exp (B(E,Pz,Vz,SS), BAcc(B(E,Pz,Vz,SS),A), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m: !res] + 
  Pr[TallyUniq_Exp (B(E,Pz,Vz,SS), BTall(B(E,Pz,Vz,SS),A), 
        HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]. 
proof.
  move => C_ll.
  cut Hp:= dbb_dbbwspread C &m C_ll.
  cut := dbbdish_dbbdishtwo &m.
  cut := dbbdishtwo_dbbresult &m.
  cut := dbbresult_dbbvf &m.
  cut := verifreplacevf_accuracy &m.
  cut := dbbvf_tallyuniq &m. 
  by smt.
qed.
end section DBB.


section Bound_TallyUniq. 
(* ** abstract algorithms ** *)
declare module C: ValidInd        . 
declare module E: Scheme          {BW, C,   BSC, BPS, JRO.RO, HRO.RO, GRO.RO, JRO.RO}.
declare module Pz: Prover         {BW, C,   E, BPS, BSC, JRO.RO, HRO.RO, GRO.RO}.
declare module Vz: Verifier       {BW, C,   Pz, E, BPS, BSC, JRO.RO, HRO.RO, GRO.RO, JRO.RO}.
declare module Ex: PoK_Extract    {BW, BSC,Pz, BPS, E, GRO.RO, JRO.RO, Vz, HRO.RO}.
declare module SS: SignScheme     {BW, BPS, Vz, E, Pz, Ex, HRO.RO, JRO.RO, GRO.RO}.

(* work around for the fact that the TallyUniq Adv that uses Wverif_Adv 
   has access to the voting scheme *)
module type TallyUniq_Adv'(V: VotingScheme, H : HOracle.ARO,
                           G : GOracle.ARO, J: JOracle.ARO) = {
  proc main() : (ident * upkey * cipher*sign) list *
                pkey * result * prf * result * prf *hash_key {H.o G.o J.o}
}.

(* ** ASSUMPTIONS ** *)
(* ** start *)

  axiom weight_dh_out:
    weight dh_out = 1%r.
  axiom weight_dg_out:
    weight dg_out = 1%r.
  axiom weight_dj_out:
    weight dj_out = 1%r.
  axiom weight_dk_out:
    weight dhkey_out = 1%r.
  axiom is_finite_h_in : Finite.is_finite predT <:h_in>.
  axiom is_finite_g_in : Finite.is_finite predT <:g_in>.
  axiom is_finite_j_in : Finite.is_finite predT <:j_in>.

   axiom get_pk_inj (x y: skey):
     get_pk x = get_pk y => x = y.
 
(* lossless assumptions *) 
  axiom Ee_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).enc.
  axiom Ed_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).dec.
  axiom Ex_ll (G <: GOracle.ARO):
    islossless G.o =>
    islossless Ex(G).extract.
  axiom Vz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Vz(G).verify.

  axiom A_ll (A<: TallyUniq_Adv') (V <: VotingScheme{A}) (H <: HOracle.ARO{A}) 
             (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
  islossless A(V, H, G, J).main.

  axiom dec_Dec_one (ge: (glob E)) (sk2: skey)(l2: upkey) (c2: cipher):
    phoare [E(HRO.RO).dec:  
           (glob E) =ge /\ arg = (sk2, l2, c2)
           ==>   
           (glob E) =ge /\
           res = dec_cipher sk2 l2 c2 HRO.RO.m ] = 1%r.

local lemma R_ll (H<: HOracle.ARO):
    islossless H.o => islossless DecRel(E, H).main.
proof.
  move => Ho.
  proc.
  wp; while{1} (true) (size stm.`2 -i); progress.
    wp; call{1} (Ed_ll H Ho).
    by auto; smt.
  by auto; smt.  
qed.

(* tally uniqueness experiment + extractor for witness *)
local module TallyUniqE_Exp (V: VotingScheme, A: TallyUniq_Adv,
                      Ex: PoK_Extract, R: Relation,
                      H: HOracle.Oracle, G: GOracle.Oracle, 
                      J: JOracle.Oracle)={
  module PO = PoK_Oracle(G) 
  proc main(): bool ={
    var bb, pk, r1 , r2, p1, p2, ev1, ev2, sk1, sk2, rel1, rel2,hk;

    BPS.b <- false;

    H.init();
    PO.init();
    J.init();

    (bb, pk, r1, p1, 
             r2, p2,hk) <@ A(H,PO,J).main();
    BW.pbb <@ V(H,G,J).publish(bb,hk);
    ev1   <@ V(H,G,J).verify ((pk, BW.pbb, r1), p1);
    sk1   <@ Ex(G).extract((pk, BW.pbb, r1), p1, BPS.io);
    rel1  <@ R.main((pk, BW.pbb, r1), sk1);

    ev2   <@ V(H,G,J).verify ((pk, BW.pbb, r2), p2);
    sk2   <@ Ex(G).extract((pk, BW.pbb, r2), p2, BPS.io);
    rel2  <@ R.main((pk, BW.pbb, r2), sk2);

    BPS.b <- (r1<> r2) /\ rel1 /\ rel2;
    return ev1 /\ ev2 /\ (r1<> r2) /\ !BPS.b;
  }
}. 

(* add witness extraction to the tally uniqueness expriment *) 
local lemma tally_uniq_witness 
            (A <: TallyUniq_Adv' { HRO.RO, JRO.RO, GRO.RO, BPS, Vz}) &m:
  Pr[TallyUniq_Exp(B(E,Pz,Vz,SS), A(B(E,Pz,Vz,SS)), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res] <=
  Pr[TallyUniqE_Exp(B(E,Pz,Vz,SS), A(B(E,Pz,Vz,SS)), Ex, DecRel(E,HRO.RO), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res ]+ 
   Pr[TallyUniqE_Exp(B(E,Pz,Vz,SS), A(B(E,Pz,Vz,SS)), Ex, DecRel(E,HRO.RO), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  BPS.b].
proof.  
 
  byequiv (_: ={glob A, glob Vz} ==> _)=>//=.
  proc.
  wp.
  call{2} (R_ll HRO.RO HRO.RO_o_ll).
  call{2} (Ex_ll GRO.RO GRO.RO_o_ll).
  call (_: ={ glob GRO.RO, glob Vz}); first by sim.
  call{2} (R_ll HRO.RO HRO.RO_o_ll).
  call{2} (Ex_ll GRO.RO GRO.RO_o_ll).
  call (_: ={glob GRO.RO, glob Vz}); first by sim.
  call (_: ={glob HRO.RO}); first by sim.
  (* adv call, where right side does more in G *)
  call (_: ={glob HRO.RO, glob GRO.RO, glob JRO.RO}).
  + sim. 
  + by proc; inline *; wp.
  + sim.
  inline TallyUniqE_Exp(B(E,Pz,Vz,SS), 
                        A(B(E,Pz,Vz,SS)), Ex, DecRel(E,HRO.RO), 
                        HRO.RO, GRO.RO, JRO.RO).PO.init.
   call (_: true ==> ={glob JRO.RO});
    first by sim.
  call (_: true ==> ={glob GRO.RO});
    first by sim.
  wp; call (_: true ==> ={glob HRO.RO});
    first by sim.
  by auto. 
qed.

module PoK_TU(A:TallyUniq_Adv, H: HOracle.Oracle, 
              J: JOracle.Oracle, G:GOracle.ARO) ={

  proc main() ={
    var bb, pk, r1 , r2, p1, p2,hk; 
    H.init();
    J.init();

    (bb, pk, r1, p1, 
             r2, p2, hk) <@ A(H,G,J).main();
    BW.pbb <- map (fun (x: ident*upkey * cipher * sign), 
                    (x, hash' hk x)) bb;
    return ((pk, BW.pbb, r1), p1, (pk, BW.pbb,r2), p2);
  }

}.

local lemma tally_uniq_pok 
            (A <: TallyUniq_Adv' {E, Ex, JRO.RO, HRO.RO, GRO.RO, Vz, BPS}) &m:
  Pr[TallyUniqE_Exp(B(E,Pz,Vz,SS), A(B(E,Pz,Vz,SS)), Ex, DecRel(E,HRO.RO), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res ] <=
  Pr[PoK_Exp(Vz, DecRel(E, HRO.RO), Ex, PoK_TU(A(B(E,Pz,Vz,SS)), HRO.RO, JRO.RO), 
         GRO.RO).main() @ &m :  res].
proof.  
  byequiv (_: ={glob A, glob E, glob Vz, glob Ex} ==> _ )=>//=.
  proc.
  inline PoK_TU(A(B(E,Pz,Vz,SS)), HRO.RO, JRO.RO, PoK_Exp(Vz, DecRel(E, HRO.RO), 
                Ex, PoK_TU(A(B(E,Pz,Vz,SS)), HRO.RO, JRO.RO), GRO.RO).PO).main
    B(E, Pz, Vz, SS, HRO.RO, GRO.RO, JRO.RO).publish.
  wp.
  conseq (_: _ ==> ={ rel1, rel2, ev1, ev2})=>//=; first by smt.
  call (_: ={glob E, glob HRO.RO});
    first by sim.
  call (_: ={glob GRO.RO}); first by sim.
  inline B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).verify.
  wp; call (_: ={glob GRO.RO}); first by sim.
  wp; call (_: ={glob E, glob HRO.RO});
    first by sim.
  call (_: ={glob GRO.RO}); first by sim.
  wp; call (_: ={glob GRO.RO}); first by sim.
  wp; call (_: ={glob HRO.RO, glob GRO.RO, glob JRO.RO, BPS.io}); 
    first 3 by sim.
  swap{2} 2 -1.
  call (_: true ==> ={glob JRO.RO});
    first by sim.
  call (_: true ==> ={glob GRO.RO, BPS.io});
    first by sim.
  call (_: true ==> ={glob HRO.RO});
    first by sim. 
  by auto.  
qed.

module Buls ={
  proc main(): bool ={
    return false;
  }
}.

local lemma bul &m:
  Pr[Buls.main() @ &m : res] = 0%r.
proof.
  by byphoare=>/>; proc.
qed.

local lemma Ax_ll (A <: TallyUniq_Adv' {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS}):
    islossless A(B(E, Pz, Vz, SS), HRO.RO, 
                 TallyUniqE_Exp(B(E, Pz, Vz, SS), A(B(E, Pz, Vz, SS)), 
                 Ex, DecRel(E, HRO.RO), HRO.RO, GRO.RO, JRO.RO).PO, JRO.RO).main.
proof.
  cut All:= (A_ll A (B(E, Pz, Vz, SS)) HRO.RO 
                (<:TallyUniqE_Exp(B(E, Pz, Vz, SS), 
                                  A(B(E, Pz, Vz, SS)), Ex, DecRel(E, HRO.RO), 
                                  HRO.RO, GRO.RO, JRO.RO).PO) 
                JRO.RO HRO.RO_o_ll _ JRO.RO_o_ll).  
    by proc; inline*; wp.
  by proc* ;call{1} All. 
qed.

local lemma tally_uniq_result_uniq 
            (A <: TallyUniq_Adv' {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS}) &m:
  Pr[TallyUniqE_Exp(B(E,Pz,Vz,SS), A(B(E,Pz,Vz,SS)), Ex, DecRel(E,HRO.RO), 
         HRO.RO, GRO.RO,JRO.RO).main() @ &m :  BPS.b ] =  Pr[Buls.main() @ &m : res].
proof.  
  byequiv=>/>.
  proc.
  wp. 
  seq 5 0: (true).  
    call{1} (Ax_ll A). 
    inline*; wp. 
    while{1} (true) (card work0{1}); progress.  
      by auto; smt. 
    wp; while{1} (true) (card work1{1}); progress.  
      by auto; smt. 
    wp; while{1} (true) (card work{1}); progress.  
      by auto; smt.  
    by auto; smt. 
  inline*; wp.
  wp; while{1} (0 <= i0{1} /\
                let f = fun (x: ident*upkey*cipher*sign),
                        (x.`2, oget (dec_cipher wit0{1} x.`2 x.`3 HRO.RO.m{1})) in
               ubb0{1} = map (f\o fst) (take i0{1} stm0.`2{1})) 
               (size stm0.`2{1} - i0{1}); progress.
    wp; sp.
    exists* (glob E), wit0, b2; elim*=> ge skx bx.
    call{1} (dec_Dec_one ge skx bx.`2 bx.`3). 
    auto =>/>; progress.
    + smt. 
    + rewrite (take_nth witness). 
        by rewrite H0 H1. 
      by rewrite map_rcons /(\o) cats1 -H //=. 
    + smt.
  wp; call{1} (Ex_ll GRO.RO GRO.RO_o_ll). 
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
  wp; while{1} (0 <= i{1} /\
                let f = fun (x: ident*upkey*cipher*sign),
                        (x.`2,oget (dec_cipher wit{1} x.`2 x.`3 HRO.RO.m{1})) in
               ubb{1} = map (f\o fst) (take i{1} stm.`2{1})) 
               (size stm.`2{1} - i{1}); progress.
    wp; sp.
    exists* (glob E), wit, b0; elim*=> ge skx bx.
    call{1} (dec_Dec_one ge skx bx.`2 bx.`3). 
    auto =>/>; progress.
    + smt. 
    + rewrite (take_nth witness). 
        by rewrite H0 H1. 
      by rewrite map_rcons /(\o) cats1 -H //=. 
    + smt.
  wp; call{1} (Ex_ll GRO.RO GRO.RO_o_ll).
  wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
  auto=>/>; progress.
  + smt (take0).
  + smt (take_oversize).
  + smt (take0).
  + smt (take_oversize).
  + pose L:= map (fun (x : ident*upkey * cipher * sign) => 
                    (may_data x, hash' hk{1} x)) (USanitize bb{1}).
    pose g1:= (fun (x : ident*upkey * cipher * sign) =>
                   oget (dec_cipher result x.`2 x.`3 HRO.RO.m{1})).
    pose g2:=(fun (x : ident*upkey * cipher * sign) =>
                   oget (dec_cipher result0 x.`2 x.`3 HRO.RO.m{1})).
    rewrite ?take_oversize; first 2 by smt.
    pose fst:= fun (p : (ident*upkey * cipher * sign) * hash_out) => p.`1. 
    case (get_pk result = get_pk result0).
    + move => Hpk. 
      cut Hsk:= get_pk_inj _ _ Hpk.
      rewrite /g1 Hsk -/g2.
      smt. 
    - move => Hpk.
      have Hsk: result <> result0.
        cut Hsk:= get_pk_inj result result0.
        smt.
      case (pk{1} = get_pk result).
      + move => Hpk1.
        have ->: !pk{1} = get_pk result0. smt.
        rewrite neqF ?neg_and //=.
      - move => Hpk1.
        by rewrite neqF ?neg_and //=.
qed.


local lemma tally_uniq_result_uniq' 
            (A <: TallyUniq_Adv' {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS}) &m:
  Pr[TallyUniqE_Exp(B(E,Pz,Vz,SS), A(B(E,Pz,Vz,SS)), Ex, DecRel(E,HRO.RO), 
         HRO.RO, GRO.RO,JRO.RO).main() @ &m :  BPS.b ] = 0%r.
proof.  
  by rewrite (tally_uniq_result_uniq A &m) (bul &m). 
qed.


lemma tally_uniq 
      (A <: TallyUniq_Adv' {E, Ex, HRO.RO, JRO.RO, GRO.RO, Vz, Pz, SS, BPS}) &m:
  Pr[TallyUniq_Exp(B(E,Pz,Vz,SS), A(B(E,Pz,Vz,SS)), 
         HRO.RO, GRO.RO,JRO.RO).main() @ &m :  res] <=
  Pr[PoK_Exp(Vz, DecRel(E, HRO.RO), Ex, PoK_TU(A(B(E,Pz,Vz,SS)), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m :  res].
proof.
  cut B1:= tally_uniq_witness A &m.
  cut B2:= tally_uniq_pok A &m.
  cut B3:= tally_uniq_result_uniq' A &m.
  by smt.
qed.

section Test_DBB.
declare module Adbb:  DBB_Adv     {BPS, Ex,   SS, BW, BSC, HRO.RO, JRO.RO, GRO.RO, Pz, Vz, C, E}.

local module BTU(A: DBB_Adv, V: VotingScheme) = BTall(V,A).

lemma bound_tally_uniq &m:
   Pr[TallyUniq_Exp (B(E,Pz,Vz,SS), BTall(B(E,Pz,Vz,SS),Adbb), 
        HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] <=
  Pr[PoK_Exp(Vz, DecRel(E, HRO.RO), Ex, 
             PoK_TU(BTall(B(E, Pz, Vz, SS), Adbb), 
                    HRO.RO, JRO.RO), GRO.RO).main () @ &m :  res].
proof.
  have ->: Pr[TallyUniq_Exp(B(E,Pz,Vz,SS), 
                   BTall(B(E,Pz,Vz,SS), Adbb), 
                   HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res]=
           Pr[TallyUniq_Exp(B(E,Pz,Vz,SS), 
                   BTU(Adbb, B(E,Pz,Vz,SS)), 
                   HRO.RO, GRO.RO, JRO.RO).main() @ &m :  res];
    first by byequiv =>//=; sim.
  have ->: Pr[PoK_Exp(Vz, DecRel(E, HRO.RO), Ex, 
                   PoK_TU(BTall(B(E,Pz,Vz,SS), Adbb), 
                   HRO.RO, JRO.RO), GRO.RO).main () @ &m :  res] =
           Pr[PoK_Exp(Vz, DecRel(E, HRO.RO), Ex, 
                   PoK_TU(BTU(Adbb, B(E,Pz,Vz,SS)), 
                   HRO.RO, JRO.RO), GRO.RO).main () @ &m :  res];
    first by byequiv =>//=; sim. 
  by rewrite (tally_uniq  (<: BTU(Adbb)) &m).
qed.

end section Test_DBB.

end section Bound_TallyUniq.


section Bound_Acc.
(* ** abstract algorithms ** *)
declare module C: ValidInd        . 
declare module E: Scheme          {BSC, BW, HRO.RO, GRO.RO, JRO.RO}.
declare module Pz: Prover         {E, BW, BPS, BSC, HRO.RO, GRO.RO, JRO.RO}.
declare module Vz: Verifier       {Pz, E, BPS, BSC, BW, HRO.RO, GRO.RO, JRO.RO}.
declare module SS: SignScheme     {E, Pz, Vz, BW, GRO.RO, HRO.RO, JRO.RO, BSC}.

(* accuracy adversary with access to a voting scheme *)
module type Acc_Adv'(V: VotingScheme, H : HOracle.ARO, 
                     G : GOracle.ARO, J: JOracle.ARO) = {
  proc main(sk : skey, ul : (ident, upkey*uskey) map,
            ull : (ident * uskey * upkey) list, hk:hash_key) 
           : (ident * upkey * cipher*sign) list 
}.

(* ** ASSUMPTIONS ** *)
(* ** start *)

(* lossless assumptions *)
  axiom Ee_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).enc.
  axiom Ed_ll (H <: HOracle.ARO): 
    islossless H.o => islossless E(H).dec.
  axiom Pz_ll  (G <: GOracle.ARO): 
    islossless G.o => islossless Pz(G).prove.
  axiom Vz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Vz(G).verify.

  axiom Ekgen_extractPk (H<: HOracle.ARO):
    equiv [E(H).kgen ~ E(H).kgen:  ={glob H, glob E} ==> 
          ={glob H, glob E,  res} /\ 
          res{2}.`1 = get_pk res{2}.`2].

(*** *)
local lemma R_ll (H<: HOracle.ARO):
    islossless H.o => islossless DecRel(E, H).main.
proof.
  move => Ho.
  proc.
  wp; while{1} (true) (size stm.`2 -i); progress.
    wp; call{1} (Ed_ll H Ho).
    by auto; smt.
  by auto; smt.  
qed.   

module BCorr_Adv(E: Scheme, B: Acc_Adv, H: HOracle.Oracle, SS: SignScheme,
                 J: JOracle.Oracle, G: GOracle.ARO) ={
  proc main(): (pkey * (pub_data * hash_out) list * result) * skey  = {
    var ubb, i, b, m, id, bb,  upk, usk, pbb, ull,e;

    H.init();
    J.init();

    (BSC.pk, BSC.sk) <@ E(H).kgen();
    BSC.hk <$ dhkey_out;
    (* assign labels for ids *)
    BSC.uL <- empty;
    ull <- [];
    i <- 0;
    while (i < size (undup Voters)){
      id     <- nth witness (undup Voters) i;
      (usk,upk) <@ SS(J).kgen();
      BSC.uL.[id]<- (upk,usk);
      ull <- ull ++ [(id, (oget BSC.uL.[id]).`2,(oget BSC.uL.[id]).`1)];
      i      <- i + 1;
    }

    bb <@ B(H,G,J).main(BSC.sk, BSC.uL, ull, BSC.hk);
  
    ubb  <- [];
    i   <- 0;
     while( i < size (map remi bb)){
      b  <- nth witness (map remi bb) i;
      m  <@ E(H).dec(BSC.sk, b.`1, b.`2);
      e  <@ SS(J).verify(b.`1, b.`2, b.`3);
      (* valid dec and sign *)
      if (m <>None /\ e){
        ubb <- ubb ++ [(b.`1, oget m)];
      }
      i  <- i + 1;
    }
    
    pbb <- map (fun (x: ident*upkey * cipher * sign), 
                    (x, hash' BSC.hk x)) bb;

    return ((BSC.pk, pbb, Count (map snd (Sanitize ubb))), BSC.sk);
  }
}.

local lemma bound_acc (BA <: Acc_Adv'  {SS, E, BSC, HRO.RO, GRO.RO, JRO.RO, Pz, Vz}) &m:
  Pr[Acc_Exp(B(E,Pz,Vz,SS), BA(B(E,Pz,Vz,SS)), 
             HRO.RO, GRO.RO, JRO.RO).main () @ &m : !res]<=
  Pr[PS.Correctness (DecRel(E,HRO.RO), BCorr_Adv(E,BA(B(E,Pz,Vz,SS)),
                                    HRO.RO, SS, JRO.RO), 
                  Pz, Vz, GRO.RO).main() @ &m: !res].
proof.
  byequiv=>//=.
  proc.
  inline BCorr_Adv(E, BA(B(E,Pz,Vz,SS)), HRO.RO, SS, JRO.RO, GRO.RO).main
         B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).setup
         B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).tally
         B(E,Pz,Vz,SS, HRO.RO, GRO.RO, JRO.RO).verify.

  seq 11 10 :( ={glob BA, glob SS, glob Vz, glob E, glob Pz, bb,
                HRO.RO.m, GRO.RO.m, JRO.RO.m}/\
              pk{1} = BSC.pk{2} /\ sk{1} = BSC.sk{2} /\
             pk{1} = get_pk sk{1}/\
             hk{1} = BSC.hk{2}).
    call(: ={glob HRO.RO, glob GRO.RO, glob JRO.RO});
      first 3 by sim. 
    while( ={i, glob SS, GRO.RO.m}/\
           uL{1} = BSC.uL{2} /\ uLL{1} = ull{2}).
      inline*; wp.
      call(: true). 
      by auto.
    wp; rnd; call{1} (Ekgen_extractPk HRO.RO). 
    swap{2} 1.
    call (_: true ==> ={glob JRO.RO});
      first by sim.
    call (_: true ==> ={glob GRO.RO});
      first by sim.
    call (_: true ==> ={glob HRO.RO});
      first by sim. 
    by auto.

  seq 10 7: (={glob Pz, glob Vz, GRO.RO.m, bb}/\
            pbb{1} = map (fun (x: ident*upkey * cipher * sign), 
                         (x, hash' BSC.hk{2} x)) bb{1}/\
                       (pk1,pbb,r0){1} = s{2} /\ sk1{1} =w{2} /\ !b{2}/\ pk{1} = pk1{1}/\
           hk{1} = BSC.hk{2}).
   wp; call{2} (R_ll HRO.RO HRO.RO_o_ll). wp; progress.

   wp; while (={glob E, glob SS, HRO.RO.m, JRO.RO.m, ubb}/\
              fbb{1} = map remi bb{2} /\ i0{1} = i{2} /\ 
              sk1{1} = BSC.sk{2}). 
     wp; call(: ={glob JRO.RO}); first by sim.
     call(: ={glob HRO.RO}); first by sim.
     by auto.
   by auto=>/>; progress. 
    
  if{2}=>//=.
  + inline*; wp. 
     call(: ={GRO.RO.m}); first by sim.
     wp; call(: ={GRO.RO.m}); first by sim.
     by auto. 
  - wp; call{1} (Vz_ll GRO.RO GRO.RO_o_ll).
    inline*; wp; call{1} (Pz_ll GRO.RO GRO.RO_o_ll).
    by auto.
qed. 

section Test_DBB.

local module BAC(A: DBB_Adv, V: VotingScheme) = BAcc(V,A).

lemma bound_acc_dbb (A <: DBB_Adv {SS, E, BSC, HRO.RO, GRO.RO, JRO.RO, Pz, Vz})&m:
  Pr[Acc_Exp (B(E,Pz,Vz,SS), BAcc(B(E,Pz,Vz,SS),A), 
         HRO.RO, GRO.RO, JRO.RO).main() @ &m: !res]<=
  Pr[PS.Correctness (DecRel(E,HRO.RO), BCorr_Adv(E,BAcc(B(E,Pz,Vz,SS),A),
                                    HRO.RO, SS, JRO.RO), 
                  Pz, Vz, GRO.RO).main() @ &m: !res].
proof.
  by rewrite (bound_acc (<: BAC(A)) &m). 
qed.
end section Test_DBB.

end section Bound_Acc.  
