require import Int Bool Real FMap IntExtra RealExtra.
require import List Distr Logic.
require import LeftOrRight.
require import DatatypesExt.
require (*  *) Belenios_SEC_VERIF_DREG_2. 
require (*  *) Ex_Plug_and_Pray.
require (*  *) ROM. 

type ident, upkey, cipher, sign.

clone include Belenios_SEC_VERIF_DREG_2 with
  type ident <- ident,
  type upkey <- upkey,
  type PKE.cipher <- cipher, 
  type sign  <- sign.
  (* type pub_data = maybe ident * upkey * cipher * sign,
   *)


(* admit. fixme: to make pub_data more general *)
(* admit. fixme: link ver_sign to the one in DBB proof *)


module BBS ={
  var bb  : (ident * upkey * cipher * sign) list
  var hvo : (int * cipher * upkey, vote) map
  var encL: (cipher * upkey) list
  var badQ: int option
}.

(* security concepts:
   + ballot privacy, with main lemma bpriv,
   + strong consistency with the lemmas consis1, consis2, consis3, and
   + strong correctness with the lemma scorr and scorr_bpriv *) 

(* ---------------------------------------------------------------------- *)
(* Ballot Privacy *)
section BPRIV. 

declare module E: Scheme      {BP, BS, BBS, BPS, HRO.RO, GRO.RO, JRO.RO,
                               H.Count, H.HybOrcl, WrapAdv}.
declare module P: Prover      {BP, BS, BPS, JRO.RO, HRO.RO, GRO.RO, E}.
declare module Ve: Verifier   {BP, JRO.RO, HRO.RO, GRO.RO, E, P}.
declare module SS: SignScheme {BP, BS, BBS, BPS, HRO.RO, GRO.RO, JRO.RO, E, P, Ve,
                               H.Count, H.HybOrcl, WrapAdv}.
declare module S : Simulator  {BP, BS, BBS, BPS, HRO.RO, GRO.RO, JRO.RO, E, P, Ve,SS,
                               H.Count, H.HybOrcl, WrapAdv}.
declare module C: ValidInd'    {BP, BS, BPS, BBS, GRO.RO, HRO.RO, JRO.RO, E,P, Ve, SS, S,
                               H.Count, H.HybOrcl, WrapAdv}.
(* BPRIV adversary *)
declare module A: BPRIV_Adv   {BP, BS, BBS, BPS, HRO.RO, GRO.RO, JRO.RO, E, P, Ve,SS, S, C,
                               H.Count, H.HybOrcl, WrapAdv}. 

(* ** ASSUMPTIONS ** *)
(* ** start *)
  (* lossless *)
  axiom weight_dg_out:
     weight dg_out = 1%r. 
  axiom weight_dj_out:
    weight dj_out = 1%r.
  axiom weight_dk_out:
    weight dhkey_out = 1%r.
  axiom is_finite_h_in : Finite.is_finite predT <:h_in>.
  axiom distr_h_out    : is_lossless dh_out.


  axiom San_map (L: (upkey*cipher*sign)list) 
                (p: upkey * cipher* sign -> vote):
   let g = (fun (x: upkey*cipher*sign), (x.`1, p x)) in
   Sanitize (map g L) = map g (USan L).

  axiom USan_USanitize (A: (ident *upkey*cipher*sign) list):
    map remi (USanitize A) = USan (map remi A). 

  (* -> for BPRIV adversary *)
  axiom Acor_ll (O <: BPRIV_Oracles{A}): 
    islossless A(O).corL.

  axiom Aa1_ll (O <: BPRIV_Oracles { A }):
 (*   islossless O.corrupt => *)
    islossless O.vote    =>
    islossless O.cast    =>
    islossless O.board   =>
    islossless O.h       =>
    islossless O.g       =>
    islossless O.j       =>
    islossless A(O).a1.
  axiom Aa2_ll (O <: BPRIV_Oracles { A }):
    islossless O.board =>
    islossless O.h     =>
    islossless O.g     =>
    islossless O.j     =>
    islossless A(O).a2.

  (* -> for proof system *)
  axiom Pp_ll (G <: GOracle.ARO): 
    islossless G.o =>
    islossless P(G).prove. 
  axiom Vv_ll (G <: GOracle.ARO):
    islossless G.o =>
    islossless Ve(G).verify.

  (* -> for encryption *)
  axiom Ek_ll (H <: HOracle.ARO): 
    islossless H.o =>
    islossless E(H).kgen.
  axiom Ee_ll(H <: HOracle.ARO): 
    islossless H.o =>
    islossless E(H).enc.
  axiom Ed_ll(H <: HOracle.ARO): 
    islossless H.o =>
    islossless E(H).dec.

  (* -> for signature *)
  axiom SSk_ll (J <: JOracle.ARO): 
    islossless J.o =>
    islossless SS(J).kgen.
  axiom SSs_ll (J <: JOracle.ARO): 
    islossless J.o =>
    islossless SS(J).sign. 
  axiom SSv_ll (J <: JOracle.ARO): 
    islossless J.o =>
    islossless SS(J).verify. 

  (* -> for ZK simulator *)
  axiom Si_ll: islossless S.init.
  axiom So_ll: islossless S.o.
  axiom Sp_ll: islossless S.prove.

  (* -> for Strong consistency Op *)
  axiom Co_ll (H <: HOracle.ARO) (J<: JOracle.ARO):
    islossless H.o =>
    islossless J.o =>
    islossless C(E,SS,H,J).validInd. 


  (* axiom for stating that the keys are generated *)
  axiom Ekgen_extractPk (H<: HOracle.ARO):
    equiv [E(H).kgen ~ E(H).kgen:  ={glob H, glob E} ==> 
          ={glob H, glob E,  res} /\ 
          res{2}.`1 = get_pk res{2}.`2].

  (* axiom for linking E.dec to dec_cipher operator *)   
  axiom Edec_Odec (ge: (glob E)) (sk2: skey)(l2: upkey) (c2: cipher):
    phoare [E(HRO.RO).dec:  
           (glob E) =ge /\ arg = (sk2, l2, c2)
           ==>   
           (glob E) =ge /\
           res = dec_cipher sk2 l2 c2 HRO.RO.m ] = 1%r.

  (* axiom on the state of the encryption scheme E *)
  axiom Ee_eq (ge: (glob E)):
    phoare [E(HRO.RO).enc: 
          (glob E) = ge ==> (glob E) = ge] = 1%r.
  
  (* axiom for transforming an encryption into decryption (one-sided) *)
  axiom Eenc_decL (ge: (glob E)) (sk2: skey) 
                  (pk2: pkey)(l2: upkey) (p2: vote): 
    (pk2 = get_pk sk2) =>
    phoare [E(HRO.RO).enc : 
          (glob E) = ge /\ arg=(pk2, l2, p2) 
          ==> 
          (glob E) = ge /\
          Some p2 = dec_cipher sk2 l2 res HRO.RO.m ]= 1%r.   

  (* axiom for transforming an encryption into decryption (two-sided) *)
  axiom Eenc_dec (sk2: skey) (pk2: pkey) (l2: upkey) (p2: vote): 
    (pk2 = get_pk sk2) =>
    equiv [E(HRO.RO).enc ~ E(HRO.RO).enc : 
          ={glob HRO.RO, glob E, arg} /\ arg{2}=( pk2, l2, p2) 
          ==> 
          ={glob HRO.RO, glob E,  res} /\
          Some p2 = dec_cipher sk2 l2 res{2} HRO.RO.m{2}].   

  (* axiom on the state of the signature scheme S *)
  axiom SSs_eq (gss: (glob SS)):
    phoare [SS(JRO.RO).sign: 
          (glob SS) = gss ==> (glob SS) = gss] = 1%r.
   axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)
             ==>
            (glob SS) =gs /\
            res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
   axiom Sig_ver_one (gs: (glob SS)) (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   phoare [SS(JRO.RO).sign :
           gs= (glob SS) /\ arg = (usk2,c2)
           ==>   
           gs = (glob SS) /\
           ver_sign upk2 c2 res JRO.RO.m ]=1%r.
   axiom Sig_ver_two (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   equiv [SS(JRO.RO).sign ~ SS(JRO.RO).sign :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (usk2,c2)
           ==>   
           ={glob JRO.RO, glob SS, res} /\
           ver_sign upk2 c2 res{2} JRO.RO.m{2} ].

   axiom validInd_dec_one (gc: (glob C)) (ge: (glob E)) (gs: (glob SS))
                      (sk: skey) (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
    (pk = get_pk sk)=>
     phoare [C(E,SS,HRO.RO,JRO.RO).validInd : 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\ arg=(b,pk) 
          ==> 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\
          res = (dec_cipher sk b.`2 b.`3 HRO.RO.m <> None /\
                 ver_sign b.`2 b.`3 b.`4 JRO.RO.m)]=1%r.
(* ** end *)

(* decryption should not change the state of an eager random oracle *) 
local lemma dec_Dec_two (sk2: skey)(l2: upkey) (c2: cipher):
  equiv [E(HRO.RO).dec ~ E(HRO.RO).dec :
          ={glob HRO.RO, glob E, arg}/\ arg{2} = (sk2, l2, c2)
           ==>   
           ={glob HRO.RO, glob E, res} /\
           res{2} = dec_cipher sk2 l2 c2 HRO.RO.m{2} ].
proof.
  proc*=>//=. 
  exists* (glob E){1}; elim* => ge.
  call{1} (Edec_Odec ge sk2 l2 c2 ).
  call{2} (Edec_Odec ge sk2 l2 c2 ). 
  by auto.
qed.

local lemma ver_Ver_two (upk2: upkey) (c2: cipher) (s2: sign):
  equiv [SS(JRO.RO).verify ~ SS(JRO.RO).verify :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (upk2,c2,s2)
           ==>   
           ={glob JRO.RO, glob SS, res} /\
           res{2} = ver_sign upk2 c2 s2 JRO.RO.m{2} ].
proof.
  proc*=>//=. 
  exists* (glob SS){1}; elim* => gs.
  call{1} (ver_Ver_one gs upk2 c2 s2).
  call{2} (ver_Ver_one gs upk2 c2 s2). 
  by auto.
qed.

local lemma validInd_dec (sk: skey) (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
    (pk = get_pk sk)=>
     equiv [C(E,SS,HRO.RO,JRO.RO).validInd ~ C(E,SS,HRO.RO,JRO.RO).validInd : 
          ={glob HRO.RO, glob JRO.RO, glob C, glob E, glob SS, arg} /\ arg{2}=(b,pk) 
          ==> 
          ={glob HRO.RO, glob JRO.RO, glob C, glob E, glob SS, res} /\
          res{1} = (dec_cipher sk b.`2 b.`3 HRO.RO.m{1} <> None /\
                    ver_sign b.`2 b.`3 b.`4 JRO.RO.m{1})].
proof.
  move => Hk.
  proc*.
  exists* (glob C){1}, (glob E){1}, (glob SS){1}; elim* => gc ge gs.
  call{1} (validInd_dec_one gc ge gs sk pk b Hk).
  call{2} (validInd_dec_one gc ge gs sk pk b Hk).
  by auto.
qed.

(* the state of the simulated is the same in two-sided call *)
local equiv So_ll2: S.o ~ S.o : ={arg, glob S} ==> ={res, glob S}
  by proc true.   


(* Constructed ZK adversary from BPRIV adversary *)
module BZK(E: Scheme, P: Prover, C: ValidInd, Ve: Verifier, SS: SignScheme,
           A:BPRIV_Adv, 
           H: HOracle.Oracle, J: JOracle.Oracle, G: GOracle.ARO) = {

  module O = BPRIV_Oracles(B'(E,P,Ve,C,SS), H, G, J, Left)

  proc a1(): (pkey * (pub_data * hash_out) list * result) * 
             skey  = {
    var upk, usk, m, b, id,e ;
    var ubb, pbb, i;
    
    i      <- 0;
    BP.qVo <- 0;
    BP.qCa <- 0;
    BP.bb0 <- [];
    BP.bb1 <- [];
    BP.uLL <- [];
    BP.coL <- [];
    ubb    <- [];
    BP.uL  <- empty;

    
                     H.init();
                     J.init();
    (BP.pk,BP.sk) <@ E(H).kgen();
    BP.hk <$ dhkey_out;
    while (i< size (undup Voters)){
      id     <- nth witness (undup Voters) i;
      (upk,usk) <@ SS(J).kgen();
      BP.uL.[id] <- (usk,upk);
      BP.uLL <- BP.uLL ++ [(id, (oget BP.uL.[id]).`1)];
      i     <- i + 1;
    }
    BP.coL <@ A(O).corL(BP.pk, BP.uLL);   
                     A(O).a1(BP.pk, filter (fun (a: ident) (b: upkey * uskey), 
                                          a \in BP.coL) BP.uL);                 
    i <- 0;
    while( i < size (map remi BP.bb0)){
      b  <- nth witness (map remi BP.bb0) i;
      m  <@ E(H).dec(BP.sk, b.`1, b.`2);
      e  <@ SS(J).verify(b.`1, b.`2, b.`3);
      if (m <>None /\ e){
        ubb <- ubb ++ [(b.`1, oget m)];
      }
      i  <- i + 1;
    }
    
    BP.r    <- Count (map snd (Sanitize ubb));
    
    pbb <- map (fun (x: ident*upkey * cipher * sign), 
                    (may_data x, hash' BP.hk x)) (USanitize BP.bb0);
    return ( (BP.pk, pbb, BP.r), BP.sk);
  }
  
  proc a2(pi: prf option ): bool = {
    BP.b <@ A(O).a2(BP.r, oget pi);
    return BP.b;
  }  
}.


(* zero-knowledge adversary that may query 3 random oracles:
   one from the encryption scheme, one from the proof system, and one from the signature *)
module type VotingAdvZK (H: HOracle.Oracle, J: JOracle.Oracle, O : GOracle.ARO) = {
  proc a1() : (pkey * (pub_data * hash_out) list * result) *  skey  {H.init H.o J.init J.o O.o}
  proc a2(p : prf option) : bool {H.o J.o O.o}
}.

(* left game for ZK without checking the relation *) 
local module ZKFL(E: Scheme, R: Relation, P:Prover, 
                  A:VotingAdvZK, 
                  H: HOracle.Oracle, J: JOracle.Oracle, O:GOracle.Oracle) = {

  proc main(): bool = {
    var p;

                      O.init();
    (BPS.s, BPS.w) <@ A(H,J,O).a1();
    BPS.rel        <@ R.main(BPS.s, BPS.w);
    p              <@ P(O).prove(BPS.s, BPS.w);
    BPS.p          <- Some p;
    BPS.b          <@ A(H,J,O).a2(BPS.p);

    return BPS.b;
  }
}.



(* right game for ZK without checking the relation *)
local module ZKFR(E: Scheme, R: Relation, S:Simulator, 
                  A:VotingAdvZK, H: HOracle.Oracle, J: JOracle.Oracle) = {

  proc main(): bool = {
    var p;

                      S.init();
    (BPS.s, BPS.w) <@ A(H,J,S).a1();
    BPS.rel        <@ R.main(BPS.s, BPS.w);
    p              <@ S.prove(BPS.s);
    BPS.p          <- Some p;
    BPS.b          <@ A(H,J,S).a2(BPS.p);

    return BPS.b;
  }
}.


(* Lemma relating ZKFL to interesting non-local modules *)
local lemma ZKFL_ZKL (O <: GOracle.Oracle {BPS, BP, E, SS, A, C, HRO.RO, JRO.RO}) 
                     (P <: Prover {BPS, BP, HRO.RO, JRO.RO, O, E, SS, C, A}) &m:
  islossless O.o     =>
  islossless P(O).prove =>
    Pr[ZKFL(E, DecRel(E,HRO.RO), P, BZK(E,P,C(E,SS),Ve,SS,A), HRO.RO, JRO.RO, O).main() @ &m : res] =
    Pr[ZK_L(DecRel(E,HRO.RO), P, BZK(E,P,C(E,SS),Ve,SS,A,HRO.RO,JRO.RO), O).main() @ &m : res].
proof.
  move=> Oo_ll Pp_ll.
  byequiv =>/>.
  proc.
  seq 3 3: (={BP.r, BP.bb1, BP.bb0, BP.pk, BPS.w, BPS.s, BPS.rel, BP.hk, 
              glob P, glob O, glob A, glob HRO.RO, glob JRO.RO}/\ BPS.rel{2}).
    inline BZK(E, P, C(E,SS), Ve, SS, A, HRO.RO, JRO.RO, O).a1.
    inline DecRel(E, HRO.RO).main. 
    wp; while (={i0, stm, glob E, glob HRO.RO, ubb0, wit}/\
               0<= i0{1} /\
               let g = fun (x: maybe_id*upkey*cipher*sign),
                           (oget (dec_cipher wit{1} x.`2 x.`3 HRO.RO.m{1})) in
               ubb0{1} = map (g \o fst) (take i0{1} stm{1}.`2)).
      wp; sp; exists* wit{1}, b0{1}; elim * => sk1 b1. 
      call{1} (dec_Dec_two sk1 b1.`2 b1.`3).
      auto=>/>; progress. 
      + smt. smt. smt. smt. smt. 
      + rewrite (take_nth witness). by rewrite H1 H2.
        rewrite map_rcons cats1. 
        smt.

    wp; while (={i, BP.bb0, BP.sk, glob E, glob SS, glob HRO.RO, glob JRO.RO, ubb}/\
              0<= i{1} /\
              let f = fun (x: upkey*cipher*sign),
                           dec_cipher BP.sk{1} x.`1 x.`2 HRO.RO.m{1} <> None in
              let p = fun (x: upkey*cipher*sign),
                           ver_sign x.`1 x.`2 x.`3 JRO.RO.m{1} in
              let g = fun (x: upkey*cipher*sign),
                           (x.`1,oget (dec_cipher BP.sk{1} x.`1 x.`2 HRO.RO.m{1})) in
               ubb{1} = map g (filter (predI f p) (take i{1} (map remi BP.bb0{1})))).
       wp; sp.
       exists* BP.sk{1}, b{1}; elim* => skx bx. 
      call{1} (ver_Ver_two bx.`1 bx.`2 bx.`3).
      call{1} (dec_Dec_two skx bx.`1 bx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite filter_rcons /predI //= H1 H2 -/predI //= map_rcons -cats1.  
      + smt.
        rewrite (take_nth witness); first by smt.
        rewrite filter_rcons /predI //=. 
        smt. 
  
    (* call A.a1 : bb *)
    wp; call(: ={BP.bb0, BP.bb1, BP.pk, BP.sk, BP.qVo, BP.uL, BP.qCa, glob E, 
                 glob SS, glob C, glob HRO.RO, BP.coL, BP.hk,
                 glob JRO.RO, glob O}/\
               (BP.pk{1} = get_pk BP.sk{1})/\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None)).
    + proc. 
      if=>//=.
      if =>//=; last by auto.
      inline *.
      wp; call(: ={glob E, glob SS, glob HRO.RO, glob JRO.RO}); 
         first 2 by sim.
      wp; call(: ={glob JRO.RO}); first by sim.
      call(: ={glob HRO.RO}); first by sim.
      sp. 
      seq 1 1: (lps{2} = oget BP.uL{2}.[id{2}] /\
               id0{2} = id{2} /\ ={BP.hk}/\
               v{2} = v0{2} /\
               pk{2} = BP.pk{2} /\
               usk{2} = lps{2}.`2 /\
               upk{2} = get_upk usk{2} /\
               lps{1} = oget BP.uL{1}.[id{1}] /\
               id0{1} = id{1} /\
               v{1} = v0{1} /\
               pk{1} = BP.pk{1} /\
               usk{1} = lps{1}.`2 /\
               upk{1} = get_upk usk{1} /\
               ={id, v0, v1} /\
                 BP.pk{2} = get_pk BP.sk{2} /\
               ={BP.bb0, BP.bb1, BP.pk, BP.sk, BP.qVo, BP.uL, BP.qCa, 
                 glob E, glob SS, glob C,
                 HRO.RO.m, JRO.RO.m, glob O, BP.coL} /\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None) /\
                 BP.qVo{1} < qVo /\
                 BP.uL{1}.[id{1}] <> None /\ ={c} /\
                 dec_cipher BP.sk{2} (get_upk usk{1}) c{1} HRO.RO.m{2} <> None).
        exists * BP.sk{1}, upk{1}, v{1}; elim*=> skx upkx vx.
        call{1} (Eenc_dec skx (get_pk skx) upkx vx _); first by done.
        by auto=>/>; progress; smt.
      wp; exists *c{1}, usk{1}; elim*=> cx ux.
      call{1} (Sig_ver_two ux (get_upk ux) cx _); first by done.  
      by auto=>/>; smt. 

    + proc.  
      if =>//=.
      if =>//=; last by auto.
      inline *.
      wp; sp; exists *b0{1}, BP.sk{1}; elim*=> cx skx.
      (*call{1} (ver_Ver_two cx.`2 cx.`3 cx.`4). *)
      wp; call{1} (validInd_dec skx (get_pk skx) cx _); 
         first by done.
      by auto=>/>; progress; smt. 
    + by proc; inline *; wp.
    + by proc. 
    + by proc*; call(: true).
    + by proc.

     (* A.corL: split voters: honest/corrupt *)
    call(: true). 
    while(={i, BP.uL, BP.uLL, glob SS}); first by sim.
    rnd; call{1} (Ekgen_extractPk HRO.RO).
    call(: true ==> ={glob JRO.RO}); first by sim.
    call(: true ==> ={glob HRO.RO}); first by sim.
    wp; call(: true).
    auto=>/>; progress.
    + by rewrite take0.
    + by rewrite take0.
    rewrite ?take_oversize; first 2 by smt.
    pose g:= (fun (x : upkey * cipher * sign) =>
              (x.`1, oget (dec_cipher result_R.`2 x.`1 x.`2 m_R))).
    pose g':= (fun (x : maybe_id*upkey * cipher * sign) =>
              oget (dec_cipher result_R.`2 x.`2 x.`3 m_R)).
    pose g'':= (fun (x : upkey * cipher * sign) =>
              oget (dec_cipher result_R.`2 x.`1 x.`2 m_R)).
    pose f1:= (fun (x : upkey * cipher * sign) =>
                    dec_cipher result_R.`2 x.`1 x.`2 m_R <> None).
    pose f2:= (fun (x : upkey * cipher * sign) =>
                    ver_sign x.`1 x.`2 x.`3 m_R0).
    move: H2; rewrite -/f2; move=>H2.
    cut Hx: forall x, x\in (map remi bb0_R) => f1 x /\ f2 x.
       rewrite /f1 /f2 //=; move => x Hx. 
       move: Hx; rewrite mapP; elim => y Hy. 
       cut := H3 y _ ; first by rewrite (andWl _ _ Hy).
       by rewrite (andWr _ _ Hy) /remi //=.
    have ->: filter (predI f1 f2) (map remi bb0_R) = (map remi bb0_R). 
      by rewrite -all_filterP allP //=.

    have ->:  forall (X: (ident*upkey*cipher*sign) list),
             (map (g' \o fun (p : (maybe_id*upkey * cipher * sign) * hash_out) => p.`1)
             (map (fun (x : ident*upkey * cipher * sign) => (may_data x, hash' hkL x)) X)) =
             map (g'\o may_data) X.
      by move => X; rewrite -?map_comp /(\o) /get_pub_data //=. 
   cut := San_map (map remi bb0_R) g''.
     rewrite /g'' -/g //=.
   move => HS. 
   by rewrite HS -USan_USanitize -?map_comp /g /g' /(\o) //=.
     

 rcondt{2} 1; progress.
 by inline *; sim.  
qed.

local lemma ZKFR_ZKR (S <: Simulator {BPS, BP, E, SS, A, C, HRO.RO, JRO.RO}) &m:
  islossless S.o     =>
  islossless S.prove =>
    Pr[ZKFR(E, DecRel(E,HRO.RO), S, BZK(E,P,C(E,SS),Ve,SS,A), HRO.RO, JRO.RO).main() @ &m : res] =
    Pr[ZK_R(DecRel(E,HRO.RO), S, BZK(E,P,C(E,SS),Ve,SS,A,HRO.RO,JRO.RO)).main() @ &m : res].
proof.
  move=> So_ll Sp_ll.
  byequiv =>/>.
  proc.
  seq 3 3: (={BP.r, BP.bb1, BP.bb0, BP.pk, BPS.w, BPS.s, BPS.rel, BP.hk,
              glob S, glob A, glob HRO.RO, glob JRO.RO}/\ BPS.rel{2}).
    inline BZK(E, P, C(E,SS), Ve, SS, A, HRO.RO, JRO.RO, S).a1.
    inline DecRel(E, HRO.RO).main. 
    wp; while (={i0, stm, glob E, glob HRO.RO, ubb0, wit}/\
               0<= i0{1} /\
               let g = fun (x: maybe_id*upkey*cipher*sign),
                           (oget (dec_cipher wit{1} x.`2 x.`3 HRO.RO.m{1})) in
               ubb0{1} = map (g \o fst) (take i0{1} stm{1}.`2)).
      wp; sp; exists* wit{1}, b0{1}; elim * => sk1 b1. 
      call{1} (dec_Dec_two sk1 b1.`2 b1.`3).
      auto=>/>; progress. 
      + smt. smt. smt. smt. smt. 
      + rewrite (take_nth witness). by rewrite H1 H2.
        rewrite map_rcons cats1. 
        smt.

    wp; while (={i, BP.bb0, BP.sk, glob E, glob SS, glob HRO.RO, glob JRO.RO, ubb,BP.hk}/\
              0<= i{1} /\
              let f = fun (x: upkey*cipher*sign),
                           dec_cipher BP.sk{1} x.`1 x.`2 HRO.RO.m{1} <> None in
              let p = fun (x: upkey*cipher*sign),
                           ver_sign x.`1 x.`2 x.`3 JRO.RO.m{1} in
              let g = fun (x: upkey*cipher*sign),
                           (x.`1,oget (dec_cipher BP.sk{1} x.`1 x.`2 HRO.RO.m{1})) in
               ubb{1} = map g (filter (predI f p) (take i{1} (map remi BP.bb0{1})))).
       wp; sp.
       exists* BP.sk{1}, b{1}; elim* => skx bx. 
      call{1} (ver_Ver_two bx.`1 bx.`2 bx.`3).
      call{1} (dec_Dec_two skx bx.`1 bx.`2).
      auto=>/>; progress.
      + smt. 
      + rewrite (take_nth witness); first by smt.
        by rewrite filter_rcons /predI //= H1 H2 -/predI //= map_rcons -cats1.  
      + smt.
        rewrite (take_nth witness); first by smt.
        rewrite filter_rcons /predI //=. 
        smt. 
  
    wp; call(: ={BP.bb0, BP.bb1, BP.pk, BP.sk, BP.qVo, BP.uL, BP.qCa, glob E, 
                 glob SS, glob C, glob HRO.RO, BP.coL, BP.hk,
                 glob JRO.RO, glob S}/\
               (BP.pk{1} = get_pk BP.sk{1})/\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None)).
    + proc. 
      if=>//=.
      if =>//=; last by auto.
      inline *.
      wp; call(: ={glob JRO.RO, glob HRO.RO}); first 2 by sim.
      wp; call(: ={glob JRO.RO}); first by sim.
      call(: ={glob HRO.RO}); first by sim.
      sp. 
      seq 1 1: (lps{2} = oget BP.uL{2}.[id{2}] /\
               id0{2} = id{2} /\
               v{2} = v0{2} /\
               pk{2} = BP.pk{2} /\
               usk{2} = lps{2}.`2 /\
               upk{2} = get_upk usk{2} /\
               lps{1} = oget BP.uL{1}.[id{1}] /\
               id0{1} = id{1} /\
               v{1} = v0{1} /\
               pk{1} = BP.pk{1} /\
               usk{1} = lps{1}.`2 /\
               upk{1} = get_upk usk{1} /\
               ={id, v0, v1} /\
                 BP.pk{2} = get_pk BP.sk{2} /\
               ={BP.bb0, BP.hk, BP.bb1, BP.pk, BP.sk, BP.qVo,
                  BP.uL, BP.qCa, glob E, glob SS, glob C,
                 HRO.RO.m, JRO.RO.m, glob S, BP.coL} /\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None) /\
                 BP.qVo{1} < qVo /\
                 BP.uL{1}.[id{1}] <> None /\ ={c} /\
                 dec_cipher BP.sk{2} (get_upk usk{1}) c{1} HRO.RO.m{2} <> None).
        exists * BP.sk{1}, upk{1}, v{1}; elim*=> skx upkx vx.
        call{1} (Eenc_dec skx (get_pk skx) upkx vx _); first by done.
        by auto=>/>; progress; smt.
      wp; exists *c{1}, usk{1}; elim*=> cx ux.
      call{1} (Sig_ver_two ux (get_upk ux) cx _); first by done.  
      by auto=>/>; progress; smt. 

    + proc.  
      if =>//=.
      if =>//=; last by auto.
      inline *.
      wp; sp; exists *b0{1}, BP.sk{1}; elim*=> cx skx.
      wp; call{1} (validInd_dec skx (get_pk skx) cx _); 
         first by done.
      auto=>/>; progress. smt. smt. 
    + by proc; inline *; wp.
    + by proc. 
    + by proc*; call(: true).
    + by proc.

     (* A.corL: split voters: honest/corrupt *)
    call(: true). 
    while(={i, BP.uL, BP.uLL, glob SS}); first by sim.
    rnd; call{1} (Ekgen_extractPk HRO.RO).
    call(: true ==> ={glob JRO.RO}); first by sim.
    call(: true ==> ={glob HRO.RO}); first by sim.
    wp; call(: true).
    auto=>/>; progress.
    + by rewrite take0.
    + by rewrite take0.
    rewrite ?take_oversize; first 2 by smt.
    pose g:= (fun (x : upkey * cipher * sign) =>
              (x.`1, oget (dec_cipher result_R.`2 x.`1 x.`2 m_R))).
    pose g':= (fun (x : maybe_id*upkey * cipher * sign) =>
              oget (dec_cipher result_R.`2 x.`2 x.`3 m_R)).
    pose g'':= (fun (x :upkey * cipher * sign) =>
              oget (dec_cipher result_R.`2 x.`1 x.`2 m_R)).
    pose f1:= (fun (x : upkey * cipher * sign) =>
                    dec_cipher result_R.`2 x.`1 x.`2 m_R <> None).
    pose f2:= (fun (x : upkey * cipher * sign) =>
                    ver_sign x.`1 x.`2 x.`3 m_R0).
    move: H2; rewrite -/f2; move=>H2.
    cut Hx: forall x, x\in (map remi bb0_R) => f1 x /\ f2 x.
       rewrite /f1 /f2 //=; move => x Hx. 
       move: Hx; rewrite mapP; elim => y Hy. 
       cut := H3 y _ ; first by rewrite (andWl _ _ Hy).
       by rewrite (andWr _ _ Hy) /remi //=.
    have ->: filter (predI f1 f2) (map remi bb0_R) = (map remi bb0_R). 
      by rewrite -all_filterP allP //=.
    have ->:  forall (X: (ident*upkey*cipher*sign) list),
             (map (g' \o fun (p : (maybe_id*upkey * cipher * sign) * hash_out) => p.`1)
             (map (fun (x : ident*upkey * cipher * sign) => (may_data x, hash' hkL x)) X)) =
             map (g'\o may_data) X.
      by move => X; rewrite -?map_comp /(\o) /get_pub_data //=. 
   cut := San_map (map remi bb0_R) g''.
     rewrite /g'' -/g //=.
   move => HS. 
   by rewrite HS -USan_USanitize -?map_comp /g /g' /(\o) //=.
 
 rcondt{2} 1; progress.
 by inline *; sim.  
qed.



(* Lemma for equivalence of BPRIVL and ZKFL *)
local lemma eq_MV_ZKFL &m :
    Pr[BPRIV_L(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ] = 
    Pr[ZKFL(E, DecRel(E,HRO.RO), P, BZK(E,P,C(E,SS),Ve, SS, A), HRO.RO, JRO.RO, GRO.RO).main() @ &m : res ].
proof.
  byequiv =>/>.  
  proc.
  inline BZK(E, P, C(E,SS), Ve, SS, A, HRO.RO, JRO.RO, GRO.RO).a2
    BZK(E, P, C(E,SS), Ve, SS, A, HRO.RO, JRO.RO, GRO.RO).a1
    BPRIV_L(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO).V.tally
    BPRIV_L(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO).V.register.
  seq 25 22 : ( ={glob E, glob A, glob SS, glob P, GRO.RO.m, JRO.RO.m, HRO.RO.m, 
                  BP.bb0, BP.bb1, BP.pk, ubb,BP.hk}/\
                 BPS.s{2} = (pk, pbb, r){1} /\
              BPS.w{2} = sk{1} /\
              r{1} = BP.r{2}). 
     wp; while (={glob E, glob SS, JRO.RO.m, HRO.RO.m, ubb}/\
                fbb{1} = map remi BP.bb0{2} /\
                i0{1} = i{2}/\
                sk{1} = BP.sk{2}). 
       wp; call(: ={JRO.RO.m}); first by sim.
       wp; call(: ={HRO.RO.m}); first by sim.
       by auto.
     wp; call (_ : ={glob P, glob E, glob HRO.RO, glob GRO.RO, glob C, glob JRO.RO, 
                glob SS, BP.bb0, BP.bb1, BP.pk, BP.uL, BP.coL, BP.hk,
                BP.qVo, BP.qCa}) ;
      [1..6: by sim].
     call(: true). 
     while (={glob SS, BP.uL, BP.uLL, i}); first by sim. 
     inline BPRIV_L(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO).V.setup.
     wp; rnd; call (Ekgen_extractPk HRO.RO).
     call(: true ==> ={glob JRO.RO}); first by sim.
     swap{2} 1 10.
     call(: true ==> ={glob GRO.RO}); first by sim.
     call(: true ==> ={glob HRO.RO}); first by sim. 
     by auto=>/>.
  inline *; wp.
  seq 0 9: (={glob E, glob A, glob SS, glob P, GRO.RO.m, JRO.RO.m,
              HRO.RO.m, BP.bb0, BP.bb1, BP.pk, ubb,BP.hk}/\
                 BPS.s{2} = (pk, pbb, r){1} /\
              BPS.w{2} = sk{1} /\
              r{1} = BP.r{2}).
    wp; while{2} (={glob E}) (size stm{2}.`2 -i0{2}); progress.
     wp; sp; exists * (glob E), b0, wit; elim *=> ge cx skx.
     call{1} (Edec_Odec ge skx cx.`2 cx.`3).
     by auto=>/>; smt.
   by auto=>/>; smt.
  call ( _ : ={ glob HRO.RO, glob GRO.RO, glob JRO.RO,
                BP.bb0, BP.bb1, BP.pk,BP.hk}); 
    [1..4:by sim].
  wp; call(: ={glob GRO.RO}); first by sim.
  by auto.
qed.

module IBPRIV(V:VotingScheme', A:BPRIV_Adv, 
              H: HOracle.Oracle, J: JOracle.Oracle, G:GOracle.Oracle, S:Simulator) = {

  module O = BPRIV_Oracles(V, H, S, J, Left)
  module A = A(O)

  proc main() : bool = {
    var pbb, pi, i, id;

    BP.qVo <- 0;
    BP.qCa <- 0;
    BP.bb0 <- [];
    BP.bb1 <- [];
    BP.uLL <- [];
    BP.coL <- [];
    BP.uL  <- empty;
    i <- 0;
    
                      H.init();
                      G.init();
                      J.init();
                      S.init();

    (BP.pk, BP.sk,BP.hk) <@ V(H,G,J).setup();
     while (i < size (undup Voters)){
      id         <- nth witness (undup Voters) i;
      BP.uL.[id] <@ V(H,G,J).register(id);
      BP.uLL <- BP.uLL ++ [(id, (oget BP.uL.[id]).`1)];
      i          <- i + 1;
    }
    BP.coL         <@ A.corL(BP.pk,BP.uLL);
                      A.a1(BP.pk, filter (fun (a: ident) (b: upkey * uskey), 
                                          a \in BP.coL) BP.uL);
    (BP.r, BP.pi)  <@ V(H,G,J).tally(BP.bb0, BP.sk,BP.hk);
    pbb            <@ V(H,G,J).publish(BP.bb0, BP.hk);
    pi             <@ S.prove(BP.pk, pbb, BP.r);
    BP.b           <@ A.a2(BP.r, pi);
    return BP.b;
  }
}.


local lemma eq_MV'_ZKFR &m :
    Pr[IBPRIV(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, JRO.RO, GRO.RO, S).main() @ &m : res] =
    Pr[ZKFR(E, DecRel(E,HRO.RO), S, BZK(E,P,C(E,SS),Ve,SS, A), HRO.RO, JRO.RO).main() @ &m : res].
proof.
 (* move => RH.*)
  byequiv =>/>. 
  proc.
  inline BZK(E, P, C(E,SS), Ve, SS, A, HRO.RO, JRO.RO, S).a2
    BZK(E, P, C(E,SS), Ve, SS, A, HRO.RO, JRO.RO, S).a1
    BPRIV_L(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO).V.tally
    BPRIV_L(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO).V.register.
  wp; call ( _ : ={ glob HRO.RO, glob S, glob JRO.RO,
                BP.bb0, BP.bb1, BP.pk,BP.hk}); 
    [1..4:by sim].
  wp; call(: true).
  inline B'(E, P, Ve, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).publish
     DecRel(E, HRO.RO).main.
  wp; while{2} (={glob E}) (size stm{2}.`2 -i0{2}); progress.
     wp; sp; exists * (glob E), b0, wit; elim *=> ge cx skx.
     call{1} (Edec_Odec ge skx cx.`2 cx.`3).
     by auto=>/>; smt.
  wp; call{1} (Pp_ll GRO.RO GRO.RO_o_ll).
   wp; while (={glob E, glob SS, JRO.RO.m, HRO.RO.m, ubb}/\
                fbb{1} = map remi BP.bb0{2} /\
                i0{1} = i{2}/\
                sk{1} = BP.sk{2}).
    wp; call(: ={JRO.RO.m}); first by sim.
    wp; call(: ={HRO.RO.m}); first by sim.
    by auto.
  wp; call (_ : ={glob E, glob HRO.RO, glob S, glob C, glob JRO.RO, 
                glob SS, BP.bb0, BP.bb1, BP.pk, BP.uL, BP.coL, 
                BP.qVo, BP.qCa,BP.hk});
      [1..6: by sim].
  call(: true).
     while (={glob SS, BP.uL, BP.uLL, i}); first by sim. 
     inline  B'(E, P, Ve, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).setup.
     wp; rnd; call (Ekgen_extractPk HRO.RO).
     swap{2} 1 11.
     call(: true).
     call(: true ==> ={glob JRO.RO}); first by sim.
     swap{1} 10 -9.
     call(: true ==> ={glob HRO.RO}); first by sim.
     inline *; wp. 
     while{1} (true) (FSet.card work{1}); progress.
       auto=>/>; progress; smt.
     by auto=>/>; progress; smt. 
qed. 


(* Lemma bounding |BPRIV-L -IBPRIV| by |ZKL-ZKR| *)
local lemma bound_MV_MV' &m :
    `|Pr[BPRIV_L(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO  ).main() @ &m : res ] -
      Pr[IBPRIV(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, JRO.RO, GRO.RO, S).main() @ &m : res]|
<=
    `|Pr[ZK_L(DecRel(E,HRO.RO), P, BZK(E,P,C(E,SS),Ve,SS,A,HRO.RO,JRO.RO), GRO.RO).main() @ &m : res] -
      Pr[ZK_R(DecRel(E,HRO.RO), S, BZK(E,P,C(E,SS),Ve,SS,A,HRO.RO,JRO.RO)   ).main() @ &m : res]|.
proof.
  rewrite (eq_MV_ZKFL &m) (eq_MV'_ZKFR &m). 
  rewrite (ZKFL_ZKL GRO.RO P &m GRO.RO_o_ll (Pp_ll GRO.RO GRO.RO_o_ll)).
  by rewrite (ZKFR_ZKR S &m So_ll Sp_ll).
qed. 

(* BPRIV Game with sg board *) 
module BPRIV_SB(E: Scheme, C: ValidInd, SS: SignScheme, A:BPRIV_Adv, 
                H: HOracle.Oracle, J: JOracle.Oracle, LR: LorR, S: BPRIV_Simulator) = {


  module BV ={
    proc vote(id: ident, v: vote, pk: pkey, usk: uskey)
         : (ident * upkey * cipher * sign) = {
      var c,s, upk;
      upk <- get_upk usk;
      c <@ E(H).enc(pk, upk, v);
      s <@ SS(J).sign(usk,c);
      return (id, upk, c, s);
    }

    proc publish (bb: (ident * upkey * cipher * sign) list, hk: hash_key) ={
      var pbb;
      pbb <- map (fun (x: ident*upkey * cipher * sign), 
                    (may_data x, hash' hk x)) (USanitize bb);
      return pbb;
    }

    proc valid(bb: (ident * upkey * cipher * sign) list, 
             b : (ident * upkey * cipher * sign),
             pk: pkey) : bool ={
      var ev1, ev2;
      ev1 <- forall x, x \in bb => 
                        (x.`1,x.`2) = (b.`1,b.`2) \/
                        (x.`1 <> b.`1 /\ x.`2 <> b.`2);
      (* check ciphertext validity *)
      ev2 <@ C(H,J).validInd(b, pk);
      return ev1 /\ ev2 ;
    } 
  }(* end BV *)
  (* BPRIV Oracles with sg board *)
  module O = {
    proc h = H.o
    proc g = S.o
    proc j = J.o

    (* vote oracle *)
    proc vote(id : ident, v0 v1 : vote) : unit = {
      var b,v, l_or_r;
      var l;
   
      if (BP.qVo < qVo){
        if (BP.uL.[id] <> None /\ !id \in BP.coL){
          l     <- oget BP.uL.[id]; 
          l_or_r <@ LR.l_or_r(); 
          v      <- l_or_r?v0:v1;
          b      <@ BV.vote(id, v, BP.pk, l.`2);
          if (forall x, x \in BBS.bb => 
                  (x.`1,x.`2) = (b.`1,b.`2) \/ (x.`1 <> b.`1 /\ x.`2 <> b.`2)) {
            (*if (!(b.`3, b.`2) \in BBS.encL){ *)
              BBS.hvo.[(size BBS.bb, b.`3, b.`2)] <- v0;
              BBS.encL<- BBS.encL++ [( b.`3,b.`2)]; 
           (* }elif(BBS.badQ = None){
              BBS.badQ <- Some BP.qVo;
            } *)
             BBS.bb  <- BBS.bb  ++ [b];
          } 
        }
        BP.qVo <- BP.qVo + 1;
      }
    }

    (* cast oracle *)
    proc cast(b : (ident * upkey * cipher * sign)) : unit={
      var ev;

      if (BP.qCa < qCa ){
        if(BP.uL.[b.`1] <> None /\ b.`1 \in BP.coL){
          ev <@ BV.valid(BBS.bb, b, BP.pk);
          if (ev){
            BBS.bb  <- BBS.bb  ++ [b];
          }
        }
        BP.qCa <- BP.qCa + 1; 
      }
    }

    proc board(): (pub_data * hash_out) list ={
      var pbb;
      pbb <@ BV.publish(BBS.bb, BP.hk);
      return pbb;
    }

    proc corrupt(id): uskey option ={
      var usk;
      usk <- None;
      if (BP.uL.[id]<> None /\ !id \in map fst4 BBS.bb){
        usk <- Some (oget BP.uL.[id]).`2;
        if(!id \in BP.coL){
          BP.coL <- BP.coL ++ [id];
        }          
      }
      return usk;
    }
  }(* end O*)


  proc main() : bool={
    var pbb, pi, i, id;
    var b, vo, ubb, usk, upk;

    BP.qVo  <- 0;
    BP.qCa  <- 0;
    i       <- 0;
    BP.uL   <- empty;
    BBS.bb  <- [];
    BP.uLL <- [];
    BP.coL <- [];
    BBS.hvo <- empty;
    ubb         <- [];
    BBS.encL    <- [];
    BBS.badQ    <- None;
                      H.init();
                      J.init();
                      S.init();

    (BP.pk, BP.sk) <@ E(H).kgen();
    BP.hk <$ dhkey_out;
    while (i < size (undup Voters)){
      id         <- nth witness (undup Voters) i;
      (upk,usk) <@ SS(J).kgen();
      BP.uL.[id] <- (usk,upk);
      BP.uLL <- BP.uLL ++ [(id, (oget BP.uL.[id]).`1)];
      i          <- i + 1;
    }
    
     BP.coL <@ A(O).corL(BP.pk, BP.uLL);
     A(O).a1(BP.pk, filter (fun (a: ident) (b: upkey * uskey), 
                                          a \in BP.coL) BP.uL);

     i <- 0;
     while( i < size (map remi BBS.bb)){ 
      b  <- nth witness (map remi BBS.bb) i;

      if ( !mem BBS.encL (b.`2,b.`1)){
        vo <@ E(H).dec(BP.sk, b.`1, b.`2);
      }else{
        vo <- BBS.hvo.[(i,b.`2,b.`1)];
      }
      ubb <- ubb ++ [(b.`1, oget vo)];
      i  <- i + 1;
    }
    
    BP.r  <- Count (map snd (Sanitize ubb));
    pbb   <@ BV.publish (BBS.bb, BP.hk);
    pi    <@ S.prove(BP.pk, pbb, BP.r);
    BP.b  <@ A(O).a2(BP.r, pi);

    return BP.b;
  }
}. 

(* Lemma equivalence IBPRIV to BPRIV with single board *)
local lemma left_to_sgBoard2  &m:
   Pr[IBPRIV   (B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, JRO.RO, GRO.RO, S).main() @ &m : 
                  res] =
   Pr[BPRIV_SB (E, C(E,SS), SS, A, HRO.RO, JRO.RO, Left, S).main() @ &m : 
                  res]. 
proof.
  byequiv (_: ={glob E, glob A, glob S, glob C, glob SS} ==> _) =>//.
  proc.
  swap{2} 9 10. 
  seq 16 18: ( ={glob E, glob A, glob S, glob C, glob HRO.RO, glob JRO.RO,
                 BP.uL, BP.pk, BP.qVo, BP.sk, BP.qCa, BP.uLL, BP.coL,BP.hk}/\
               BP.qVo{2} <= qVo /\  
               size BBS.encL{2} <= BP.qVo{2}/\
               BP.pk{1} = get_pk BP.sk{1} /\
               BP.bb0{1} = BBS.bb{2}/\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None)/\
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} x.`2 x.`1 HRO.RO.m{2})/\
                (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})).

    (* A.a1 call *) 
    wp; call (_: ={glob E, glob S, glob C, glob HRO.RO, glob JRO.RO, glob SS,
                   BP.uL, BP.pk, BP.qVo, BP.sk, BP.qCa, BP.coL,BP.hk}/\
                BP.qVo{2} <= qVo /\  
               size BBS.encL{2} <= BP.qVo{2}/\
               BP.pk{1} = get_pk BP.sk{1} /\
               BP.bb0{1} = BBS.bb{2}/\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None)/\
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} x.`2 x.`1 HRO.RO.m{2})/\
               (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})/\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})
               (*forall x, x \in BBS.encL{2} =>
                    BBS.hvo{2}.[x] =
                    dec_cipher BP.sk{2} x.`2 x.`1 HRO.RO.m{2}*)).
         
    (* A.a1: vote oracle *) 
    + proc. 
      if =>//=; sp.
      if =>//=; last by auto; smt.
      inline Left.l_or_r; wp.
      seq 2 4: (={glob E, glob S, glob C, glob HRO.RO, glob SS, 
                 glob JRO.RO,  BP.uL, BP.coL, BP.hk,
                BP.pk, BP.sk, BP.qVo, BP.qCa, id, v0, v1} /\
              BP.qVo{2} < qVo  /\
              size BBS.encL{2} <= BP.qVo{2}/\
               BP.pk{1} = get_pk BP.sk{1} /\
               BP.bb0{1} = BBS.bb{2}/\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None)/\
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} x.`2 x.`1 HRO.RO.m{2}) /\
              (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})/\
              b0{1}  = b{2}              /\
              BP.uL{1}.[id{1}] <> None   /\
              Some v0{1} <> None         /\
              b{2}.`1 = id{2}            /\
              lps{1} = l{2} /\ !b0{1}.`1 \in BP.coL{1}/\
              Some v0{1} = 
                dec_cipher BP.sk{1} b0{1}.`2 b0{1}.`3 HRO.RO.m{1}/\
              ver_sign b0{1}.`2 b0{1}.`3 b0{1}.`4 JRO.RO.m{1} /\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})).       
      + inline *; wp; sp.
         seq 1 1: (l{2} = oget BP.uL{2}.[id{2}] /\
            l_or_r{2} = true /\
              v{2} = (if l_or_r{2} then v0{2} else v1{2}) /\
              id0{2} = id{2} /\
              v2{2} = v{2} /\
              pk{2} = BP.pk{2} /\
              usk{2} = l{2}.`2 /\
              upk{2} = get_upk usk{2} /\
              lps{1} = oget BP.uL{1}.[id{1}] /\
              id0{1} = id{1} /\
              v{1} = v0{1} /\
              pk{1} = BP.pk{1} /\
              usk{1} = lps{1}.`2 /\
              upk{1} = get_upk usk{1} /\
              ={id, v0, v1} /\
              ={glob E, glob S, glob C, HRO.RO.m, JRO.RO.m, glob SS, 
                BP.uL, BP.pk, BP.hk,
                BP.qVo, BP.sk, BP.qCa, BP.coL} /\
                BP.qVo{2} <= qVo /\
              size BBS.encL{2} <= BP.qVo{2}/\
               BP.pk{1} = get_pk BP.sk{1} /\
               BP.bb0{1} = BBS.bb{2}/\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None)/\
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} x.`2 x.`1 HRO.RO.m{2})/\
                (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2}) /\
                BP.qVo{1} < qVo /\
                BP.uL{1}.[id{1}] <> None /\ ={c}/\ !id{1} \in BP.coL{2}/\
              Some v{2} = dec_cipher BP.sk{1} upk{1} c{1} HRO.RO.m{1}/\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})).
        exists *  BP.sk{1}, upk{1}, v{1}; 
        elim * => sk1 l1 p1.
        call{1}( Eenc_dec sk1 (get_pk sk1) l1 p1 _ ); first by done.
        by auto =>/>; progress; smt. 
      wp; exists *c{1}, usk{1}; elim*=> cx ux.
      call{1} (Sig_ver_two ux (get_upk ux) cx _); first by done.  
      by auto=>/>; progress. 

      seq 1 0: (={glob E, glob S, glob C, glob HRO.RO, glob JRO.RO, 
                  glob SS, BP.uL, BP.pk, BP.sk, BP.qVo, BP.coL,
                  BP.qCa, id, v0, v1,BP.hk} /\
              BP.qVo{2} < qVo               /\
              b0{1}  = b{2}              /\
              BP.uL{1}.[id{1}] <> None   /\
              Some v0{1} <> None         /\
              b{2}.`1 = id{2}            /\
              lps{1} = l{2} /\
              Some v0{1} = 
                dec_cipher BP.sk{1} b0{1}.`2 b0{1}.`3 HRO.RO.m{2} /\
              ver_sign b0{1}.`2 b0{1}.`3 b0{1}.`4 JRO.RO.m{1} /\
              size BBS.encL{2} <= BP.qVo{2}/\
               BP.pk{1} = get_pk BP.sk{1} /\
               BP.bb0{1} = BBS.bb{2}/\
               !b0{1}.`1 \in BP.coL{1}/\
               (forall x, x\in BP.bb0{1} =>
                   ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
                   dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None)/\
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} x.`2 x.`1 HRO.RO.m{2})/\
               (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})/\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})).
  
      + inline *; wp; sp. 
        exists* (glob E){1}, (glob SS){1}; elim* => ge gss.
        call{1} (SSs_eq gss).  
        call{1} (Ee_eq ge).  
        by auto=>/>; progress. 

      inline *.
      wp; sp. exists *(glob C){1}, (glob E){1}, (glob SS){1}, b2{1}, BP.sk{1}; 
         elim*=> gc ge gs cx skx.
      (* wp; call{1} (ver_Ver_one gs cx.`2 cx.`3 cx.`4). *)
      wp; call{1} (validInd_dec_one gc ge gs skx (get_pk skx) cx _); 
         first by done.
      auto; progress. 
      + smt. smt.  
      + rewrite mem_cat mem_seq1 in H12.
        smt.  
      + rewrite mem_cat mem_seq1 in H12.
        smt. 
      + (* rewrite mem_cat mem_seq1 in H10.
        smt.*)
        
        case (i < size BBS.bb{2}). 
        + move => Hi.
          move: H14; rewrite ?nth_cat Hi //=. 
          rewrite get_set_neq. smt. 
          rewrite mem_cat //=.
          case (((nth witness BBS.bb{2} i).`3, 
                (nth witness BBS.bb{2} i).`2) \in BBS.encL{2}). smt.
          simplify. 
          move => Hnm. elim => Hb3 Hb2. 
          cut := H9 (nth witness BBS.bb{2} i) _; first by smt. 
          rewrite Hb2 //=; move => Hb1.
          cut := H7 (nth witness BBS.bb{2} i) _; first by smt. 
          rewrite Hb1 Hb2 Hb3 H4. smt.
        - move => His.
          have Hi: i = size BBS.bb{2} by smt.
          move: H14; rewrite nth_cat Hi //=. 
          smt.
      + rewrite mem_cat mem_seq1 in H12; smt.
      + rewrite mem_cat mem_seq1 in H12. 
        case (x \in BBS.encL{2}).
        + move => Hm.
          cut := H8 x Hm. smt.
        - move => Hm.
          exists b{2}.`1 b{2}.`4. smt.
      + smt. smt. smt. 
      + rewrite mem_cat mem_seq1 in H13.
        smt. 
      + smt. smt. smt. smt. 
      
    + proc.  
      if =>//=.
      if =>//=; last by auto.
      inline *.
      wp; sp; exists *b0{1}, BP.sk{1}; elim*=> cx skx.
      wp; call{1} (validInd_dec skx (get_pk skx) cx _); 
         first by done.
      auto; progress.
      + rewrite mem_cat mem_seq1 in H11.
        smt.
      + rewrite mem_cat mem_seq1 in H11.
        smt.
      + case (i < size BBS.bb{2}).
        + move => Hi.
          move: H13; rewrite ?nth_cat Hi //=. smt.
        - move => His.
          have Hi: i = size BBS.bb{2} by smt.
          move: H13; rewrite nth_cat Hi //=.
          move => HbM.
          cut := H4 (b{2}.`3, b{2}.`2) HbM. 
          simplify. elim => id s HmBB.
          cut := H8 (id, b{2}.`2, b{2}.`3, s).
            rewrite //= HmBB //=.
          move => Hb1.
          cut := H3 (id, b{2}.`2, b{2}.`3, s).      
          by rewrite //= Hb1 H7 HbM //=-Hb1. 
      + rewrite mem_cat mem_seq1 in H11. smt.
      + cut := H4 x H11.
        elim => id s HmBB.
        by exists id s; rewrite mem_cat HmBB.
    (* A.a1: board, HRO.RO, S, JRO.RO oracle *)
    by proc; inline *; wp. 
    by proc; auto.
    by conseq So_ll2; progress.
    by proc. 

    (* before A.a1 call *)
    call(: true).
    inline B'(E, P, Ve, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).setup
      B'(E, P, Ve, C(E,SS), SS, HRO.RO, GRO.RO, JRO.RO).register.
    wp; while (={BP.uL, BP.uLL, i, glob SS});
      first by sim.
    wp; rnd; call{1} (Ekgen_extractPk HRO.RO).
    wp; call(_: true).
    call (_: true ==> ={glob JRO.RO});
      first by sim.
    swap{1} 10 -9.
    call (_: true ==> ={glob HRO.RO});
      first by sim.
    inline *; wp.
    while{1} (true) (FSet.card work{1}); progress.
      by auto=>/>; progress; smt.
    by auto=>/>; progress; smt. 
    

  (* A.a2 call *)
  call (_: ={glob E, glob HRO.RO, glob S, glob C, glob JRO.RO,
             BP.pk,  BP.sk, BP.sk, BP.qVo,BP.hk} /\
             BP.bb0{1}=BBS.bb{2}).
  + by proc; inline *; auto. 
  + by sim. by sim. by sim.

  conseq (_: ={BP.r, pi, glob A, glob E, glob S, glob C, 
               glob HRO.RO, BP.pk, BP.sk, BP.qVo,BP.hk}/\
             BP.bb0{1} = BBS.bb{2} )=>//=.
  inline *.  
  call(: true).
  wp; call{1} (Pp_ll GRO.RO GRO.RO_o_ll).
  wp.
  while (={glob E, glob HRO.RO, ubb} /\
          fbb{1} = (map remi BBS.bb{2}) /\ sk{1} = BP.sk{2} /\ i0{1} = i{2}/\
          0<= i0{1} /\
          (forall i, 0<= i < size BBS.bb{2} =>
             let x = ((nth witness BBS.bb{2} i).`3,
                      (nth witness BBS.bb{2} i).`2) in
             x \in BBS.encL{2} => 
                BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} x.`2 x.`1 HRO.RO.m{2})/\
          (forall x, x\in BBS.bb{2} =>
             ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}/\
             dec_cipher BP.sk{2} x.`2 x.`3 HRO.RO.m{1} <> None)/\
          let g = fun (x: upkey*cipher*sign),
                           (x.`1,oget (dec_cipher BP.sk{2} x.`1 x.`2 HRO.RO.m{1})) in
          ubb{1} = map g  (take i0{1} (map remi BBS.bb){2})).
  wp; sp.
  exists * (glob SS){1}, (glob E){1}, b{1}, sk{1} ; elim* => gs ge bx skx.
  call{1} (ver_Ver_one gs bx.`1 bx.`2 bx.`3).
  if{2}.  
  + call{1}( dec_Dec_two skx bx.`1 bx.`2).
    auto=>/>; progress. 
    + smt. 
    + rewrite (take_nth witness); first by smt. 
      by rewrite map_rcons -cats1 //=. 
    + move : H4. 
      rewrite (nth_map witness witness remi i{2} BBS.bb{2}).
        smt.
      smt.
    + smt. 
    + move : H0 H1 H3 H4. 
      rewrite (nth_map witness witness remi i{2} BBS.bb{2}).
        smt. 
      pose a:= (nth witness BBS.bb{2} i{2}).
      move => H0 H1 H3.
      rewrite /remi //=.      
      move => H4.
      cut Hn:= H1 a _. smt.
      smt.
  call{1} (Edec_Odec ge skx bx.`1 bx.`2).
  auto=>/>; progress. 
  + rewrite (nth_map witness witness remi i{2} BBS.bb{2}).
        smt.
      smt.
  + smt. 
  + rewrite (take_nth witness); first by smt.
    by rewrite map_rcons cats1 //= -/remi. 
  + move: H4. 
    rewrite (nth_map witness witness remi i{2} BBS.bb{2}).
      smt. 
    pose a:= (nth witness BBS.bb{2} i{2}).
    rewrite /remi //=.      
    move => H4.
    rewrite -/remi.  smt.
  + smt.   
  + move: H4. 
    rewrite (nth_map witness witness remi i{2} BBS.bb{2}).
      smt. 
    pose a:= (nth witness BBS.bb{2} i{2}).
    rewrite /remi //=.      
    move => H4.
    rewrite -/remi.  smt.
  auto=>/>; progress. 
  by rewrite take0.
qed. 
 
(* Lemma equivalence BPRIVR to BPRIV with single board *)
local lemma right_to_sgBoard2 &m:
   Pr[BPRIV_R (B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO, S).main() @ &m : res ] =
   Pr[BPRIV_SB (E, C(E,SS), SS, A, HRO.RO, JRO.RO, Right, S).main() @ &m :  res ]. 
proof.
  byequiv =>//=.
  proc. 

  inline BPRIV_R(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO, S).V.tally.
  seq 25 22: (={glob E, glob S, glob C, glob HRO.RO, glob JRO.RO,
              glob A, BP.pk,  BP.sk, BP.sk, BP.qVo, BP.coL, BP.uLL,BP.hk} /\
             BP.bb1{1}=BBS.bb{2} /\
             r{1} = BP.r{2}/\
             get_pk BP.sk{1} = BP.pk{1}/\
             sk{1} = BP.sk{1})=>//=.

  wp.
 
  (* tally phase *)
  auto; while (={ubb, glob E, glob HRO.RO, BP.sk}    /\ 
             0 <= i0{1} /\
             i0{1} = i{2} /\
             BP.sk{2} = sk{1} /\  
             map remi BP.bb0{1} = fbb{1} /\
             size BBS.bb{2} = size fbb{1}  /\
             (forall x, x\in BP.bb0{1} =>
                dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None /\
                ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1})/\
             (forall i, 0 <= i < size BBS.bb{2} =>
                (nth witness BBS.bb{2} i).`2 =  (nth witness BP.bb0{1} i).`2) /\ 
             (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BBS.bb{2} i).`3,
                            (nth witness BBS.bb{2} i).`2) in
                   let y = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} y.`2 y.`1 HRO.RO.m{2})/\
             (forall i, let b = nth witness BP.bb0{1} i in 
                        let b'= nth witness BBS.bb{2} i in
                0 <= i < size BP.bb0{1} /\ 
                BBS.hvo{2}.[(i,b'.`3, b'.`2)] = None =>
                  b = b')/\
             (forall (c,l), mem BBS.encL{2} (c,l) =>
                exists id s, mem BBS.bb{2} (id,l,c,s))/\
             (forall (l,c), ! mem BBS.encL{2} (c,l) =>
                forall i, BBS.hvo{2}.[(i,c,l)]  = None)).

    + wp; sp.
      exists* (glob E){1}, (glob SS){1}, sk{1}, b{1}; elim * => ge gs sk1 b1. 
      call{1} (ver_Ver_one gs b1.`1 b1.`2 b1.`3).
      if{2}.    
      + call{1} (dec_Dec_two sk1 b1.`1 b1.`2).
        auto=>/>; progress. 
        + move: H9.
          rewrite ?(nth_map witness witness); first 2  by smt. 
          rewrite /remi //=.
          pose x:= (nth witness BBS.bb{2} i{2}).           
          move => H9. 
          cut Hi:= H6 x.`2 x.`3 H9.
          cut := H4 i{2} _ ; first by  rewrite H -(size_map remi) H7 (Hi i{2}). 
          by rewrite -/x.
        + move: H9.
          rewrite ?(nth_map witness witness); first 2  by smt. 
          rewrite /remi //=.
          pose x:= (nth witness BBS.bb{2} i{2}).           
          move => H9. 
          cut Hi:= H6 x.`2 x.`3 H9.
          cut := H4 i{2} _ ; first by  rewrite H -(size_map remi) H7 (Hi i{2}). 
          by rewrite -/x.
        + move: H9.
          rewrite ?(nth_map witness witness); first 2  by smt. 
          rewrite /remi //=.
          pose x:= (nth witness BBS.bb{2} i{2}).           
          move => H9. 
          cut Hi:= H6 x.`2 x.`3 H9.
          cut := H4 i{2} _ ; first by  rewrite H -(size_map remi) H7 (Hi i{2}). 
          by rewrite -/x.
        + move: H9.
          rewrite ?(nth_map witness witness); first 2  by smt. 
          rewrite /remi //=.
          pose x:= (nth witness BBS.bb{2} i{2}).           
          move => H9. 
          cut Hi:= H6 x.`2 x.`3 H9.
          cut := H4 i{2} _ ; first by  rewrite H -(size_map remi) H7 (Hi i{2}). 
          by rewrite -/x. 
        + by rewrite H10 H11. 
        + smt.
        + smt (size_map). 
        + smt (size_map). 
        + move : H14.
          rewrite ?(nth_map witness witness); first 2  by smt. 
          rewrite /remi //=.
          pose x:= (nth witness BP.bb0{1} i{2}). 
          move => H14. 
          cut H14n:= H1 x _; first by smt.
          smt.
        + smt.
        + smt (size_map). smt (size_map).
      - call{1} (Edec_Odec ge sk1 b1.`1 b1.`2).
        auto=>/>; progress.
        + move: H9.
          rewrite ?(nth_map witness witness); first 2  by smt. 
          rewrite /remi //=. 
          pose x:= (nth witness BBS.bb{2} i{2}).
          move => H9.
          cut Hhvo:= H3 i{2} _ H9; first by smt.
          by rewrite -/x Hhvo //= (H2 i{2} _); first by smt.   
        + smt. smt (size_map). smt (size_map).       
        + move: H10.
          rewrite ?(nth_map witness witness); first 2  by smt. 
          rewrite /remi //=.
          pose x:= (nth witness BP.bb0{1} i{2}). 
          move => H10. 
          cut H10n:= H1 x _; first by smt.
          smt.
        + smt. smt (size_map). smt (size_map).

  
              
  (* A.a1 call *)
  wp; call (_: ={glob E, glob S, glob C, glob HRO.RO, glob SS, BP.coL,
                 glob JRO.RO, BP.uL, BP.pk, BP.qVo, BP.qCa, BP.sk,BP.hk} /\
               (forall x, x\in BP.bb0{1} =>
                    dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None/\
                    ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1})/\
               BP.qVo{2} <= qVo /\
               BP.bb1{1}=BBS.bb{2}               /\
               size BBS.bb{2} = size BP.bb0{1}  /\
               (BP.pk{1} = get_pk BP.sk{1})         /\
               (forall i, 0 <= i < size BBS.bb{2} =>
                  (nth witness BBS.bb{2} i).`2 =  (nth witness BP.bb0{1} i).`2) /\ 
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BBS.bb{2} i).`3,
                            (nth witness BBS.bb{2} i).`2) in
                   let y = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} y.`2 y.`1 HRO.RO.m{2})/\
               (forall i, let b = nth witness BP.bb0{1} i in 
                          let b'= nth witness BBS.bb{2} i in
                  0 <= i < size BP.bb0{1} /\ 
                  BBS.hvo{2}.[(i,b'.`3, b'.`2)] = None => b = b')/\
               (forall (c,l), mem BBS.encL{2} (c,l) =>
                  exists id s, mem BBS.bb{2} (id,l,c,s))/\
               (forall (l,c), ! mem BBS.encL{2} (c,l) =>
                forall i, BBS.hvo{2}.[(i,c,l)]  = None)/\
                (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})/\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})
              ).       
                      
    (* A.a1: vote oracle *)
    + proc. 
      if =>//=; sp.
      if; auto; last by progress; smt.
      inline Right.l_or_r; wp.
      seq 6 4: (={glob E, glob S, glob C, glob HRO.RO, glob SS, 
                  BP.coL, glob JRO.RO, BP.uL, BP.pk, BP.sk, BP.qVo, 
                  BP.qCa, id, v0, v1,b,BP.hk} /\
              BP.qVo{2} < qVo /\
              b0{1}.`1 = b1{1}.`1  /\ 
              b0{1}.`2 = b{2}.`2 /\
              b1{1}  = b{2} /\
              bb{1} = BBS.bb{2} /\ 
              BP.bb1{1} = BBS.bb{2} /\
              size BBS.bb{2} = size BP.bb0{1} /\ 
              (BP.pk{1} =get_pk BP.sk{1})       /\
              BP.uL{1}.[id{1}] <> None /\ !id{1} \in BP.coL{1} /\
              id{1} = b0{1}.`1 /\
              (forall x, x\in BP.bb0{1} =>
                    dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None/\
                    ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1})/\
              (forall i, 0 <= i < size BBS.bb{2} =>
                  (nth witness BBS.bb{2} i).`2 =  (nth witness BP.bb0{1} i).`2) /\ 
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BBS.bb{2} i).`3,
                            (nth witness BBS.bb{2} i).`2) in
                   let y = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} y.`2 y.`1 HRO.RO.m{2})/\
               (forall i, let b = nth witness BP.bb0{1} i in 
                          let b'= nth witness BBS.bb{2} i in
                  0 <= i < size BP.bb0{1} /\ 
                  BBS.hvo{2}.[(i,b'.`3, b'.`2)] = None => b = b')/\
               (forall (c,l), mem BBS.encL{2} (c,l) =>
                  exists id s, mem BBS.bb{2} (id,l,c,s))/\
               (forall (l,c), ! mem BBS.encL{2} (c,l) =>
                forall i, BBS.hvo{2}.[(i,c,l)]  = None)/\
                (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})/\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})/\ 
              Some v0{1} = dec_cipher BP.sk{1} b0{1}.`2 b0{1}.`3 
                                      HRO.RO.m{1} /\
              ver_sign b0{1}.`2 b0{1}.`3 b0{1}.`4 JRO.RO.m{1}/\
              dec_cipher BP.sk{1} b1{1}.`2 b1{1}.`3 HRO.RO.m{1} <> None /\
              ver_sign b1{1}.`2 b1{1}.`3 b1{1}.`4 JRO.RO.m{1} ).
      + inline *; wp; sp.
        seq 1 0: (l{2} = oget BP.uL{2}.[id{2}] /\
  l_or_r{2} = false /\
  v{2} = (if l_or_r{2} then v0{2} else v1{2}) /\
  id0{2} = id{2} /\
  v2{2} = v{2} /\
  pk{2} = BP.pk{2} /\
  usk{2} = l{2}.`2 /\
  upk{2} = get_upk usk{2} /\
  lps{1} = oget BP.uL{1}.[id{1}] /\
  id0{1} = id{1} /\
  v{1} = v0{1} /\
  pk{1} = BP.pk{1} /\
  usk{1} = lps{1}.`2 /\
  upk{1} = get_upk usk{1} /\
  ={id, v0, v1} /\
    ={glob E, glob S, glob C, HRO.RO.m, glob SS, BP.coL, JRO.RO.m, BP.uL,
        BP.pk, BP.qVo, BP.qCa, BP.sk,BP.hk} /\
    (forall (x : ident * upkey * cipher * sign),
       x \in BP.bb0{1} => 
        dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None/\
        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}) /\
    BP.qVo{2} <= qVo /\
    BP.bb1{1} = BBS.bb{2} /\
    size BBS.bb{2} = size BP.bb0{1} /\
    BP.pk{1} = get_pk BP.sk{1} /\
    BP.uL{1}.[id{1}]<> None /\ !id{1} \in BP.coL{1} /\
   BP.qVo{1} < qVo /\
   (forall i, 0 <= i < size BBS.bb{2} =>
                  (nth witness BBS.bb{2} i).`2 =  (nth witness BP.bb0{1} i).`2) /\ 
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BBS.bb{2} i).`3,
                            (nth witness BBS.bb{2} i).`2) in
                   let y = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} y.`2 y.`1 HRO.RO.m{2})/\
               (forall i, let b = nth witness BP.bb0{1} i in 
                          let b'= nth witness BBS.bb{2} i in
                  0 <= i < size BP.bb0{1} /\ 
                  BBS.hvo{2}.[(i,b'.`3, b'.`2)] = None => b = b')/\
               (forall (c,l), mem BBS.encL{2} (c,l) =>
                  exists id s, mem BBS.bb{2} (id,l,c,s))/\
               (forall (l,c), ! mem BBS.encL{2} (c,l) =>
                forall i, BBS.hvo{2}.[(i,c,l)]  = None)/\
                (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})/\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})/\
 BP.uL{1}.[id{1}] <> None /\ ! (id{1} \in BP.coL{1}) /\
  Some v0{1} = dec_cipher BP.sk{1} upk{1} c{1} HRO.RO.m{1}   
). 
        exists* (glob E){1}, BP.sk{1}, upk{1}, v{1}; 
          elim * => ge sk1 l1 v1.
        call{1} (Eenc_decL ge sk1 (get_pk sk1) l1 v1 _); first by done.
        by auto. 

seq 8 1: ( l{2} = oget BP.uL{2}.[id{2}] /\
  l_or_r{2} = false /\
  v{2} = (if l_or_r{2} then v0{2} else v1{2}) /\
  id0{2} = id{2} /\
  v2{2} = v{2} /\
  pk{2} = BP.pk{2} /\
  usk{2} = l{2}.`2 /\
  upk{2} = get_upk usk{2} /\
  lps{1} = oget BP.uL{1}.[id{1}] /\
  id0{1} = id{1} /\
  v{1} = v0{1} /\
  pk{1} = BP.pk{1} /\
  usk{1} = lps{1}.`2 /\
  upk{1} = get_upk usk{1} /\
  ={id, v0, v1} /\
    ={glob E, glob S, glob C, HRO.RO.m, glob SS, BP.coL, JRO.RO.m, BP.uL,
        BP.pk, BP.qVo, BP.qCa, BP.sk,BP.hk} /\
    (forall (x : ident * upkey * cipher * sign),
       x \in BP.bb0{1} => 
        dec_cipher BP.sk{1} x.`2 x.`3 HRO.RO.m{1} <> None/\
        ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}) /\
    BP.qVo{2} <= qVo /\
    BP.bb1{1} = BBS.bb{2} /\
    size BBS.bb{2} = size BP.bb0{1} /\
    BP.pk{1} = get_pk BP.sk{1} /\
    BP.uL{1}.[id{1}]<> None /\ !id{1} \in BP.coL{1} /\
   BP.qVo{1} < qVo /\
  BP.uL{1}.[id{1}] <> None /\ ! (id{1} \in BP.coL{1}) /\
  Some v0{1} = dec_cipher BP.sk{1} upk{1} c{1} HRO.RO.m{1} /\
  b0{1} = (id0, upk, c, s){1}  /\ id1{1} = id{1} /\ v2{1} = v1{1} /\ 
  pk0{1} = BP.pk{1} /\ usk0{1} = lps{1}.`2 /\ upk0{1} = get_upk usk0{1} /\
  c0{1} = c{2} /\
  dec_cipher BP.sk{1} upk{2} c{2} HRO.RO.m{1} <> None /\
  ver_sign b0{1}.`2 b0{1}.`3 b0{1}.`4 JRO.RO.m{1}/\
  (forall i, 0 <= i < size BBS.bb{2} =>
                  (nth witness BBS.bb{2} i).`2 =  (nth witness BP.bb0{1} i).`2) /\ 
               (forall i, 0<= i < size BP.bb0{1} =>
                   let x = ((nth witness BBS.bb{2} i).`3,
                            (nth witness BBS.bb{2} i).`2) in
                   let y = ((nth witness BP.bb0{1} i).`3,
                            (nth witness BP.bb0{1} i).`2) in
                   x \in BBS.encL{2} => 
                   BBS.hvo{2}.[(i,x.`1,x.`2)] =
                    dec_cipher BP.sk{2} y.`2 y.`1 HRO.RO.m{2})/\
               (forall i, let b = nth witness BP.bb0{1} i in 
                          let b'= nth witness BBS.bb{2} i in
                  0 <= i < size BP.bb0{1} /\ 
                  BBS.hvo{2}.[(i,b'.`3, b'.`2)] = None => b = b')/\
               (forall (c,l), mem BBS.encL{2} (c,l) =>
                  exists id s, mem BBS.bb{2} (id,l,c,s))/\
               (forall (l,c), ! mem BBS.encL{2} (c,l) =>
                forall i, BBS.hvo{2}.[(i,c,l)]  = None)/\
                (forall x, x\in BBS.bb{2} =>
                           (x.`3,x.`2) \in BBS.encL{2} /\ !x.`1 \in BP.coL{2} \/
                           !(x.`3,x.`2) \in BBS.encL{2} /\ x.`1 \in BP.coL{2})/\
               (forall x, x\in BBS.encL{2} => 
                  exists id s, (id,x.`2,x.`1,s) \in BBS.bb{2})
). 

 swap{1} [1..2] 6.
 wp.
 exists* (glob SS){1}, usk{1}, c{1}; 
          elim * => gss l1 c1.
        call{1} (Sig_ver_one gss l1 (get_upk l1) c1 _); first by done.
 sp; exists* BP.sk{1}, upk{2}, v2{2}; elim* => skx upkx vx. 
 call{1} (Eenc_dec skx (get_pk skx) upkx vx _); first by done.
        by auto =>/>; progress; smt. 


        exists * usk0{1}, c0{1}; elim* => uskx cx.
        call{1} (Sig_ver_two uskx (get_upk uskx) cx _); first by done.
        by auto =>/>; progress; smt. 

      inline *; wp; sp.
      exists *(glob C){1}, (glob SS){1}, (glob E){1}, b2{1}, BP.sk{1}; 
        elim*=> gc gs ge cx skx.
      wp; call{1} (validInd_dec_one gc ge gs skx (get_pk skx) cx _); 
         first by done.  
      auto=>/>; progress. 
      + rewrite mem_cat mem_seq1 in H18; smt.
      + rewrite mem_cat mem_seq1 in H18; smt.
      + smt. smt (size_cat). smt (nth_cat).  
      + rewrite mem_cat mem_seq1 in H20.
        case (i = size BP.bb0{1}). smt.
        move => Hi.
        have His: i < size BP.bb0{1}. smt.
        rewrite ?nth_cat H2 His //=. 
        move: H20; rewrite nth_cat H2 His //=; move => H20. 
        case (((nth witness BBS.bb{2} i).`3, 
                 (nth witness BBS.bb{2} i).`2) \in BBS.encL{2}). smt. 
        move => Hn.
        move: H20; rewrite Hn //=. 
        elim => Hb3 Hb2.
          smt.
      + case (i < size BP.bb0{1}). 
        + move => Hi. rewrite ?nth_cat H2 Hi //=. 
          move: H20; rewrite nth_cat H2 Hi //=. 
          smt.
        - move => Hi.
          move: H20; rewrite ?nth_cat H2 Hi //=. 
          smt.
      + move: H18; rewrite ?mem_cat ?mem_seq1; move => Hx18. 
        case ((c, l0) \in BBS.encL{2}). 
        + move => Hm. cut := H9 c l0 Hm. 
          elim => id s Hmids. 
          by exists id s; rewrite mem_cat Hmids. 
        - move => Hnm. 
          have ->: c = b{2}.`3 by smt.
          have ->: l0 =b{2}.`2 by smt.
          exists b{2}.`1 b{2}.`4.
          smt.
      + smt. 
      + rewrite mem_cat mem_seq1 in H18; smt.
      + rewrite mem_cat mem_seq1 in H18. 
        case (x \in BBS.encL{2}).
        + move => Hm.
          cut := H12 x Hm. 
          elim => id s HmBB.
          by exists id s; rewrite mem_cat HmBB. 
        - move => Hm.
          exists b{2}.`1 b{2}.`4. 
          by rewrite mem_cat; smt. 
      + smt.

    (* A.a1: cast oracle *)
    + proc. 
      if =>//=.
      if =>//=; last by auto.
      inline *; wp; sp.   
      exists* b0{1}, BP.sk{1}; elim* => bx skx.
      call{1} (validInd_dec skx (get_pk skx) bx _); first by done.
      auto; progress. 
      + rewrite mem_cat mem_seq1 in H15; smt.
      + rewrite mem_cat mem_seq1 in H15; smt.
      + smt. smt. 
      + case (i < size BP.bb0{1}). 
        + move => Hi. 
          move: H17; rewrite ?nth_cat H1 Hi //=. move=> H17. 
          smt.
        - move => His.
          have Hi: i = size BP.bb0{1} by smt. 
          move: H17; rewrite ?nth_cat H1 Hi //=. move => H17.
          cut := H8 (b{2}.`3, b{2}.`2) H17.
          simplify. elim => id s HmBB.
          cut := H12 (id, b{2}.`2, b{2}.`3, s) HmBB. simplify.
          move => Hb1.
          cut := H7 (id, b{2}.`2, b{2}.`3, s) HmBB.
          by rewrite //= H17 Hb1 H11 //=.
      + case (i < size BP.bb0{1}). 
        + move => Hi.
          move: H17; rewrite ?nth_cat H1 Hi //=. 
          smt.
        - move => His.
          have Hi: i = size BP.bb0{1} by smt. search nth size.
          by move: H17; rewrite ?cats1 ?nth_rcons His H1 Hi //=.
      + cut := H8 (c,l) H15.
        simplify; elim => id s Hm.
        by exists id s; rewrite mem_cat Hm.
      + rewrite mem_cat mem_seq1 in H15; smt.
      + cut := H8 x H15.
        simplify; elim => id s Hm.
        by exists id s; rewrite mem_cat Hm.

    + by proc; inline*; auto.
    + by proc. 
    + by conseq So_ll2; progress.
    + by proc.
     
  
  (* before  A.a1 *)
  call(: true).
  inline BPRIV_R(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO, S).V.setup
         BPRIV_R(B'(E, P, Ve, C(E,SS), SS), A, HRO.RO, GRO.RO, JRO.RO, S).V.register.
  wp; while (={ i, BP.uL, BP.uLL, glob SS, glob JRO.RO});
    first by sim.
  wp; rnd; call{1} (Ekgen_extractPk HRO.RO).
  wp; call(_: true).
  call (_: true ==> ={glob JRO.RO});
    first by sim.
  swap{1} 10 -9.
  call (_: true ==> ={glob HRO.RO});
    first by sim. 
   inline *; wp.
    while{1} (true) (FSet.card work{1}); progress.
      by auto=>/>; progress; smt.
    by auto=>/>; progress; smt. 

  (* A.a2 call *)
  call (_: ={glob E, glob S, glob C, glob HRO.RO, glob JRO.RO,
             BP.pk,  BP.sk, BP.sk, BP.qVo,BP.hk} /\
             BP.bb1{1}=BBS.bb{2}).
  + proc; inline *; auto. 
  + by sim. by sim. by sim.
  
  call(_: true).
  inline *; wp.
  wp; call{1} (Pp_ll GRO.RO GRO.RO_o_ll).
  by auto.
qed.

module BIND(V:VotingScheme', A: BPRIV_Adv, SS: SignScheme, J: JOracle.Oracle,
            S: BPRIV_Simulator, IO: Ind1CCA_Oracles) ={

  module O = {

    proc h = IO.o
    proc g = S.o
    proc j = J.o

    (* vote oracle *)
    proc vote(id : ident, v0 v1 : vote) : unit = {
      var b, c, l, s;
   
      if (BP.qVo < qVo){
        if (BP.uL.[id] <> None /\ !id \in BP.coL){
          l   <- oget BP.uL.[id]; 
          (* challenge vote*)
          if (forall x, x \in BBS.bb => 
                  (x.`1,x.`2) = (id,get_upk l.`2) \/ 
                  (x.`1 <> id /\ x.`2 <> get_upk l.`2)) {
            c    <@ IO.enc(get_upk l.`2, v0, v1);
            BBS.encL <- BBS.encL++[(c,get_upk l.`2)];
            s    <@ SS(J).sign(l.`2,c);
            b    <- (id,get_upk l.`2, c, s);
            BBS.hvo.[(size BBS.bb, b.`3,b.`2)] <- v0;
            BBS.bb  <- BBS.bb ++ [b];
          }
        }
        BP.qVo <- BP.qVo + 1;
      }
    }
    
  
    (* cast oracle *)
    proc cast(b : (ident * upkey * cipher * sign)) : unit={
      var ev;
      
      if (BP.qCa < qCa){
        if(BP.uL.[b.`1]<> None /\ b.`1 \in BP.coL){
          ev <@ V(IO,S,J).valid(BBS.bb, b, BP.pk);
          if (ev){
            BBS.bb <- BBS.bb ++ [b];         
          }
        }
        BP.qCa <- BP.qCa + 1;
      }
    }

    (* public board *)
    proc board(): (pub_data * hash_out) list={
      var pbb;
      pbb <@ V(IO,S,J).publish(BBS.bb, BP.hk);
      return pbb;
    }
  }(* end O module *)

  module A = A(O)

  proc main (pk: pkey): bool = {

    var i, id, pbb, pi, cL, mL;
    var b, vo, ubb;
    BP.hk <$ dhkey_out;
    BP.qVo  <- 0;
    BP.qCa  <- 0;
    i       <- 0;
    BP.uL   <- empty;
    BBS.bb      <- [];
    BBS.hvo     <- empty;    
    BBS.encL    <- [];   
    BP.uLL      <- [];  
    BP.pk   <- pk;              
    i <- 0;

    J.init();
    S.init();

    while (i < size (undup Voters)){
      id         <- nth witness (undup Voters) i;
      BP.uL.[id] <@ V(IO,S,J).register(id);
      BP.uLL <- BP.uLL ++ [(id, (oget BP.uL.[id]).`1)];
      i          <- i + 1;
    }
    
    BP.coL <@ A.corL(BP.pk, BP.uLL);
    A.a1(BP.pk, filter (fun (a: ident) (b: upkey * uskey), 
                                          a \in BP.coL) BP.uL);

    i       <- 0;
    cL      <- [];
    mL      <- [];
    ubb     <- [];
    
    (* remove ID *)
    cL <- map (fun (x: ident*upkey*cipher*sign), (x.`3,x.`2)) BBS.bb;

    (* decrypt all, with challenge to None *)
    mL <@ IO.dec(cL);

    while( i < size (map remi BBS.bb)){ 
      b        <- nth witness (map remi BBS.bb) i;

      if (!mem BBS.encL (b.`2,b.`1)){ 
       (* cast vote, use decryption from oracle *)
        vo   <- nth witness mL i;
      }else{
       (* honest vote, use vo saved *) 
        vo <- BBS.hvo.[(i,b.`2,b.`1)];
      }
      ubb <- ubb ++ [(b.`1, oget vo)];
      i  <- i + 1;
    }
    
    BP.r  <- Count (map snd (Sanitize ubb));
    pbb   <@ V(IO,S,J).publish (BBS.bb, BP.hk);
    pi    <@ S.prove(BP.pk, pbb, BP.r);
    BP.b  <@ A.a2(BP.r, pi);
    return BP.b;
  }
}.

(* lemma bounding reduction from BPRIV single board to IND1CCA multi-challenge *)
local lemma sgBoard_ind1CCA (LR<: LorR {BS, BP, BBS, HRO.RO, GRO.RO, 
                                    JRO.RO, SS, E, S, C, A}) &m:
  islossless LR.l_or_r =>
  Pr[BPRIV_SB(E, C(E,SS), SS, A, HRO.RO, JRO.RO, LR, S).main() @ &m: res ] =
  Pr[Ind1CCA(E,BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO, S),HRO.RO,LR).main() 
         @ &m: res /\ size BS.encL <= qVo ].
proof.
  move => LR_ll.
  byequiv =>//=. 
  proc.
  inline Ind1CCA(E, BIND(B'(E, P, Ve, C(E,SS), SS), A, SS, JRO.RO, S), HRO.RO, LR).A.main.
  swap{1} 9 10.
  seq 18 21: (={glob A, glob C, glob E, glob S, 
                JRO.RO.m, HRO.RO.m, BP.uL, BP.pk, BP.qVo, 
                BP.qCa, BBS.encL, BBS.bb, BBS.hvo,BP.hk} /\ 
             !BS.decQueried{2}            /\
             BP.pk{1} = BS.pk{2} /\
             BP.sk{1} = BS.sk{2} /\
             size BS.encL{2} <= qVo /\
             BBS.encL{1} = BS.encL{2}).
  (* A.a1 call *) 
    call (_: ={glob C, glob E, glob S, glob HRO.RO,
             glob SS, glob JRO.RO, BP.uL, BP.pk, BP.coL,
             BP.qVo, BP.qCa, BBS.encL, BBS.bb, BBS.hvo,BP.hk} /\ 
             !BS.decQueried{2}            /\
             BP.pk{1} = BS.pk{2} /\
             BP.sk{1} = BS.sk{2} /\
             size BS.encL{2} <= BP.qVo{2} /\
             BP.qVo{2} <= qVo /\
             BBS.encL{1} = BS.encL{2}).   
    + proc.
      if =>//=. 
      if =>//=; last by auto=>/>; smt.
      sp. if{2}=>//=.
      + inline *. 
        rcondt{1} 11; progress.
        + wp; call(: true); first by proc. 
          call(: true); first by proc.
          wp; call(: true).      
          by auto. 
        wp; call(: ={glob JRO.RO}); first by sim.
        wp; call(: ={glob HRO.RO}); first by sim.
        wp; call(: true); wp.
        by auto=>/>; progress; smt.
      - inline *. 
        rcondf{1} 11; progress.
        + wp; call(: true); first by proc. 
          call(: true); first by proc.
          wp; call(: true).      
          by auto. 
        seq 7 0: (l{2} = oget BP.uL{2}.[id{2}] /\
                  l{1} = oget BP.uL{1}.[id{1}] /\
                  ={id, v0, v1, l, glob C, glob E, glob S, HRO.RO.m, 
                    glob SS, JRO.RO.m, BP.uL, BP.pk, BP.hk,
                    BP.coL, BP.qVo, BP.qCa, BBS.encL, BBS.bb, BBS.hvo} /\
                    !BS.decQueried{2} /\
                    BP.pk{1} = BS.pk{2} /\
                    BP.sk{1} = BS.sk{2} /\
                    size BS.encL{2} <= BP.qVo{2} /\
                    BP.qVo{2} <= qVo /\ 
                    BBS.encL{1} = BS.encL{2} /\
                    BP.qVo{1} < qVo /\
                    BP.uL{1}.[id{1}] <> None /\ 
                    ! (id{1} \in BP.coL{1}) /\
                    ! (forall (x : ident * upkey * cipher * sign),
                       x \in BBS.bb{2} =>
                       x.`1 = id{2} && x.`2 = get_upk l{2}.`2 \/
                       x.`1 <> id{2} /\ x.`2 <> get_upk l{2}.`2)).
          by wp; call{1} LR_ll.
        
        exists* (glob E){1}, (glob SS){1}; elim* => ge gs.
        wp; call{1} (SSs_eq gs); call{1} (Ee_eq ge).
        by auto=>/>; progress; smt.

    + proc.
      if =>//=.
      if =>//=; last by auto. 
      inline*; wp.
      call(: ={glob JRO.RO, glob HRO.RO}); first 2 by sim.
      by auto.
    + by proc; inline*; wp.
    + by proc.
    + by conseq So_ll2.
    + by proc.
    call(: true).
    while (={i, BP.uL, glob SS, BP.uLL});
      first by inline*; sim.
    swap{1} [14..15] -2.
    call(: true).
    call(: true ==> ={glob JRO.RO}); first by sim.    
    wp; rnd; wp;call(: true).
    call(: true ==> ={glob HRO.RO}); first by sim. 
    by auto=>/>; smt. 

  wp; call(: ={HRO.RO.m, JRO.RO.m, glob S, BBS.bb, BP.pk,BP.hk}).
  + by proc; inline*; auto. 
  + by proc. by sim. by proc. 
  call(: true).  
  inline B'(E, P, Ve, C(E,SS), SS, Ind1CCA_Oracles(E, HRO.RO, LR), S, JRO.RO).publish
     BPRIV_SB(E, C(E,SS), SS, A, HRO.RO, JRO.RO, LR, S).BV.publish.
  wp. 
  while (={i, BBS.bb, BBS.encL, BBS.hvo, ubb, HRO.RO.m, glob E}/\
         0<= i{1} /\
         BP.sk{1} = BS.sk{2}/\ 
         mL{2} = map (fun (x : ident * upkey * cipher * sign) =>
                           if ! (x.`3,x.`2) \in BBS.encL{2}
                           then dec_cipher BS.sk{2} x.`2 x.`3 HRO.RO.m{2}
                           else None) BBS.bb{2}). 
    seq 1 1: (={i, b, BBS.bb, BBS.encL, BBS.hvo, ubb, HRO.RO.m, glob E} /\
              0<= i{1}/\  BP.sk{1} = BS.sk{2}/\ 
              mL{2} = map (fun (x : ident * upkey * cipher * sign) =>
                           if ! (x.`3,x.`2) \in BBS.encL{2}
                           then dec_cipher BS.sk{2} x.`2 x.`3 HRO.RO.m{2}
                           else None) BBS.bb{2} /\
              i{1} < size BBS.bb{1}/\
              b{1} = remi (nth witness BBS.bb{1} i{1})).
      auto=>/>; smt.       
    if=>//=.
    + exists* (glob E){1}, BP.sk{1}, b{1}; elim* => ge skx bx.
      wp; call{1} (Edec_Odec ge skx bx.`1 bx.`2).
      auto=>/>; progress. 
      + rewrite (nth_map witness witness 
                   (fun (x : ident * upkey * cipher * sign) =>
                   if ! ((x.`3, x.`2) \in BBS.encL{2})
                   then dec_cipher BS.sk{2} x.`2 x.`3 HRO.RO.m{2}
                   else None) 
                   i{2} BBS.bb{2} _);
          first by rewrite H H0.
        by rewrite //= H1. 
      + smt.
    - by auto=>/>; progress; smt.
  inline*.
  rcondt{2} 9; progress.
    by auto.
  wp; while{2} ( ={glob E, HRO.RO.m} /\
                 0 <= i0{2} /\ 
                 (mL0{2} = map (fun (cl: cipher* upkey) =>
                        if ! cl \in BS.encL{2}
                        then dec_cipher BS.sk{2} cl.`2 cl.`1 HRO.RO.m{2}
                        else None) (take i0{2} cL0{2})))
             (size cL0{2} - i0{2}); progress. 
      wp; sp.
      exists * (glob E), c, l, BS.sk{hr}; elim* => ge c2 l2 sk2.
      call{1} (Edec_Odec ge sk2 l2 c2).
      auto; progress. 
      + by smt. 
      + have Hx: 0 <= i0{hr}  < size cL0{hr} by smt.
        rewrite (take_nth witness) 1: Hx // -cats1.
        by smt.
      + by smt. + by smt. 
      + have Hx: 0 <= i0{hr}  <  size cL0{hr} by done.
        rewrite (take_nth witness) 1: Hx // -cats1.
        by smt.
      + by smt.

  auto=>/>; progress.
  + smt (take0).
  + smt.
  + rewrite take_oversize; first by smt.
    by rewrite -map_comp /(\o).
qed.           

(* guess bad oracle *)
clone  Ex_Plug_and_Pray as EPP with
  type tin  <- unit,
  type tres <- bool,
  op bound  <- qVo
proof bound_pos. 
realize bound_pos. by apply qVo_bound.  qed. 

 
(* Lemma lossless constructed IND1CCA adversary *)
local lemma BINDmain_ll (IO <: Ind1CCA_Oracles{BIND(B'(E,P,Ve,C(E,SS),SS),A,SS,JRO.RO,S)}):
  islossless IO.enc =>
  islossless IO.dec =>
  islossless IO.o =>
  islossless BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO, S, IO).main.
proof.
  move => IOe_ll IOd_ll IOo_ll.
  proc.
  call (Aa2_ll (<: BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO, S, IO).O) _ _ _ _) =>//.
  + by proc; inline*; wp.
  + by apply So_ll.
  + by proc.
  wp; call{1} Sp_ll.
  inline B'(E, P, Ve, C(E,SS), SS, IO, S, JRO.RO).publish. 
  wp; while{1} (true) (size BBS.bb - i); progress.
    by auto; progress; smt.
  call{1} IOd_ll.
  wp.
  call (Aa1_ll (<: BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO, S, IO).O) _ _ _ _ _ _) =>//.
  + proc. 
    if =>//=.
    if =>//=; last by auto.
    sp;if{1}; auto.
    wp; call{1} (SSs_ll JRO.RO JRO.RO_o_ll).
    wp; call{1} IOe_ll.
    by auto; progress; smt.
  + proc.
    if =>//=. 
    if; auto. 
    inline *; wp. 
    call{1} (Co_ll IO JRO.RO IOo_ll JRO.RO_o_ll).     
    by auto; progress; smt.   
  + by proc; inline*; wp.
  + by apply So_ll.
  + by proc. 
  call{1} (Acor_ll (<: BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO, S, IO).O)).
  wp; while{1} (true)
               (size (undup Voters) - i); progress.
    inline *; wp.
    call{1} (SSk_ll JRO.RO JRO.RO_o_ll).
    by auto; progress; smt.
  call{1} Si_ll.
  inline *. 
  while{1} (true) (FSet.card work); progress.
    by auto; progress; smt.
  by auto;progress; smt.
qed. 

(* Lemma apply the hybrid argument *) 
local lemma hybrid &m  (H <: HOracle.Oracle { BP, BS, BPS, BBS, H.Count, 
                                H.HybOrcl, WrapAdv, GRO.RO, E, S, Ve, P, C, A,
                                JRO.RO, SS}):
  islossless H.init =>
  islossless H.o    =>
  `|Pr[Ind1CCA(E,BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S),H,Left).main() 
            @ &m: res /\ size BS.encL <= qVo ] -
    Pr[Ind1CCA(E,BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S),H,Right).main() 
            @ &m: res /\ size BS.encL <= qVo ]|
 = qVo%r *
  `|Pr[Ind1CCA(E,WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S),E,H),H,Left).main() 
            @ &m: res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1 ] -
    Pr[Ind1CCA(E,WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S),E,H),H,Right).main() 
            @ &m: res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1 ]|.
proof.
  move=> Hi_ll Ho_ll; rewrite eq_sym.
  cut Ht: forall (n a b : real), 
          n >0%r => n * a = b <=> a = 1%r/n * b by progress; smt.  
  pose a:= `|Pr[Ind1CCA(E, WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO, S), E, H), 
                        H, Left).main () @ &m : 
                        res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1] -
             Pr[Ind1CCA(E, WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO, S), E, H), 
                        H, Right).main () @ &m : 
                        res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1]|.
  pose b:= `|Pr[Ind1CCA(E, BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S), 
                        H, Left).main() @ &m : res /\ size BS.encL <= qVo] -
             Pr[Ind1CCA(E, BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S), 
                        H, Right).main() @ &m : res /\ size BS.encL <= qVo]|.
  rewrite -/a -/b.
  rewrite (Ht qVo%r a b). 
    cut t:= qVo_bound. 
    by smt.
  rewrite /a /b. 

  by apply (Ind1CCA_multi_enc H E  (BIND(B'(E,P,Ve,C(E,SS),SS),A,SS,JRO.RO,S)) Hi_ll Ho_ll
           (Ek_ll H) (Ee_ll H) (Ed_ll H) BINDmain_ll &m). 
qed.

(* Lemma bounding |IBPRIV - BPRIV-R| by n* strong correctness and n* |IND1CCAL-IND1CCAR| *)
local lemma bound_MV_Ind1CCA &m:
  `|Pr[IBPRIV (B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, JRO.RO, GRO.RO, S).main() @ &m : res ] -
    Pr[BPRIV_R(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO, S).main() @ &m : res]|
  <= 
  qVo%r *
   `|Pr[Ind1CCA(E,WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS),A,SS,JRO.RO,S),
                E,HRO.RO),HRO.RO,Left).main() @ &m: 
                res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1 ] -
     Pr[Ind1CCA(E,WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS),A,SS, JRO.RO,S),
                E,HRO.RO),HRO.RO,Right).main() @ &m: 
                res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1 ]|.
proof.
  rewrite (left_to_sgBoard2 &m) (right_to_sgBoard2 &m).
  rewrite (sgBoard_ind1CCA Left &m _); first by proc.
  rewrite (sgBoard_ind1CCA Right &m _); first by proc.
  rewrite -(hybrid &m HRO.RO _ _).
  + exact/HRO.RO_init_ll/distr_h_out/is_finite_h_in.
  + exact/HRO.RO_o_ll.
  by done.
qed.

(* Lemma bounding BPRIV  *) 
lemma bpriv &m:
    `|Pr[BPRIV_L(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ]-
      Pr[BPRIV_R(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ]| 
<=
    `|Pr[ZK_L(DecRel(E,HRO.RO), P, BZK(E,P,C(E,SS),Ve,SS,A,HRO.RO,JRO.RO), GRO.RO).main() @ &m : res] -
      Pr[ZK_R(DecRel(E,HRO.RO), S, BZK(E,P,C(E,SS),Ve,SS,A,HRO.RO,JRO.RO)).main() @ &m : res]| +
qVo%r *
    `|Pr[Ind1CCA(E, WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S), E, HRO.RO), 
               HRO.RO, Left).main() @ &m : 
               res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1] -
      Pr[Ind1CCA(E, WrapAdv(BIND(B'(E,P,Ve,C(E,SS),SS), A, SS, JRO.RO,S), E, HRO.RO), 
               HRO.RO, Right).main() @ &m : 
               res /\ WrapAdv.l <= qVo /\ size BS.encL <= 1]|.
proof.
  (* lemma bound_MV_MV' for BPRIV_L to IBPRIV  *)
  cut L:= bound_MV_MV' &m.  
  (* lemma bound_MV_Ind1CCA for IBPRIV to BPRIV_R *)
  cut R:= bound_MV_Ind1CCA &m. 

  cut trig: forall (a b c: real), `|a - b| <= `|a-c| + `|c -b| by smt.
  cut M:= trig 
         (Pr[BPRIV_L(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ])
         (Pr[BPRIV_R(B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ])
         (Pr[IBPRIV (B'(E,P,Ve,C(E,SS),SS), A, HRO.RO, JRO.RO,GRO.RO, S).main() @ &m : res]).
  by smt. 
qed.

end section BPRIV.

(* ---------------------------------------------------------------------- *)
(* Strong Consistency  *)
section StrongConsistency.

declare module H : HOracle.Oracle 
                   { BP, JRO.RO}.
declare module G : GOracle.Oracle 
                   { BP, HRO.RO, JRO.RO, H}.
declare module E : Scheme        
                   { BP, H, HRO.RO, G, JRO.RO}.
declare module S : Simulator 
                   { HRO.RO, H, G, E}.
declare module Ve: Verifier
                   { HRO.RO, H, G, E, S}.
declare module P : Prover
                   { HRO.RO, H, G, E, S, Ve}.
declare module C: ValidInd'       
                   { BP, HRO.RO, JRO.RO, H, G, E, S, Ve, P}.
declare module SS: SignScheme
                   { BP, H, G, HRO.RO, JRO.RO, E, C, Ve, P}.

declare module AC2: SConsis2_Adv {BP, H, HRO.RO, JRO.RO, G}.
declare module AC3: SConsis3_Adv {BP, HRO.RO, JRO.RO, E, C, G, SS}.

(* ** ASSUMPTIONS ** *)
(* ** start *)
  (* valid operator for ballots *)
  op validInd: pkey -> (ident * upkey * cipher) -> 
                     (h_in, h_out) map -> bool.
  axiom weight_dj_out:
    weight dj_out = 1%r.
  axiom weight_dk_out:
    weight dhkey_out = 1%r.
  axiom is_finite_h_in : Finite.is_finite predT <:h_in>.
  axiom distr_h_out    : is_lossless dh_out.

  axiom is_finite_j_in : Finite.is_finite predT <:j_in>.
  axiom distr_j_out    : is_lossless dj_out.

  (* lossless *)
  (* -> for oracles *)
  axiom Gi_ll: islossless G.init.
  axiom Go_ll: islossless G.o.
  
  (* -> for validInd *)
  axiom Co_ll (H <: HOracle.ARO) (J<: JOracle.ARO):
    islossless H.o =>
    islossless J.o=>
    islossless C(E,SS,H,J).validInd.
   
  (* -> for proof system *)
  axiom Pp_ll (G <: GOracle.ARO): 
    islossless G.o =>
    islossless P(G).prove. 

  (* -> for encryption *)
  axiom Ek_ll (H <: HOracle.ARO): 
    islossless H.o =>
    islossless E(H).kgen.
  (* -> for strong consistency adversary *)
  axiom AC2_ll (O <: SCons_Oracle { AC2 }):
    islossless O.h       =>
    islossless O.g       =>
    islossless AC2(O).main.

  axiom SSk_ll (J <: JOracle.ARO):
    islossless J.o =>
    islossless SS(J).kgen.
  axiom SSs_ll (J <: JOracle.ARO):
    islossless J.o =>
    islossless SS(J).sign.

   axiom Ekgen_extractPk (H<: HOracle.ARO):
    equiv [E(H).kgen ~ E(H).kgen:  ={glob H, glob E} ==> 
          ={glob H, glob E,  res} /\ 
          res{2}.`1 = get_pk res{2}.`2].

  axiom Ekgen_extractPk_one (ge: glob E) (H<: HOracle.ARO):
    phoare [E(H).kgen: 
           ge = (glob E) ==> 
           ge = (glob E) /\ 
          res.`1 = get_pk res.`2] = 1%r.

  axiom validInd_dec_one (gc: (glob C)) (ge: (glob E)) (gs: (glob SS))
                      (sk: skey) (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
    (pk = get_pk sk)=>
     phoare [C(E,SS,HRO.RO,JRO.RO).validInd : 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\ arg=(b,pk) 
          ==> 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\
          res = (dec_cipher sk b.`2 b.`3 HRO.RO.m <> None /\
                 ver_sign b.`2 b.`3 b.`4 JRO.RO.m)]=1%r.

  axiom validInd_ax (gc: (glob C)) (ge: (glob E)) (gs: (glob SS))
                    (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
     phoare [C(E,SS,HRO.RO,JRO.RO).validInd : 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\ arg=(b,pk) 
          ==> 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\
          res = (validInd pk (b.`1,b.`2, b.`3) HRO.RO.m /\
                 ver_sign b.`2 b.`3 b.`4 JRO.RO.m)]=1%r.
  
  axiom Edec_Odec (ge: (glob E)) (sk2: skey)(l2: upkey) (c2: cipher):
    phoare [E(HRO.RO).dec:  
           (glob E) =ge /\ arg = (sk2, l2, c2)
           ==>   
           (glob E) =ge /\
           res = dec_cipher sk2 l2 c2 HRO.RO.m ] = 1%r.

   axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)
             ==>
            (glob SS) =gs /\
            res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
(* ** end *)

(* decryption should not change the state of an eager random oracle *) 
local lemma dec_Dec_two (sk2: skey)(l2: upkey) (c2: cipher):
  equiv [E(HRO.RO).dec ~ E(HRO.RO).dec :
          ={glob HRO.RO, glob E, arg}/\ arg{2} = (sk2, l2, c2)
           ==>   
           ={glob HRO.RO, glob E, res} /\
           res{2} = dec_cipher sk2 l2 c2 HRO.RO.m{2} ].
proof.
  proc*=>//=. 
  exists* (glob E){1}; elim* => ge.
  call{1} (Edec_Odec ge sk2 l2 c2 ).
  call{2} (Edec_Odec ge sk2 l2 c2 ). 
  by auto.
qed.

local lemma validInd_dec (sk: skey) (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
    (pk = get_pk sk)=>
     equiv [C(E,SS,HRO.RO,JRO.RO).validInd ~ C(E,SS,HRO.RO,JRO.RO).validInd : 
          ={glob HRO.RO, glob JRO.RO, glob C, glob E, glob SS, arg} /\ arg{2}=(b,pk) 
          ==> 
          ={glob HRO.RO, glob JRO.RO, glob C, glob E, glob SS, res} /\
          res{1} = (dec_cipher sk b.`2 b.`3 HRO.RO.m{1} <> None /\
                    ver_sign b.`2 b.`3 b.`4 JRO.RO.m{1})].
proof.
  move => Hk.
  proc*.
  exists* (glob C){1}, (glob E){1}, (glob SS){1}; elim* => gc ge gs.
  call{1} (validInd_dec_one gc ge gs sk pk b Hk).
  call{2} (validInd_dec_one gc ge gs sk pk b Hk).
  by auto.
qed.


(* extract procedure *)
module CE(E: Scheme, H: HOracle.ARO)={
  proc extract(b: (ident * upkey * cipher), sk: skey): (upkey * vote option) = {
    var v;
    v <@ E(H).dec(sk,b.`2, b.`3);
    return (b.`2, v);
  }
}.

(*# lemma consistency part 1,
          break correctness of encryption used for the VotingScheme *) 
lemma consis1(id: ident, v: vote, l: uskey) &m: 
   Pr[SConsis1(B'(E,P,Ve,C(E,SS),SS), CE(E), H, G, JRO.RO).main(id,v, l) @ &m: res]  >=
   Pr[PKE.Correctness(E, H).main(v,get_upk l) @ &m: res].
proof.
  byequiv =>//=. 
  proc.
  inline *.
  wp; call (_: ={glob H}); first by sim.
  wp; call{2} (SSs_ll JRO.RO JRO.RO_o_ll).
  wp; call (_: ={glob H}); first by sim.
  wp; rnd{2}; call(_: ={glob H}).
  while{2} (true) (FSet.card work{2}); progress.
    by auto; progress; smt.
  wp; call{2} Gi_ll.
  call(_: true).
  by auto; progress; smt.
qed.  

(*# lemma consistency part 3,
          result consistency: *) 
equiv consis3 &m:
    SConsis3_L(B'(E,P,Ve,C(E,SS),SS), C(E,SS), AC3, HRO.RO, G, JRO.RO).main ~ 
    SConsis3_R(B'(E,P,Ve,C(E,SS),SS), CE(E), C(E,SS), AC3, HRO.RO, G, JRO.RO).main
    : ={glob HRO.RO, glob G, glob JRO.RO, glob E, 
        glob C, glob SS, glob AC3} ==> ={res}. 
proof.
  proc.
  seq 13 14: ( ={glob E, glob HRO.RO, BP.pk, BP.sk, bb, r, ev}/\
               ub{2} = []/\
               (ev{1} => (forall x, x\in bb{1} =>
                          dec_cipher BP.sk{2} x.`2 x.`3 HRO.RO.m{1} <> None /\
                          ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}))).
    wp; while (={ glob E, glob C, glob HRO.RO, glob SS, glob JRO.RO, i, bb, BP.pk, ev}/\
               get_pk BP.sk{2} = BP.pk{2} /\
               0 <= i{1} /\
               (ev{1} => (forall x, x\in (take i{1} bb{1}) =>
                          dec_cipher BP.sk{2} x.`2 x.`3 HRO.RO.m{1} <> None /\
                          ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1}))).
      sp; wp.
      if; auto.
      exists* BP.sk{2}, b{1}; elim* => skx bx.
      call{1} (validInd_dec skx (get_pk skx) bx _); first by done.
      auto=>/>; progress. 
      + smt. 
      + move: H5; rewrite (take_nth witness); first by rewrite H H1.
        rewrite mem_rcons. 
        smt.
      + move: H5; rewrite (take_nth witness); first by rewrite H H1.
        rewrite mem_rcons. 
        smt.
      progress; smt. 

    wp; call(_: ={glob E, glob HRO.RO, glob C, glob G, 
                  BP.pk, BP.sk, BP.uL}); 
      [1..2: by sim].
    inline *. 
    while (={glob SS, JRO.RO.m, BP.uL,i});
      first by sim.
    wp; rnd; call{1} (Ekgen_extractPk  HRO.RO). 
    while (={work0, JRO.RO.m});
      first by sim.
    wp;  call (_: true).
    while (={work, HRO.RO.m});
      first by sim.
    auto=>/>; progress. 
    + by rewrite H. 
    + smt (take0).
    + smt (take0).
    + smt (take_oversize).
    + smt (take_oversize).

  inline *. 
  if=>//=.
  wp; call{1} (Pp_ll G Go_ll); auto. 
  wp; while ( ={ glob E, glob HRO.RO} /\
          i0{1} = i{2}/\ fbb{1} = map remi bb{2}/\
          sk{1} = BP.sk{2} /\
          ubb{1} = ub{2}/\ 0<= i{2}/\
          (forall x, x\in bb{2} =>
                          dec_cipher BP.sk{2} x.`2 x.`3 HRO.RO.m{1} <> None /\
                          ver_sign x.`2 x.`3 x.`4 JRO.RO.m{1})).
    sp; exists * (glob SS){1}, b0{1}, sk{1}; elim* => gs bx skx.
    wp; call{1} (ver_Ver_one gs bx.`1 bx.`2 bx.`3).
    call(dec_Dec_two skx bx.`1 bx.`2). 
    auto=>/>; progress. 
    + rewrite (nth_map witness witness); first by rewrite H H2. 
      by rewrite /remi. 
    + rewrite (nth_map witness witness); first by rewrite H H2. 
      by rewrite /remi. 
    + rewrite (nth_map witness witness); first by rewrite H H2. 
      by rewrite /remi. 
    + rewrite (nth_map witness witness); first by rewrite H H2. 
      by rewrite /remi. 
    + by rewrite H3 H4.
    + smt.
    + smt (size_map). smt (size_map).
    + cut := H0 (nth witness bb{2} i{2}) _. 
        by rewrite mem_nth; first by rewrite H H2.
      elim => Hd Hv.
      have Hx: (nth witness (map remi bb{2}) i{2}).`3 = (nth witness bb{2} i{2}).`4.
        by rewrite (nth_map witness witness); first by rewrite H H2.
      by move: H7; rewrite H3 H4 Hx Hd Hv.
    + smt.  smt (size_map). smt (size_map).
  by auto; smt.
qed. 

(*# lemma consistency part 2,
          valid => validInd *)
lemma consis2 &m: 
  Pr[SConsis2(B'(E,P,Ve,C(E,SS),SS), C(E,SS), AC2, HRO.RO, G, JRO.RO).main() @ &m: res] = 1%r.
proof.
  byphoare=>//.
  proc.
  inline
      SConsis2(B'(E, P, Ve, C(E, SS), SS), C(E, SS), AC2, HRO.RO, G, JRO.RO).V.valid.
  seq 13: (b0 = b/\ pk = BP.pk)=>//=.
    wp. 
    call{1} ( AC2_ll (<: SConsis2(B'(E,P,Ve,C(E,SS),SS), C(E,SS), AC2, HRO.RO, G,JRO.RO).O) _ _).
    + by apply HRO.RO_o_ll. 
    + by apply Go_ll.
    inline SConsis2(B'(E,P,Ve,C(E,SS),SS), C(E,SS), AC2, HRO.RO, G, JRO.RO).V.setup.
    wp; while{1} (true)
             (size Voters - i); progress.
      inline*; wp.
      call{1} (SSk_ll JRO.RO JRO.RO_o_ll).
      by auto; progress; smt. 
    exists * (glob E); elim* => ge.
    wp; rnd; call{1} (Ekgen_extractPk_one ge HRO.RO).
    call{1} (JRO.RO_init_ll _ _ ); first 2 by smt.
    auto; call{1} Gi_ll. 
    call{1} (HRO.RO_init_ll _ _ ); first 2 by smt.
    by auto; progress; smt. 
   
    exists* (glob C), (glob E), (glob SS), BP.pk, b; elim* =>gc ge gs pk bv. 
    call{1} (validInd_ax gc ge gs pk bv). 
    wp; call{1} (validInd_ax gc ge gs pk bv). 
    by auto; progress; smt.

  hoare; progress. 
  wp; call(_: true).
  + by conseq HRO.RO_o_ll. 
  + by conseq Go_ll.
  wp; while(true).
    by inline *; wp; call (_: true).
  inline SConsis2(B'(E, P, Ve, C(E, SS), SS), C(E, SS), AC2, HRO.RO, G, JRO.RO).V.setup.
  wp; rnd; call(_: true); first by call(_: true).
qed.

end section StrongConsistency.


(* ---------------------------------------------------------------------- *)
(* Strong Correctness  *)
section StrongCorrectness.
require import FSet.

declare module E : Scheme        
                   { BS, BP, BSC,  HRO.RO, JRO.RO, GRO.RO}.
declare module S : Simulator 
                   { BSC, BP, BS, HRO.RO, JRO.RO,E}.
declare module Ve: Verifier
                   { BS, BP,  HRO.RO, JRO.RO, E, S}.
declare module Pe : Prover
                   { BS, BP, HRO.RO, JRO.RO, E, S, Ve}.
declare module C: ValidInd'       
                   { BS, BP, BSC,  HRO.RO, JRO.RO, GRO.RO, E, S, Ve, Pe}.
declare module SS: SignScheme
                   {BS, BP, BSC, HRO.RO, JRO.RO, GRO.RO, E, S, Ve, Pe, C}.

(* ** ASSUMPTIONS ** *) 
 axiom Kgen_get_pk (H<: HOracle.ARO):
    equiv [E(H).kgen ~ E(H).kgen:  ={glob H, glob E} ==> 
          ={glob H, glob E,  res} /\ 
          res{2}.`1 = get_pk res{2}.`2].

 axiom Kgen_get_upk (J<: JOracle.ARO):
    equiv [SS(J).kgen ~ SS(J).kgen:  ={glob J, glob SS} ==> 
          ={glob J, glob SS,  res} /\ 
          res{2}.`2 = get_upk res{2}.`1].

(* axiom for transforming an encryption into decryption (one-sided) *)
  axiom Eenc_decL (ge: (glob E)) (sk2: skey) 
                  (pk2: pkey)(l2: upkey) (p2: vote): 
    (pk2 = get_pk sk2) =>
    phoare [E(HRO.RO).enc : 
          (glob E) = ge /\ arg=(pk2, l2, p2) 
          ==> 
          (glob E) = ge /\
          Some p2 = dec_cipher sk2 l2 res HRO.RO.m ]= 1%r.   
 
   axiom Sig_ver_one (gs: (glob SS)) (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   phoare [SS(JRO.RO).sign :
           gs= (glob SS) /\ arg = (usk2,c2)
           ==>   
           gs = (glob SS) /\
           ver_sign upk2 c2 res JRO.RO.m ]=1%r.

   axiom validInd_dec_one (gc: (glob C)) (ge: (glob E)) (gs: (glob SS))
                      (sk: skey) (pk: pkey) 
                      (b: ident * upkey * cipher*sign):
    (pk = get_pk sk)=>
     phoare [C(E,SS,HRO.RO,JRO.RO).validInd : 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\ arg=(b,pk) 
          ==> 
          (glob C) = gc /\ (glob E) = ge /\ (glob SS) = gs /\
          res = (dec_cipher sk b.`2 b.`3 HRO.RO.m <> None /\
                 ver_sign b.`2 b.`3 b.`4 JRO.RO.m)]=1%r.

(*
module type SCorr_Adv'(H: HOracle.ARO, G: GOracle.ARO, J: JOracle.ARO) = {
  proc main(pk: pkey, uL: (ident, upkey * uskey) map) 
       : ident * vote * (ident * upkey * cipher * sign) list
}. *)


module SCorr_mem(V:VotingScheme', A: SCorr_Adv, 
             H: HOracle.Oracle, G: GOracle.Oracle, J: JOracle.Oracle) = {
  module V = V(H, G, J)
  module A = A(H,G,J)
  
  proc main () : bool = {

    var id, i, v, bb;
    BSC.uL    <- empty;
    BSC.valid <- false;
    i         <- 0;

       H.init();
       G.init();
       J.init();

    (BSC.pk, BSC.sk,BSC.hk) <@ V.setup();
    while (i < size (undup Voters)){
      id <- nth witness (undup Voters) i;
      BSC.uL.[id] <@ V.register(id);
      i<- i + 1;
    }
    
    (id, v, bb) <@ A.main(BSC.pk, BSC.uL);
    return BSC.valid;
  }
}.


local lemma scorr_to_scorr(AS <: SCorr_Adv { E, Pe, Ve, C, HRO.RO, 
                                             JRO.RO, GRO.RO, SS, BSC} ) &m:
 Pr[SCorr(B'(E,Pe,Ve,C(E,SS),SS), 
           AS, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res ] =
 Pr[SCorr_mem(B'(E,Pe,Ve,C(E,SS),SS), 
           AS, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res ].
proof.
  byequiv=>/>.
  proc.
  seq 9 9 :( ={BSC.uL, id, v, bb, BSC.pk, BSC.sk, BSC.valid}/\
              !BSC.valid{1}/\
              BSC.pk{1} = get_pk BSC.sk{1}/\
              (forall x, BSC.uL{1}.[x] <> None =>
                 (oget BSC.uL{1}.[x]).`1 = get_upk (oget BSC.uL{1}.[x]).`2)).
    wp; call(: ={glob HRO.RO, glob JRO.RO, glob GRO.RO}); first 3 by sim.
    while (={i, BSC.uL, JRO.RO.m, glob SS}/\ 0<= i{1} /\
           (forall x, BSC.uL{1}.[x] <> None =>
                 (oget BSC.uL{1}.[x]).`1 = 
                  get_upk (oget BSC.uL{1}.[x]).`2)).
      inline*; wp; sp.
      call{1} (Kgen_get_upk JRO.RO).
      by auto=>/>; try rewrite get_set; smt. 
    inline 
       SCorr(B'(E, Pe, Ve, C(E, SS), SS), AS, HRO.RO, GRO.RO, JRO.RO).V.setup
        SCorr_mem(B'(E, Pe, Ve, C(E, SS), SS), AS, HRO.RO, GRO.RO, JRO.RO).V.setup.
    wp; rnd; call{1} (Kgen_get_pk HRO.RO).
    call (: true ==> ={JRO.RO.m}); first by sim.
    call(: true ==> ={glob GRO.RO}); first by sim.
    call(: true ==> ={HRO.RO.m}); first by sim. 
    by auto=>/>; smt. 
  if{1}.   
  + inline*; wp. swap{1} 2 13.
    seq 8 0: (={BSC.uL, id, v, bb, BSC.pk, BSC.sk, BSC.valid} /\
              !BSC.valid{1} /\ 
              BSC.pk{1} = get_pk BSC.sk{1} /\
              (oget BSC.uL{1}.[id{1}]).`1 = 
                  get_upk (oget BSC.uL{1}.[id{1}]).`2/\
              BSC.uL{1}.[id{1}] <> None /\
              dec_cipher BSC.sk{1} upk{1} c{1} HRO.RO.m{1} <> None /\
              ver_sign upk{1} c{1} s{1} JRO.RO.m{1} /\
              lps{1}= oget BSC.uL.[id]{1}/\
              id0{1} = id{1} /\
              pk{1} = BSC.pk{1}/\
              usk{1} = lps.`2{1} /\
              upk{1} = get_upk usk{1}/\
              (forall x, BSC.uL{1}.[x] <> None =>
                 (oget BSC.uL{1}.[x]).`1 = get_upk (oget BSC.uL{1}.[x]).`2)).
     seq 7 0: (lps{1} = oget BSC.uL{1}.[id{1}] /\
              id0{1} = id{1} /\
              v0{1} = v{1} /\
              pk{1} = BSC.pk{1} /\
              usk{1} = lps{1}.`2 /\
              upk{1} = get_upk usk{1} /\
              ={BSC.uL, id, v, bb, BSC.pk, BSC.sk, BSC.valid} /\
                !BSC.valid{1}/\
                BSC.pk{1} = get_pk BSC.sk{1} /\
                (forall (x : ident),
                BSC.uL{1}.[x] <> None =>
                (oget BSC.uL{1}.[x]).`1 = 
                  get_upk (oget BSC.uL{1}.[x]).`2) /\
                  BSC.uL{1}.[id{1}] <> None/\
                dec_cipher BSC.sk{1} upk{1} c{1} HRO.RO.m{1} <> None ).
       sp; exists* (glob E){1}, BSC.sk{1}, upk{1}, v0{1}; 
         elim* => ge skx upkx vx.
       call{1}( Eenc_decL ge skx (get_pk skx) upkx vx _); first by done.
       by auto; smt.
     exists * (glob SS){1}, usk{1}, c{1}; elim* => gs uskx cx.
     call{1} (Sig_ver_one gs uskx (get_upk uskx) cx _); first by done.
     by auto; smt. 
    sp; exists* (glob C){1}, (glob E){1}, (glob SS){1}, BSC.sk{1}, b{1};
      elim* => gc ge gs skx bx.
    wp; call{1} (validInd_dec_one gc ge gs skx (get_pk skx) bx _); first by done.    
    by auto=>/>; smt.    
  - by auto=>/>; smt.
qed.

local lemma scorr_zero(AS <: SCorr_Adv { E, Pe, Ve, C, HRO.RO, 
                                             JRO.RO, SS, BSC} )  &m:
 Pr[SCorr_mem(B'(E,Pe,Ve,C(E,SS),SS), 
           AS, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res ] =0%r.
proof.
  byphoare=>/>.
  hoare=>/>.
  proc.
  call(: true). + by proc. + by proc. by proc.
  while (true).
    inline*; wp.
    call(: true).
    by auto.
  inline SCorr_mem(B'(E, Pe, Ve, C(E, SS), SS), AS, HRO.RO, GRO.RO, JRO.RO).V.setup.
  wp; rnd; call(: true).
  call(: true).  
    while (true); first by auto.
    by auto.
  call(: true).  
    while (true); first by auto.
    by auto.
  call(: true).  
    while (true); first by auto.
    by auto.
  by auto.
qed.

lemma scorr (AS <: SCorr_Adv { E, Pe, Ve, C, HRO.RO, 
                                JRO.RO, GRO.RO, SS, BSC} )  &m:
  Pr[SCorr(B'(E,Pe,Ve,C(E,SS),SS), 
           AS, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res ] = 0%r.
proof.
  by rewrite (scorr_to_scorr AS &m) (scorr_zero AS &m).
qed.

end section StrongCorrectness.













