require import AllCore List.
require (*  *) Belenios_DEF_Hom_2 DiffieHellman.
require import Distr Int FMap FSet Real Mu_mem. 
require import LeftOrRight.

(* ###############################################################
   BELENIOS version 4 
   Policy = First   // first vote is kept based on public credential
   maybe_id = ident // we publish First ballot with the voter id, and
                      the hash of this 

   ############################################################### *)

(* the space for the secret key and public key *)
  clone export CyclicGroup as Ksp.
  type skey = t.
  type pkey = group.

(*  ---------------------------------------------------------------------- *)
  (* voter identity *)
  type ident.


(* ---------------------------------------------------------------------- *)
(* First vote policy  *)
  (* last policy*)
  op San['a 'b] (bb : ('a * 'b) list) =
    with bb = "[]"      => []
    with bb = (::) b bb => if   has (pred1 b.`1 \o fst) bb
                         then San bb
                         else b :: San bb.
  (* first policy *)
  op Sanitize['a 'b] (bb : ('a * 'b) list) = rev (San (rev bb)).

clone export Belenios_DEF_Hom_2 as Hv3H with
  type ident            <- ident,
  type HH.MV2.maybe_id   <- ident,  (* publish the voter id *)
  op HH.MV2.Sanitize['a 'b]  <- Sanitize. (* First Policy *)


module Belenios (Pe: PSpke.Prover, Ve: PSpke.Verifier, Ve': PSpke.Verifier, 
                  Pz: Prover,  Vz: Verifier, SS: SignScheme,
                  H: HOracle.ARO,   G: GOracle.ARO, J: JOracle.ARO) =
         BeleniosHom (Pe, Ve, Ve', Pz, Vz, SS, H, G, J).

section Security.

declare module S : Simulator 
                   { BP, BS, BPS, BSC, BBS, HRO.RO, JRO.RO, GRO.RO, 
                     H.Count, H.HybOrcl, WrapAdv}.
declare module Vz: Verifier
                   { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, JRO.RO, GRO.RO, S}.
declare module Pz : Prover
                   { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, JRO.RO, GRO.RO, S, Vz}.
declare module SS: SignScheme
                   { BP, BS, BPS, BH, BW, WU, BU, GRO.RO, JRO.RO, S, Vz, Pz, HRO.RO, 
                     BSC, BBS, H.Count, H.HybOrcl, WrapAdv}.
declare module Ex: PoK_Extract    {BW, BSC,Pz, BPS, GRO.RO, JRO.RO, Vz, HRO.RO, SS}.


declare module BPRIV_A: BPRIV_Adv        
                  { BP, BS, BPS, BSC, BBS, HRO.RO, GRO.RO, S, Vz, Pz, SS, JRO.RO,
                    H.Count, H.HybOrcl, WrapAdv}. 
declare module CONSIS2_A: SConsis2_Adv {BP, HRO.RO, JRO.RO, GRO.RO, SS}.
declare module CONSIS3_A: SConsis3_Adv {BP, HRO.RO, JRO.RO, GRO.RO, SS, Pz}.

declare module DBB_A : DBB_Adv    
                  {BS, BSC, BPS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO,  Pz, Vz, SS,Ex}. 

declare module DREG_A : DREG_Adv    
                  {BS, BSC, BPS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO,  Pz, Vz, SS,Ex}. 


(* this are needed for the LPKE scheme *)
declare module Ve: PSpke.Verifier
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, 
                    S, Vz, Pz, BPRIV_A, DBB_A, DREG_A, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO, Ex}.
declare module Pe: PSpke.Prover
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO,
                    S, Vz, Pz, Ve, BPRIV_A, DBB_A, DREG_A, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO,Ex}.

(* admit. fixme: link between E and C *)
declare module Ve': PSpke.Verifier
                  { BP, BS, BPS, BSC, BBS, BH, BW, WU, BU, HRO.RO, GRO.RO, JRO.RO, 
                    S, Vz, Pz, BPRIV_A, DBB_A, DREG_A, Ve, Pe, CONSIS2_A, CONSIS3_A,
                    H.Count, H.HybOrcl, WrapAdv, SS, JRO.RO,Ex}.
(* ** ASSUMPTIONS ** *)
(* ** start *)
  axiom size_Voters:
    0 < size Voters.

  axiom weight_dh_out: weight dh_out = 1%r.
  axiom weight_dg_out: weight dg_out = 1%r. 
  axiom weight_dj_out: weight dj_out = 1%r.
  axiom weight_dk_out: weight dhkey_out = 1%r.
  axiom is_finite_h_in : Finite.is_finite predT <:h_in>.
  axiom distr_h_out    : is_lossless dh_out.
  axiom is_finite_j_in : Finite.is_finite predT <:j_in>.
  axiom distr_j_out    : is_lossless dj_out.
  axiom is_finite_g_in : Finite.is_finite predT <:g_in>.
  axiom distr_g_out    : is_lossless dg_out.

  axiom inj_get_upk (x y : uskey): get_upk x = get_upk y => x = y.
  axiom inj_get_pk  (x y : skey) : get_pk x  = get_pk y  => x = y.


  (* Policy restrictions *)
  lemma San_mem_a['a 'b] (L: ('a*'b) list) x:
    x \in (San L) => x \in L.
    by elim: L =>//=; smt. qed.

  lemma San_mem_f['a 'b] (L: ('a*'b) list) x:
    x \in (Sanitize L) => x \in L.
  proof.
    rewrite /Sanitize mem_rev. 
    move => HS.
    by rewrite -mem_rev (San_mem_a _ x HS).
  qed.

  lemma San_mem (L: (ident * (plain * upkey * cipher*sign)) list) x:
    mem (Sanitize L) x => mem L x
    by exact (San_mem_f L x).
    
  lemma San_mem'(L : (ident * (upkey * cipher * sign)) list) x:
    x \in Sanitize L => x \in L
    by exact (San_mem_f L x).

  lemma San_mem''(L : (upkey * (ident * cipher * sign)) list) x:
    x \in Sanitize L => x \in L
    by exact (San_mem_f L x).

  lemma San_mem''' (sbb: (upkey * h_cipher) list) (b: upkey * h_cipher):
    mem (Sanitize sbb) b => mem sbb b
    by exact (San_mem_f sbb b).

  lemma San_map_l (L: (upkey*cipher*sign)list) 
                (p: upkey * cipher* sign -> plain):
   let g = (fun (x: upkey*cipher*sign), (x.`1, p x)) in
   San (map g L) = map g (map uopen_2 (San (map upack_2 L))).
   proof.
    move => g. rewrite /USan.
    elim: L=>//=. 
    move => x L HL.
    have ->: has (pred1 (g x).`1 \o fst) (map g L) =
           has (pred1 (upack_2 x).`1 \o fst) (map upack_2 L).
     rewrite ?hasP //= /upack_2 /(\o).
     rewrite eq_iff; progress.
      + move: H; rewrite mapP //=.
        elim => y [HyA Hyx].         
        exists (y.`1, (y.`2, y.`3)). smt.   
      - move: H; rewrite mapP //=.
        elim => y [HyA Hyx].         
        exists (g y). smt.
     smt.
   qed. 

  lemma San_map (L: (upkey*cipher*sign)list) 
                (p: upkey * cipher* sign -> plain):
   let g = (fun (x: upkey*cipher*sign), (x.`1, p x)) in
   Sanitize (map g L) = map g (USan L).
   proof.
    move => g. rewrite /USan /Sanitize.
    by rewrite -?map_rev San_map_l  -/g -?map_comp ?map_rev.
   qed.
    
  lemma San_uniq_a['a 'b] (L: ('a * 'b) list): 
    uniq (San L).
  proof. 
    elim: L =>//=. 
    move => x L Hu.
    search uniq has.
    case (has (pred1 x.`1 \o fst) L).
    + move => Hhas. smt.
    - rewrite hasPn. move => Hnhas.
      rewrite Hu //=. 
      cut HL:= Hnhas x. smt.
  qed.

  lemma San_uniq_f['a 'b] (L: ('a * 'b) list): 
    uniq (Sanitize L).
  proof. 
    by rewrite /Sanitize rev_uniq San_uniq_a. 
  qed.

  lemma San_uniq (L: (ident * plain * upkey * cipher * sign) list): 
    uniq (Sanitize (map pack4 L))
    by exact San_uniq_f.

  lemma San_uniq' (L : (ident * (upkey * cipher * sign)) list):
      uniq (Sanitize L)
    by exact San_uniq_f.

  lemma San_uniq_fst_a['a 'b] (L: ('a * 'b) list):
     uniq (map fst (San L)).
  proof.  
    elim: L =>//=. 
    move => x L Hu.
    case (has (pred1 x.`1 \o fst) L).
    + move => Hhas. smt.
    - move => Hnhas. rewrite Hu //=.
      cut := Hnhas. rewrite mapP negb_exists //=. 
      rewrite hasP negb_exists /pred1 /(\o) //=.
      move => Ha a.
      cut Hor:= Ha a.
      case (a.`1 <> x.`1). smt.
      simplify. move => Heq.
      move: Hor; rewrite Heq //=; move => Hor.
      smt.
  qed.

  lemma San_uniq_fst_f['a 'b] (L: ('a * 'b) list):
     uniq (map fst (Sanitize L)).
  proof.  
    by rewrite /Sanitize map_rev rev_uniq San_uniq_fst_a.
  qed.

  lemma San_uniq_fst (L: (upkey * (ident * cipher * sign)) list):
     uniq (map fst (Sanitize L))
     by exact San_uniq_fst_f.

  lemma San_uniq_fst' (L : (ident * (upkey * cipher * sign)) list):
     uniq (map fst (Sanitize L))
     by exact San_uniq_fst_f. 

  lemma San_uniq_snd_a['a 'b] (L: ('a * 'b) list ):
     uniq (map snd L) =>
     uniq (map snd (San L)).
  proof.
    pose snd:= (fun (p : 'a * 'b) => p.`2).    
    elim: L =>//=. 
    move => x L Hu Hs.
    case (has (pred1 x.`1 \o fst) L).
    + smt.
    smt.
  qed. 

  lemma San_uniq_snd_f['a 'b] (L: ('a * 'b) list ):
     uniq (map snd L) =>
     uniq (map snd (Sanitize L)).
  proof.
    pose snd:= (fun (p : 'a * 'b) => p.`2).    
    move => HsL.
    by rewrite /Sanitize map_rev rev_uniq San_uniq_snd_a map_rev rev_uniq.
  qed. 

  lemma San_uniq_snd (S: (ident * (upkey * cipher * sign)) list ):
     uniq (map snd S) =>
     uniq (map snd (Sanitize S))
     by exact San_uniq_snd_f. 
 
  lemma San_uniq_filter_a (L: (ident * upkey * cipher * sign) list) 
                        (f: ident * upkey * cipher * sign -> ident):
      uniq (map f L) => uniq (map f (map open3 (San (map pack3 L)))). 
  proof.
    rewrite /Sanitize' -map_comp. 
    elim: L =>//=.
    move => x L Hu Hand.  
    case (has (pred1 (pack3 x).`1 \o fst) (map pack3 L)).
    + move => Hhas. smt.
    - move => Hnhas. 
      rewrite (Hu _) //=; first by rewrite (andWr _ _ Hand). 
      rewrite /(\o) -open_pack_3. 
      rewrite mapP negb_exists //=.
      move => a.
      rewrite negb_and //=.
      cut := andWl _ _ Hand.
      rewrite mapP negb_exists //=.
      move => Ha.
      cut := Ha (open3 a). 
      rewrite negb_and //=.
      case (f x <> f (open3 a)). smt.
      + simplify. move => Heq.
      - move => Hop. 
        have Hn: ! a \in (map pack3 L). smt.
        smt.
  qed.

  lemma San_uniq_filter (L: (ident * upkey * cipher * sign) list) 
                        (f: ident * upkey * cipher * sign -> ident):
      uniq (map f L) => uniq (map f (Sanitize' L)). 
  proof.
    rewrite /Sanitize'/Sanitize. 
    move=> HfL. 
    by rewrite ?map_rev rev_uniq -map_rev San_uniq_filter_a map_rev rev_uniq. 
  qed.

  (* axiom that maintains Sanitize membership w.r.t a filtered list *)
  lemma San_mem_filter_a (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in San L => 
     x \in San (filter f L).
  proof.
    move => Hf.
    elim: L =>//=.
    move => y L Hm. 
    case (has (pred1 y.`1 \o fst) L).
    + smt.
    - move => Hnhas Hop. 
    case (x <> y). smt.
    simplify. move => Heq. rewrite -Heq Hf //=. 
    have ->:! has (pred1 x.`1 \o fun (p : ident * (upkey * cipher * sign)) => p.`1)
     (filter f L).
       cut := Hnhas; rewrite ?hasPn. smt.
    smt.
  qed.
     
  lemma San_mem_filter_f (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in Sanitize L => 
     x \in Sanitize (filter f L).
  proof.
    move => Hf.
    rewrite /Sanitize ?mem_rev -filter_rev.
    by exact San_mem_filter_a.
  qed.

  lemma San_filter_ident_a['a 'b] (L: ('a * 'b) list) p:
    let f = (fun (x: 'a * 'b), p x.`1) in
    San (filter f L) =  filter f (San L). 
  proof.
    move => f.
    elim: L =>//=.
    move => x L Hs. 
    case (f x). 
    + move => Hfx. 
      have ->: has (pred1 x.`1 \o fun (p0 : 'a * 'b) => p0.`1) (filter f L) =
               has (pred1 x.`1 \o fun (p0 : 'a * 'b) => p0.`1) L.
        rewrite ?hasP eq_iff.
        progress. smt. smt.
      smt.
    - move => Hnfx.
      smt.       
  qed.

  lemma San_filter_ident_f['a 'b] (L: ('a * 'b) list) p:
    let f = (fun (x: 'a * 'b), p x.`1) in
    Sanitize (filter f L) =  filter f (Sanitize L). 
  proof.
    move => f.
    rewrite /Sanitize.
    by rewrite -filter_rev San_filter_ident_a filter_rev -/f.       
  qed.

  
  (* axiom that maintains Sanitize membership w.r.t a filtered list *)
  lemma San_mem_filter_l (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in San L => 
     x \in San (filter f L).
  proof.
    move => Hf.
    elim: L =>//=.
    move => y L Hm. 
    case (has (pred1 y.`1 \o fst) L).
    + smt.
    - move => Hnhas Hop. 
    case (x <> y). smt.
    simplify. move => Heq. rewrite -Heq Hf //=. 
    have ->:! has (pred1 x.`1 \o fun (p : ident * (upkey * cipher * sign)) => p.`1)
     (filter f L).
       cut := Hnhas; rewrite ?hasPn. smt.
    smt.
  qed.
     
  lemma San_mem_filter (L : (ident * (upkey * cipher*sign)) list) f x:
     f x => 
     x \in Sanitize L => 
     x \in Sanitize (filter f L).
  by exact San_mem_filter_f. qed.


  lemma San_filter_ident (L: (ident * (upkey * cipher * sign)) list) p:
    let f = (fun (x: ident * (upkey * cipher * sign)), p x.`1) in
    Sanitize (filter f L) =  filter f (Sanitize L).
  proof. 
    by rewrite (San_filter_ident_f L p). 
  qed.

  (* axiom on commutativity of policy and decryption *)
  lemma san_tallymap_l (sbb: (upkey * h_cipher) list) (sk: skey):
    let f = (fun (b : upkey * h_cipher) =>
                (b.`1, (oget (decrypt sk b.`2)))) in
  San (map f sbb)  =  map f (San sbb).
  proof.
  elim sbb =>//=.
  move => x l Hxl. 
  pose f:= (fun (b : upkey * h_cipher) =>
                (b.`1, (oget (decrypt sk b.`2)))).
              
  have ->:  (has (pred1 (f x).`1 \o fst) (map f l)) = (has (pred1 x.`1 \o fst) l).
    rewrite has_map. 
    have ->: (preim f (pred1 (f x).`1 \o fst)) = (pred1 x.`1 \o fst).
      rewrite /preim. 
    rewrite /(\o) /pred1 //=.
    smt.
  case (has (pred1 x.`1 \o fst) l); 
    move: Hxl; rewrite -/f; smt. 
  qed.

  lemma san_tallymap (sbb: (upkey * h_cipher) list) (sk: skey):
    let f = (fun (b : upkey * h_cipher) =>
                (b.`1, (oget (decrypt sk b.`2)))) in
  Sanitize (map f sbb)  =  map f (Sanitize sbb).
  proof.
    move => f. rewrite /Sanitize.
    cut := san_tallymap_l (rev sbb) sk. 
    rewrite -/f //=; move => Ho. 
    by rewrite map_rev -Ho map_rev.
  qed.

  lemma uniq_to_San_a['a 'b] (A: ('a * 'b) list):
    uniq (map fst A) => San A = A.  
  proof.
    elim: A=>/>.   
    move => x L HuL HfL HL. 
    have ->: !has (pred1 x.`1 \o fun (p : 'a * 'b) => p.`1) L.
      rewrite hasPn /(\o) /pred1 //=. 
      smt.
    by rewrite //= (HuL HL). 
  qed.

  lemma uniq_to_San_f['a 'b] (A: ('a * 'b) list):
    uniq (map fst A) => Sanitize A = A.  
  proof.
    move => HfA.
    rewrite /Sanitize uniq_to_San_a.
    + by rewrite map_rev rev_uniq HfA.
    by rewrite revK.
  qed.

  lemma uniq_to_San (A: (upkey * (cipher * sign)) list):
    uniq (map fst A) =>
    Sanitize A = A
  by exact uniq_to_San_f.   
           
  lemma USan_USanitize_l (A: (ident *upkey*cipher*sign) list):
    map (remi \o uopen) (San (map upack A)) =
    map uopen_2 (San (map (upack_2 \o remi) A)).
  proof.
    rewrite  -?map_comp.
     elim: A=>//=. 
     move => x A HA. 
     have ->: has (pred1 (upack x).`1 \o fst ) (map upack A) = 
           has (pred1 ((\o) upack_2 remi x).`1 \o fst) (map (upack_2 \o remi) A).
      rewrite ?hasP /upack /upack_2 /(\o) /remi ?mapP //=. 
      rewrite eq_iff; progress. 
      + move: H; rewrite mapP //=.
        elim => y [HyA Hyx0].         
        exists (y.`2, (y.`3, y.`4)). smt.   
      - move: H; rewrite mapP //=.
        elim => y [HyA Hyx0].         
        exists (y.`2, (y.`1, y.`3, y.`4)). smt.
     smt.
  qed.

  lemma USan_USanitize (A: (ident *upkey*cipher*sign) list):
    map remi (USanitize A) = USan (map remi A). 
  proof.
    rewrite /USan /USanitize /Sanitize -?map_comp.
    cut := USan_USanitize_l (rev A).
    rewrite -?map_comp ?map_rev.
    move => Hl. 
    by rewrite Hl.
  qed.

  lemma San'_USan_l (L: (ident*upkey*cipher*sign) list):
    (forall x y, x \in L => y \in L => x.`1 = y.`1 => x.`2 = y.`2) =>
    (forall x y, x \in L => y \in L => x.`2 = y.`2 => x.`1 = y.`1) =>
    map open3 (San (map pack3 L)) = map uopen (San (map upack L)).
  proof.
    rewrite /Sanitize' /USanitize.
    elim: L=>//=. 
    move => a L HL HL1 HL2.
    have ->: has (pred1 (pack3 a).`1 \o fst) (map pack3 L) = 
           has (pred1 (upack a).`1 \o fst) (map upack L).
      rewrite ?hasP //= /pack3 /upack /(\o). 
      rewrite eq_iff; progress.
      + move: H; rewrite mapP //=.
        elim => y [HyA Hyx].         
        exists (y.`2, (y.`1, y.`3, y.`4)). smt.   
      - move: H; rewrite mapP //=.
        elim => y [HyA Hyx].         
        exists (y.`1, (y.`2, y.`3, y.`4)). smt.
    smt.
  qed. 

  lemma San'_USan (L: (ident*upkey*cipher*sign) list):
    (forall x y, x \in L => y \in L => x.`1 = y.`1 => x.`2 = y.`2) =>
    (forall x y, x \in L => y \in L => x.`2 = y.`2 => x.`1 = y.`1) =>
    Sanitize' L = USanitize L.
  proof.
    rewrite /Sanitize' /USanitize /Sanitize.
    move => Hf1 Hf2. rewrite -?map_rev.
    cut := San'_USan_l (rev L) _ _. 
    + move => x y. by rewrite ?mem_rev; exact (Hf1 x y).
    + move => x y. by rewrite ?mem_rev; exact (Hf2 x y).
    move => Hl.
    by rewrite map_rev Hl -map_rev.
  qed. 

(* BPRIV adversary *)
  axiom Acor_ll (O <: BPRIV_Oracles{BPRIV_A}): 
    islossless BPRIV_A(O).corL.

  axiom BAa1_ll (O <: BPRIV_Oracles { BPRIV_A }):
    islossless O.vote    => islossless O.cast    =>
    islossless O.board   => islossless O.h       =>
    islossless O.g       => islossless O.j       =>
    islossless BPRIV_A(O).a1.
  axiom BAa2_ll (O <: BPRIV_Oracles { BPRIV_A }):
    islossless O.board => islossless O.h     =>
    islossless O.g     => islossless O.j     =>
    islossless BPRIV_A(O).a2.

  axiom CAa_ll (O <: SCons_Oracle { CONSIS2_A }):
    islossless O.h       => islossless O.g       =>
    islossless CONSIS2_A(O).main.

  axiom DAa1_ll (O <: DBB_Oracle { DBB_A }):
    islossless O.corrupt => islossless O.vote => 
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DBB_A(O).a1.
  axiom DAa2_ll (O <: DBB_Oracle { DBB_A }):
    islossless O.check =>  islossless O.h  =>
    islossless O.g     =>  islossless O.j  =>
    islossless DBB_A(O).a2.

  axiom RAa2_ll (O <: DREG_Oracle { DREG_A }):
    islossless O.corrupt => islossless O.vote => 
    islossless O.cast    => islossless O.board =>
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DREG_A(O).a2.
  axiom RAa3_ll (O <: DREG_Oracle { DREG_A }):
    islossless O.check   => islossless O.board =>
    islossless O.h       => islossless O.g   =>
    islossless O.j       => islossless DREG_A(O).a3.

  axiom ATU_ll (A<: TallyUniq_Adv') (V <: VotingScheme{A}) 
               (H <: HOracle.ARO{A}) 
              (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
    islossless A(V, H, G, J).main.

  axiom ATU2_ll (A<: TallyUniq_Adv_2) (V <: VotingScheme'{A}) 
               (H <: HOracle.ARO{A}) 
              (G <: GOracle.ARO{A}) (J <: JOracle.ARO{A}):
    islossless H.o =>
    islossless G.o => 
    islossless J.o => 
    islossless A(V, H, G, J).main.

  (* -> Proof system *)
  axiom Pz_ll (G <: GOracle.ARO): 
    islossless G.o => islossless Pz(G).prove. 
  axiom Vz_ll (G <: GOracle.ARO):
    islossless G.o => islossless Vz(G).verify.

  (* -> Small proof system, for ciphertexts *)
  axiom Pe_ll (H <: HOracle.ARO):
    islossless H.o =>  islossless Pe(H).prove.
  axiom Ve_ll (H <: HOracle.ARO):
    islossless H.o =>  islossless Ve(H).verify.
  axiom Ve_ll' (H <: HOracle.ARO):
    islossless H.o =>  islossless Ve'(H).verify.

  (* -> Signature *)
  axiom SSk_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).kgen.
  axiom SSs_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).sign. 
  axiom SSv_ll (SS<: SignScheme) (J <: JOracle.ARO): 
    islossless J.o => islossless SS(J).verify. 

  (* -> for ZK simulator *)
  axiom Si_ll: islossless S.init.
  axiom So_ll: islossless S.o.
  axiom Sp_ll: islossless S.prove.

  (* for extractor *)
  axiom Ex_ll (G <: GOracle.ARO):
    islossless G.o =>
    islossless Ex(G).extract.

  axiom Vz_keepstate (gv: (glob Vz)) :
    phoare [Vz(GRO.RO).verify:  
           (glob Vz) = gv ==> (glob Vz) = gv ] = 1%r.

  axiom Verify_to_verify (gv: (glob Ve)) (s: h_stm) (p: h_prf):
    phoare[Ve(HRO.RO).verify: (glob Ve) = gv /\ arg = (s, p)
         ==>
         (glob Ve) = gv /\
         res = verify s p HRO.RO.m] = 1%r.
  axiom Verify_to_verify' (gv: (glob Ve')) (s: h_stm) (p: h_prf):
    phoare[Ve'(HRO.RO).verify: (glob Ve') = gv /\ arg = (s, p)
         ==>
         (glob Ve') = gv /\
         res = verify s p HRO.RO.m] = 1%r.

  axiom Prover_to_verify_one (gp: (glob Pe)) (s: h_stm) (w: h_wit):
    phoare[Pe(HRO.RO).prove: 
          (glob Pe) = gp /\ arg = (s, w)
          ==>
          (glob Pe) = gp /\ verify s res HRO.RO.m]= 1%r.

  (* axiom for transforming a proof in a verification (two-sided) *)
  axiom Prover_to_verify_two (s: h_stm) (w: h_wit):
    equiv[Pe(HRO.RO).prove ~ Pe(HRO.RO).prove: 
         ={glob HRO.RO, glob Pe, arg} /\ arg{2} = (s, w)
         ==>
         ={glob HRO.RO, glob Pe, res} /\
         verify s res{2} HRO.RO.m{2}].

  axiom Kgen_get_upk (J<: JOracle.ARO):
    equiv [SS(J).kgen ~ SS(J).kgen:  
           ={glob J, glob SS} ==> 
           ={glob J, glob SS,  res} /\  res{2}.`2 = get_upk res{2}.`1].
  axiom  Kgen_get_upk' (J1 <: JOracle.ARO) (J2 <: JOracle.ARO):
    equiv [SS(J1).kgen ~ SS(J2).kgen:  
           ={ glob SS} ==>  ={glob SS,  res} /\ res{2}.`2 = get_upk res{2}.`1].

  axiom SSk_eq (gs : (glob SS)) (O <: ARO):
    phoare [SS(O).kgen: 
          (glob SS) = gs ==>  
           (glob SS) = gs /\ res.`2 = get_upk res.`1] = 1%r.

  axiom ver_Ver_one (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(JRO.RO).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2)  ==>
             (glob SS) =gs /\ res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
  (* admit for know: trye to prove using above *)
  axiom ver_Ver_one' (gs: (glob SS)) (upk2: upkey) (c2: cipher) (s2: sign):
     phoare [SS(MEUF_Oracle(SS,JRO.RO)).verify:
             (glob SS) = gs /\ arg =(upk2,c2,s2) ==>
             (glob SS) =gs /\ res = ver_sign upk2 c2 s2 JRO.RO.m ] = 1%r.
  axiom Sig_ver_one (gs: (glob SS)) (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   phoare [SS(JRO.RO).sign :
           gs = (glob SS) /\ arg = (usk2,c2) ==>   
           gs = (glob SS) /\ ver_sign upk2 c2 res JRO.RO.m ]=1%r.
   axiom Sig_ver_two (usk2: uskey) (upk2: upkey) (c2: cipher):
   (upk2 = get_upk usk2) =>
   equiv [SS(JRO.RO).sign ~ SS(JRO.RO).sign :
          ={glob JRO.RO, glob SS, arg}/\ arg{2} = (usk2,c2) ==>   
          ={glob JRO.RO, glob SS, res} /\ ver_sign upk2 c2 res{2} JRO.RO.m{2} ].


(* Lemma bounding bpriv *)
lemma bpriv &m: 
  `|Pr[BPRIV_L(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ]-
    Pr[BPRIV_R(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ]|
<=
   `|Pr[ZK_L(DecRel(IPKE(Pe, Ve),HRO.RO), Pz, 
                     BZK(IPKE(Pe, Ve),Pz,CIND(Ve',IPKE(Pe, Ve),SS),Vz,SS,
                     BPRIV_A,HRO.RO,JRO.RO), GRO.RO).main() @ &m : res] -
      Pr[ZK_R(DecRel(IPKE(Pe, Ve),HRO.RO), S, 
                     BZK(IPKE(Pe, Ve),Pz,CIND(Ve',IPKE(Pe, Ve),SS),Vz,SS,
                     BPRIV_A,HRO.RO,JRO.RO)).main() @ &m : res]| +
n%r *
    `|Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
                                               BPRIV_A, SS, JRO.RO,S), IPKE(Pe, Ve), HRO.RO), 
               HRO.RO, Left).main() @ &m : 
               res /\ WrapAdv.l <= n /\ size BS.encL <= 1] -
      Pr[Ind1CCA(IPKE(Pe, Ve), WrapAdv(BIND(B'(IPKE(Pe, Ve),Pz,Vz,CIND(Ve',IPKE(Pe, Ve),SS),SS), 
                                               BPRIV_A, SS, JRO.RO,S), IPKE(Pe, Ve), HRO.RO), 
               HRO.RO, Right).main() @ &m : 
               res /\ WrapAdv.l <= n /\ size BS.encL <= 1]|.
proof.
   
  have ->: Pr[BPRIV_L(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ] =
           Pr[BPRIV_L(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res ]
    by byequiv =>/>; sim.
  have ->: Pr[BPRIV_R(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ] =
           Pr[BPRIV_R(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               BPRIV_A, HRO.RO, GRO.RO, JRO.RO,S).main() @ &m : res ]
    by byequiv =>/>; sim.

  by rewrite (bpriv S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out  weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_mem''' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan san_tallymap Acor_ll BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll 
              RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll 
              So_ll Sp_ll Ex_ll  Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
           
qed. 

lemma consis1(id: ident, v: plain, l: uskey) &m: 
   Pr[PKE.Correctness(IPKE(Pe, Ve), HRO.RO).main(v,get_upk l) @ &m: res] <=
   Pr[SConsis1(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
       CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id,v, l) @ &m: res].
proof.
  have ->:  Pr[SConsis1(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
               CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id, v, l) @ &m : res] =
            Pr[SConsis1(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               CE(IPKE(Pe, Ve)), HRO.RO, GRO.RO, JRO.RO).main(id, v, l) @ &m : res].
    by byequiv =>/>; sim.
   by rewrite (consis1 S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out  weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_mem''' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan san_tallymap Acor_ll BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll 
              RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll 
              So_ll Sp_ll Ex_ll  Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two id v l &m).
qed.

lemma consis2 &m: 
  Pr[SConsis2(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
                 CIND(Ve',IPKE(Pe,Ve),SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res] = 1%r.
proof.
  have ->:  Pr[SConsis2(Belenios(Pe,Ve,Ve',Pz,Vz,SS), 
               CIND(Ve', IPKE(Pe, Ve), SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res] =
            Pr[SConsis2(BeleniosHom(Pe,Ve,Ve',Pz,Vz,SS), 
               CIND(Ve', IPKE(Pe, Ve), SS), CONSIS2_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m : res].
    by byequiv =>/>; sim.
  by rewrite (consis2 S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out  weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_mem''' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan san_tallymap Acor_ll BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll 
              RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll 
              So_ll Sp_ll Ex_ll  Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
qed.
 

(* Lemma bounding strong consistency part 3 *)
equiv consis3 &m:
    SConsis3_L(Belenios(Pe,Ve, Ve',Pz,Vz,SS), CIND(Ve',IPKE(Pe,Ve),SS), 
               CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main ~ 
    SConsis3_R(Belenios(Pe,Ve, Ve',Pz,Vz,SS), CE(IPKE(Pe,Ve)),
               CIND(Ve',IPKE(Pe,Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main
    : ={glob HRO.RO,glob GRO.RO, glob JRO.RO, glob IPKE(Pe,Ve),glob Ve',glob Pz, glob SS,glob CONSIS3_A}
     ==> ={res}.
proof.

  transitivity SConsis3_L(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), CIND(Ve',IPKE(Pe,Ve),SS), 
               CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main 
  (={glob HRO.RO,glob GRO.RO,glob JRO.RO,glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})
  (={glob HRO.RO,glob GRO.RO,glob JRO.RO,glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})=>//=. 

  + by progress; smt.

  + by proc; sim.

  transitivity SConsis3_R(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), CE(IPKE(Pe,Ve)),
               CIND(Ve',IPKE(Pe,Ve),SS), CONSIS3_A, HRO.RO, GRO.RO, JRO.RO).main 
  (={glob HRO.RO, glob GRO.RO,glob JRO.RO, glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})
  (={glob HRO.RO, glob GRO.RO,glob JRO.RO, glob IPKE(Pe,Ve), glob Ve', glob Pz, glob SS, glob CONSIS3_A} ==> 
   ={res})=>//=. 

  + by progress; smt.

  + by rewrite (consis3 S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out  weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_mem''' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan san_tallymap Acor_ll BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll 
              RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll 
              So_ll Sp_ll Ex_ll  Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
  - by sim. 
qed.

lemma bound_dbb (COR <: Corr_Adv{WU, BU, JRO.RO, SS}) &m:
  islossless COR(JRO.RO).main=>
  Pr[DBB_Exp (Belenios(Pe,Ve, Ve',Pz,Vz,SS), 
              DBB_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv(B(IPKE(Pe, Ve), Pz, Vz, SS), 
              DBB_A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
n%r ^2 * 
  Pr[Wspread_Exp(IPKE(Pe, Ve), 
                 BWspread(B(IPKE(Pe, Ve), Pz, Vz,SS),GRO.RO,JRO.RO,DBB_A), 
                 HRO.RO).main() @ &m: BS.eq ] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BE(SS, COR), JRO.RO).main() @ &m : res] +
(size (undup Voters))%r *
   Pr[EUF_Exp(SS, BF(SS, WOO(BMEUF(IPKE(Pe, Ve), HRO.RO, GRO.RO, DBB_A, SS), JRO.RO)), 
              JRO.RO).main () @ &m : res] +
(size (undup Voters))%r * 
  Pr[Correctness(SS, COR, JRO.RO).main() @ &m : !res] +
  Pr[PS.Correctness(DecRel(IPKE(Pe, Ve), HRO.RO), 
          BCorr_Adv(IPKE(Pe, Ve), BAcc(B(IPKE(Pe, Ve), Pz, Vz, SS), DBB_A), 
          HRO.RO, SS, JRO.RO), Pz, Vz, GRO.RO).main() @ &m : !res] + 
  Pr[PoK_Exp(Vz, DecRel(IPKE(Pe, Ve), HRO.RO), Ex, 
         PoK_TU(BTall(B(IPKE(Pe, Ve), Pz, Vz, SS), DBB_A), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m : res]. 
proof.
  move => Co. 
  have ->: Pr[DBB_Exp(Belenios(Pe,Ve, Ve',Pz,Vz,SS), 
                DBB_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] =
           Pr[DBB_Exp(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), 
                DBB_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
    by byequiv =>/>; sim.
  by rewrite (bound_dbb S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out  weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_mem''' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan san_tallymap Acor_ll BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll 
              RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll 
              So_ll Sp_ll Ex_ll  Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two COR &m Co).
qed.

lemma bound_dreg &m:
  Pr[DREG_Exp (Belenios(Pe,Ve, Ve',Pz,Vz,SS), 
               DREG_A, HRO.RO, GRO.RO, JRO.RO).main() @ &m: res]
  <=
  Pr[Hash_Exp(BHash_Adv'(B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS), SS),
               DREG_A, HRO.RO, GRO.RO, JRO.RO)).main() @ &m : res] +
n%r ^2 * 
  Pr[Wspread_Exp(IPKE(Pe,Ve), 
                 BWspread'(B'(IPKE(Pe,Ve), Pz, Vz, CIND(Ve',IPKE(Pe,Ve),SS),SS),
                    GRO.RO,JRO.RO,DREG_A), HRO.RO).main() @ &m: BS.eq ] +
  Pr[PS.Correctness(DecRel(IPKE(Pe, Ve), HRO.RO), 
                    BCorr_Adv(IPKE(Pe, Ve), 
                              BAcc'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe,Ve), SS), SS), 
                              DREG_A), 
          HRO.RO, SS, JRO.RO), Pz, Vz, GRO.RO).main() @ &m : !res] + 
  Pr[PoK_Exp(Vz, DecRel(IPKE(Pe, Ve), HRO.RO), Ex, 
         PoK_TU(BTall'(B'(IPKE(Pe, Ve), Pz, Vz, CIND(Ve', IPKE(Pe,Ve), SS), SS), DREG_A), 
         HRO.RO, JRO.RO), GRO.RO).main() @ &m : res].  
proof.
  have ->: Pr[DREG_Exp(Belenios(Pe,Ve, Ve',Pz,Vz,SS), 
                         DREG_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res] =
           Pr[DREG_Exp(BeleniosHom(Pe,Ve, Ve',Pz,Vz,SS), 
                         DREG_A, HRO.RO, GRO.RO, JRO.RO).main () @ &m : res].
    by byequiv =>/>; sim.
  by rewrite (bound_dreg S Vz Pz SS Ex BPRIV_A CONSIS2_A CONSIS3_A DBB_A DREG_A Ve Pe Ve' 
              size_Voters weight_dh_out weight_dg_out weight_dj_out  weight_dk_out is_finite_h_in distr_h_out
              is_finite_j_in distr_j_out is_finite_g_in distr_g_out inj_get_upk inj_get_pk
              San_mem San_mem' San_mem'' San_mem''' San_map San_uniq San_uniq_fst San_uniq_fst' 
              San_uniq_snd San_uniq' San_mem_filter San_filter_ident uniq_to_San USan_USanitize 
              San'_USan san_tallymap Acor_ll BAa1_ll BAa2_ll CAa_ll DAa1_ll DAa2_ll RAa2_ll 
              RAa3_ll ATU_ll ATU2_ll Pz_ll Vz_ll Pe_ll Ve_ll Ve_ll' SSk_ll SSs_ll SSv_ll Si_ll 
              So_ll Sp_ll Ex_ll Vz_keepstate Verify_to_verify Verify_to_verify'
              Prover_to_verify_one Prover_to_verify_two Kgen_get_upk Kgen_get_upk' SSk_eq ver_Ver_one
              ver_Ver_one' Sig_ver_one Sig_ver_two &m).
qed.
end section Security.

